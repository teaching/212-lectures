\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{derivative}

\title{The Derivative}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Partial Derivatives}
\subsection{One Variable}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{derivative} of $f:\mathbb{R}\to\mathbb{R}$ is
    \[
      f^\prime(x) = \lim_{h\to 0}\frac{f(x+h)-f(x)}{h}
    \]
    provided this limit exists.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTangent}{
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=3/4
        , xscale=2
        ]

        \draw[<->] (-1,0)-- (3,0) node [right] {$x$};
        \draw[<->] (0,-1)-- (0,7/2);

        \onslide<2->{
          \draw[] (7/4, 3pt) -- (7/4, -3pt) node [below] {$p$};
        }

        \draw[beamblue, domain=-1/2:5/2, samples=1000, <->]
        plot (\x, {\x*\x*\x-3*\x*\x+2*\x+1}) node [right] {$f(x)$};

        \onslide<4->{
          \draw[red, <->] (-1/3, -45/64) -- (3, 49/32)
          node[above, overlay] {$\scriptscriptstyle L_p(x)=\onslide<5->{f(p)+f^\prime(p)\cdot(x-p)}$};
        }

        \coordinate (P) at (7/4, 43/64);

        \onslide<3->{
          \node at (P) {\textbullet};
        }

      \end{tikzpicture}
  }
  \begin{block}{Interpretation}
    $f^\prime(p)$ is the slope of the line tangent to the graph of $f(x)$ at
    $x=p$.
    \[
      \myTangent
    \]
    \onslide<6->{We call $L_p(x)$ the \emph{local linearization of $f(x)$ at
        $x=p$}.}
  \end{block}

\end{frame}


\begin{sagesilent}
  var('t')
  f = 3*t**2+4*t
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}<+->
    Consider the approximation
    \[
      f(p+1)\approx
      \onslide<+->{L_p(p+1)=}
      \onslide<+->{f(p)+f^\prime(p)\cdot(p+1-p)=}
      \onslide<+->{f(p)+f^\prime(p)}
    \]
    \onslide<+->{Increasing $p$ by one approximately increases $f(p)$ by
      $f^\prime(p)$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Temperature $t$ hours past \DTMdisplaytime{12}{00}{00} is
    \[
      f(t) = \sage{f}\,\si{\celsius}
    \]
    \onslide<2->{Consider the calculations}
    \begin{align*}
      \onslide<2->{f(2)} &\onslide<2->{=} \onslide<3->{\sage{f(t=2)}\,\si{\celsius}} & \onslide<4->{f^\prime(t)} &\onslide<4->{=} \onslide<5->{\sage{f.diff(t)}\,\si{\celsius\per\hour}} & \onslide<6->{f^\prime(2)} &\onslide<6->{=} \onslide<7->{\sage{f.diff(t)(t=2)}\,\si{\celsius\per\hour}}
    \end{align*}
    \onslide<8->{From \DTMdisplaytime{14}{00}{00} to \DTMdisplaytime{15}{00}{00}
      temperature approximately \onslide<9->{\emph{increases} by
        \onslide<10->{$16\,\si{\celsius}$.}}}
  \end{example}

\end{frame}


\subsection{Several Variables}
\begin{sagesilent}
  var('x y')
  f = x**2-2*x*y+y**3
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTemperature}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      ]

      \coordinate (O) at (0, 0);
      \coordinate (p) at (3, 1);

      \draw[<->] (-1/2,  0) -- (6, 0) node[right] {$x$};
      \draw[<->] ( 0, -1/2) -- (0, 3) node[above] {$y$};

      \onslide<8->{
        \draw[beamblue, ->] (p) -- ++(1, 0) node[right] {$?\,\si{\celsius\per\metre}$};
      }

      \onslide<9->{
        \draw[beamgreen, ->] (p) -- ++(0, 1) node[above] {$?\,\si{\celsius\per\metre}$};
      }

      \onslide<4->{
        \node[myDot] at (p) {};
      }

      \onslide<5->{
        \node[overlay, red, below left= 0mm and 3mm of p] (text) {$T(3, 1)=\onslide<6->{\sage{f(x=3, y=1)}\,\si{\celsius}}$};
        \draw[overlay, <-, red, thick, shorten <=2pt] (p.south) |- (text.east);
      }

      \onslide<3->{
        \draw (3, 3pt) -- (3, -3pt) node[below] {$3$};
        \draw (3pt, 1) -- (-3pt, 1) node[left] {$1$};
      }

    \end{tikzpicture}
  }
  \begin{example}
    Consider $T\in\mathscr{C}(\mathbb{R}^2)$ given by
    $T(x, y)=\sage{f}\,\si{\celsius}$.
    \[
      \onslide<2->{\myTemperature}
    \]
    \onslide<7->{What is the rate of change of temperature due East or due
      North?}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}<+->
    The \emph{partial derivative of $f\in\mathscr{C}(\mathbb{R}^n)$ with respect
      to $x_i$} is
    \[
      \pdv{f}{x_i}
      = f_{x_i}
      = \lim_{h\to0}\frac{f(\bv{x}+h\cdot\bv{e}_i)-f(\bv{x})}{h}
    \]
    provided this limit exists.
  \end{definition}

  \begin{block}{Observation}<+->
    To compute $f_{x_i}$ treat variables $\neq x_i$ as constants and
    differentiate.
  \end{block}

\end{frame}


\begin{sagesilent}
  var('x1 x2')
  f = x1**2+x2**2
  var('x y z')
  g = x*sin(x*y*z)
  h = x*exp(x**2*z)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The partial derivatives of $f(x_1, x_2)=\sage{f}$ are
    \begin{align*}
      \onslide<2->{\pdv{f}{x_1}} &\onslide<2->{=} \onslide<3->{\sage{f.diff(x1)}} & \onslide<4->{\pdv{f}{x_2}} &\onslide<4->{=} \onslide<5->{\sage{f.diff(x2)}}
    \end{align*}
    \onslide<6->{The partial derivatives of $g(x, y, z)=\sage{g}$ are}
    \begin{gather*}
      \begin{align*}
        \onslide<7->{g_x} &\onslide<7->{=} \onslide<8->{\sage{g.diff(x)}} & \onslide<9->{g_y} &\onslide<9->{=} \onslide<10->{\sage{g.diff(y)}} & \onslide<11->{g_z} &\onslide<11->{=} \onslide<12->{\sage{g.diff(z)}}
      \end{align*}
    \end{gather*}
    \onslide<13->{The partial derivatives of $h(r, s, t)=re^{r^2t}$ are}
    \begin{align*}
      \onslide<14->{h_r} &\onslide<14->{=} \onslide<15->{2\,r^2te^{r^2t}+e^{r^2t}} & \onslide<16->{h_s} &\onslide<16->{=} \onslide<17->{0} & \onslide<18->{h_t} &\onslide<18->{=} \onslide<19->{r^3e^{r^2t}}
    \end{align*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  f = y*z*log(x**2+1)
  fx, fy, fz = f.gradient([x, y, z])
  g = (x**2+1)**(y**2+1)
  gx, gy = g.gradient([x, y])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The partial derivatives of $f(x, y, z)=\sage{f}$ are
    \begin{align*}
      \onslide<2->{f_x} &\onslide<2->{=} \onslide<3->{\sage{fx}} & \onslide<4->{f_y} &\onslide<4->{=} \onslide<5->{\sage{fy}} & \onslide<6->{f_z} &\onslide<6->{=} \onslide<7->{\sage{fz}}
    \end{align*}
    \onslide<8->{The partial derivatives of $g(x, y)=\sage{g}$ are}
    \begin{gather*}
      \begin{align*}
        \onslide<9->{g_x} &\onslide<9->{=} \onslide<10->{(y^2+1)(x^2+1)^{y^2}(2\,x)} & \onslide<11->{g_y} &\onslide<11->{=} \onslide<12->{(x^2+1)^{y^2+1}\log(x^2+1)(2\,y)}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y')
  T = x**2-2*x*y+y**3
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTemperature}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      , scale=0.75
      ]

      \coordinate (O) at (0, 0);
      \coordinate (p) at (3, 1);

      \draw[<->] (-1,  0) -- (6, 0) node[right] {$x$};
      \draw[<->] ( 0, -1) -- (0, 3) node[above] {$y$};

      \onslide<8->{
        \draw[beamblue, ->] (p) -- ++(1, 0) node[right] {$\scriptstyle T_x(3, 1)=\sage{T.diff(x)(x=3, y=1)}\,\si{\celsius\per\metre}$};
      }

      \onslide<9->{
        \draw[beamgreen, ->] (p) -- ++(0, 1) node[above] {$\scriptstyle T_y(3, 1)=\sage{T.diff(y)(x=3, y=1)}\,\si{\celsius\per\metre}$};
      }

      \onslide<2->{
        \node[myDot] at (p) {};
      }

      \onslide<3->{
        \node[overlay, red, below left= 0mm and 3mm of p] (text) {$\scriptstyle T(3, 1)=\sage{T(x=3, y=1)}\,\si{\celsius}$};
        \draw[overlay, <-, red, thick, shorten <=2pt] (p.south) |- (text.east);
      }

      \onslide<2->{
        \draw (3, 3pt) -- (3, -3pt) node[below] {$3$};
        \draw (3pt, 1) -- (-3pt, 1) node[left] {$1$};
      }

    \end{tikzpicture}
  }
  \begin{example}
    Consider $T\in\mathscr{C}(\mathbb{R}^2)$ given by
    $T(x, y)=\sage{T}\,\si{\celsius}$.
    \[
      \myTemperature
    \]
    \onslide<4->{The partial derivatives of $T$ are}
    \begin{align*}
      \onslide<4->{T_x} &\onslide<4->{=} \onslide<5->{\sage{T.diff(x)}\,\si{\celsius\per\metre}} & \onslide<6->{T_y} &\onslide<6->{=} \onslide<7->{\sage{T.diff(y)}\,\si{\celsius\per\metre}}
    \end{align*}
    \onslide<10->{Temperature is \emph{increasing} due East and
      \emph{decreasing} due North.}
  \end{example}

\end{frame}


\section{The Jacobian Derivative}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{Jacobian derivative of $\bv{f}:\mathbb{R}^n\to\mathbb{R}^m$} is the $m\times n$ matrix
    \[
      \onslide<2->{D\bv{f}=}
      \onslide<3->{\frac{\partial(f_1, f_2, \dotsc, f_m)}{\partial(x_1, x_2, \dotsc, x_n)}=}
      \onslide<4->{
      \left[
        \begin{array}{cccc}
          \pdv{f_1}{x_1} & \pdv{f_1}{x_2} & \cdots & \pdv{f_1}{x_n} \\
          \pdv{f_2}{x_1} & \pdv{f_2}{x_2} & \cdots & \pdv{f_2}{x_n} \\
          \vdots         & \vdots         & \ddots & \vdots         \\
          \pdv{f_m}{x_1} & \pdv{f_m}{x_2} & \cdots & \pdv{f_m}{x_n}
        \end{array}
      \right]
    }
    \]
    \onslide<5->{where $f_1, f_2, \dotsc, f_m$ are the component functions of
      $\bv{f}$.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = vector([x**2*y, x*y-sin(y)])
  f1, f2 = f
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The components of $\bv{f}(x, y)=\sage{f}$ are
    \begin{align*}
      \onslide<2->{f_1(x, y)} &\onslide<2->{=} \onslide<3->{\sage{f1}} & \onslide<4->{f_2(x, y)} &\onslide<4->{=} \onslide<5->{\sage{f2}}
    \end{align*}
    \onslide<6->{The Jacobian derivative of $\bv{f}$ is then}
    \[
      \onslide<6->{D\bv{f}=}
      \onslide<7->{
      \left[
        \begin{array}{cc}
          \pdv{f_1}{x} & \pdv{f_1}{y} \\
          \pdv{f_2}{x} & \pdv{f_2}{y}
        \end{array}
      \right]
      =}
    \onslide<8->{\sage{jacobian(f, (x, y))}}
    \]
    \onslide<9->{Note that $D\bv{f}$ is $2\times 2$ since
      $\bv{f}:\mathbb{R}^2\to\mathbb{R}^2$.}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('u v')
  g = vector([u, u*cos(v), u*sin(v)])
  g1, g2, g3 = g
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The components of $\bv{g}(u, v)=\sage{g}$ are
    \begin{align*}
      \onslide<2->{g_1(u, v)} &\onslide<2->{=} \onslide<3->{\sage{g1}} & \onslide<4->{g_2(u, v)} &\onslide<4->{=} \onslide<5->{\sage{g2}} & \onslide<6->{g_3(u, v)} &\onslide<6->{=} \onslide<7->{\sage{g3}}
    \end{align*}
    \onslide<8->{The Jacobian derivative of $\bv{g}$ is then}
    \[
      \onslide<9->{D\bv{g}=}
      \onslide<10->{
      \left[
        \begin{array}{ccc}
          \pdv{g_1}{u} & \pdv{g_1}{v} \\
          \pdv{g_2}{u} & \pdv{g_2}{v} \\
          \pdv{g_3}{u} & \pdv{g_3}{v}
        \end{array}
      \right]
      =}
    \onslide<11->{\sage{jacobian(g, (u, v))}}
    \]
    \onslide<12->{Note that $D\bv{g}$ is $3\times 2$ since
      $\bv{g}:\mathbb{R}^2\to\mathbb{R}^3$.}
  \end{example}

\end{frame}



\begin{sagesilent}
  var('x1 x2 x3')
  h = vector([x1, 3*x1**2*x3, 4*x2**2-22*x3, x3*sin(x1)])
  h1, h2, h3, h4 = h
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\bv{h}:\mathbb{R}^3\to\mathbb{R}^4$ given by
    \[
      \bv{h}(x_1, x_2, x_3)=\sage{h}
    \]
    \onslide<2->{The Jacobian derivative of $\bv{h}$ is}
    \begin{gather*}
      \onslide<3->{D\bv{h} =}
      \onslide<4->{
      \left[
        \begin{array}{ccc}
          \pdv{h_1}{x_1} & \pdv{h_1}{x_2} & \pdv{h_1}{x_3} \\
          \pdv{h_2}{x_1} & \pdv{h_2}{x_2} & \pdv{h_2}{x_3} \\
          \pdv{h_3}{x_1} & \pdv{h_3}{x_2} & \pdv{h_3}{x_3} \\
          \pdv{h_4}{x_1} & \pdv{h_4}{x_2} & \pdv{h_4}{x_3}
        \end{array}
      \right]
      =}
    \onslide<5->{\sage{jacobian(h, (x1, x2, x3))}}
    \end{gather*}
    \onslide<6->{Note that $D\bv{h}$ is $4\times 3$ since
      $\bv{h}:\mathbb{R}^3\to\mathbb{R}^4$.}
  \end{example}

\end{frame}




\subsection{Linear Approximations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{local linearization of $\bv{f}:\mathbb{R}^n\to\mathbb{R}^m$ at
      $P\in\mathbb{R}^n$ is}
    \[
      \bv{L}_P(\bv{x}) = \bv{f}(P)+D\bv{f}(P)(\bv{x}-\bv{P})
    \]
    \onslide<2->{Note that $\bv{L}_P:\mathbb{R}^n\to\mathbb{R}^m$.}
  \end{definition}

  \begin{block}{Theme}<3->
    We use $\bv{L}_P$ to approximate values of $\bv{f}$ ``near'' $P$ with
    \begin{align*}
      \onslide<4->{\bv{f}(\bv{P}+\Delta\bv{P})}
      &\onslide<4->{\approx} \onslide<5->{\bv{L}_P(\bv{P}+\Delta\bv{P})} \\
      &\onslide<5->{=} \onslide<6->{\bv{f}(P)+D\bv{f}(P)(\bv{P}+\Delta\bv{P}-\bv{P})} \\
      &\onslide<6->{=} \onslide<7->{\bv{f}(P)+D\bv{f}(P)\Delta\bv{P}}
    \end{align*}
    \onslide<8->{This is the idea behind \emph{linear approximations}.}
  \end{block}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = matrix.column([-5*x*y-y, -10*x-3*y])
  x0, y0 = 1, -2
  dx, dy = -1/10, 1/3
  Df = jacobian(f, (x, y))
  DfP = Df(x=x0, y=y0)
  Delta = matrix.column([dx, dy])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\bv{f}(x, y)=\sage{vector(f)}$, which satisfies
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\bv{f}(\sage{x0}, \sage{y0})} &\onslide<2->{=} \onslide<3->{\sage{f(x=x0, y=y0)}} & \onslide<4->{D\bv{f}} &\onslide<4->{=} \onslide<5->{\sage{Df}} & \onslide<6->{D\bv{f}(\sage{x0}, \sage{y0})} &\onslide<6->{=} \onslide<7->{\sage{DfP}}
      \end{align*}
    \end{gather*}
    \onslide<8->{We may approximate $\bv{f}(1-\frac{1}{10}, -2+\frac{1}{3})$
      with}
    \begin{gather*}
      \onslide<8->{\bv{f}\pair*{1-\sfrac{1}{10}, -2+\sfrac{1}{3}}\approx}
      \onslide<9->{
      \overset{\bv{f}(1, -2)}{\sage{f(x=x0, y=y0)}}
      +
      \overset{D\bv{f}(1, -2)}{\sage{DfP}}
      \overset{\Delta\bv{P}}{\sage{Delta}}
      =} \onslide<10->{\sage{f(x=x0, y=y0)+DfP*Delta}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = vector([sin(x*y+1), x*cos(y+1), x*y**3])
  x0, y0 = 1, -1
  Df = jacobian(f, (x, y))
  DfP = Df(x=x0, y=y0)
  Delta = matrix.column([-1/3, 1/4])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\bv{f}:\mathbb{R}^2\to\mathbb{R}^3$ given by
    \begin{align*}
      \bv{f}(x, y) &= \sage{f} & \onslide<2->{\bv{f}(\sage{x0}, \sage{y0})} &\onslide<2->{=} \onslide<3->{\sage{f(x=x0, y=y0)}}
    \end{align*}
    \onslide<4->{The Jacobian derivative of $\bv{f}$ is}
    \begin{align*}
      \onslide<4->{D\bv{f}} &\onslide<4->{=} \onslide<5->{\sage{Df}} & \onslide<6->{D\bv{f}(\sage{x0}, \sage{y0})} &\onslide<6->{=} \onslide<7->{\sage{DfP}}
    \end{align*}
    \onslide<8->{We may approximate $\bv{f}(1-\frac{1}{3}, -1+\frac{1}{4})$
      with}
    \[
      \onslide<9->{\bv{f}\pair*{1-\sfrac{1}{3}, -1+\sfrac{1}{4}}\approx}
      \onslide<10->{
      \overset{\bv{f}(1, -1)}{\sage{matrix.column(f(x=x0, y=y0))}}
      +
      \overset{D\bv{f}(1, -1)}{\sage{DfP}}
      \overset{\Delta\bv{P}}{\sage{Delta}}
      =}
    \onslide<11->{\sage{matrix.column(f(x=x0, y=y0))+DfP*Delta}}
    \]
  \end{example}

\end{frame}



\end{document}

\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\tikzset{
  , myfill/.style={fill=blue!40!white, fill opacity=.6}
  , mydraw/.style={draw=blue!70!black}
  , bluefilldraw/.style={fill=beamblue!40!white, fill opacity=.6, draw=beamblue}
  , redfilldraw/.style={draw=red, fill=red!40, fill opacity=0.6}
  , greenfill/.style={fill=beamgreen!40, fill opacity=0.6}
  , greenfilldraw/.style={draw=beamgreen, fill=beamgreen!40, fill opacity=0.6}
}

\title{Parameterized Surfaces}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Parameterized Surfaces}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \draw[<->] (-2, 0) -- (2, 0);
        \draw[<->] (0, -2)-- (0, 2);

        \pgfmathsetseed{3}
        \filldraw[bluefilldraw] plot [smooth cycle, samples=8,domain={1:8}]
        (\x*360/8+5*rnd:0.5cm+1cm*rnd) ;

        \node at (-1/2, -1/2) {$D$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myX}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        \draw[->] (O) -- ($ 2*(x) $);% node[pos=1.05] {$x$};
        \draw[->] (O) -- ($ 2*(y) $);% node[pos=1.05] {$y$};
        \draw[->] (O) -- ($ 2*(z) $);% node[pos=1.05] {$z$};

        \begin{scope}[shift={(-1.25, -0.5)}, xscale=2/3, yscale=1/2]
          \filldraw[bluefilldraw] (0, 0)
          to[bend left]  (3, -1)
          to[bend left]  (5,  2)
          to[bend right] (2,  3)
          to[bend right] (0,  0)
          --cycle;

          \node at (4, 2) {$S$};
        \end{scope}

      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    A \emph{parameterized surface} is a function
    $\bv{X}:D\subset\mathbb{R}^2\to\mathbb{R}^3$.
    \[
      \onslide<2->{\myD}
      \onslide<3->{\xrightarrow{\bv{X}}}
      \onslide<4->{\myX}
    \]
    \onslide<5->{The \emph{underlying surface $S$ of $\bv{X}$} is
      $S=\Range(\bv{X})$.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}s}$};
      \onslide<2->{
        \node[overlay, below left= 1mm and 3mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}t}$};
      \onslide<3->{
        \node[overlay, below right= 1mm and 3mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamblue}s^2+t^2}$};
      \onslide<4->{
        \node[overlay, above right= 0mm and -1mm of a] (text) {$z=x^2+y^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the parameterization
    \begin{align*}
      \bv{X}(s, t) &= \langle \myx, \myy, \myz\rangle && 0\leq s^2+t^2\leq 1
    \end{align*}
    \onslide<5->{The underlying surface $S$ is a paraboloid.}
    \[
        \begin{tikzpicture}[
          , line join=round
          , line cap=round
          , ultra thick
          , myDot/.style={circle, fill, inner sep=2pt}
          , axis/.style={<->}
          , vector/.style={->}
          ]

          \coordinate (O) at (0, 0);

          \pgfmathsetmacro{\R}{2}
          \pgfmathsetmacro{\H}{2}
          \pgfmathsetmacro{\a}{\R/8}

          \coordinate (left) at (-\R, \H);
          \coordinate (right) at (\R, \H);

          \onslide<6->{
            \filldraw[bluefilldraw]
            (O) parabola (right)
            plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
            parabola[bend at end] (O);
          }

          \onslide<7->{
            \draw[red] plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H});
          }

          \onslide<6->{
            \draw[<->] (0, -2*\a) -- (0, \H+4*\a) node[above] {$z$};
          }

          \onslide<6->{
            \filldraw[bluefilldraw]
            (O) parabola (right)
            plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
            parabola[bend at end] (O);
          }

          \onslide<7->{
            \draw[red] plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H});

            \draw[<-, thick, red, overlay]
            (right.east) -| ++(1, 2mm) node[above] {$x^2+y^2=1$};
          }
        \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}r\cos(\theta)}$};
      \onslide<2->{
        \node[overlay, below left= 1mm and 0mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}r\sin(\theta)}$};
      \onslide<3->{
        \node[overlay, below right= 1mm and 0mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamblue}r^2}$};
      \onslide<4->{
        \node[overlay, above right= 0mm and 1mm of a] (text) {$z=x^2+y^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the parameterization
    \begin{align*}
      \bv{X}(r, \theta) &= \langle \myx, \myy, \myz\rangle && 0\leq r\leq 1 && 0\leq\theta\leq2\,\pi
    \end{align*}
    \onslide<5->{The underlying surface $S$ is a also paraboloid.}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\H}{2}
        \pgfmathsetmacro{\a}{\R/8}

        \coordinate (left) at (-\R, \H);
        \coordinate (right) at (\R, \H);

        \onslide<6->{
          \filldraw[bluefilldraw]
          (O) parabola (right)
          plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
          parabola[bend at end] (O);
        }

        \onslide<7->{
          \draw[red] plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H});
        }

        \onslide<6->{
          \draw[<->] (0, -2*\a) -- (0, \H+4*\a) node[above] {$z$};

          \filldraw[bluefilldraw]
          (O) parabola (right)
          plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
          parabola[bend at end] (O);
        }

        \onslide<7->{
          \draw[red] plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H});

          \draw[<-, thick, red, overlay]
          (right.east) -| ++(1, 2mm) node[above] {$x^2+y^2=1$};
        }

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<1->{red}3\cos(\theta)}$};
      \onslide<1->{
        \node[overlay, below left= 1mm and 0mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<1->{beamgreen}3\sin(\theta)}$};
      \onslide<1->{
        \node[overlay, below right= 1mm and 0mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<1->{beamblue}z}$};
      \onslide<1->{
        \node[overlay, above right= 0mm and 1mm of a] (text) {$z=x^2+y^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the parameterization
    \begin{align*}
      \bv{X}(\theta, z) &= \langle 3\cos(\theta), 3\sin(\theta), z\rangle && 0\leq \theta\leq 2\,\pi && -1\leq z\leq 1
    \end{align*}
    \onslide<2->{The underlying surface $S$ is a cylinder.}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\H}{\R*2/3}
        \pgfmathsetmacro{\a}{\R/10}

        \coordinate (nw) at (-\R, \H);
        \coordinate (ne) at (\R, \H);
        \coordinate (sw) at (-\R, -\H);
        \coordinate (se) at (\R, -\H);

        \onslide<3->{
          \filldraw[bluefilldraw]
          (se) -- (ne)
          -- plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
          -- (sw)
          -- plot[smooth, domain=180:0] ({\R*cos(\x)}, {\a*sin(\x)-\H})
          -- cycle
          ;
        }

        \onslide<5->{
          \draw[red] plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H});
        }

        \onslide<6->{
          \draw[red] plot[smooth, domain=180:0] ({\R*cos(\x)}, {\a*sin(\x)-\H});
        }

        \onslide<3->{
          \draw[<->] (0, -\H-3*\a) -- (0, \H+3*\a) node[above] {$z$};
        }

        \onslide<4->{
          \node[name=Rlabel] at (\R/2, 0) {$3$};
          \draw[dashed] (O) -- (Rlabel.west);
          \draw[dashed] (Rlabel.east) -- (\R, 0);
        }

        \onslide<3->{
          \filldraw[bluefilldraw]
          (se) -- (ne)
          -- plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
          -- (sw)
          -- plot[smooth, domain=-180:0] ({\R*cos(\x)}, {\a*sin(\x)-\H})
          -- cycle
          ;
        }

        \onslide<5->{
          \draw[red] plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H});
        }

        \onslide<6->{
          \draw[red] plot[smooth, domain=-180:0] ({\R*cos(\x)}, {\a*sin(\x)-\H});
        }

        \onslide<5->{
          \draw[thick, <-, red, overlay]
          (ne.east) -- ++(4mm, 0) node[right] {$z=1$};
        }

        \onslide<6->{
          \draw[thick, <-, red, overlay]
          (se.east) -- ++(4mm, 0) node[right] {$z=-1$};
        }

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\sin(\varphi)\cos(\theta)}$};
      \onslide<2->{
        \node[overlay, below left= 1mm and 0mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}\sin(\varphi)\sin(\theta)}$};
      \onslide<3->{
        \node[overlay, below left= 1mm and 0mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamblue}\cos(\varphi)}$};
      \onslide<4->{
        \node[overlay, below left= 1mm and 0mm of a] (text) {$z$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{example}
    Consider the parameterization
    \begin{gather*}
      \begin{align*}
        \bv{X}(\varphi, \theta) &= \langle \myx, \myy, \myz\rangle && 0\leq\varphi\leq\pi && 0\leq\theta\leq2\,\pi
      \end{align*}
    \end{gather*}
    \onslide<5->{The coordinates satisfy $x^2+y^2+z^2=1$, so $S$ is the unit
      sphere.}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\a}{\R/6}

        \onslide<6->{
          \draw[beamblue] plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)});
          \filldraw[bluefilldraw] (O) circle (\R);
          \filldraw[bluefilldraw] (O) circle (\R);
          \draw[beamblue] plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)});
        }

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\subsection{Normal Vectors}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{red}\onslide<5->{\bv{N} =} \onslide<6->{\bv{T}_s\times\bv{T}_t}}$};
      \onslide<7->{
        \node[overlay, above= 10mm of a] (text) {``standard normal vector''};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) -- (text.south);
      }
    }
  }
  \begin{definition}
    Given $\bv{X}(s, t)=\langle x(s, t), y(s, t), z(s, t)\rangle$ we define
    \begin{align*}
      \bv{T}_s= \onslide<2->{\langle x_s, y_s, z_s\rangle} && \onslide<3->{\bv{T}_t =} \onslide<4->{\langle x_t, y_t, z_t\rangle} && \myN
    \end{align*}
    \onslide<8->{We say that $S$ is \emph{smooth} at $P=\bv{X}(s_0, t_0)$ if
      $\bv{N}(s_0, t_0)\neq\bv{O}$.}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\H}{3}
        \pgfmathsetmacro{\a}{\R/6}
        \pgfmathsetmacro{\myx}{\R/2}
        \pgfmathsetmacro{\myy}{\H - \H*\myx*\myx / \R / \R}

        \coordinate (top) at (0, \H);
        \coordinate (right) at (\R, 0);
        \coordinate (left) at (-\R, 0);
        \coordinate (p) at (\myx, \myy);
        \coordinate (N) at ({2*\H*\myx / \R / \R}, {1});
        \coordinate (Ts) at ({1}, {-2*\H*\myx / \R / \R});

        \onslide<9->{
        \filldraw[bluefilldraw]
        (left)
        parabola[bend at end] (top)
        parabola (right)
        plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)})
        ;

        \filldraw[bluefilldraw]
        (left)
        parabola[bend at end] (top)
        parabola (right)
        plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)})
        ;
        }

        % \draw[beamgreen, ->] (p) -- ++($ 0.75*(N) $)
        % node[right] {$\bv{N}=\bv{T}_s\times\bv{T}_t$};
        % \draw[beamgreen, ->] (p) -- ++($ 0.75*(Ts) $) node[right] {$\bv{T}_s$};
        % \begin{scope}[shift={(p)}, rotate=-100]
        %   \coordinate (Tt) at ({1}, {-2*\H*\myx / \R / \R});
        %   \draw[beamgreen, ->] (0, 0) -- ++($ 0.75*(Tt) $)
        %   node[left] {$\bv{T}_s$};
        % \end{scope}

        \begin{scope}[shift={(p)}, rotate=-40]
          \coordinate (y) at ({cos(-15)}, {sin(-15)});
          \coordinate (z) at (0, 1);
          \coordinate (x) at ({cos(210)}, {sin(210)});

          \onslide<11->{
            \draw[beamgreen, ->] (0, 0) -- ($ 1*(x) $)
            node[left] {$\bv{T}_s$};% node[pos=1.05] {$x$};
          }

          \onslide<12->{
            \draw[beamgreen, ->] (0, 0) -- ($ 1*(y) $)
            node[right] {$\bv{T}_t$};% node[pos=1.05] {$y$};
          }

          \onslide<13->{
            \draw[beamgreen, ->] (0, 0) -- ($ 1*(z) $)
            node[right] {$\bv{N}$};% node[pos=1.05] {$z$};
          }
        \end{scope}

        \onslide<10->{\node[myDot] at (p) {};}

      \end{tikzpicture}
    \]
    \onslide<14->{We say that $S$ is \emph{singular} at $P=\bv{X}(s_0, t_0)$ if
      $\bv{N}(s_0, t_0)=\bv{O}$.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('r theta')
  X = vector([r*cos(theta), r*sin(theta), r])
  Xr = X.diff(r)
  Xtheta = X.diff(theta)
  N = (Xr.cross_product(Xtheta)).simplify_trig()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}r\cos(\theta)}$};
      \onslide<2->{
        \node[overlay, below left= 1mm and 3mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}r\sin(\theta)}$};
      \onslide<3->{
        \node[overlay, below right= 1mm and 3mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamblue}r}$};
      \onslide<4->{
        \node[overlay, above right= 1mm and 2mm of a] (text) {$z^2=x^2+y^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myCone}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\H}{2}
        \pgfmathsetmacro{\R}{1.5}
        \pgfmathsetmacro{\a}{\R/8}

        \coordinate (ne) at (\R, \H);
        \coordinate (nw) at (-\R, \H);
        \coordinate (sw) at (-\R, -\H);
        \coordinate (se) at (\R, -\H);

        \filldraw[bluefilldraw]
        (sw)
        -- (ne)
        -- plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
        -- (se)
        -- plot[smooth, domain=0:180] ({\R*cos(\x)}, {\a*sin(\x)-\H})
        -- cycle;

        \filldraw[bluefilldraw]
        (sw)
        -- (ne)
        -- plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)+\H})
        -- (se)
        -- plot[smooth, domain=0:-180] ({\R*cos(\x)}, {\a*sin(\x)-\H})
        -- cycle;


        \onslide<16->{\node[myDot] at (O) {};}

        \onslide<17->{\draw[thick, <-, overlay, red, shorten <=2pt] (O.west) -| ++(-14mm, -2mm) node[below] {singular pt};}

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myA}{
    \left\lvert
      \begin{array}{rr}
        \sin(\theta)  & 1 \\
        r\cos(\theta) & 0
      \end{array}
    \right\rvert
  }
  \newcommand{\myB}{
    \left\lvert
      \begin{array}{rr}
        \cos(\theta)  & 1 \\
        -r\sin(\theta) & 0
      \end{array}
    \right\rvert
  }
  \newcommand{\myC}{
    \left\lvert
      \begin{array}{rr}
        \cos(\theta)   & \sin(\theta) \\
        -r\sin(\theta) & r\cos(\theta)
      \end{array}
    \right\rvert
  }
  \newcommand{\myN}{
    \begin{aligned}
      \onslide<7->{\bv{T}_r}      &\onslide<7->{=}  \onslide<8->{\sage{Xr}}                    \\
      \onslide<9->{\bv{T}_\theta} &\onslide<9->{=}  \onslide<10->{\sage{Xtheta}}               \\
      \onslide<11->{\bv{N}}       &\onslide<11->{=} \onslide<12->{\bv{T}_r\times\bv{T}_\theta} \\
      &\onslide<12->{=} \onslide<13->{\left\langle \myA, -\myB, \myC\right\rangle} \\
      &\onslide<13->{=} \onslide<14->{\sage{N}}
    \end{aligned}
  }
  \begin{example}
    Consider the parameterization
    \begin{align*}
      \bv{X}(r, \theta) &= \langle \myx, \myy, \myz\rangle && -1\leq r\leq 1 && 0\leq\theta\leq2\,\pi
    \end{align*}
    \onslide<5->{The underlying surface is the \emph{double cone}.}
    \begin{gather*}
      \begin{align*}
        \onslide<6->{\myCone} && \myN
      \end{align*}
    \end{gather*}
    \onslide<15->{The only singular point occurs when $r=0$.}
  \end{example}

\end{frame}


\subsection{Tangent Planes}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $S$ is smooth at $P=\bv{X}(s_0, t_0)$ so that
    \[
      \bv{N}=\bv{T}_s\times\bv{T}_t\neq\bv{O}
    \]
    Then $\bv{N}$ is normal to the tangent plane $T_PS$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , myFill/.style={fill=blue!40!white, fill opacity=0.60}
        , myDraw/.style={draw=blue!70!black, ultra thick}
        , scale=2.5
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\myR}{1}
        \pgfmathsetmacro{\myr}{\myR/8}
        \pgfmathsetmacro{\myb}{sqrt(\myR)}

        \coordinate (l) at (-\myR, 0);
        \coordinate (r) at ( \myR, 0);

        \onslide<2->{
          \node[above=2mm of l, beamblue] {$S$};

          \filldraw[bluefilldraw]
          (l) --
          plot[domain=-\myb:\myb] ({\x}, {\myR*\myR-\x*\x}) --
          (r) --
          plot[domain=0:180] ({\myR*cos(\x)}, {\myr*sin(\x)}) --
          (l) --
          cycle;

          \filldraw[bluefilldraw]
          (l) --
          plot[domain=-\myb:\myb] ({\x}, {\myR*\myR-\x*\x}) --
          (r) --
          plot[domain=0:-180] ({\myR*cos(\x)}, {\myr*sin(\x)}) --
          (l) --
          cycle;
        }

        \pgfmathsetmacro{\x}{1/3}
        \pgfmathsetmacro{\y}{\myR*\myR-\x*\x}
        \coordinate (p) at (\x, \y);

        \coordinate (v) at (1, -2/3);
        \coordinate (w) at (1, 0);

        \begin{scope}[shift={($ (p)-0.5*(v)-0.5*(w) $)}]
          \coordinate (v) at (1, -2/3);
          \coordinate (w) at (1, 0);
          \coordinate (vpw) at ($ (v)+(w) $);

          \onslide<4->{
            \draw[white] (0, 0) -- (w) node[midway, sloped, above, beamgreen] {$T_PS$};

            \filldraw[fill=beamgreen!75, draw=beamgreen, fill opacity=0.60]
            (0, 0) -- (v) -- ($ (v)+(w) $) -- (w) -- (0, 0) -- cycle;
          }

          % \onslide<11->{
          % \node[overlay, red, below right= 2mm and -12mm of vpw] (text)
          % {$\scriptstyle -2\cdot(x+1)+2\cdot(y-1)+(z-2)=0$};
          % \draw[overlay, <-, red, thick, shorten <=2pt] (vpw.east) -| (text.north);
          % }
        \end{scope}

        \begin{scope}[shift={(p)}]
          \coordinate (O) at (0, 0);
          \coordinate (gradf) at (\x, 1/2);
          \coordinate (gradfperp) at (1/2, -\x);
          \coordinate (perpa) at ($ (O)!0.15!(gradf) $);
          \coordinate (perpb) at ($ (O)!0.15!(gradfperp) $);

          \onslide<5->{
            \draw (perpa) -- ($ (perpa)+(perpb) $) -- (perpb);
            \draw[->, beamgreen] (O) -- (gradf) node[right] {$\bv{N}=\bv{T}_s\times\bv{T}_t$};
          }
        \end{scope}


        \onslide<3->{\node[myDot, label=left:{$P$}] at (p) {};}

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}

\begin{sagesilent}
  r0, t0 = 2, pi/6
  X0 = X(r=r0, theta=t0)
  N0 = N(r=r0, theta=t0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider again the double cone and its standard normal vector
    \begin{gather*}
      \begin{align*}
        \bv{X}(r, \theta) &= \langle r\cos(\theta), r\sin(\theta), r\rangle & \bv{N} &= \sage{N}
      \end{align*}
    \end{gather*}
    \onslide<2->{When $r=\sage{r0}$ and $\theta=\frac{\pi}{6}$ we have}
    \begin{align*}
      \onslide<2->{\bv{X}(\sage{r0}, \sfrac{\pi}{6})} &\onslide<2->{=} \onslide<3->{\sage{X0}} & \onslide<4->{\bv{N}} &\onslide<4->{=} \onslide<5->{\sage{N0}}
    \end{align*}
    \onslide<6->{The plane tangent to the cone at $P(\sqrt{3}, 1, 2)$ is then
      given by}
    \[
      \onslide<7->{-\sqrt{3}\cdot(x-\sqrt{3})-(y-1)+2\cdot(z-2) = 0}
    \]
  \end{example}

\end{frame}


\subsection{Surface Area}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \draw[<->] (-2, 0) -- (2, 0);
        \draw[<->] (0, -2)-- (0, 2);

        \pgfmathsetseed{3}
        \filldraw[bluefilldraw] plot [smooth cycle, samples=8,domain={1:8}]
        (\x*360/8+5*rnd:0.5cm+1cm*rnd) ;

        \node at (-1/2, -1/2) {$D$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myX}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        % \draw[->] (O) -- ($ 2*(x) $);% node[pos=1.05] {$x$};
        % \draw[->] (O) -- ($ 2*(y) $);% node[pos=1.05] {$y$};
        % \draw[->] (O) -- ($ 2*(z) $);% node[pos=1.05] {$z$};

        \begin{scope}[shift={(-1.25, -0.5)}, xscale=1, yscale=2/3]
          \filldraw[bluefilldraw] (0, 0)
          to[bend left]  (3, -1)
          to[bend left]  (5,  2)
          to[bend right] (2,  3)
          to[bend right] (0,  0)
          --cycle;

          \node at (4, 2) {$S$};

          \onslide<5->{
            \filldraw[greenfilldraw, thick]
            (1/2, 1/2) -- ++(2, 0) -- ++(1, 1) -- ++(-2, 0);
          }

          \onslide<3->{\draw[beamgreen, ->] (1/2, 1/2) -- ++(1, 1) node[above] {$\bv{T}_s$};}
          \onslide<4->{\draw[beamgreen, ->] (1/2, 1/2) -- ++(2, 0) node[below] {$\bv{T}_t$};}

          \onslide<2->{\node[myDot] at (1/2, 1/2) {};}
        \end{scope}

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    We may approximate area in $S$ at each point.
    \[
      \myX
    \]
    \onslide<6->{``Area density'' is given by
      $\norm{\bv{T}_s\times\bv{T}_t}=\norm{\bv{N}}$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $S$ is parameterized by
    $\bv{X}:D\subset\mathbb{R}^2\to\mathbb{R}^3$. Then
    \[
      \area(S) = \iint_D\norm{\bv{N}}\,dA
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myCone}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \pgfmathsetmacro{\H}{2}
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\a}{\R/8}

        \coordinate (O) at (0, 0);
        \coordinate (right) at (\R, \H);
        \coordinate (left) at (-\R, \H);

        \filldraw[bluefilldraw]
        (O)
        -- (right) arc (0:180:{\R} and {\a})
        -- (O)
        -- cycle;

        \filldraw[bluefilldraw]
        (O)
        -- (right) arc (0:-180:{\R} and {\a})
        -- (O)
        -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\bv{N} = \sage{N}}$};
      \onslide<2->{
        \node[overlay, above= 12mm of a] (text) {$\norm{\bv{N}}=\sqrt{r^2\cos^2(\theta)+r^2\sin^2(\theta)+r^2}=r\sqrt{2}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) -- (text.south);
      }
    }
  }
  \newcommand{\myArea}{
    \begin{aligned}
      \onslide<4->{\area}
      &\onslide<4->{=} \onslide<5->{\int_0^{2\,\pi}\int_0^1\norm{\bv{N}}\,dr\,d\theta} \\
      &\onslide<5->{=} \onslide<6->{\sqrt{2}\int_0^{2\,\pi}\int_0^1r\,dr\,d\theta} \\
      &\onslide<6->{=} \onslide<7->{\frac{\sqrt{2}}{2}\int_0^{2\,\pi}d\theta} \\
      &\onslide<7->{=} \onslide<8->{\sqrt{2}\,\pi}
    \end{aligned}
  }
  \begin{example}
    Consider the cone and its standard normal vector
    \begin{gather*}
      \begin{align*}
        \bv{X}(r, \theta) &= \langle r\cos(\theta), r\sin(\theta), r\rangle && 0\leq r\leq 1 && 0\leq\theta\leq2\,\pi && \myN
      \end{align*}
    \end{gather*}
    \onslide<3->{The surface area of the cone is}
    \begin{align*}
      \onslide<3->{\myCone} && \myArea
    \end{align*}
  \end{example}

\end{frame}




\end{document}

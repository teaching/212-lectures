\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}
\usepackage{fitzparaboloid}

\tikzset{
  , myfill/.style={fill=blue!40!white, fill opacity=.6}
  , mydraw/.style={draw=blue!70!black}
}

\title{Triple Integrals}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Triple Integrals}
\subsection{Motivation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    An object conforms to the shape of a solid $W$ in $\mathbb{R}^3$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=3/4
        , xscale=4/3
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]
        % \filldraw[fill=beamblue!45] (0,0) circle (2cm);
        % \draw (-2,0) arc (180:360:2 and 0.6);
        % \draw[dashed] (2,0) arc (0:180:2 and 0.6);

        \coordinate (O) at (0, 0);
        \coordinate (P1) at (-1, 0);
        \coordinate (P2) at (0, 1);
        \coordinate (P3) at (1, 0);
        \coordinate (P4) at (0, -1);

        \draw[mydraw] plot[smooth, domain=0:180] ({2*cos(\x)}, {sin(\x)/2});
        \filldraw[myfill, mydraw] (O) circle (2);

        \onslide<3->{
          \node[myDot, label=right:{$P_1$}] at (P1) {};
          \node[overlay, red, below left= 2mm and 20mm of P1]
          (text) {$\scriptstyle f(P_1)=14\,\si{\kg\per\metre\cubed}$};
          \draw[overlay, red, ->, thick, shorten >=4pt]
          (text.north) |- (P1.west);
        }

        \onslide<4->{
          \node[myDot, label=above:{$P_2$}] at (P2) {};
          \node[overlay, red, above left= 2mm and 30mm of P2]
          (text) {$\scriptstyle f(P_2)=7\,\si{\kg\per\metre\cubed}$};
          \draw[overlay, red, ->, thick, shorten >=4pt]
          (text.south) |- (P2.west);
        }

        \onslide<5->{
          \node[myDot, label=left:{$P_3$}] at (P3) {};
          \node[overlay, red, above right= 2mm and 20mm of P3]
          (text) {$\scriptstyle f(P_3)=9\,\si{\kg\per\metre\cubed}$};
          \draw[overlay, red, ->, thick, shorten >=4pt]
          (text.south) |- (P3.east);
        }

        \onslide<6->{
          \node[myDot, label=below:{$P_4$}] at (P4) {};
          \node[overlay, red, below right= 2mm and 30mm of P4]
          (text) {$\scriptstyle f(P_4)=21\,\si{\kg\per\metre\cubed}$};
          \draw[overlay, red, ->, thick, shorten >=4pt]
          (text.north) |- (P4.west);
        }

        \filldraw[myfill, mydraw] (O) circle (2);
        \draw[mydraw] plot[smooth, domain=0:-180] ({2*cos(\x)}, {sin(\x)/2});

      \end{tikzpicture}
    \]
    \onslide<2->{Suppose $f\in\mathscr{C}(\mathbb{R}^3)$ measures density
      (\SI{}{\kg\per\metre\cubed}) throughout $W$.}
  \end{example}

  \begin{definition}<7->
    The \emph{triple integral of $f$ on $W$} is
    \[
      \iiint_W f\,dV = \textnormal{mass of $W$ (in \SI{}{\kg})}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}f}$};
      \onslide<2->{
        \node[overlay, below left = 4mm and 2mm of a] (text)
        {$\dfrac{\textnormal{mass units}}{\textnormal{volume unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mydV}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}dV}$};
      \onslide<3->{
        \node[overlay, below right = 3mm and 1mm of a] (text)
        {$\textnormal{volume unit}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units allows us to interpret double integrals.
    \[
      \iiint_W \myf\,\mydV
      = \onslide<4->{\textnormal{mass of }W}
    \]
  \end{block}

\end{frame}


\subsection{Iterated Integrals}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we \emph{calculate} a triple integral?
  \end{block}

  \begin{block}{Answer}<2->
    Use iterated integrals!
  \end{block}

\end{frame}


\begin{sagesilent}
  var('x y z')
  f = x*y-z
  a1, a2 = 1, 3
  b1, b2 = 3, 5
  c1, c2 = -2, 2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $f=\sage{f}\,\si{\celsius\per\metre\cubed}$ measures density throughout
    \[
      W=[\sage{a1}, \sage{a2}]\times[\sage{b1}, \sage{b2}]\times[\sage{c1},
      \sage{c2}]
    \]
    \onslide<2->{The mass of $W$ is}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\iiint_W f\,dV}
        &\onslide<2->{=} \onslide<3->{\int_{\sage{a1}}^{\sage{a2}}\int_{\sage{b1}}^{\sage{b2}}{\color<4->{red} \int_{\sage{c1}}^{\sage{c2}}\sage{f}\,dz}\,dy\,dx}                        &&\onslide<8->{=} \onslide<9->{\int_{\sage{a1}}^{\sage{a2}}\sage{integral(integral(f, z, c1, c2), y, b1, b2)}\,dx}                             \\
        &\onslide<3->{=} \onslide<5->{\int_{\sage{a1}}^{\sage{a2}}\int_{\sage{b1}}^{\sage{b2}}{\color{red} \left.\sage{integral(f, z)}\right\rvert_{z=\sage{c1}}^{z=\sage{c2}}}\,dy\,dx} &&\onslide<9->{=} \onslide<10->{\left.\sage{integral(integral(integral(f, z, c1, c2), y, b1, b2), x)}\right\rvert_{x=\sage{a1}}^{x=\sage{a2}}} \\
        &\onslide<5->{=} \onslide<6->{\int_{\sage{a1}}^{\sage{a2}}{\color<7->{red} \int_{\sage{b1}}^{\sage{b2}}\sage{integral(f, z, c1, c2)}\,dy}\,dx}                                   &&\onslide<10->{=} \onslide<11->{\sage{integral(integral(integral(f, z, c1, c2), y, b1, b2), x, a1, a2)}\,\si{\celsius}}                       \\
        &\onslide<6->{=} \onslide<8->{\int_{\sage{a1}}^{\sage{a2}}{\left.\sage{integral(integral(f, z, c1, c2), y)}\right\rvert_{y=\sage{b1}}^{y=\sage{b2}}}\,dx}                        &&
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myx}{\int_{x_1}^{x_2}}
  \newcommand{\myy}{\int_{y_1}^{y_2}}
  \newcommand{\myz}{\int_{z_1}^{z_2}}
  \begin{block}{Observation}
    Double integrals over rectangular regions come in \emph{two flavors}
    \begin{align*}
      \onslide<2->{{\color<3->{red}\int_{x_1}^{x_2}\int_{y_1}^{y_2}f\,dy\,dx\quad\onslide<4->{(x\textnormal{-slicing})}}} && \onslide<2->{{\color<5->{beamgreen}\int_{y_1}^{y_2}\int_{x_1}^{x_2}f\,dx\,dy\quad\onslide<6->{(y\textnormal{-slicing})}}}
    \end{align*}
    \onslide<7->{Triple integrals over rectangular regions come in \emph{six flavors}}
    \begin{align*}
      \onslide<8->{{\color<9->{red}\myx\myy\myz f\,dz\,dy\,dx}}        && \onslide<8->{{\color<9->{red}\myx\myz\myy f\,dy\,dz\,dx \quad\onslide<10->{(x\textnormal{-slicing})}}}       \\
      \onslide<8->{{\color<11->{beamgreen}\myy\myx\myz f\,dz\,dx\,dy}} && \onslide<8->{{\color<11->{beamgreen}\myy\myz\myx f\,dx\,dz\,dy\quad\onslide<12->{(y\textnormal{-slicing})}}} \\
      \onslide<8->{{\color<13->{beamblue}\myz\myx\myy f\,dy\,dx\,dz}}  && \onslide<8->{{\color<13->{beamblue}\myz\myy\myx f\,dx\,dy\,dz \quad\onslide<14->{(z\textnormal{-slicing})}}}
    \end{align*}
  \end{block}

\end{frame}


\subsection{Nonrectangular Regions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we compute $\iiint_W f\,dV$ if $W$ is not rectangular?
  \end{block}

  \begin{block}{Answer}<2->
    Our slicing method will depend on the shape of $W$.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myinner}[1]{
    \pgfmathsetmacro{\myc}{1-####1}
    \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
    ($ ####1*(x) $) -- ($ ####1*(x)+\myc*(y) $) -- ($ ####1*(x)+\myc*(z) $) -- cycle;
  }
  \newcommand{\myW}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=2
        ]

        \coordinate (O) at (0, 0);

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        \draw[->] (O) -- ($ 1.25*(x) $) node[pos=1.05] {$x$};
        \draw[->] (O) -- ($ 1.25*(y) $) node[pos=1.05] {$y$};
        \draw[->] (O) -- ($ 1.25*(z) $) node[pos=1.05] {$z$};

        \node[above left]  at (x) {$6$};
        \node[above right] at (y) {$3$};
        \node[right]       at (z) {$2$};

        \onslide<4->{\myinner{0.5}}

        \filldraw[myfill, mydraw] (x) -- (z) -- (y) -- (x) -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myYZ}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-1/2, 0) -- (3, 0) node[right] {$y$};
        \draw[<->] (0, -1/2) -- (0, 3) node[above] {$z$};

        \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen] (O) -- (5/2, 0) -- (0, 5/2) -- cycle;

        \draw (5/2, 3pt) -- (5/2, -3pt) node[below] {$\oldfrac{6-x}{2}$};
        \draw (3pt, 5/2) -- (-3pt, 5/2) node[left] {$\oldfrac{6-x}{3}$};

        \node[right] at (5/4, 5/4) {$x+2\,y+3\,z=6$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the ``first octant'' part of $x+2\,y+3\,z\leq 6$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myW} && \onslide<5->{\myYZ}
      \end{align*}
    \end{gather*}
    \onslide<3->{Each $x$-slice leaves an imprint on the $yz$-plane.}
    \begin{gather*}
      \onslide<6->{\iiint_W f\,dV
        =} \onslide<7->{\int_0^6\int_{0}^{\oldfrac{6-x}{2}}\int_{0}^{\oldfrac{6-x-2\,y}{3}} f\,dz\,dy\,dx
        =} \onslide<8->{\int_0^6\int_{0}^{\oldfrac{6-x}{3}}\int_{0}^{\oldfrac{6-x-3\,z}{2}} f\,dy\,dz\,dx}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myinner}[1]{
    \pgfmathsetmacro{\myc}{1-####1}
    \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
    ($ ####1*(y) $) -- ($ ####1*(y)+\myc*(x) $) -- ($ ####1*(y)+\myc*(z) $) -- cycle;
  }
  \newcommand{\myW}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=2
        ]

        \coordinate (O) at (0, 0);

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        \draw[->] (O) -- ($ 1.25*(x) $) node[pos=1.05] {$x$};
        \draw[->] (O) -- ($ 1.25*(y) $) node[pos=1.05] {$y$};
        \draw[->] (O) -- ($ 1.25*(z) $) node[pos=1.05] {$z$};

        \node[above left]  at (x) {$6$};
        \node[above right] at (y) {$3$};
        \node[right]       at (z) {$2$};

        \onslide<4->{\myinner{0.5}}

        \filldraw[myfill, mydraw] (x) -- (z) -- (y) -- (x) -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myYZ}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-1/2, 0) -- (3, 0) node[right] {$x$};
        \draw[<->] (0, -1/2) -- (0, 3) node[above] {$z$};

        \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen] (O) -- (5/2, 0) -- (0, 5/2) -- cycle;

        \draw (5/2, 3pt) -- (5/2, -3pt) node[below] {$6-2\,y$};
        \draw (3pt, 5/2) -- (-3pt, 5/2) node[left] {$\oldfrac{6-2\,y}{3}$};

        \node[right] at (5/4, 5/4) {$x+2\,y+3\,z=6$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the ``first octant'' part of $x+2\,y+3\,z\leq 6$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myW} && \onslide<5->{\myYZ}
      \end{align*}
    \end{gather*}
    \onslide<3->{Each $y$-slice leaves an imprint on the $xz$-plane.}
    \begin{gather*}
      \onslide<6->{\iiint_W f\,dV
      =} \onslide<7->{\int_0^3\int_{0}^{6-2\,y}\int_{0}^{\oldfrac{6-x-2\,y}{3}} f\,dz\,dx\,dy
      =} \onslide<8->{\int_0^3\int_{0}^{\oldfrac{6-2\,y}{3}}\int_{0}^{6-2\,y-3\,z} f\,dx\,dz\,dy}
    \end{gather*}
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myinner}[1]{
    \pgfmathsetmacro{\myc}{1-####1}
    \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
    ($ ####1*(z) $) -- ($ ####1*(z)+\myc*(y) $) -- ($ ####1*(z)+\myc*(x) $) -- cycle;
  }
  \newcommand{\myW}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=2
        ]

        \coordinate (O) at (0, 0);

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        \draw[->] (O) -- ($ 1.25*(x) $) node[pos=1.05] {$x$};
        \draw[->] (O) -- ($ 1.25*(y) $) node[pos=1.05] {$y$};
        \draw[->] (O) -- ($ 1.25*(z) $) node[pos=1.05] {$z$};

        \node[above left]  at (x) {$6$};
        \node[above right] at (y) {$3$};
        \node[right]       at (z) {$2$};

        \onslide<4->{\myinner{0.5}}

        \filldraw[myfill, mydraw] (x) -- (z) -- (y) -- (x) -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myYZ}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-1/2, 0) -- (3, 0) node[right] {$x$};
        \draw[<->] (0, -1/2) -- (0, 3) node[above] {$y$};

        \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen] (O) -- (5/2, 0) -- (0, 5/2) -- cycle;

        \draw (5/2, 3pt) -- (5/2, -3pt) node[below] {$6-3\,z$};
        \draw (3pt, 5/2) -- (-3pt, 5/2) node[left] {$\oldfrac{6-3\,z}{2}$};

        \node[right] at (5/4, 5/4) {$x+2\,y+3\,z=6$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the ``first octant'' part of $x+2\,y+3\,z\leq 6$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myW} && \onslide<5->{\myYZ}
      \end{align*}
    \end{gather*}
    \onslide<3->{Each $z$-slice leaves an imprint on the $xy$-plane.}
    \begin{gather*}
      \onslide<6->{\iiint_W f\,dV
        =} \onslide<7->{\int_0^2\int_{0}^{6-3\,z}\int_{0}^{\oldfrac{6-x-3\,z}{2}} f\,dy\,dx\,dz
        =} \onslide<8->{\int_0^2\int_{0}^{\oldfrac{6-3\,z}{2}}\int_{0}^{6-2\,y-3\,z} f\,dx\,dy\,dz}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myinner}[1]{
    \pgfmathsetmacro{\myc}{1-####1}
    \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
    ($ ####1*(z) $) -- ($ ####1*(z)+\myc*(y) $) -- ($ ####1*(z)+\myc*(x) $) -- cycle;
  }
  \newcommand{\myW}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=2
        ]

        \coordinate (O) at (0, 0);

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        \path[name path=lparabola] (O) parabola ($ (x)+(z) $);
        \path[name path=rparabola] (O) parabola ($ (y)+(z) $);
        \path[name path=ledge] (z) -- ($ (x)+(z) $);
        \path[name path=redge] (z) -- ($ (y)+(z) $);
        \path[name path=curve] ($ (x)+(z) $) to[bend right=10] ($ (y)+(z) $);
        \path[name path=lhoriz] ($ 0.5*(x)+(z) $) -- ++(y);
        \path[name path=rhoriz] ($ 0.5*(y)+(z) $) -- ++(y);
        \path[name path=lvert] ($ 0.5*(x)+(z) $) -- ++($ -1*(z) $);
        \path[name path=rvert] ($ 0.5*(y)+(z) $) -- ++($ -1*(z) $);

        \path[name intersections={of=lhoriz and curve, by={P}}];
        \path[name intersections={of=lvert and lparabola, by={Q}}];

        \draw[->] (O) -- ($ 1.25*(x) $) node[pos=1.05] {$x$};
        \draw[->] (O) -- ($ 1.25*(y) $) node[pos=1.05] {$y$};
        \draw[->] (O) -- ($ 1.25*(z) $) node[pos=1.05] {$z$};


        \fill[myfill] (O) parabola ($ (x)+(z) $) -- ($ (x)+(z) $) -- (z) -- (O) -- cycle;
        \fill[myfill] (O) parabola ($ (y)+(z) $) -- ($ (y)+(z) $) -- (z) -- (O) -- cycle;
        \onslide<4->{\filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen] (Q) parabola (P) -- ($ 0.5*(x)+(z) $) -- (Q);}
        %\onslide<4->{\filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen] (O) parabola ($ (y)+(z) $) -- ($ (y)+(z) $) -- (z) -- (O) -- cycle;}
        \fill[myfill] (O) parabola ($ (x)+(z) $) -- ($ (x)+(z) $) -- (z) -- (O) -- cycle;
        \fill[myfill] (O) parabola ($ (y)+(z) $) -- ($ (y)+(z) $) -- (z) -- (O) -- cycle;

        \draw[mydraw] (O) parabola ($ (x)+(z) $);
        \draw[mydraw] (O) parabola ($ (y)+(z) $);

        \draw[mydraw] (z) -- ($ (x)+(z) $) to[bend right=10] ($ (y)+(z) $) -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myYZ}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-1/2, 0) -- (3, 0) node[right] {$y$};
        \draw[<->] (0, -1/2) -- (0, 3) node[above] {$z$};

        \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen] (0, 1/2) parabola (5/2, 5/2) -- (0, 5/2) -- cycle;

        \draw (5/2, 3pt) -- (5/2, -3pt) node[below] {$\sqrt{1-x^2}$};
        \draw (3pt, 1/2) -- (-3pt, 1/2) node[left] {$x^2$};
        \draw (3pt, 5/2) -- (-3pt, 5/2) node[left] {$1$};

        \node[right=0.70cm] at (5/4, 5/4) {$z=x^2+y^2$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the ``first octant'' part of $x^2+y^2\leq z\leq 1$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myW} && \onslide<5->{\myYZ}
      \end{align*}
    \end{gather*}
    \onslide<3->{Each $x$-slice leaves an imprint on the $yz$-plane.}
    \begin{gather*}
      \onslide<6->{\iiint_W f\,dV
        =} \onslide<7->{\int_{0}^{1}\int_{0}^{\sqrt{1-x^2}}\int_{x^2+y^2}^{1} f\,dz\,dy\,dx
        =} \onslide<8->{\int_{0}^{1}\int_{x^2}^{1}\int_{0}^{\sqrt{z-x^2}} f\,dy\,dz\,dx}
    \end{gather*}
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myinner}[1]{
    \pgfmathsetmacro{\myc}{1-####1}
    \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
    ($ ####1*(z) $) -- ($ ####1*(z)+\myc*(y) $) -- ($ ####1*(z)+\myc*(x) $) -- cycle;
  }
  \newcommand{\myW}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=2
        ]

        \coordinate (O) at (0, 0);

        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at (0, 1);
        \coordinate (x) at ({cos(210)}, {sin(210)});

        \path[name path=lparabola] (O) parabola ($ (x)+(z) $);
        \path[name path=rparabola] (O) parabola ($ (y)+(z) $);
        \path[name path=ledge] (z) -- ($ (x)+(z) $);
        \path[name path=redge] (z) -- ($ (y)+(z) $);
        \path[name path=curve] ($ (x)+(z) $) to[bend right=10] ($ (y)+(z) $);
        \path[name path=lhoriz] ($ 0.5*(x)+(z) $) -- ++(y);
        \path[name path=rhoriz] ($ 0.5*(y)+(z) $) -- ++(y);
        \path[name path=lvert] ($ 0.75*(x)+(z) $) -- ++($ -1*(z) $);
        \path[name path=rvert] ($ 0.75*(y)+(z) $) -- ++($ -1*(z) $);

        \path[name intersections={of=lvert and lparabola, by={P}}];
        \path[name intersections={of=rvert and rparabola, by={Q}}];

        \path[name path=toaxis] (P) -- ++($ -1*(x) $);
        \path[name path=zaxis] (O) -- (z);
        \path[name intersections={of=toaxis and zaxis, by={R}}];

        \draw[->] (O) -- ($ 1.25*(x) $) node[pos=1.05] {$x$};
        \draw[->] (O) -- ($ 1.25*(y) $) node[pos=1.05] {$y$};
        \draw[->] (O) -- ($ 1.25*(z) $) node[pos=1.05] {$z$};

        \onslide<4->{
          \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
          (P) to[bend right=10] (Q) -- (R) -- (P) -- cycle;
        }

        \fill[myfill] (O) parabola ($ (x)+(z) $) -- ($ (x)+(z) $) -- (z) -- (O) -- cycle;
        \fill[myfill] (O) parabola ($ (y)+(z) $) -- ($ (y)+(z) $) -- (z) -- (O) -- cycle;
        \fill[myfill] (O) parabola ($ (x)+(z) $) -- ($ (x)+(z) $) -- (z) -- (O) -- cycle;
        \fill[myfill] (O) parabola ($ (y)+(z) $) -- ($ (y)+(z) $) -- (z) -- (O) -- cycle;

        \draw[mydraw] (O) parabola ($ (x)+(z) $);
        \draw[mydraw] (O) parabola ($ (y)+(z) $);

        \draw[mydraw] (z) -- ($ (x)+(z) $) to[bend right=10] ($ (y)+(z) $) -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myYZ}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-1/2, 0) -- (3, 0) node[right] {$x$};
        \draw[<->] (0, -1/2) -- (0, 3) node[above] {$y$};

        \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
        (O) -- (5/2, 0) plot[smooth, domain=0:90] ({5/2*cos(\x)}, {5/2*sin(\x)}) -- (O) -- cycle;

        \draw (5/2, 3pt) -- (5/2, -3pt) node[below] {$\sqrt{z}$};
        %\draw (3pt, 1/2) -- (-3pt, 1/2) node[left] {$x^2$};
        \draw (3pt, 5/2) -- (-3pt, 5/2) node[left] {$\sqrt{z}$};

        \node[right=0.90cm] at (5/4, 5/4) {$z=x^2+y^2$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the ``first octant'' part of $x^2+y^2\leq z\leq 1$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myW} && \onslide<5->{\myYZ}
      \end{align*}
    \end{gather*}
    \onslide<3->{Each $z$-slice leaves an imprint on the $xy$-plane.}
    \begin{gather*}
      \onslide<6->{\iiint_W f\,dV
        =} \onslide<7->{\int_{0}^{1}\int_{0}^{\sqrt{z}}\int_{0}^{\sqrt{z-x^2}} f\,dy\,dx\,dz
        =} \onslide<8->{\int_{0}^{1}\int_{0}^{\sqrt{z}}\int_{0}^{\sqrt{z-y^2}} f\,dx\,dy\,dz}
    \end{gather*}
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myW}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , xscale=1.25
        , yscale=0.65
        ]

        % ftop = x^2/2+2
        % fbottom = x^2
        \paraboloidFillBack{1}{2}{1/4}
        \paraboloidFillBack[2]{1/2}{2}{1/4}
        \draw[ultra thick, <->] (0, -1/2) -- (0, 5) node[above] {$y$};
        \paraboloidFillFront[2]{1/2}{2}{1/4}
        \paraboloidFillFront{1}{2}{1/4}

        \pgfmathsetmacro{\x}{1}
        \pgfmathsetmacro{\ytop}{\x*\x/2+2}
        \pgfmathsetmacro{\ybottom}{\x*\x}

        \coordinate (ptop) at (-\x, \ytop);
        \coordinate (pbottom) at (-\x, \ybottom);

        \onslide<3->{
          \node[red, below left= 1mm and 8mm of ptop]
          (text) {$y=x^2+z^2+4$};
          \draw[red, <-, thick, shorten <=2pt]
          (ptop.south) |- (text.east);
        }

        \onslide<2->{
          \node[red, below left= 0mm and 4mm of pbottom]
          (text) {$y=2\,x^2+2\,z^2$};
          \draw[red, <-, thick, shorten <=2pt]
          (pbottom.south) |- (text.east);
        }

        \coordinate (p) at (-2, 4);

        \onslide<4->{
          \node[red, left= 4mm of p]
          (text) {$x^2+z^2=4$};
          \draw[red, <-, thick, shorten <=2pt]
          (p.east) -- (text.east);
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myYZ}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-5/2, 0) -- (5/2, 0) node[right] {$x$};
        \draw[<->] (0, -1/2) -- (0, 4) node[above] {$y$};

        \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
        (-2, 3) parabola[bend at end] (0, 2)
        -- (0, 2) parabola (2, 3)
        -- (2, 3) parabola[bend at end] (0, 1/2)
        -- (0, 1/2) parabola (-2, 3)
        --cycle;

        \draw[draw=beamgreen] (0, 1/2) parabola (2, 3);
        \draw[draw=beamgreen] (2, 3) parabola[bend at end] (0, 2);

        \node[overlay, right] at (0, 7/2) {$y=x^2+z^2+4$};
        \node[overlay, right] at (0, 0.40) {$y=2\,x^2+2\,z^2$};


        \draw (2, 3pt) -- (2, -3pt) node[below] {$\sqrt{4-z^2}$};
        \draw (-2, 3pt) -- (-2, -3pt) node[below] {$-\sqrt{4-z^2}$};

        % \filldraw[fill=beamgreen, fill opacity=0.6, draw=beamgreen]
        % (O) -- (5/2, 0) plot[smooth, domain=0:90] ({5/2*cos(\x)}, {5/2*sin(\x)}) -- (O) -- cycle;

        % \draw (5/2, 3pt) -- (5/2, -3pt) node[below] {$\sqrt{z}$};
        % %\draw (3pt, 1/2) -- (-3pt, 1/2) node[left] {$x^2$};
        % \draw (3pt, 5/2) -- (-3pt, 5/2) node[left] {$\sqrt{z}$};

        % \node[right=0.90cm] at (5/4, 5/4) {$z=x^2+y^2$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the region $2\,x^2+2\,z^2\leq y\leq x^2+z^2+4$.
    \begin{gather*}
      \begin{align*}
        \myW && \onslide<6->{\myYZ}
      \end{align*}
    \end{gather*}
    \onslide<5->{Each $z$-slice leaves an imprint on the $xy$-plane.}
    \begin{gather*}
      \onslide<7->{\iiint_W f\,dV
        =} \onslide<8->{\int_{-2}^{2}\int_{-\sqrt{4-z^2}}^{\sqrt{4-z^2}}\int_{2\,x^2+2\,z^2}^{x^2+z^2+4} f\,dy\,dx\,dz}
    \end{gather*}
  \end{example}

\end{frame}







\end{document}

\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}
\usepackage{fitzparaboloid}
\usepackage{tikz-3dplot}%
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{patchplots}

\title{The Hessian}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Single-Variable}
\subsection{Concavity}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myUp}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<2->{
          \draw[beamblue, <->] plot[domain=-sqrt(2):sqrt(2)] ({\x}, {\x*\x});
          \node[myDot] at (O) {};
        }

        \onslide<3->{
          \node[above, beamgreen] at (current bounding box.north) {Concave Up};
        }

        \onslide<4->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {\stackanchor{$f^{\prime\prime}(p)>0$}{(sufficient, unnecessary)}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myDown}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<5->{
          \draw[beamblue, <->] plot[domain=-sqrt(2):sqrt(2)] ({\x}, {-\x*\x});
          \node[myDot] at (O) {};
        }

        \onslide<6->{
          \node[above, beamgreen] at (current bounding box.north) {Concave Down};
        }

        \onslide<7->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {\stackanchor{$f^{\prime\prime}(p)<0$}{(sufficient, unnecessary)}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myInflection}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<8->{
          \draw[beamblue, <->] plot[domain=-1:1] ({\x}, {\x*\x*\x});
          \node[myDot] at (O) {};
        }

        \onslide<9->{
          \node[above, beamgreen] at (current bounding box.north) {Inflection Point};
        }

        \onslide<10->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {\stackanchor{$f^{\prime\prime}(p)=0$}{(insufficient, necessary)}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    The second derivative can measure \emph{concavity}.
    \begin{gather*}
      \begin{align*}
        \myUp && \myDown && \myInflection
      \end{align*}
    \end{gather*}
  \end{block}

\end{frame}


\subsection{Taylor Approximations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How does concavity relate to the second derivative, exactly?
  \end{block}

  \newcommand{\myfp}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{red}f^\prime(p)}$};
      \onslide<3->{
        \node[overlay, red, below left= 0mm and 3mm of a] (text) {controls \emph{variation}};
        \draw[overlay, red, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myfpp}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamgreen}f^{\prime\prime}(p)}$};
      \onslide<4->{
        \node[overlay, beamgreen, below right= 0mm and 3mm of a] (text) {controls \emph{concavity}};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Answer}<2->
    Recall the \emph{second degree Taylor polynomial} of $f(x)$ at $x=p$.
    \[
      T_2(x) = f(p) + \myfp\cdot(x-p) + \frac{1}{2!}\myfpp\cdot(x-p)^2
    \]
    \onslide<5->{This quadratic is used to approximate values of $f(x)$ ``near''
      $x=p$.}
    \[
      \onslide<5->{
        f(p+\Delta p)
        \approx f(p)+f^\prime(p)\cdot\Delta p+\frac{1}{2!}f^{\prime\prime}(p)\cdot\Delta p^2
      }
    \]
    \onslide<6->{When $f^{\prime\prime}(p)=0$, we can use \emph{higher degree
        Taylor polynomials}.}
  \end{block}

\end{frame}



\begin{sagesilent}
  var('x')
  f = 2*exp(x-1)*sin(x-1)-3
  x0 = 1
  T1 = f.taylor(x, x0, 1)
  T2 = f.taylor(x, x0, 2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $f(x)=2\,e^{x-1}\cdot\sin(x-1)-3$ at $x=\sage{x0}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , xscale=3/2
        , yscale=.85
        ]

        \coordinate (O) at (0, 0);

        \draw[<->, beamblue] plot[domain=-3:1] ({\x}, {exp(\x)*sin(\x r)}) node[above] {$f(x)$};

        \onslide<2->{
          \draw[<->, red] plot[domain=2:-1] ({\x}, {\x}) node[below] {$\scriptstyle T_1(x)=-3+2\cdot(x-1)$};
        }

        \onslide<3->{
          \draw[<->, beamgreen] plot[domain=1:-2] ({\x}, {\x*\x+\x}) node[above] {$\scriptstyle T_2(x)=-3+2\cdot(x-1)+\oldfrac{4}{2!}\cdot(x-1)^2$};
        }

        \node[myDot] at (O) {};

        \onslide<6->{
          \node[myDot] at (1/2, 1/2) {};
        }

        \onslide<8->{
          \node[myDot] at (1/2, 3/4) {};
        }

      \end{tikzpicture}
    \]
    \onslide<4->{We can approximate $f(\frac{3}{2})$ by using
      $\Delta p=\frac{1}{2}$.}
    \begin{gather*}
      \begin{align*}
        \onslide<5->{f(\sfrac{3}{2})} &\onslide<5->{\approx -3+2\cdot(\sfrac{1}{2}) = -2} & \onslide<7->{f(\sfrac{3}{2})} &\onslide<7->{\approx -3+2\cdot(\sfrac{1}{2}) + \frac{4}{2!}(\sfrac{1}{2})^2 = -\sfrac{3}{2}}
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}


\subsection{Orientation of Error}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myUp}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \pgfmathsetmacro{\myrt}{sqrt(2)}
        \coordinate (O) at (0, 0);

        \onslide<2->{
          \draw[beamblue, <->] plot[domain=-sqrt(2):sqrt(2)] ({\x}, {\x*\x});
        }

        \onslide<3->{
          \draw[red, <->] (-\myrt, 0) -- (\myrt, 0);
        }

        \onslide<2->{
          \node[myDot] at (O) {};

          \node[above, beamgreen] at (current bounding box.north) {Concave Up};
        }

        \onslide<4->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {$L_p(x)$ \emph{underestimate}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myDown}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \pgfmathsetmacro{\myrt}{sqrt(2)}
        \coordinate (O) at (0, 0);

        \onslide<5->{
          \draw[beamblue, <->] plot[domain=-sqrt(2):sqrt(2)] ({\x}, {-\x*\x});
        }

        \onslide<6->{
          \draw[red, <->] (-\myrt, 0) -- (\myrt, 0);
        }

        \onslide<5->{
          \node[myDot] at (O) {};
          \node[above, beamgreen] at (current bounding box.north) {Concave Down};
        }

        \onslide<7->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {$L_p(x)$ \emph{overestimate}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myInflection}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<8->{
          \draw[beamblue, <->] plot[domain=-1:1] ({\x}, {\x*\x*\x});
        }

        \onslide<9->{
          \draw[red, <->] (-1, 0) -- (1, 0);
        }

        \onslide<8->{
          \node[myDot] at (O) {};
          \node[above, beamgreen] at (current bounding box.north) {Inflection Point};
        }

        \onslide<10->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {direction matters};
        }

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    Concavity helps us understand the error in a linear approximation.
    \begin{gather*}
      \begin{align*}
        \myUp && \myDown && \myInflection
      \end{align*}
    \end{gather*}
  \end{block}

\end{frame}


\section{Multi-Variable}
\subsection{Second-Order Partials}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{second-order partial derivatives} of $f\in\mathscr{C}(\mathbb{R}^n)$ are
    \begin{align*}
      \onslide<2->{\pdv{f}{x_i, x_j}} &\onslide<2->{=} \onslide<3->{\pdv*{\pdv{f}{x_j}}{x_i}} && \onslide<6->{=} & \onslide<4->{f_{x_jx_i}} &\onslide<4->{=} \onslide<5->{(f_{x_j})_{x_i}}
    \end{align*}

  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = sin(x-3*y)
  fx = f.diff(x)
  fy = f.diff(y)
  fxx = fx.diff(x)
  fyy = fy.diff(y)
  fxy = fx.diff(y)
  fyx = fy.diff(x)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The first-order partial derivatives of $f(x, y)=\sage{f}$ are
    \begin{align*}
      \onslide<2->{f_x} &\onslide<2->{=} \onslide<3->{\sage{f.diff(x)}} & \onslide<4->{f_y} &\onslide<4->{=} \onslide<5->{\sage{f.diff(y)}}
    \end{align*}
    \onslide<6->{The second-order partial derivatives of $f$ are}
    \begin{gather*}
      \begin{align*}
        \onslide<6->{f_{xx}} &\onslide<6->{=} \onslide<7->{\pdv*{f_x}{x}}             & \onslide<10->{f_{yy}} &\onslide<10->{=} \onslide<11->{\pdv*{f_y}{y}}             & \onslide<14->{f_{xy}} &\onslide<14->{=} \onslide<15->{\pdv*{f_x}{y}}             & \onslide<18->{f_{yx}} &\onslide<18->{=} \onslide<19->{\pdv*{f_y}{x}}             \\
                             &\onslide<7->{=} \onslide<8->{\pdv*{\Set{\sage{fx}}}{x}} &                       &\onslide<11->{=} \onslide<12->{\pdv*{\Set{\sage{fy}}}{y}} &                       &\onslide<15->{=} \onslide<16->{\pdv*{\Set{\sage{fx}}}{y}} &                       &\onslide<19->{=} \onslide<20->{\pdv*{\Set{\sage{fy}}}{x}} \\
                             &\onslide<8->{=} \onslide<9->{\sage{fxx}}                &                       &\onslide<12->{=} \onslide<13->{\sage{fyy}}                &                       &\onslide<16->{=} \onslide<17->{\sage{fxy}}                &                       &\onslide<20->{=} \onslide<21->{\sage{fyx}}
      \end{align*}
    \end{gather*}
    \onslide<22->{Note that $f_{xy}=f_{yx}$!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Clairaut's Theorem]
    $\displaystyle\pdv{f}{x_i, x_j}=\pdv{f}{x_j, x_i}{\color{red}\onslide<2->{\leftarrow\textnormal{order does not matter!}}}$
  \end{theorem}

\end{frame}


\subsection{The Hessian}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{Hessian} of $f\in\mathscr{C}(\mathbb{R}^n)$ is
    \[
      Hf =
      \left[
        \begin{array}{cccc}
          f_{x_1x_1}                       & {\color<3->{red}f_{x_1x_2}}       & \cdots & {\color<5->{beamblue}f_{x_1x_n}} \\
          {\color<2->{red}f_{x_2x_1}}      & f_{x_2x_2}                        & \cdots & {\color<7->{beamgreen}f_{x_2x_n}} \\
          \vdots                           & \vdots                            & \ddots & \vdots     \\
          {\color<4->{beamblue}f_{x_nx_1}} & {\color<6->{beamgreen}f_{x_nx_2}} & \cdots & f_{x_nx_n}
        \end{array}
      \right]
    \]
    \onslide<8->{Clairaut's Theorem implies that $Hf$ is \emph{$n\times n$ and
        real-symmetric}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The Jacobian derivative of $f(x, y)=\sage{f}$ is
    \[
      Df = \sage{jacobian(f, (x, y))}
    \]
    \pause The Hessian of $f$ is
    \[
      Hf = \sage{f.hessian()}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  f = x**2*z+y**3/z
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the scalar field $f\in\mathscr{C}(\mathbb{R}^n)$ given by
    \[
      f(x, y, z) = \sage{f}
    \]
    \pause The Jacobian derivative of $f$ is
    \[
      Df = \sage{jacobian(f, (x, y, z))}
    \]
    \pause The Hessian of $f$ is
    \[
      Hf = \sage{f.hessian()}
    \]
  \end{example}

\end{frame}


\subsection{Taylor Approximations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}<+->
    A \emph{quadratic form} on $\mathbb{R}^n$ is a function
    $q:\mathbb{R}^n\to\mathbb{R}$ given by
    \[
      q(x_1, x_2, \dotsc, x_n) = \sum_{i=1}^n\sum_{j=1}^n c_{ij}\cdot x_ix_j
    \]
    where $c_{ij}\in\mathbb{R}$.
  \end{definition}

  \begin{theorem}<+->
    We have $q(\bv{x})=\bv{x}^\intercal H\bv{x}$ for a unique real-symmetric $H$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{blockarray}{*{4}c}
      \begin{block}{*{4}c}
        & \mybox{x_1}{4-5, 7-8, 12-13} & \mybox{x_2}{9-10, 17-18, 20-21} & \mybox{x_3}{14-15, 22-23, 25-26} \\
      \end{block}
      \begin{block}{c[*{3}r]}
        \mybox{x_1}{4-5, 9-10, 14-15}    & \onslide<5->{-3}  & \onslide<10->{ 3} & \onslide<15->{-5} \\
        \mybox{x_2}{7-8, 17-18, 22-23}   & \onslide<8->{ 3}  & \onslide<18->{ 1} & \onslide<23->{-6} \\
        \mybox{x_3}{12-13, 20-21, 25-26} & \onslide<13->{-5} & \onslide<21->{-6} & \onslide<26-27>{ 7} \\
      \end{block}%
    \end{blockarray}%
  }
  \begin{example}
    Consider the quadratic form on $\mathbb{R}^3$ given by
    \[
      q(x_1, x_2, x_3)
      =
      \mybox{-3\,x_1^2}{3-5}
      \mybox{+6\,x_1x_2}{6-10}
      \mybox{-10\,x_1x_3}{11-15}
      \mybox{+x_2^2}{16-18}
      \mybox{-12\,x_2x_3}{19-23}
      \mybox{+7\,x_3^2}{24-26}
    \]
    \onslide<2->Then $q(\bv{x})=\bv{x}^\intercal H\bv{x}$ where
    \[
      H = \myS
    \]
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{second degree Taylor polynomial} of
    $f\in\mathscr{C}(\mathbb{R}^n)$ at $P$ is
    \[
      T_2(\bv{x})
      = f(P)+Df(P)\Delta\bv{P}+\onslide<2->{\frac{1}{2!}\Delta\bv{P}^\intercal Hf(P)\Delta\bv{P}}
    \]
    \onslide<3->{We use $T_2(\bv{x})$ to approximate $f$ when
      $\norm{\Delta\bv{P}}$ is small.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = cos(x+2*y)+x*y**2
  Df = jacobian(f, (x, y))
  Hf = f.hessian()
  p = (-4, 2)
  x0, y0 = p
  dx, dy = 1, -1
  Dp = matrix.column([dx, dy])
  pnew = tuple(vector(p)+vector(Dp))
  f0 = f(x=x0, y=y0)
  Df0 = Df(x=x0, y=y0)
  Hf0 = Hf(x=x0, y=y0)
  answer = f.taylor((x, x0), (y, y0), 2)(x=pnew[0], y=pnew[1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The Jacobian derivative of $f(x, y)=\sage{f}$ is
    \[
      Df = \sage{Df}
    \]
    \pause The Hessian of $f$ is
    \[
      Hf = \sage{Hf}
    \]
    \pause To approximate $f\sage{pnew}$ using $P\sage{p}$, we have
    \newcommand{\myoverset}[2]{\overset{{\color{beamgreen}####1}}{####2}}
    \begin{gather*}
      f\sage{pnew}
      \approx
      \myoverset{f\sage{p}}{\sage{f0}}
      +\myoverset{Df\sage{p}}{\sage{Df0}}\myoverset{\Delta\bv{P}}{\sage{Dp}}
      +\frac{1}{2!}\myoverset{\Delta\bv{P}^\intercal}{\sage{Dp.T}}\myoverset{Hf\sage{p}}{\sage{Hf0}}\myoverset{\Delta\bv{P}}{\sage{Dp}}
      = \sage{answer}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  f = x**2+y**2+(z-2)**2+sin(x*y*z)
  x0, y0, z0 = 1, -1, 0
  p = (x0, y0, z0)
  Df = jacobian(f, (x, y, z))
  Hf = f.hessian()
  Df0 = Df(x=x0, y=y0, z=z0)
  Hf0 = Hf(x=x0, y=y0, z=z0)
  dx, dy, dz = 1/2, -1/3, 1/4
  Dp = matrix.column([dx, dy, dz])
  pnew = tuple(vector(p)+vector(Dp))
  f0 = f(x=x0, y=y0, z=z0)
  answer = f.taylor((x, x0), (y, y0), (z, z0), 2)(x=pnew[0], y=pnew[1], z=pnew[2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the scalar field $f\in\mathscr{C}(\mathbb{R}^3)$ given by
    \[
      f(x, y, z) = \sage{f}
    \]
    \pause At $P\sage{p}$, the derivative data is
    \begin{align*}
      Df\sage{p} &= \sage{Df0} & Hf\sage{p} &= \sage{Hf0}
    \end{align*}
    \pause To approximate $f\sage{pnew}$ using $P\sage{p}$, we have
    \newcommand{\myoverset}[2]{\overset{{\color{beamgreen}####1}}{####2}}
    \begin{gather*}
      f\sage{pnew}
      \approx
      \myoverset{f(P)}{\sage{f0}}
      +\myoverset{Df(P)}{\sage{Df0}}\myoverset{\Delta\bv{P}}{\sage{Dp}}
      +\frac{1}{2!}\myoverset{\Delta\bv{P}^\intercal}{\sage{Dp.T}}\myoverset{Hf(P)}{\sage{Hf0}}\myoverset{\Delta\bv{P}}{\sage{Dp}}
      = \sage{answer}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Orientation of Error}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{definiteness} of $q(\bv{x})=\bv{x}^\intercal H\bv{x}$ is
    determined where $\bv{x}\neq\bv{O}$.
    \begin{description}[<+->][negative semidefinite]
    \item[positive definite] $q(\bv{x})>0$
    \item[positive semidefinite] $q(\bv{x})\geq0$
    \item[negative definite] $q(\bv{x})<0$
    \item[negative semidefinite] $q(\bv{x})\leq0$
    \item[indefinite] $q(\bv{x})>0$ and $q(\bv{x})<0$
    \end{description}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Row-reducing can quickly determine definiteness.
  \end{block}

\end{frame}


\begin{sagesilent}
  H1 = matrix([(-1, 2), (2, -12)])
  from functools import partial
  elem = partial(elementary_matrix, H1.nrows())
  E = elem(row1=1, row2=0, scale=2)
  H2 = matrix([(2, 0, -1), (0, 2, 1), (-1, 1, 2)])
  elem = partial(elementary_matrix, H2.nrows())
  E1 = elem(row1=2, row2=0, scale=1/2)
  E2 = elem(row1=2, row2=1, scale=-1/2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myHA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{E*H1}$};
      \onslide<2->{
        \node[overlay, above right= -3mm and 2mm of a] (text) {\stackanchor{\emph{negative definite}}{(pivots $<0$)}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }
    }
  }
  \begin{example}
    $\sage{H1}\xrightarrow{R_2+2\cdot R_1\to R_2}\myHA$
  \end{example}

  \newcommand{\myHB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{E2*E1*H2}$};
      \onslide<6->{
        \node[overlay, above right= -5mm and 2mm of a] (text) {\stackanchor{\emph{positive definite}}{(pivots $>0$)}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }
    }
  }
  \begin{example}<3->
    $
    \begin{aligned}
      \sage{H2}
      &\onslide<4->{\xrightarrow{R_3+(\frac{1}{2})\cdot R_1\to R_2}\sage{E1*H2}}    \\
      &\onslide<5->{\xrightarrow{R_3-(\frac{1}{2})\cdot R_2\to R_3}\myHB}
    \end{aligned}
    $
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myUp}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , myFill/.style={fill=blue!40!white, fill opacity=0.60}
        , myDraw/.style={draw=blue!70!black, ultra thick}
        ]

        \coordinate (O) at (0, 0);

        \filldraw[myFill, myDraw]
        (-1, 1)
        -- plot[smooth, domain=-1:1] ({\x}, {\x*\x})
        -- (1, 1)
        -- plot[smooth, domain=0:180] ({cos(\x)}, {1+sin(\x)/6})
        -- (-1, 1)
        -- cycle;

        \filldraw[myFill, myDraw]
        (-1, 1)
        -- plot[smooth, domain=-1:1] ({\x}, {\x*\x})
        -- (1, 1)
        -- plot[smooth, domain=0:-180] ({cos(\x)}, {1+sin(\x)/6})
        -- (-1, 1)
        -- cycle;

        \node[above, beamgreen] at (current bounding box.north) {Concave Up};

        \onslide<2->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {\stackanchor{$Hf(p)$ \emph{positive definite}}{($L_P(\bv{x})$ \emph{underestimate})}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myDown}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , myFill/.style={fill=blue!40!white, fill opacity=0.60}
        , myDraw/.style={draw=blue!70!black, ultra thick}
        ]

        \coordinate (O) at (0, 0);

        \onslide<3->{
          \filldraw[myFill, myDraw]
          (-1, 0)
          -- plot[smooth, domain=-1:1] ({\x}, {1-\x*\x})
          -- (1, 0)
          -- plot[smooth, domain=0:180] ({cos(\x)}, {sin(\x)/6})
          -- (-1, 0)
          -- cycle;

          \filldraw[myFill, myDraw]
          (-1, 0)
          -- plot[smooth, domain=-1:1] ({\x}, {1-\x*\x})
          -- (1, 0)
          -- plot[smooth, domain=0:-180] ({cos(\x)}, {sin(\x)/6})
          -- (-1, 0)
          -- cycle;

          \node[above, beamgreen] at (current bounding box.north) {Concave Down};
        }

        \onslide<4->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {\stackanchor{$Hf(p)$ \emph{negative definite}}{($L_P(\bv{x})$ \emph{overestimate})}};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\mySaddle}{
    \begin{array}{c}
      \begin{tikzpicture}

        \onslide<5->{
          \begin{axis}[domain=-1:1, y domain=-1:1, scale=1/2, hide axis]
            \addplot3[
            , surf
            , samples=20
            , thick
            % , opacity=0.05
            , color=blue!40!white
            , faceted color=blue!70!black
            , fill opacity=0.60
            ]
            {x^2-y^2};
            % \node[label={87.5:{\tiny saddle pt}},circle,fill,inner sep=2pt] at (axis cs:0,0,0) {};
          \end{axis}

          \node[above, beamgreen] at (current bounding box.north) {Saddle};
        }

        \onslide<6->{
          \node[below, font=\scriptsize] at (current bounding box.south)
          {\stackanchor{$Hf(p)$ \emph{indefinite}}{(direction matters)}};
        }
      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    We can use the definiteness of $Hf(P)$ to \emph{define} concavity.
    \begin{gather*}
      \begin{align*}
        \myUp && \myDown && \mySaddle
      \end{align*}
    \end{gather*}
    \onslide<7->{Further analysis is required if $Hf(P)$ is semidefinite.}
  \end{block}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = x*y**2+cos(x+2*y)
  x0, y0 = -4, 2
  p = (x0, y0)
  dx, dy = 1/4, -1/5
  Dp = matrix.column([dx, dy])
  Df0 = jacobian(f, (x, y))(x=x0, y=y0)
  Hf0 = f.hessian()(x=x0, y=y0)
  pnew = tuple(vector(Dp)+vector(p))
  answer = f.taylor((x, x0), (y, y0), 1)(x=pnew[0], y=pnew[1])
  from functools import partial
  elem = partial(elementary_matrix, Hf0.nrows())
  E = elem(row1=1, row2=0, scale=2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myHA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{E*H1}$};
      \onslide<7->{
        \node[overlay, above right= 0mm and -10mm of a] (text) {\stackanchor{\emph{negative definite}}{(pivots $<0$)}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }
    }
  }
  \begin{example}
    Consider again the scalar field $f\in\mathscr{C}(\mathbb{R}^2)$ given by
    \[
      f(x, y) = \sage{f}
    \]
    \onslide<2->{Using $P\sage{p}$ to linearly approximate $f\sage{pnew}$ gives}
    \newcommand{\myoverset}[2]{\overset{{\color{beamgreen}####1}}{####2}}
    \[
      \onslide<3->{
      \textstyle
      f\sage{pnew}
      \approx
      \myoverset{f(P)}{\sage{f0}}
      +\myoverset{Df(P)}{\sage{Df0}}\myoverset{\Delta\bv{P}}{\sage{Dp}}
      = \sage{answer}
      }
    \]
    \onslide<4->{Here, the Hessian is}
    \[
      \onslide<5->{Hf\sage{p} = \sage{Hf0}}\onslide<6->{\xrightarrow{R_2+2\cdot R_1\to R_2}\myHA}
    \]
    \onslide<8->{We expect our approximation to be an \emph{overestimate}.}
  \end{example}

\end{frame}

\end{document}

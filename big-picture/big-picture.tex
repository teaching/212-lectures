\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\tikzset{
  , myfill/.style={fill=blue!40!white, fill opacity=.6}
  , mydraw/.style={draw=blue!70!black}
  , bluefilldraw/.style={fill=beamblue!40!white, fill opacity=.6, draw=beamblue}
  , redfilldraw/.style={draw=red, fill=red!40, fill opacity=0.6}
  , greenfill/.style={fill=beamgreen!40, fill opacity=0.6}
  , greenfilldraw/.style={draw=beamgreen, fill=beamgreen!40, fill opacity=0.6}
}

\title{Big Picture of Vector Calculus}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Summary of Constructions}
\subsection{Shapes in $\mathbb{R}^3$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , dot/.style={circle, fill, inner sep=2pt}
        ]

        \node[dot] at (0, 0) {};
        \node[dot] at (30:1) {};
        \node[dot] at (60:1) {};
        \node[dot] at (120:1) {};
        \node[dot] at (180:1) {};

        \node[below] at (current bounding box.south)
        {{\color{beamgreen}points}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myC}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , declare function = { f(\x) = sin(-\x r)*2/3 ;}
        , scale=1/2
        ]

        \pgfmathsetmacro{\a}{0}
        \pgfmathsetmacro{\b}{3*pi/2}

        \coordinate (O) at (0, 0);

        \coordinate (P) at (\a, {f(\a)});
        \coordinate (Q) at (\b, {f(\b)});

        \draw[beamblue] plot[domain=\a:\b] ({\x}, {f(\x)});

        \node[myDot] at (P) {};
        \node[myDot] at (Q) {};

        \node[below] at (current bounding box.south)
        {{\color{beamgreen}curves}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myS}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        ]

        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\H}{1}
        \pgfmathsetmacro{\r}{\R/8}

        \coordinate (O) at (0, 0);
        \coordinate (right) at (\R, \H);
        \coordinate (left) at (-\R, \H);

        \filldraw[bluefilldraw]
        (O)
        parabola (right)
        arc(0:180:{\R} and {\r})
        parabola[bend at end] (O)
        --cycle;

        \filldraw[bluefilldraw]
        (O)
        parabola (right)
        arc(0:-180:{\R} and {\r})
        parabola[bend at end] (O)
        --cycle;

        \node[below] at (current bounding box.south)
        {{\color{beamgreen}surfaces}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        ]

        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\r}{\R/8}

        \coordinate (O) at (0, 0);
        \coordinate (right) at (\R, 0);
        \coordinate (left) at (-\R, 0);

        \draw[beamblue] (right) arc(0:180:{\R} and {\r});
        \filldraw[bluefilldraw, fill opacity=1] (O) circle (\R);
        \draw[beamblue] (right) arc(0:-180:{\R} and {\r});

        \node[below] at (current bounding box.south)
        {{\color{beamgreen}solids}};

      \end{tikzpicture}
    \end{array}
  }

  \begin{block}{Observation}
    In $\mathbb{R}^3$, we have four types of shapes.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myP} && \onslide<3->{\myC} && \onslide<4->{\myS} && \onslide<5->{\myD}
      \end{align*}
    \end{gather*}
    \onslide<6->{The \emph{boundary} of a shape is a lower-dimensional shape.}
    \[
      \onslide<10->{
        (\operatorname{points})
      }
      \onslide<9->{
        \xleftarrow{\partial}
        (\operatorname{curves})
      }
      \onslide<8->{
        \xleftarrow{\partial}
        (\operatorname{surfaces})
      }
      \onslide<7->{
        \xleftarrow{\partial}
        (\operatorname{solids})
      }
    \]
    \onslide<11->{The boundary of a shape is ``closed'' so this sequence is
      \emph{exact}.}
  \end{block}

\end{frame}


\subsection{Exact Sequences}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myExact}{
    \begin{tikzcd}[row sep=tiny]
      {\color<4>{red}(\operatorname{points})}
      \pgfmatrixnextcell
      {\color<4, 6>{red}(\operatorname{curves})}\only<4, 6>{\arrow[l, red, "\partial"']}\only<1-3, 5->{\arrow[l, "\partial"']}
      \pgfmatrixnextcell
      {\color<6, 8>{red}(\operatorname{surfaces})}\only<6, 8>{\arrow[l, red, "\partial"']}\only<1-5, 7->{\arrow[l, "\partial"']}
      \pgfmatrixnextcell
      {\color<8>{red}(\operatorname{solids})}\only<8>{\arrow[l, red, "\partial"']}\only<1-7, 9->{\arrow[l, "\partial"']} \\
      {\color<4>{red}\mathscr{C}(\mathbb{R}^3)}\only<4>{\arrow[r, red, "\grad"]}\only<1-3, 5->{\arrow[r, "\grad"]}
      \pgfmatrixnextcell
      {\color<4, 6>{red}\mathfrak{X}(\mathbb{R}^3)}\only<6>{\arrow[r, red, "\curl"]}\only<1-5, 7->{\arrow[r, "\curl"]}
      \pgfmatrixnextcell
      {\color<6, 8>{red}\mathfrak{X}(\mathbb{R}^3)}\only<8>{\arrow[r, red, "\vdiv"]}\only<1-7, 9->{\arrow[r, "\vdiv"]}
      \pgfmatrixnextcell
      {\color<8>{red}\mathscr{C}(\mathbb{R}^3)}
    \end{tikzcd}
  }
  \begin{block}{Observation}
    In $\mathbb{R}^3$, we have the \emph{operator exact sequence}.
    \[
      \mathscr{C}(\mathbb{R}^3)
      \xrightarrow{\grad}
      \mathfrak{X}(\mathbb{R}^3)
      \xrightarrow{\curl}
      \mathfrak{X}(\mathbb{R}^3)
      \xrightarrow{\vdiv}
      \mathscr{C}(\mathbb{R}^3)
    \]
    \onslide<2->{Somehow, these two sequences ``match up.''}
    \[
      \onslide<2->{\myExact}
    \]
    \onslide<3->{There are three ``sections'' of these sequences.}
  \end{block}

\end{frame}


\subsection{Vector Calculus Theorems and Exact Sequences}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myExact}{
    \begin{tikzcd}[row sep=tiny]
      {\color<4>{red}(\operatorname{points})}
      \pgfmatrixnextcell
      {\color<4, 6>{red}(\operatorname{curves})}\only<4, 6>{\arrow[l, red, "\partial"']}\only<1-3, 5->{\arrow[l, "\partial"']}
      \pgfmatrixnextcell
      {\color<6, 8>{red}(\operatorname{surfaces})}\only<6, 8>{\arrow[l, red, "\partial"']}\only<1-5, 7->{\arrow[l, "\partial"']}
      \pgfmatrixnextcell
      {\color<8>{red}(\operatorname{solids})}\only<8>{\arrow[l, red, "\partial"']}\only<1-7, 9->{\arrow[l, "\partial"']} \\
      {\color<4>{red}\mathscr{C}(\mathbb{R}^3)}\only<4>{\arrow[r, red, "\grad"]}\only<1-3, 5->{\arrow[r, "\grad"]}
      \pgfmatrixnextcell
      {\color<4, 6>{red}\mathfrak{X}(\mathbb{R}^3)}\only<6>{\arrow[r, red, "\curl"]}\only<1-5, 7->{\arrow[r, "\curl"]}
      \pgfmatrixnextcell
      {\color<6, 8>{red}\mathfrak{X}(\mathbb{R}^3)}\only<8>{\arrow[r, red, "\vdiv"]}\only<1-7, 9->{\arrow[r, "\vdiv"]}
      \pgfmatrixnextcell
      {\color<8>{red}\mathscr{C}(\mathbb{R}^3)}
    \end{tikzcd}
  }
  \begin{block}{Observation}
    Incidentally, we also have \emph{three vector calculus theorems}.
    \begin{gather*}
      \begin{align*}
        \overset{{\color<1->{beamgreen}\textnormal{Fundamental Theorem of Line Integrals}}}{{\color<4>{red}\int_{C}\grad(f)\cdot d\bv{s}=f(Q)-f(P)}} &&
                                                                                                                                                        \overset{{\color<1->{beamgreen}\textnormal{Stokes' Theorem}}}{{\color<6>{red}\iint_{S}\curl(\bv{F})\cdot d\bv{S}=\oint_{\partial S}\bv{F}\cdot d\bv{s}}} &&
                                                                                                                                                                                                                                                                                                                    \overset{{\color<1->{beamgreen}\textnormal{Divergence Theorem}}}{{\color<8>{red}\iiint_{D}\vdiv(\bv{F})\,dV=\oiint_{\partial D}\bv{F}\cdot d\bv{S}}}
      \end{align*}
    \end{gather*}
    \onslide<2->{Each theorem ``pairs'' with a section of our exact sequences!}
    \[
      \onslide<3->{\myExact}
    \]
  \end{block}

\end{frame}




% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \newcommand{\myC}{
%   \begin{tikzpicture}[
%     , line join=round
%     , line cap=round
%     , ultra thick
%     , myDot/.style={circle, fill, inner sep=2pt}
%     , declare function = { f(\x) = sin(-\x r)*2/3 ;}
%     ]

%     \pgfmathsetmacro{\a}{0}
%     \pgfmathsetmacro{\b}{3*pi/2}

%     \coordinate (O) at (0, 0);

%     \coordinate (P) at (\a, {f(\a)});
%     \coordinate (Q) at (\b, {f(\b)});

%     \draw[beamblue, ->] plot[domain=\a:\b/2] ({\x}, {f(\x)});
%     \draw[beamblue] plot[domain=\b/2:\b] ({\x}, {f(\x)});


%     \node[myDot, label=left:{$P$}] at (P) {};
%     \node[myDot, label=right:{$Q$}] at (Q) {};

%   \end{tikzpicture}
% }
%   \begin{definition}
%     The \emph{boundary $\partial C$} of a curve $C$ is the collection of
%     endpoints.
%     \[
%       \myC
%     \]
%     Here, we write $\partial C=Q-P$ to account for orientation.
%   \end{definition}

% \end{frame}

\end{document}

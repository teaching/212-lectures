\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{fitzparaboloid}

\title{The Gradient}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{The Gradient}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}<+->
    The \emph{gradient} of a scalar field $f\in\mathscr{C}(\mathbb{R}^n)$ is
    \[
      \nabla f
      = \left\langle \pdv{f}{x_1}, \pdv{f}{x_2}, \dotsc, \pdv{f}{x_n}\right\rangle
    \]
    \onslide<+->{Note that the gradient is a \emph{vector field}, so
      $\nabla f\in\mathfrak{X}(\mathbb{R}^n)$.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x1 x2')
  f = x2*sin(x1*x2)
  var('x y z')
  g = z*arctan(x+y**2)
  h = x**2-y**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}<+->
    The gradient of $f(x_1, x_2)=\sage{f}$ is
    \[
      \onslide<+->{\nabla f =} \onslide<+->{\sage{f.gradient()}}
    \]
    \onslide<+->{The gradient of $g(x, y, z)=\sage{g}$ is}
    \[
      \onslide<+->{\nabla g =} \onslide<+->{\sage{g.gradient()}}
    \]
    \onslide<+->{The gradient of $h(x, y, z)=\sage{h}$ is}
    \[
      \onslide<+->{\nabla h =} \onslide<+->{\sage{h.gradient([x, y, z])}}
    \]
  \end{example}

\end{frame}


\subsection{Level Sets}

\begin{sagesilent}
  var('x y')
  f = x**2+y**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the level set $x^2+y^2=1$.
    \begin{align*}
      \onslide<2->{f(x, y)} &\onslide<2->{=} \onslide<3->{\sage{f}} & \onslide<4->{\nabla f(x, y)} &\onslide<4->{=} \onslide<5->{\sage{f.gradient([x, y])}}
    \end{align*}
    \onslide<6->{Plotting the points on $f(x, y)=1$ along with $\nabla f$ gives}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=0.55
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\myrta}{sqrt(2)}
        \pgfmathsetmacro{\myrtb}{sqrt(3)}

        \onslide<7->{
          \draw[beamblue] (O) circle (1);
        }

        \coordinate (p) at (1, 0);
        \onslide<9->{
          \draw[beamgreen, ->] (p) -- ++(2, 0)
          node[right, overlay] {$\nabla f(1, 0)=\langle2, 0\rangle$};
        }
        \onslide<8->{\node[myDot] at (p) {};}

        \coordinate (p) at (\myrta/2, \myrta/2);
        \onslide<11->{
          \draw[beamgreen, ->] (p) -- ++(\myrta, \myrta)
          node[right, overlay] {$\nabla f(\frac{\sqrt{2}}{2}, \frac{\sqrt{2}}{2})=\langle\sqrt{2}, \sqrt{2}\rangle$};
        }
        \onslide<10->{\node[myDot] at (p) {};}

        \coordinate (p) at (0, 1);
        \onslide<13->{
          \draw[beamgreen, ->] (p) -- ++(0, 2)
          node[right, overlay] {$\nabla f(0, 1)=\langle0, 2\rangle$};
        }
        \onslide<12->{\node[myDot] at (p) {};}

        \newcommand{\myGrad}[5][]{
          \pgfmathsetmacro{\x}{####2}
          \pgfmathsetmacro{\y}{####3}

          \coordinate (p) at (\x, \y);

          \onslide<####5->{
            \draw[beamgreen, ->] (p) -- ++(2*\x, 2*\y)
            node[midway, above, sloped] {####1};
          }
          \onslide<####4->{\node[myDot] at (p) {};}
        }

        % \myGrad[$\nabla f$]{1}{0}
        % \myGrad{\myrta/2}{\myrta/2}
        % \myGrad{0}{1}
        \myGrad{-\myrta/2}{\myrta/2}{14}{15}
        \myGrad{-1}{0}{16}{17}
        \myGrad{-\myrta/2}{-\myrta/2}{18}{19}
        \myGrad{0}{-1}{20}{21}
        \myGrad{\myrta/2}{-\myrta/2}{22}{23}

      \end{tikzpicture}
    \]
    \onslide<24->{The gradient is orthogonal to the level set!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider a point $P$ on the level set $X$ given by
    \[
      f(x_1, x_2, \dotsc, x_n) = c
    \]
    Then $\nabla f(P)$ is orthogonal to $X$ at $P$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the hyperbola
    \[
      x^2-y^2=1
    \]
    \onslide<2->{Here, $f(x, y)=x^2-y^2$ }\onslide<3->{so
      $\nabla f(x, y)=\onslide<4->{\langle 2\,x, -2\,y\rangle}$\onslide<4->{.}}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<5->{
          \draw[beamblue, <->] plot[domain=-3/2:3/2]
          ({cosh(\x)}, {sinh(\x)});
          \draw[beamblue, <->] plot[domain=-3/2:3/2]
          ({-cosh(\x)}, {sinh(\x)});
        }

        \coordinate (p) at (1, 0);
        \onslide<7->{\draw[beamgreen, ->] (p) -- ++(1/2, 0);}
        \onslide<6->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\x}{cosh(1)}
        \pgfmathsetmacro{\y}{sinh(1)}
        \coordinate (p) at (\x, \y);
        \onslide<9->{\draw[beamgreen, ->] (p) -- ++(\x/2, -\y/2);}
        \onslide<8->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\x}{cosh(-1)}
        \pgfmathsetmacro{\y}{sinh(-1)}
        \coordinate (p) at (\x, \y);
        \onslide<11->{\draw[beamgreen, ->] (p) -- ++(\x/2, -\y/2);}
        \onslide<10->{\node[myDot] at (p) {};}

        \coordinate (p) at (-1, 0);
        \onslide<13->{\draw[beamgreen, ->] (p) -- ++(-1/2, 0);}
        \onslide<12->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\x}{-cosh(1)}
        \pgfmathsetmacro{\y}{sinh(1)}
        \coordinate (p) at (\x, \y);
        \onslide<15->{\draw[beamgreen, ->] (p) -- ++(\x/2, -\y/2);}
        \onslide<14->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\x}{-cosh(-1)}
        \pgfmathsetmacro{\y}{sinh(-1)}
        \coordinate (p) at (\x, \y);
        \onslide<17->{\draw[beamgreen, ->] (p) -- ++(\x/2, -\y/2);}
        \onslide<16->{\node[myDot] at (p) {};}

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the hyperboloid
    \[
      x^2+y^2-z^2=1
    \]
    \onslide<2->{Here, $f(x, y, z)=x^2+y^2-z^2$ }\onslide<3->{so
      $\nabla f(x, y, z)=\onslide<4->{\langle 2\,x, 2\,y,
        -2\,z\rangle}$\onslide<4->{.}}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , myFill/.style={fill=blue!40!white, fill opacity=0.60}
        , myDraw/.style={draw=blue!70!black, ultra thick}
        , scale=1.00
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\ymax}{2}
        \pgfmathsetmacro{\myb}{ln(\ymax+sqrt(1+\ymax*\ymax))}
        \pgfmathsetmacro{\xmax}{cosh(\myb)}
        \pgfmathsetmacro{\myr}{\xmax/8}

        \coordinate (ne) at ( \xmax,  \ymax);
        \coordinate (nw) at (-\xmax,  \ymax);
        \coordinate (sw) at (-\xmax, -\ymax);
        \coordinate (se) at ( \xmax, -\ymax);

        \onslide<5->{
          \filldraw[myFill, myDraw]
          (ne) --
          plot[domain=0:180] ({\xmax*cos(\x)}, {\myr*sin(\x)+\ymax}) --
          (nw) --
          plot[domain=\myb:-\myb] ({-cosh(\x)}, {sinh(\x)}) --
          (sw) --
          plot[domain=180:0] ({\xmax*cos(\x)}, {\myr*sin(\x)-\ymax}) --
          (se) --
          plot[domain=-\myb:\myb] ({cosh(\x)}, {sinh(\x)}) --
          (ne) --
          cycle;

          \filldraw[myFill, myDraw]
          (ne) --
          plot[domain=0:-180] ({\xmax*cos(\x)}, {\myr*sin(\x)+\ymax}) --
          (nw) --
          plot[domain=\myb:-\myb] ({-cosh(\x)}, {sinh(\x)}) --
          (sw) --
          plot[domain=180:360] ({\xmax*cos(\x)}, {\myr*sin(\x)-\ymax}) --
          (se) --
          plot[domain=-\myb:\myb] ({cosh(\x)}, {sinh(\x)}) --
          (ne) --
          cycle;
        }

        \pgfmathsetmacro{\myx}{1}
        \pgfmathsetmacro{\myy}{0}
        \coordinate (p) at (\myx, \myy);
        \onslide<7->{\draw[beamgreen, ->] (p) -- ++(\myx, -\myy);}
        \onslide<6->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\myx}{cosh(\xmax/2)}
        \pgfmathsetmacro{\myy}{sinh(\xmax/2)}
        \coordinate (p) at (\myx, \myy);
        \onslide<9->{\draw[beamgreen, ->] (p) -- ++(\myx/2, -\myy/2);}
        \onslide<8->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\myx}{cosh(-\xmax/2)}
        \pgfmathsetmacro{\myy}{sinh(-\xmax/2)}
        \coordinate (p) at (\myx, \myy);
        \onslide<11->{\draw[beamgreen, ->] (p) -- ++(\myx/2, -\myy/2);}
        \onslide<10->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\myx}{-1}
        \pgfmathsetmacro{\myy}{0}
        \coordinate (p) at (\myx, \myy);
        \onslide<13->{\draw[beamgreen, ->] (p) -- ++(\myx, -\myy);}
        \onslide<12->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\myx}{-cosh(\xmax/2)}
        \pgfmathsetmacro{\myy}{sinh(\xmax/2)}
        \coordinate (p) at (\myx, \myy);
        \onslide<15->{\draw[beamgreen, ->] (p) -- ++(\myx/2, -\myy/2);}
        \onslide<14->{\node[myDot] at (p) {};}

        \pgfmathsetmacro{\myx}{-cosh(-\xmax/2)}
        \pgfmathsetmacro{\myy}{sinh(-\xmax/2)}
        \coordinate (p) at (\myx, \myy);
        \onslide<17->{\draw[beamgreen, ->] (p) -- ++(\myx/2, -\myy/2);}
        \onslide<16->{\node[myDot] at (p) {};}

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}<+->
    Recall that the \emph{graph} of $f\in\mathscr{C}(\mathbb{R}^2)$ is the level
    set
    \[
      z-f(x, y) = 0
    \]
    \onslide<+->{The calculation}
    \[
      \onslide<+->{\nabla(z-f(x, y)) =} \onslide<+->{\langle -f_x, -f_y, 1\rangle}
    \]
    \onslide<+->{shows that $\bv{N}=\langle -f_x, -f_y, 1\rangle$ is orthogonal
      to the graph of $f$.}
  \end{block}

  \begin{definition}<+->
    The vector field
    \[
      \bv{N}=\langle -f_x, -f_y, 1\rangle
    \]
    is the \emph{upward pointing normal vector} to the graph of $f$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The graph of $f(x, y)=x^2+y^2$ is $z-(x^2+y^2)=0$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<2->{
          \paraboloidFillBack{1}{2}{1/6}
        }

        \pgfmathsetmacro{\x}{0}
        \coordinate (p) at (\x, \x*\x);
        \onslide<5->{\draw[beamgreen, ->] (p) -- ++(-2*\x/2, 1/2);}

        \pgfmathsetmacro{\x}{1}
        \coordinate (p) at (\x, \x*\x);
        \onslide<6->{\draw[beamgreen, ->] (p) -- ++(-2*\x/4, 1/4);}
        \coordinate (p) at (-\x, \x*\x);
        \onslide<7->{\draw[beamgreen, ->] (p) -- ++(2*\x/4, 1/4);}

        \pgfmathsetmacro{\x}{sqrt(2)}
        \coordinate (p) at (\x, \x*\x);
        \onslide<8->{\draw[beamgreen, ->] (p) -- ++(-2*\x/4, 1/4);}
        \coordinate (p) at (-\x, \x*\x);
        \onslide<9->{\draw[beamgreen, ->] (p) -- ++(2*\x/4, 1/4);}

        \pgfmathsetmacro{\x}{sqrt(3)}
        \coordinate (p) at (\x, \x*\x);
        \onslide<10->{\draw[beamgreen, ->] (p) -- ++(-2*\x/4, 1/4);}
        \coordinate (p) at (-\x, \x*\x);
        \onslide<11->{\draw[beamgreen, ->] (p) -- ++(2*\x/4, 1/4);}

        \pgfmathsetmacro{\x}{1.95}
        \coordinate (p) at (\x, \x*\x);
        \onslide<12->{\draw[beamgreen, ->] (p) -- ++(-2*\x/4, 1/4);}
        \coordinate (p) at (-\x, \x*\x);
        \onslide<13->{\draw[beamgreen, ->] (p) -- ++(2*\x/4, 1/4);}

        \onslide<2->{
          \paraboloidFillFront{1}{2}{1/6}
        }

      \end{tikzpicture}
    \]
    \onslide<3->{The upward pointing normal vector to the graph of $f$ is}
    \[
      \onslide<3->{\bv{N} = \langle-f_x, -f_y, 1\rangle =} \onslide<4->{\langle -2\,x, -2\,y, 1\rangle}
    \]
  \end{example}

\end{frame}


\subsection{Tangent Planes}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{tangent plane $T_PX$ to a level set $X$ at $P$ is}
    \[
      \nabla f(P)\cdot(\bv{x}-\bv{P})=0
    \]
    where $X$ is given by $f(\bv{x})=c$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y z')
  f = x**2+y**2+z**2
  x0, y0, z0 = 1/3, 2/3, 2/3
  P = (x0, y0, z0)
  gradf = f.gradient([x, y, z])
  gradfP = gradf(x=x0, y=y0, z=z0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the point $P\sage{P}$ on the sphere $S$ given by
    \begin{align*}
      \sage{f} &= 1 & \onslide<2->{f} &\onslide<2->{=} \onslide<3->{\sage{f}} & \onslide<4->{\nabla f} &\onslide<4->{=} \onslide<5->{\sage{gradf}}
    \end{align*}
    \onslide<6->{Note that
      $\nabla f(P)=\onslide<7->{\sage{gradfP}}$\onslide<7->{.}}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , myFill/.style={fill=blue!40!white, fill opacity=0.60}
        , myDraw/.style={draw=blue!70!black, ultra thick}
        , scale=1.5
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\myR}{1}
        \pgfmathsetmacro{\myr}{\myR/4}

        \onslide<8->{
          \draw[myDraw] plot[domain=0:180] ({\myR*cos(\x)}, {\myr*sin(\x)});
          \filldraw[myDraw, myFill] (O) circle (\myR);
          \filldraw[myDraw, myFill] (O) circle (\myR);
          \draw[myDraw] plot[domain=0:-180] ({\myR*cos(\x)}, {\myr*sin(\x)}) node[left, blue!70!black] {$S$};
        }

        \pgfmathsetmacro{\myrt}{sqrt(3)}
        \coordinate(p) at (1/2, \myrt/2);

        \coordinate (v) at (\myrt, -1);
        \coordinate (w) at (1, 0);

        % \draw (0, 0) -- (v) -- ($ (v)+(w) $) -- (w) -- cycle;

        \begin{scope}[shift={($ (p)-0.5*(v)-0.5*(w) $)}]
          \coordinate (v) at (\myrt, -1);
          \coordinate (w) at (1, 0);
          \coordinate (vpw) at ($ (v)+(w) $);

          \onslide<10->{
            \draw[fill=white] (0, 0) -- (w) node[midway, above, sloped, beamgreen] {$\scriptstyle T_PS$};

            \filldraw[fill=beamgreen!75, draw=beamgreen, fill opacity=0.60]
            (0, 0) -- (v) -- ($ (v)+(w) $) -- (w) -- cycle;
          }

          \onslide<12->{
            \node[overlay, red, below right= 2mm and -12mm of vpw] (text)
            {$\scriptstyle\frac{2}{3}\cdot(x-\frac{1}{3})+\frac{4}{3}\cdot(y-\frac{2}{3})+\frac{4}{3}\cdot(z-\frac{2}{3})=0$};
            \draw[overlay, <-, red, thick, shorten <=2pt] (vpw.east) -| (text.north);
          }
        \end{scope}

        \begin{scope}[shift={(p)}]
          \coordinate (O) at (0, 0);
          \coordinate (gradf) at (1/3, \myrt/3);
          \coordinate (gradfperp) at (\myrt/3, -1/3);
          \coordinate (perpa) at ($ (O)!0.25!(gradf) $);
          \coordinate (perpb) at ($ (O)!0.25!(gradfperp) $);

          \onslide<11->{
            \draw (perpa) -- ($ (perpa)+(perpb) $) -- (perpb);
            \draw[->, beamgreen] (0, 0) -- (1/3, \myrt/3) node[right] {$\nabla f(P)$};
          }
        \end{scope}

        \onslide<9->{\node[myDot, label=left:{$P$}] at (p) {};}

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The tangent plane to the graph of $f:\mathbb{R}^2\to\mathbb{R}$ at $P$ is
    \[
      \bv{N}\cdot(\bv{x}-\bv{P})=0
    \]
    where $\bv{N}=\langle -f_x(P), -f_y(P), 1\rangle$.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  var('x y z')
  f = 4-x**2-y**2
  x0, y0 = -1, 1
  z0 = f(x=x0, y=y0)
  P = (x0, y0, z0)
  fx, fy = f.gradient([x, y])
  N = vector([-fx(x=x0, y=y0), -fy(x=x0, y=y0), 1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $P\sage{P}$ on the graph $X$ of $f(x, y)=\sage{f}$.
    \begin{align*}
      \onslide<2->{f_x} &\onslide<2->{=} \onslide<3->{\sage{fx}} & \onslide<4->{f_y} &\onslide<4->{=} \onslide<5->{\sage{fy}}
    \end{align*}
    \onslide<6->{The upward normal to $X$ at $P$ is $\bv{N}=\sage{N}$.}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , myFill/.style={fill=blue!40!white, fill opacity=0.60}
        , myDraw/.style={draw=blue!70!black, ultra thick}
        , scale=2.5
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\myR}{1}
        \pgfmathsetmacro{\myr}{\myR/8}
        \pgfmathsetmacro{\myb}{sqrt(\myR)}

        \coordinate (l) at (-\myR, 0);
        \coordinate (r) at ( \myR, 0);

        \onslide<7->{
          \node[above=2mm of l, blue!70!black] {$X$};

          \filldraw[myFill, myDraw]
          (l) --
          plot[domain=-\myb:\myb] ({\x}, {\myR*\myR-\x*\x}) --
          (r) --
          plot[domain=0:180] ({\myR*cos(\x)}, {\myr*sin(\x)}) --
          (l) --
          cycle;

          \filldraw[myFill, myDraw]
          (l) --
          plot[domain=-\myb:\myb] ({\x}, {\myR*\myR-\x*\x}) --
          (r) --
          plot[domain=0:-180] ({\myR*cos(\x)}, {\myr*sin(\x)}) --
          (l) --
          cycle;
        }

        \pgfmathsetmacro{\x}{1/3}
        \pgfmathsetmacro{\y}{\myR*\myR-\x*\x}
        \coordinate (p) at (\x, \y);

        \coordinate (v) at (1, -2/3);
        \coordinate (w) at (1, 0);

        \begin{scope}[shift={($ (p)-0.5*(v)-0.5*(w) $)}]
          \coordinate (v) at (1, -2/3);
          \coordinate (w) at (1, 0);
          \coordinate (vpw) at ($ (v)+(w) $);

          \onslide<9->{
          \draw[white] (0, 0) -- (w) node[midway, sloped, above, beamgreen] {$T_PX$};

          \filldraw[fill=beamgreen!75, draw=beamgreen, fill opacity=0.60]
          (0, 0) -- (v) -- ($ (v)+(w) $) -- (w) -- (0, 0) -- cycle;
          }

          \onslide<11->{
          \node[overlay, red, below right= 2mm and -12mm of vpw] (text)
          {$\scriptstyle -2\cdot(x+1)+2\cdot(y-1)+(z-2)=0$};
          \draw[overlay, <-, red, thick, shorten <=2pt] (vpw.east) -| (text.north);
          }
        \end{scope}

        \begin{scope}[shift={(p)}]
          \coordinate (O) at (0, 0);
          \coordinate (gradf) at (\x, 1/2);
          \coordinate (gradfperp) at (1/2, -\x);
          \coordinate (perpa) at ($ (O)!0.15!(gradf) $);
          \coordinate (perpb) at ($ (O)!0.15!(gradfperp) $);

          \onslide<10->{
            \draw (perpa) -- ($ (perpa)+(perpb) $) -- (perpb);
            \draw[->, beamgreen] (O) -- (gradf) node[right] {$\bv{N}$};
          }
        \end{scope}


        \onslide<8->{\node[myDot, label=left:{$P$}] at (p) {};}

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\section{Directional Derivatives}
\subsection{Definition}

\begin{sagesilent}
  var('x y')
  f = x**2-2*x*y+y**3
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTemperature}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      ]

      \coordinate (O) at (0, 0);
      \coordinate (p) at (3, 1);

      \pgfmathsetmacro{\myrt}{sqrt(2)}

      \draw[<->] (-1/2,  0) -- (6, 0) node[right] {$x$};
      \draw[<->] ( 0, -1/2) -- (0, 3) node[above] {$y$};

      \onslide<8->{
        \draw[beamblue, ->] (p) -- ++(\myrt/2, \myrt/2) node[above] {$?\,\si{\celsius\per\metre}$};
      }

      \onslide<4->{
        \node[myDot] at (p) {};
      }

      \onslide<5->{
        \node[overlay, red, below left= 0mm and 3mm of p] (text) {$T(3, 1)=\onslide<6->{\sage{f(x=3, y=1)}\,\si{\celsius}}$};
        \draw[overlay, <-, red, thick, shorten <=2pt] (p.south) |- (text.east);
      }

      \onslide<3->{
        \draw (3, 3pt) -- (3, -3pt) node[below] {$3$};
        \draw (3pt, 1) -- (-3pt, 1) node[left] {$1$};
      }

    \end{tikzpicture}
  }
  \begin{example}
    Consider $T\in\mathscr{C}(\mathbb{R}^2)$ given by
    $T(x, y)=\sage{f}\,\si{\celsius}$.
    \[
      \onslide<2->{\myTemperature}
    \]
    \onslide<7->{What is the rate of change of temperature due northeast?}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}<+->
    The \emph{directional derivative} of $f$ with respect to a unit vector
    $\bv{u}$ is
    \[
      D_{\bv{u}}f = \lim_{h\to0}\frac{f(\bv{x}+h\cdot\bv{u})-f(\bv{x})}{h}
    \]
    provided this limit exists.
  \end{definition}

  \begin{theorem}<+->
    The directional derivative $D_{\bv{u}}f$ at a point $P$ is given by
    \[
      D_{\bv{u}}f(P)
      = \nabla f(P)\cdot\bv{u}
    \]
    assuming that $f$ has continuous partial derivatives.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{alertblock}{Warning}<+->
    For non-unit vectors $\bv{v}$, we use
    \[
      \onslide<+->{D_{\bv{v}}f(P) =}
      \onslide<+->{\nabla f(P)\cdot\widehat{\bv{v}} =}
      \onslide<+->{\nabla f(P)\cdot\frac{\bv{v}}{\norm{\bv{v}}}}
    \]
    \onslide<+->{This is then \emph{the rate of change of $f$ at $P$ in the
        direction of $\bv{v}$}.}
  \end{alertblock}

\end{frame}


\subsection{Examples}
\begin{sagesilent}
  var('x y z')
  f = sin(x**3*y+z)
  x0, y0, z0 = -1, 2, 2
  P = (x0, y0, z0)
  v = vector([-2, 1, 3])
  gradf = f.gradient([x, y, z])
  gradfP = gradf(x=x0, y=y0, z=z0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      f(x, y, z) &= \sage{f} & \bv{v} &= \sage{v} & P &= \sage{P}
    \end{align*}
    \onslide<2->{The gradient of $f$ is}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\nabla f} &\onslide<2->{=} \onslide<3->{\sage{gradf}} & \onslide<4->{\nabla f(P)} &\onslide<4->{=} \onslide<5->{\sage{gradfP}}
      \end{align*}
    \end{gather*}
    \onslide<6->{The directional derivative of $f$ at $P$ in the direction of
      $\bv{v}$ is}
    \[
      \onslide<6->{D_{\bv{v}}f(P) =}
      \onslide<7->{\nabla f(P)\cdot\frac{\bv{v}}{\norm{\bv{v}}} =}
      \onslide<8->{\sage{gradfP}\cdot\dfrac{\sage{v}}{\sage{v.norm()}} =}
      \onslide<9->{\dfrac{\sage{gradfP*v}}{\sage{v.norm()}}}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = x**2-2*x*y+y**3
  x0, y0 = 3, 1
  P = (x0, y0)
  gradf = f.gradient([x, y])
  gradfP = gradf(x=x0, y=y0)
  v = vector([1, 1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTemperature}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      ]

      \coordinate (O) at (0, 0);
      \coordinate (p) at (3, 1);

      \pgfmathsetmacro{\myrt}{sqrt(2)}

      \draw[<->] (-1/2,  0) -- (6, 0) node[right] {$x$};
      \draw[<->] ( 0, -1/2) -- (0, 3) node[above] {$y$};

      \onslide<12->{
        \draw[beamblue, ->] (p) -- ++(\myrt/2, \myrt/2) node[right] {$\dfrac{\sage{gradfP*v}}{\sage{v.norm()}}\,\si{\celsius\per\metre}$};
      }

      \onslide<4->{
        \node[myDot] at (p) {};
      }

      \onslide<5->{
        \node[overlay, red, below left= 0mm and 3mm of p] (text) {$T(3, 1)=\onslide<6->{\sage{f(x=3, y=1)}\,\si{\celsius}}$};
        \draw[overlay, <-, red, thick, shorten <=2pt] (p.south) |- (text.east);
      }

      \onslide<3->{
        \draw (3, 3pt) -- (3, -3pt) node[below] {$3$};
        \draw (3pt, 1) -- (-3pt, 1) node[left] {$1$};
      }

    \end{tikzpicture}
  }
  \begin{example}
    Consider $T\in\mathscr{C}(\mathbb{R}^2)$ given by
    $T(x, y)=\sage{f}\,\si{\celsius}$.
    \[
      \onslide<2->{\myTemperature}
    \]
    \onslide<7->{The gradient of $f$ is $\nabla f=\sage{gradf}$ so that}
    \[
      \onslide<8->{D_{\bv{u}}f\sage{P} =}
      \onslide<9->{\nabla f\sage{P}\cdot\dfrac{\sage{v}}{\norm{\sage{v}}} =}
      \onslide<10->{\sage{gradfP}\cdot\dfrac{\sage{v}}{\sage{v.norm()}} =}
      \onslide<11->{\dfrac{\sage{gradfP*v}}{\sage{v.norm()}}}
    \]
  \end{example}

\end{frame}


\subsection{Optimization}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    In what direction from $P$ is $f$ increasing most rapidly?
  \end{block}


  \newcommand{\myFig}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=1.75
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\myrt}{sqrt(2)}

        \coordinate (u) at (1, 0);
        \coordinate (gradf) at (\myrt, \myrt);
        \coordinate (gradfnorm) at (\myrt/2, \myrt/2);

        \onslide<5-9>{
          \draw pic["$\theta$", draw=black,<->,angle eccentricity=1.2,angle radius=1cm] {angle=u--O--gradf};
        }

        \draw[->, beamblue] (O) -- (gradf) node[midway, above, sloped] {$\nabla f(P)$};
        \onslide<5-9>{
          \draw[->, beamgreen] (O) -- (u) node[midway, below, sloped] {$\bv{u}$};
        }
        \onslide<10->{
          \draw[->, beamgreen] (O) -- (gradfnorm) node[midway, below, sloped] {$\bv{u}$};
        }

        \node[myDot, label=left:{$P$}] (O) {};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myUnit}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\norm{\bv{u}}$};
      \onslide<6->{
        \node[overlay, above right= 1mm and 3mm of a] (text) {$\bv{u}$ unit vector};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myMax}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\cos(\theta)$};
      \onslide<8->{
        \node[overlay, below right= 1mm and -4mm of a] (text) {max when $\theta=\onslide<9->{0}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myGrad}{
    \begin{aligned}
      \onslide<3->{D_{\bv{u}} f(P) &= \nabla f(P)\cdot\bv{u}} \\
      &\onslide<3->{=} \onslide<4->{\norm{\nabla f(P)}\cdot\myUnit\cdot\cos(\theta)} \\
      &\onslide<4->{=} \onslide<7->{\norm{\nabla f(P)}\cdot\myMax} \\
      &\onslide<13->{\leq \norm{\nabla f(P)}}
    \end{aligned}
  }
  \begin{block}{Answer}<2->
    We wish to find $\bv{u}$ that maximizes $D_{\bv{u}}f(P)$.
    \begin{align*}
      \onslide<5->{\myFig} && \myGrad
    \end{align*}
    \onslide<11->{The direction that maximizes $D_{\bv{u}} f(P)$ is
      $\bv{u}=\onslide<12->{\dfrac{\nabla f(P)}{\norm{\nabla
            f(P)}}}$\onslide<12->{.}}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The directional derivative $D_{\bv{u}}f(P)$ is maximized when
    \[
      \bv{u} = \frac{\nabla f(P)}{\norm{\nabla f(P)}}
    \]
    \onslide<2->{The maximum value of the directional derivative is thus}
    \[
      \onslide<2->{D_{\bv{u}}f(P) =}
      \onslide<3->{\nabla f(P)\cdot\frac{\nabla f(P)}{\norm{\nabla f(P)}} =}
      \onslide<4->{\frac{\norm{\nabla f(P)}^2}{\norm{\nabla f(P)}} =}
      \onslide<5->{\norm{\nabla f(P)}}
    \]
  \end{theorem}

\end{frame}


\begin{sagesilent}
  var('x y z')
  T = 40-(x-4)**2-(y+2)**2+z
  x0, y0, z0 = 3, -4, 7
  P = (x0, y0, z0)
  TP = T(x=x0, y=y0, z=z0)
  gradT = T.gradient([x, y, z])
  gradTP = gradT(x=x0, y=y0, z=z0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose temperature in $\mathbb{R}^3$ is given by
    \[
      T(x, y, z) = \sage{T}\,\si{\celsius}
    \]
    \onslide<2->{A bird is located at $P\sage{P}$, where the temperature is}
    \[
      \onslide<2->{T\sage{P} =} \onslide<3->{\sage{TP}\,\si{\celsius}}
    \]
    \onslide<4->{The gradient data is}
    \begin{align*}
      \onslide<4->{\nabla T} &\onslide<4->{=} \onslide<5->{\sage{gradT}} & \onslide<6->{\nabla T\sage{P}} &\onslide<6->{=} \onslide<7->{\sage{gradTP}}
    \end{align*}
    \onslide<8->{To warm up as quickly as possible, the bird flies in the direction of}
    \[
      \onslide<9->{\bv{u} =}
      \onslide<10->{\oldfrac{\nabla f\sage{P}}{\norm{\nabla f\sage{P}}} =}
      \onslide<11->{\oldfrac{\sage{gradTP}}{\sage{gradTP.norm()}}}
    \]
  \end{example}

\end{frame}


\end{document}

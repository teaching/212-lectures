\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{Orientations and Cross Products}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Orientations}
\subsection{Orientations in $\mathbb{R}^2$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPos}{
    \begin{tikzpicture}[
      , line cap=round
      , line join=round
      , ultra thick
      , ->
      , rotate=15
      ]

      \coordinate (O) at (0, 0);
      \coordinate (a) at (3, 0);
      \coordinate (b) at (2, 2);

      \draw[beamblue] (O) -- (a) node[midway, sloped, below] {$\bv{v}$};
      \draw[red] (O) -- (b) node[midway, sloped, above] {$\bv{w}$};

      \draw
      pic["$\theta$", ->, draw=black, ultra thick, angle eccentricity=1.25, angle radius=1cm] {angle=a--O--b};

      \usebeamercolor[fg]{title}
      \node[above, fg] at (current bounding box.north) {Positively Oriented};
    \end{tikzpicture}
  }
  \newcommand{\myNeg}{
    \begin{tikzpicture}[
      , line cap=round
      , line join=round
      , ultra thick
      , ->
      , rotate=15
      ]

      \coordinate (O) at (0, 0);
      \coordinate (b) at (3, 0);
      \coordinate (a) at (2, 2);

      \draw[beamblue] (O) -- (a) node[midway, sloped, above] {$\bv{v}$};
      \draw[red] (O) -- (b) node[midway, sloped, below] {$\bv{w}$};

      \draw
      pic["$\theta$", <-, draw=black, ultra thick, angle eccentricity=1.25, angle radius=1cm] {angle=b--O--a};

      \usebeamercolor[fg]{title}
      \node[above, fg] at (current bounding box.north) {Negatively Oriented};
    \end{tikzpicture}
  }
  \newcommand{\myDep}{
    \begin{tikzpicture}[
      , line cap=round
      , line join=round
      , ultra thick
      , ->
      , rotate=15
      ]

      \coordinate (O) at (0, 0);
      \coordinate (a) at (1, 1);
      \coordinate (b) at (-1, -1);

      \draw[beamblue] (O) -- (a) node[midway, sloped, above] {$\bv{v}$};
      \draw[red] (O) -- (b) node[midway, sloped, above] {$\bv{w}$};

      % \draw
      % pic["$\theta$", <-, draw=black, ultra thick, angle eccentricity=1.25, angle radius=1cm] {angle=b--O--a};

      \usebeamercolor[fg]{title}
      \node[above, fg] at (current bounding box.north) {Colinear};
    \end{tikzpicture}
  }

  \begin{definition}
    Every list of two vectors $\Set{\bv{v}, \bv{w}}$ in $\mathbb{R}^2$ is either
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\underset{\onslide<3->{\textnormal{counterclockwise rotation}}}{\myPos}} && \onslide<4->{\underset{\onslide<5->{\textnormal{clockwise rotation}}}{\myNeg}} && \onslide<6->{\underset{\onslide<7->{\textnormal{linearly dependent}}}{\myDep}}
      \end{align*}
    \end{gather*}

  \end{definition}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPos}{
    \begin{tikzpicture}[
      , line cap=round
      , line join=round
      , ultra thick
      , ->
      , scale=2
      ]

      \coordinate (O) at (0, 0);
      \coordinate (e1) at (1, 0);
      \coordinate (e2) at (0, 1);

      \draw[thick, <->] (-1/4, 0) -- (5/4, 0);
      \draw[thick, <->] (0, -1/4) -- (0, 5/4);

      \draw[beamblue] (O) -- (e1) node[midway, below] {$\bv{i}$};
      \draw[red] (O) -- (e2) node[midway, left] {$\bv{j}$};

      \draw
      pic["$\theta$", ->, draw=black, thick, angle eccentricity=1.5, angle radius=0.5cm] {angle=e1--O--e2};
    \end{tikzpicture}
  }
  \newcommand{\myNeg}{
    \begin{tikzpicture}[
      , line cap=round
      , line join=round
      , ultra thick
      , scale=2
      ]

      \coordinate (O) at (0, 0);
      \coordinate (e1) at (1, 0);
      \coordinate (e2) at (0, 1);

      \draw[thick, <->] (-1/4, 0) -- (5/4, 0);
      \draw[thick, <->] (0, -1/4) -- (0, 5/4);

      \draw[beamblue, ->] (O) -- (e1) node[midway, below] {$\bv{i}$};
      \draw[red, ->] (O) -- (e2) node[midway, left] {$\bv{j}$};

      \draw
      pic["$\theta$", <-, draw=black, thick, angle eccentricity=1.5, angle radius=0.5cm] {angle=e1--O--e2};
    \end{tikzpicture}
  }
  \begin{example}
    The order of the list matters when establishing an orientation.
    \begin{align*}
      \underset{\Set{\bv{i}, \bv{j}}\textnormal{ positively oriented}}{\myPos} && \underset{\Set{\bv{j}, \bv{i}}\textnormal{ negatively oriented}}{\myNeg}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\mathcal{P}$ be the parallelogram formed by
    $\bv{v},\bv{w}\in\mathbb{R}^2$.
    \[
      \begin{tikzpicture}[
        , rotate=5
        , transform shape
        ]
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={(1cm, 1.25cm)}
          , line join=round
          , line cap=round
          , ultra thick
          , fill=beamblue!45
          ]

          \filldraw (0, 0) -- (1, 0) -- (1, 1) -- (0, 1) -- cycle;
          \draw[->, beamblue] (0, 0) -- (1, 0) node[midway, sloped, below] {$\bv{v}$};
          \draw[->, red] (0, 0) -- (0, 1) node[midway, sloped, above] {$\bv{w}$};

          \node at (1/2, 1/2) {$\mathcal{P}$};

        \end{scope}
      \end{tikzpicture}
    \]
    The orientation of $\Set{\bv{v}, \bv{w}}$ is determined by the sign of
    \[
      \det[\bv{v}\quad\bv{w}]
    \]
    and the area of $\mathcal{P}$ is the absolute value of this determinant.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  A = matrix.column([(6145, 7718), (3751, 5533)])
  a, b = A.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , rotate=5
        , transform shape
        ]
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={(1cm, 1.25cm)}
          , line join=round
          , line cap=round
          , ultra thick
          , fill=beamblue!45
          , scale=1.5
          ]

          \filldraw (0, 0) -- (1, 0) -- (1, 1) -- (0, 1) -- cycle;

          % \node[
          % , circle
          % , fill=black
          % , inner sep=0pt
          % , minimum size=5pt
          % , label=below:{$P()$}]
          % (P) at (0, 0) {};

          % \node[
          % , circle
          % , fill=black
          % , inner sep=0pt
          % , minimum size=5pt
          % , label=below:{$Q()$}]
          % (Q) at (1, 0) {};

          % \node[
          % , circle
          % , fill=black
          % , inner sep=0pt
          % , minimum size=5pt
          % , label=above:{$R()$}]
          % (R) at (1, 1) {};

          % \node[
          % , circle
          % , fill=black
          % , inner sep=0pt
          % , minimum size=5pt
          % , label=above:{$S()$}]
          % (S) at (0, 1) {};


          \draw[->, beamblue] (0, 0) -- (1, 0)
          node[midway, sloped, below] {$\scriptscriptstyle\bv{v}=\sage{a}$};

          \draw[->, red] (0, 0) -- (0, 1)
          node[midway, sloped, above] {$\scriptscriptstyle\bv{w}=\sage{b}$};

          \node at (1/2, 1/2) {$\mathcal{P}$};

        \end{scope}
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the parallelogram
    \begin{gather*}
      \begin{align*}
        \myP && \det\sage{A}&=\sage{A.det()}
      \end{align*}
    \end{gather*}
    \pause $\Set{\bv{v}, \bv{w}}$ is \pause \emph{positively oriented} \pause
    and $\Area(\mathcal{P})=\pause\sage{abs(A.det())}$.
  \end{example}

\end{frame}




\subsection{Orientations in $\mathbb{R}^3$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myNeg}{
    \begin{array}{c}
      \begin{tikzpicture}
        \node at (0, 0) {\includegraphics[width=.3\textwidth]{lho.pdf}};

        \usebeamercolor[fg]{title}
        \node[above, fg] at (current bounding box.north) {Negatively Oriented};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myPos}{
    \begin{array}{c}
      \begin{tikzpicture}
        \node at (0, 0) {\includegraphics[width=.3\textwidth]{rho.pdf}};

        \usebeamercolor[fg]{title}
        \node[above, fg] at (current bounding box.north) {Positively Oriented};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myDep}{
    \tdplotsetmaincoords{70}{110}
    \begin{array}{c}
      \begin{tikzpicture}[baseline=3ex, line cap=round, line join=round, tdplot_main_coords]
        \filldraw[draw=black, very thick, fill=beamblue!45,] (9/2,1,0) -- (0,-2,0) -- (-3, 0, 0) -- (3/2, 3, 0) -- cycle;

        \draw[ultra thick, beamblue, ->] (0, 0, 0) -- (2, 0, 0) node[below] {$\bv{v}$};
        \draw[ultra thick, red, ->] (0, 0, 0) -- (0, 1, 0) node[below] {$\bv{w}$};
        \draw[ultra thick, beamgreen, ->] (0, 0, 0) -- (2, 1, 0) node[below] {$\bv{x}$};

        \usebeamercolor[fg]{title}
        \node[above, fg] at (current bounding box.north) {Coplanar};
      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    Every list of three vectors $\Set{\bv{v}, \bv{w}, \bv{x}}$ in $\mathbb{R}^3$
    is either
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\underset{\textnormal{left hand order}}{\myNeg}} && \onslide<3->{\underset{\textnormal{right hand order}}{\myPos}} && \onslide<4->{\underset{\textnormal{linearly dependent}}{\myDep}}
      \end{align*}
    \end{gather*}

  \end{definition}

\end{frame}




\begin{sagesilent}
  e1, e2, e3 = map(matrix.column, identity_matrix(3).columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myRR}{
    \tdplotsetmaincoords{70}{110}
    \begin{array}{c}
      \begin{tikzpicture}[tdplot_main_coords, scale=1]
        \draw[thick,<->] (-1,0,0) -- (3,0,0);
        \draw[thick,<->] (0,-1,0) -- (0,2,0);
        \draw[thick,<->] (0,0,-1) -- (0,0,2);

        \draw[ultra thick, ->, beamblue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\bv{i}$};
        \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\bv{j}$};
        \draw[ultra thick, ->, beamgreen] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\bv{k}$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myOrients}{
    \begin{aligned}
      \Set{\bv{i}, \bv{j}, \bv{k}}               & \onslide<2->{\textnormal{\emph{ positively oriented}}} \\
      \onslide<3->{\Set{\bv{k}, \bv{j}, \bv{i}}} & \onslide<4->{\textnormal{\emph{ negatively oriented}}} \\
    \end{aligned}
  }
  \begin{example}
    Consider the standard basis of $\mathbb{R}^3$.
    \begin{gather*}
      \begin{align*}
        \myRR && \myOrients
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\mathcal{P}$ be the parallelepiped formed by
    $\bv{v}, \bv{w}, \bv{x}\in\mathbb{R}^3$.
    \[
      \begin{tikzpicture}
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
          , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
          , line join=round
          , line cap=round
          , fill opacity=0.5
          , ultra thick
          ]

          \draw[fill=Orange] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
          \draw[fill=Cyan] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
          \draw[fill=Red] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=Cyan] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
          \draw[fill=Yellow] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=Thistle] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

          \draw[beamblue, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\bv{v}$};
          \draw[red, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\bv{w}$};
          \draw[beamgreen, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\bv{x}$};
        \end{scope}
      \end{tikzpicture}
    \]
    The orientation of $\Set{\bv{v}, \bv{w}, \bv{x}}$ is determined by the sign
    of
    \[
      \det[\bv{v}\quad\bv{w}\quad\bv{x}]
    \]
    and the volume of $\mathcal{P}$ is the absolute value of this determinant.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  set_random_seed(1480233)
  A = random_matrix(ZZ, 3, x=-10000, y=10000)
  a, b, c = A.columns()
  a = -a
  A = matrix.column([a, b, c])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPa}{
    \begin{tikzpicture}
      \begin{scope}[
        , x={(2cm, 0cm)}
        , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
        , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
        , line join=round
        , line cap=round
        , fill opacity=0.5
        , ultra thick
        ]

        \draw[fill=Orange] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
        \draw[fill=Cyan] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
        \draw[fill=Red] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
        \draw[fill=Cyan] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
        \draw[fill=Yellow] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
        \draw[fill=Thistle] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

        \draw[beamblue, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\bv{v}$};
        \draw[red, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\bv{w}$};
        \draw[beamgreen, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\bv{x}$};
      \end{scope}
    \end{tikzpicture}
  }
  \newcommand{\myPb}{
    \begin{tikzpicture}
      \begin{scope}[
        , x={(2cm, 0cm)}
        , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
        , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
        , line join=round
        , line cap=round
        , fill opacity=0.5
        , ultra thick
        ]

        \draw[fill=Orange] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
        \draw[fill=Cyan] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
        \draw[fill=Red] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
        \draw[fill=Cyan] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
        \draw[fill=Yellow] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
        \draw[fill=Thistle] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

        \draw[red, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\bv{w}$};
        \draw[beamblue, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\bv{v}$};
        \draw[beamgreen, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\bv{x}$};
      \end{scope}
    \end{tikzpicture}
  }
  \begin{example}
    Which of these figures is correct, and what is the volume?
    \noindent
    \begin{minipage}{.666666666666\textwidth}
      \begin{gather*}
        \begin{align*}
          \myPa && \myPb
        \end{align*}
      \end{gather*}
    \end{minipage}% This must go next to Y
    \begin{minipage}{.333333333333\textwidth}
      \begin{gather*}
        \begin{align*}
          \bv{v} &= \sage{a} \\
          \bv{w} &= \sage{b} \\
          \bv{x} &= \sage{c}
        \end{align*}
      \end{gather*}
    \end{minipage}
    \pause The calculation
    \[
      \det\sage{A}=\sage{A.det()}
    \]
    shows that the list $\Set{\bv{v}, \bv{w}, \bv{x}}$ is \pause
    \emph{negatively oriented} (second figure) \pause and the volume is \pause
    $\sage{abs(A.det())}$.
  \end{example}

\end{frame}


\section{Cross Products}
\subsection{Definition}
\begin{sagesilent}
  var('v1 v2 v3 w1 w2 w3')
  v = vector([v1, v2, v3])
  w = vector([w1, w2, w3])
  a = vector([3, 0, 1])
  b = vector([1, 2, 0])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myCross}{
    \left|
      \begin{array}{rrr}
        \bv{i} & \bv{j} & \bv{k} \\
        v_1    & v_2    & v_3    \\
        w_1    & w_2    & w_3    \\
      \end{array}
    \right|
  }
  \newcommand{\myCrossCoord}[2]{
    \left|
      \begin{array}{rrr}
        v_{####1} & v_{####2} \\
        w_{####1} & w_{####2}
      \end{array}
    \right|
  }
  \begin{definition}
    The \emph{cross product} of $\bv{v}=\sage{v}$ and $\bv{w}=\sage{w}$ is
    \begin{gather*}
      \bv{v}\times\bv{w}
      = \myCross
      =
      \left\langle
        \myCrossCoord{2}{3}
        , -\myCrossCoord{1}{3}
        , \myCrossCoord{1}{2}
      \right\rangle
    \end{gather*}
    \pause Note that $\bv{v}\times\bv{w}$ is a vector in $\mathbb{R}^3$, \emph{not} a
    scalar.
  \end{definition}

  \newcommand{\myCrossE}{
    \left|
      \begin{array}{rrr}
        \bv{i}      & \bv{j}      & \bv{k}      \\
        \sage{a[0]} & \sage{a[1]} & \sage{a[2]} \\
        \sage{b[0]} & \sage{b[1]} & \sage{b[2]}
      \end{array}
    \right|
  }
  \newcommand{\myCrossCoordE}[2]{
    \left|
      \begin{array}{rrr}
        \sage{a[####1-1]} & \sage{a[####2-1]} \\
        \sage{b[####1-1]} & \sage{b[####2-1]}
      \end{array}
    \right|
  }
  \pause
  \begin{example}
    The cross product of $\bv{v}=\sage{a}$ and $\bv{w}=\sage{b}$ is
    \begin{gather*}
      \bv{v}\times\bv{w}
      = \myCrossE
      =
      \left\langle
        \myCrossCoordE{2}{3}
        , -\myCrossCoordE{1}{3}
        , \myCrossCoordE{1}{2}
      \right\rangle
      = \sage{a.cross_product(b)}
    \end{gather*}

  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The cross product obeys the following laws.
    \begin{description}[scalar triple product]
    \item[left distributive] $\bv{v}\times(\bv{w}+\bv{x})=\bv{v}\times\bv{w}+\bv{v}+\bv{x}$\pause
    \item[right distributive] $(\bv{v}+\bv{w})\times\bv{x}=\bv{v}\times\bv{x}+\bv{w}\times\bv{x}$\pause
    \item[scalar law] $c\cdot(\bv{v}\times\bv{w})=(c\cdot\bv{v})\times\bv{w}=\bv{v}\times(c\cdot\bv{w})$\pause
    \item[anticommutative] $\bv{v}\times\bv{w}=-\bv{w}\times\bv{v}$\pause
    \item[scalar triple product] $\bv{v}\cdot(\bv{w}\times\bv{x})=\det[\bv{v}\quad\bv{w}\quad\bv{x}]$\pause
    \item[parallel test] $\bv{v}\parallel\bv{w}\Leftrightarrow\bv{v}\times\bv{w}=\bv{O}$
    \end{description}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , rotate=5
        , transform shape
        ]
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={(1cm, 1.25cm)}
          , line join=round
          , line cap=round
          , ultra thick
          , fill=beamblue!45
          , scale=.55
          ]

          \filldraw (0, 0) -- (1, 0) -- (1, 1) -- (0, 1) -- cycle;

          \draw[->, beamblue] (0, 0) -- (1, 0)
          node[midway, sloped, below] {$\bv{v}$};

          \draw[->, red] (0, 0) -- (0, 1)
          node[midway, sloped, above] {$\bv{w}$};

          % \node at (1/2, 1/2) {$\mathcal{P}$};

        \end{scope}
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myPiped}{
    \begin{array}{c}
      \begin{tikzpicture}[
        transform shape
        ]
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
          , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
          , line join=round
          , line cap=round
          , fill opacity=0.5
          , ultra thick
          , scale=.45
          ]

          \draw[fill=Orange] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
          \draw[fill=Cyan] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
          \draw[fill=Red] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=Cyan] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
          \draw[fill=Yellow] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=Thistle] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

          \draw[red, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\bv{w}$};
          \draw[beamblue, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\bv{v}$};
          \draw[beamblue, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\bv{v}$};
        \end{scope}
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}
    $\bv{v}\times\bv{w}$ is orthogonal to $\bv{v}$ and to $\bv{w}$.
  \end{theorem}
  \onslide<2->
  \begin{proof}\renewcommand{\qedsymbol}{}
    The dot product of $\bv{v}$ and $\bv{v}\times\bv{w}$ is
    \begin{gather*}
      \bv{v}\cdot(\bv{v}\times\bv{w})
      = \onslide<3->{\det[\bv{v}\quad\bv{v}\quad\bv{w}]
        =} \onslide<4->{\pm\Volume\pair*{\alt<5->{\myP}{\myPiped}}
        =} \onslide<6->{0\hfill{\color{beamblue}{\Box}}}
    \end{gather*}
  \end{proof}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , rotate=5
        , transform shape
        ]
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={(1cm, 1.25cm)}
          , line join=round
          , line cap=round
          , ultra thick
          , fill=beamblue!45
          , scale=.55
          ]

          \filldraw (0, 0) -- (1, 0) -- (1, 1) -- (0, 1) -- cycle;

          \draw[->, beamblue] (0, 0) -- (1, 0)
          node[midway, sloped, below] {$\bv{v}$};

          \draw[->, red] (0, 0) -- (0, 1)
          node[midway, sloped, above] {$\bv{w}$};

          % \node at (1/2, 1/2) {$\mathcal{P}$};

        \end{scope}
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myPiped}{
    \begin{array}{c}
      \begin{tikzpicture}[
        transform shape
        ]
        \begin{scope}[
          % , x={(2cm, 0cm)}
          % , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
          % , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
          , line join=round
          , line cap=round
          , fill opacity=0.5
          , ultra thick
          , scale=1.0
          ]

          \draw[fill=Orange] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
          \draw[fill=Cyan] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
          \draw[fill=Red] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=Cyan] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
          \draw[fill=Yellow] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=Thistle] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

          \draw[red, ->, fill opacity=1] (1, 0, 1) -- (1, 0, 0) node[midway, below, sloped] {$\bv{w}$};
          \draw[beamgreen, ->, fill opacity=1] (1, 0, 1) -- (1, 1, 1) node[midway, above, sloped] {$\scriptscriptstyle\bv{v}\times\bv{w}$};
          \draw[beamblue, ->, fill opacity=1] (1, 0, 1) -- (0, 0, 1) node[midway, below, sloped] {$\bv{v}$};
        \end{scope}
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}
    $\norm{\bv{v}\times\bv{w}}=\Area\pair*{\myP}$
  \end{theorem}
  \onslide<2->
  \begin{proof}
    Note that
    \begin{align*}\renewcommand{\qedsymbol}{}
      \norm{\bv{v}\times\bv{w}}^2
      &= \onslide<3->{(\bv{v}\times\bv{w})\cdot(\bv{v}\times\bv{w})} \\
      &\onslide<3->{=} \onslide<4->{\det[\bv{v}\times\bv{w}\quad\bv{v}\quad\bv{w}]} \\
      &\onslide<4->{=} \onslide<5->{\Volume\pair*{\myPiped}} \\
      &\onslide<5->{=} \onslide<6->{\norm{\bv{v}\times\bv{w}}\cdot\Area\pair*{\myP}&&\color{beamblue}{\Box}}
    \end{align*}
  \end{proof}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \noindent
  \begin{minipage}{.45\textwidth}
    \begin{theorem}
      $\norm{\bv{v}\times\bv{w}}=\norm{\bv{v}}\cdot\norm{\bv{w}}\cdot\sin(\theta)$
    \end{theorem}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.55\textwidth}
    \[
      \begin{tikzpicture}[
        , line cap=round
        , line join=round
        , ultra thick
        , rotate=15
        , transform shape
        ]

        \coordinate (O) at (0, 0);
        \coordinate (a) at (3, 0);
        \coordinate (b) at (2, 2);

        \filldraw[fill=beamblue!45] (O) -- (a) -- ($ (a)+(b) $) -- (b) -- cycle;

        \onslide<3->{
          \coordinate (p) at ($ (O)!(b)!(a) $);
          \draw[dotted] (b) -- (p) node[pos=0.25, right] {$\scriptstyle h=\onslide<4->{\norm{w}\cdot\sin(\theta)}$};

        \pgfmathsetmacro{\myPerp}{.125}
        \draw ($ (p)!\myPerp!(b) $) -- ($ (p)!\myPerp!(b)+(O)!\myPerp!(p) $) -- ($ (p)+(O)!\myPerp!(p) $);
      }

        \draw
        pic["$\theta$", <->, draw=black, ultra thick, angle eccentricity=1.25, angle radius=1cm] {angle=a--O--b};

        \draw[beamblue, ->] (O) -- (a) node[midway, sloped, below] {$\bv{v}$};
        \draw[red, ->] (O) -- (b) node[midway, sloped, above] {$\bv{w}$};

      \end{tikzpicture}
    \]
  \end{minipage}
  \begin{proof}<2->
    Let $h$ be the height of the parallelogram formed by $\bv{v}$ and $\bv{w}$. \onslide<5->{Then }
    \[
      \onslide<5->{\norm{\bv{v}\times\bv{w}} =}
      \onslide<6->{\Area(\mathcal{P}) =}
      \onslide<7->{\norm{\bv{v}}\cdot h =}
      \onslide<8->{\norm{\bv{v}}\cdot\norm{\bv{w}}\cdot\sin(\theta)\qedhere}
    \]
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \noindent
  \begin{minipage}{.6\textwidth}
    \begin{theorem}
      $\Set{\bv{v}, \bv{w}, \bv{v}\times\bv{w}}$ is positively oriented.
    \end{theorem}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.4\textwidth}
    \[
      \includegraphics[width=.75\textwidth]{cross.pdf}
    \]
  \end{minipage}
  \onslide<2->
  \begin{proof}\renewcommand{\qedsymbol}{}
    Note that
    \begin{align*}
      \det[\bv{v}\quad\bv{w}\quad\bv{v}\times\bv{w}]
      &= \onslide<3->{-\det[\bv{v}\quad\bv{v}\times\bv{w}\quad\bv{w}]} \\
      &\onslide<3->{=} \onslide<4->{\det[\bv{v}\times\bv{w}\quad\bv{v}\quad\bv{w}]} \\
      &\onslide<4->{=} \onslide<5->{(\bv{v}\times\bv{w})\cdot(\bv{v}\times\bv{w})} \\
      &\onslide<5->{=} \onslide<6->{\norm{\bv{v}\times\bv{w}}^2} \\
      &\onslide<6->{>} \onslide<7->{0}&&\onslide<7->{\color{beamblue}{\Box}}
    \end{align*}
  \end{proof}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\bv{v}\times\bv{w}$ is the unique vector making this figure accurate.
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , tdplot_main_coords
        ]

        \coordinate (O) at (0, 0, 0);
        \coordinate (e1) at (0, 4, 0);
        \coordinate (e2) at (-5, 0, 0);
        \coordinate (e3) at (0, 0, 3);

        \pgfmathsetmacro{\myPerp}{.125}
        \coordinate (n1) at ($ (O)!\myPerp!(e1) $);
        \coordinate (n2) at ($ (O)!\myPerp!(e2) $);
        \coordinate (n3) at ($ (O)!\myPerp!(e3) $);
        %\draw (n3) -- ($ (n2)+(n3) $) -- (n2);

        \onslide<5->{
          \draw[fill=beamblue!45]
          (O) -- (e1) -- ($ (e1)+(e2) $) -- (e2) -- cycle;

          \node at ($ .5*(e1)+.5*(e2) $)
          {$\Area(\mathcal{P})=\norm{\bv{v}\times\bv{w}}$};
        }

        \onslide<2->{
          \draw[->, beamgreen] (O) -- (e3) node[above]
          {$\bv{v}\times\bv{w}$};
        }

        \draw[->, beamblue] (O) -- (e1) node[midway, sloped, below] {$\bv{v}$};
        \draw[->, red] (O) -- (e2) node[midway, sloped, above] {$\bv{w}$};

        \onslide<3->{
          \draw (n3) -- ($ (n1)+(n3) $) -- (n1);
        }

        \onslide<4->{
          \draw (n3) -- ($ (n2)+(n3) $) -- (n2);
        }

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}



\subsection{Examples}
\begin{sagesilent}
  v = vector(ZZ, [-2, 2, 3])
  w = vector(ZZ, [-3, -2, 2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

\noindent
\begin{minipage}{.5\textwidth}
  \begin{example}
    Compute $\Area(\mathcal{P})$.
  \end{example}
\end{minipage}% This must go next to Y
\begin{minipage}{.5\textwidth}
      \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , tdplot_main_coords
        ]

        \coordinate (O) at (0, 0, 0);
        \coordinate (e1) at (0, 3, 0);
        \coordinate (e2) at (-4, 0, 0);
        \coordinate (e3) at (0, 0, 3);

        \draw[fill=beamblue!45]
        (O) -- (e1) -- ($ (e1)+(e2) $) -- (e2) -- cycle;

        \node at ($ .5*(e1)+.5*(e2) $)
        {$\mathcal{P}$};

        \draw[->, beamblue] (O) -- (e1) node[midway, sloped, below] {$\scriptstyle\bv{v}=\sage{v}$};
        \draw[->, red] (O) -- (e2) node[midway, sloped, above] {$\scriptstyle\bv{w}=\sage{w}$};

      \end{tikzpicture}
    \]
  \end{minipage}
    \newcommand{\myCrossCoordE}[2]{
    \left|
      \begin{array}{rr}
        \sage{v[####1-1]} & \sage{v[####2-1]} \\
        \sage{w[####1-1]} & \sage{w[####2-1]}
      \end{array}
    \right|
  }
  \pause
  \begin{block}{Solution}
    The cross product of $\bv{v}=\sage{v}$ and $\bv{w}=\sage{w}$ is
    \begin{gather*}
      \bv{v}\times\bv{w}
      =
      \left\langle
        \myCrossCoordE{2}{3}
        , -\myCrossCoordE{1}{3}
        , \myCrossCoordE{1}{2}
      \right\rangle
      = \sage{v.cross_product(w)}
    \end{gather*}
    \pause The area of $\mathcal{P}$ is then
    \[
      \Area(\mathcal{P})
      = \norm{\bv{v}\times\bv{w}}
      = \norm{\sage{v.cross_product(w)}}
      = 5\cdot\norm{\sage{v.cross_product(w)/5}}
      = \sage{v.cross_product(w).norm()}
    \]
  \end{block}

\end{frame}


\section{Torque}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{F}$};
      \onslide<11->{
      \node[overlay, below left= 1mm and 2mm of a] (text) {Newton \SI{}{\newton}};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myr}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{r}$};
      \onslide<12->{
      \node[overlay, above right= 1mm and 4mm of a] (text) {metre \SI{}{\metre}};
      \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myCross}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\norm{\bv{r}\times\bv{F}}$};
      \onslide<13->{
      \node[overlay, above right= 4mm and -10mm of a] (text) {Joule $\si{J}=\si{\newton\cdot\metre}$};
      \draw[overlay, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }
    }
  }

  \begin{definition}
    A force\myF acts on a particle at position\myr from its axis of
    rotation.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , rotate=15
        , myDot/.style={circle, fill, inner sep=2pt}
        ]

        \pgfmathsetmacro{\myr}{2}
        \pgfmathsetmacro{\myFx}{1*\myr}
        \pgfmathsetmacro{\myFy}{.75*\myr}

        \coordinate (O) at (0, 0);
        \coordinate (r) at (\myr, 0);

        \draw[->, beamgreen] ($ -1*(r) $) -- (O) node [midway, below, sloped] {$\bv{r}$};

        \node[myDot, red] at ($ -1*(r) $) {};
        \draw[red] ($ -1*(r) $) circle (2mm) node[below left=1mm and 1mm] {$\onslide<10->{\tau}$};

        \coordinate (F) at (\myFx, \myFy);
        \coordinate (p) at ($ (O)!(F)!(r) $);

        \onslide<5->{
          \draw
          pic["$\theta$", draw=black, ultra thick, angle eccentricity=1.25, angle radius=0.75cm] {angle=p--O--F};
        }

        \draw[->, beamblue] (O) -- (F) node[midway, above, sloped] {$\bv{F}$};
        \onslide<2->{
          \draw[beamblue, dashed, thick, ->] (O) -- (p) node[midway, below, sloped] {$\bv{F}_{\parallel}$};
        }

        \onslide<3->{
          \draw[beamblue, dashed, thick, ->] (p) -- (F) node[midway, right] {$\bv{F}_{\perp}$};
        }

        \begin{scope}[shift=(p)]
          \onslide<4->{
            \draw (0, .25) -- (-.25, .25) -- (-.25, 0);
          }
        \end{scope}

        \node[myDot] at (O) {};

      \end{tikzpicture}
    \]
    \onslide<6->{The \emph{torque of $\bv{F}$} on the particle is}
    \[
      \onslide<6->{\tau =}
      \onslide<7->{\norm{\bv{r}}\cdot\norm{\bv{F}_{\perp}} =}
      \onslide<8->{\norm{\bv{r}}\cdot\norm{\bv{F}}\cdot\sin(\theta) =}
      \onslide<9->{\myCross}
    \]
    \onslide<14->{The \emph{torque vector} is $\bv{\tau}=\bv{r}\times\bv{F}$, so
      that $\norm{\bv{\tau}}=\tau$.}
  \end{definition}

\end{frame}



\end{document}

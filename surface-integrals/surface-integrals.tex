\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\tikzset{
  , myfill/.style={fill=blue!40!white, fill opacity=.6}
  , mydraw/.style={draw=blue!70!black}
  , bluefilldraw/.style={fill=beamblue!40!white, fill opacity=.6, draw=beamblue}
  , redfilldraw/.style={draw=red, fill=red!40, fill opacity=0.6}
  , greenfill/.style={fill=beamgreen!40, fill opacity=0.6}
  , greenfilldraw/.style={draw=beamgreen, fill=beamgreen!40, fill opacity=0.6}
}

\title{Surface Integrals}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Scalar Surface Integrals}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    A sheet of material conforms to the shape of a surface $S$ in
    $\mathbb{R}^3$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=.60
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \filldraw[fill=beamblue!45, draw=black] (0, 0)
        to[bend left]  (3, -1)
        to[bend left]  (5,  2)
        to[bend right] (2,  3)
        to[bend right] (0,  0)
        --cycle;

        \coordinate (P1) at (1, 1);
        \coordinate (P2) at (5/2, 1/2);
        \coordinate (P3) at (2, 2);
        \coordinate (P4) at (7/2, 2);

        \onslide<3->
        \node[myDot, label=left:{$\scriptstyle P_1$}] at (P1) {};
        \node[overlay, red, below left= 0.5mm and 16mm of P1]
        (text) {$\scriptstyle f(P_1)=7\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P1.south) |- (text.east);

        \onslide<4->
        \node[myDot, label=right:{$\scriptstyle P_2$}] at (P2) {};
        \node[overlay, red, below right= 1mm and 16mm of P2]
        (text) {$\scriptstyle f(P_2)=17\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P2.south) |- (text.west);

        \onslide<5->
        \node[myDot, label=left:{$\scriptstyle P_3$}] at (P3) {};
        \node[overlay, red, above left= 1mm and 16mm of P3]
        (text) {$\scriptstyle f(P_3)=3\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P3.south) |- (text.east);

        \onslide<6->
        \node[myDot, label=right:{$\scriptstyle P_4$}] at (P4) {};
        \node[overlay, red, above right= 1mm and 16mm of P4]
        (text) {$\scriptstyle f(P_4)=21\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P4.north) |- (text.west);

      \end{tikzpicture}
    \]
    \onslide<2->{Suppose $f\in\mathscr{C}(\mathbb{R}^3)$ measures density
      (\SI{}{\kg\per\metre\squared}) at every point of $S$.}
  \end{example}

  \begin{definition}<7->
    The \emph{scalar surface integral of $f$ over $S$} is
    \[
      \iint_{S}f\,dS=\textnormal{mass of $S$ (in \SI{}{\kg})}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{scalar surface integral of $f$ along
      $\bv{X}:D\subset\mathbb{R}^2\to\mathbb{R}^3$} is
    \[
      \iint_{\bv{X}}f\,{\color<4->{red}dS} = \iint_{D} f(\bv{X}(s, t))\cdot{\color<6->{beamgreen}\norm{\bv{N}}\,dA}
    \]
    \onslide<2->{Here, we adopt the notation
      ${\color<3->{red}dS}={\color<5->{beamgreen}\norm{\bv{N}}\,dA}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}f(\bv{X}(s, t))}$};
      \onslide<2->{
        \node[overlay, below left = 6mm and -4mm of a] (text)
        {$\dfrac{\textnormal{mass units}}{S\textnormal{-area unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myNorm}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamblue}\norm{\bv{T}_s\times\bv{T}_t}}$};
      \onslide<3->{
        \node[overlay, below = 6mm of a] (text)
        {$\dfrac{S\textnormal{-area unit}}{D\textnormal{-area unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \newcommand{\mydA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamgreen}dA}$};
      \onslide<4->{
        \node[overlay, below right = 3mm and 1mm of a] (text)
        {$D\textnormal{-area unit}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units allows us to interpret scalar surface integrals.
    \[
      \iint_{\bv{X}}f\,dS
      = \iint_D \myf\cdot\myNorm\,\mydA
      = \onslide<5->{\textnormal{mass of the surface}}
    \]
  \end{block}

\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y z')
  X = vector([x, y, 12-4*x-2*y])
  Tx = X.diff(x)
  Ty = X.diff(y)
  N = Tx.cross_product(Ty)
  f = x*z
  d = SR(f(z=X[2]))
  mass = integral(integral(d, y, 0, 6-2*x), x, 0, 3)*N.norm()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \coordinate (x) at ({cos(210)}, {sin(210)});
        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at ({cos(90)}, {sin(90)});

        \pgfmathsetmacro{\coordstretch}{1.50}
        \draw[->] (O) -- ($ \coordstretch*(x) $) node[pos=1.15, sloped] {$x$};
        \draw[->] (O) -- ($ \coordstretch*(y) $) node[pos=1.15, sloped] {$y$};
        \draw[->] (O) -- ($ \coordstretch*(z) $) node[above] {$z$};

        \draw (O) -- (x) node[pos=1.0, sloped, above] {$3$};
        \draw (O) -- (y) node[pos=1.0, sloped, above] {$6$};
        \node[right] at (z) {$12$};

        \onslide<2->{\filldraw[greenfilldraw] (O) -- (x) -- (y) -- cycle;}

        \onslide<1->{\filldraw[bluefilldraw] (x) -- (y) -- (z) -- cycle;}

        \onslide<2->{\draw[beamgreen] (x) -- (y);}

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);
        \coordinate (x) at (1, 0);
        \coordinate (y) at (0, 1);

        \pgfmathsetmacro{\coordstretch}{1.50}
        \draw[<->] ($ -0.5*(x) $) -- ($ \coordstretch*(x) $) node[right] {$x$};
        \draw[<->] ($ -0.5*(y) $) -- ($ \coordstretch*(y) $) node[above] {$y$};

        \filldraw[greenfilldraw] (O) -- (x) -- (y);

        \node at ($ 0.25*(x)+0.25*(y) $) {$D$};

        \draw ($ (x)+(0, 3pt)$) -- ++(0, -6pt) node[below] {$3$};
        \draw ($ (y)+(3pt, 0)$) -- ++(-6pt, 0) node[left] {$6$};

        \node[above right, beamgreen] at ($ 0.5*(x)+0.5*(y) $)
        {$\scriptstyle y=6-2\,x$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<6->{beamblue}12-4\,x-2\,y}$};
      \onslide<6->{
        \node[overlay, above left= 2mm and 1mm of a] (text) {$z=12-4\,x-2\,y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<13->{red}\sage{N}}$};
      \onslide<13->{
        \node[overlay, below right= 0mm and 0mm of a] (text) {$\norm{\bv{N}}=\sage{N.norm()}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myX}{
    \begin{aligned}
      \onslide<4->{\bv{X}(x, y)} &\onslide<4->{=} \onslide<5->{\langle x, y, \myz\rangle}  \\
      \onslide<7->{\bv{T}_x}     &\onslide<7->{=} \onslide<8->{\sage{Tx}} \\
      \onslide<9->{\bv{T}_y}     &\onslide<9->{=} \onslide<10->{\sage{Ty}} \\
      \onslide<11->{{\color<13->{red}\bv{N}}}       &\onslide<11->{{\color<13->{red}\,=\,}} \onslide<12->{\myN}
    \end{aligned}
  }
  \newcommand{\myfz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<18->{beamblue}(12-4\,x-2\,y)}$};
      \onslide<18->{
        \node[overlay, below left= 6mm  and -11mm of a] (text) {$z$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myNnorm}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<19->{red}\sage{N.norm()}}$};
      \onslide<19->{
        \node[overlay, below left= 6mm and -2mm of a] (text) {$\norm{\bv{N}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{example}
    Let $S$ be the ``first octant'' part of $4\,x+2\,y+z=12$.
    \begin{gather*}
      \begin{align*}
        \myS && \onslide<3->{\myD} && \myX
      \end{align*}
    \end{gather*}
    \onslide<14->{Suppose density throught $S$ is
      $f(x, y, z)=\sage{f}\,\si{\kg\per\metre\squared}$.}
    \begin{gather*}
      \onslide<15->{\mass(S)
      =} \onslide<16->{\iint_{\bv{X}}f\,dS
      =} \onslide<17->{\int_0^3\int_{0}^{6-2\,x} x\cdot\myfz\cdot\myNnorm\,dy\,dx
      =} \onslide<20->{\sage{mass}\,\si{\kilogram}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('r theta')
  X = vector([r*cos(theta), r*sin(theta), r])
  Tr = X.diff(r)
  Ttheta = X.diff(theta)
  N = Tr.cross_product(Ttheta).simplify_trig()
  Nnorm = N.norm().simplify_trig().canonicalize_radical()
  f = 5*x**2*z
  fs = f(x=X[0], y=X[1], z=X[2])
  mass = integral(integral(fs*Nnorm, r, 0, 1), theta, 0, 2*pi)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myCone}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\H}{1}
        \pgfmathsetmacro{\pitch}{\R/8}

        \filldraw[bluefilldraw]
        (O) -- (\R, \H) arc(0:180:{\R} and {\pitch}) -- (O) --cycle;

        \draw[<->] (0, -2*\pitch) -- (0, \H+4*\pitch) node[above] {$z$};

        \filldraw[bluefilldraw]
        (O) -- (\R, \H) arc(0:-180:{\R} and {\pitch}) -- (O) --cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{red}\sage{X[0]}}$};
      \onslide<5->{
        \node[overlay, above left= 1mm and -1mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<6->{beamgreen}\sage{X[1]}}$};
      \onslide<6->{
        \node[overlay, above left= 1mm and -1mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{beamblue}\sage{X[2]}}$};
      \onslide<7->{
        \node[overlay, above left= 2mm and 1mm of a] (text) {$z$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<14->{red}\sage{N}}$};
      \onslide<14->{
        \node[overlay, below right= 0mm and -8mm of a] (text) {$\norm{\bv{N}}=\sage{Nnorm}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myParam}{
    \begin{aligned}
      \onslide<3->{\bv{X}(r, \theta)} &\onslide<3->{=} \onslide<4->{\langle \myx, \myy, \myz\rangle} \\
      \onslide<8->{\bv{T}_r}          &\onslide<8->{=} \onslide<9->{\sage{Tr}} \\
      \onslide<10->{\bv{T}_{\theta}}   &\onslide<10->{=} \onslide<11->{\sage{Ttheta}} \\
      \onslide<12->{{\color<14->{red}\bv{N}}}            &\onslide<12->{{\color<14->{red}\,=\,}} \onslide<13->{\myN}
    \end{aligned}
  }
  \newcommand{\myfs}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<18->{red}5\,r^2\cos^2(\theta)r}$};
      \onslide<18->{
        \node[overlay, below left= 3mm and -8mm of a] (text) {$f(r\cos(\theta), r\sin(\theta), r)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myNnorm}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<19->{beamgreen}\sage{Nnorm}}$};
      \onslide<19->{
        \node[overlay, below right= 2mm and -1mm of a] (text) {$\norm{\bv{N}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    Let $S$ be the cone of height one and radius one.
    \begin{align*}
      \onslide<2->{\myCone} && \myParam
    \end{align*}
    \onslide<15->{Suppose temperature throughout $S$ is
      $f(x, y, z)=\sage{f}\,\si{\celsius\per\metre\squared}$.}
    \[
      \onslide<16->{\iint_{\bv{X}}f\,dS
      =} \onslide<17->{\int_{0}^{2\,\pi}\int_0^1 \myfs\cdot\myNnorm\,dr\,d\theta
      =} \onslide<20->{\sage{mass}\,\si{\celsius}}
    \]
  \end{example}

\end{frame}


\section{Vector Surface Integrals}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A fluid flow $\bv{F}$ (\SI{}{\metre\per\second}) flows through a surface
    $S$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=.60
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \filldraw[fill=beamblue!45, draw=black] (0, 0)
        to[bend left]  (3, -1)
        to[bend left]  (5,  2)
        to[bend right] (2,  3)
        to[bend right] (0,  0)
        --cycle;

        \begin{scope}[shift={(5/2, 2)}]
          \coordinate (O) at (0, 0);
          \coordinate (N) at (0, 3);
          \coordinate (F) at (1, 2);
          \coordinate (p) at ($ (O)!(F)!(N) $);

          \pgfmathsetmacro{\myPerp}{0.25}
          \onslide<4->{
            \draw (0, \myPerp) -- (-\myPerp, \myPerp) -- (-\myPerp, 0);
          }

          \onslide<5->{
            \draw[thick, dashed, red] (F) -- (p);
          }

          \onslide<4->{
            \draw[->, beamblue] (O) -- (N) node[left] {$\bv{N}$};
          }
          \draw[->, red] (O) -- (1, 2) node[right] {$\bv{F}$};

          \onslide<6->{
            \draw[red] (O) -- (p);
          }

          \node[myDot, label=below:{$P$}] at (O) {};
        \end{scope}

      \end{tikzpicture}
    \]
    \onslide<2->{\emph{Flux} (in \SI{}{\metre\cubed\per\second}) is the rate at
      which fluid volume flows through $S$.}
  \end{definition}

  \begin{definition}<3->%
    \emph{Flux density at $P$ through $\bv{N}$} is $\comp_{\bv{N}}(\bv{F})$ (in
    \SI{}{\metre\per\second}).
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider a surface $S$ in $\mathbb{R}^3$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=.60
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \filldraw[fill=beamblue!45, draw=black] (0, 0)
        to[bend left]  (3, -1)
        to[bend left]  (5,  2)
        to[bend right] (2,  3)
        to[bend right] (0,  0)
        --cycle;

        \coordinate (P1) at (1, 1);
        \coordinate (P2) at (5/2, 1/2);
        \coordinate (P3) at (2, 2);
        \coordinate (P4) at (7/2, 2);

        \onslide<3->
        \draw[red, ->] (P1) -- ($ (P1)+(-1, 2) $);
        \onslide<4->
        \draw[red, ->] (P2) -- ($ (P2)+( 1, 1) $);
        \onslide<5->
        \draw[red, ->] (P3) -- ($ (P3)+(-2, 2) $);
        \onslide<6->
        \draw[red, ->] (P4) -- ($ (P4)+(-1, 2) $);

      \end{tikzpicture}
    \]
    \onslide<2->{Suppose $\bv{F}\in\mathscr{\mathfrak{X}}(\mathbb{R}^3)$ models
      fluid flow (in \SI{}{\metre\per\second}) at every point of $S$.}
  \end{example}

  \begin{definition}<7->%
    The \emph{vector surface integral of $\bv{F}$ over a parameterized surface
      $\bv{X}$} is
    \[
      \iint_{\bv{X}}\bv{F}\cdot\,d\bv{S}=\textnormal{flux of
        $\bv{F}$ through $S$ (in \SI{}{\metre\cubed\per\second})}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{vector surface integral of $\bv{F}$ along
      $\bv{X}:D\subset\mathbb{R}^2\to\mathbb{R}^3$} is
    \[
      \iint_{\bv{X}}\bv{F}\cdot {\color<4->{red}d\bv{S}}
      = \iint_D \bv{F}(\bv{X}(s, t))\cdot{\color<6->{beamgreen}\bv{N}\,dA}
    \]
    \onslide<2->{Here, we adopt the notation
      ${\color<3->{red}d\bv{S}}={\color<5->{beamgreen}\bv{N}dA}$.}
  \end{definition}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\bv{F}(\bv{X}(s, t))}$};
      \onslide<2->{
        \node[overlay, below left = 6mm and -4mm of a] (text)
        {flow velocity};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamblue}\bv{N}}$};
      \onslide<3->{
        \node[overlay, below = 6mm of a] (text)
        {$\dfrac{S\textnormal{-area unit}}{D\textnormal{-area unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) -- (text.north);
        \node[below=12mm of a, opacity=0.0] (foo) {};
      }
    }
  }
  \newcommand{\mydA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamgreen}dA}$};
      \onslide<4->{
        \node[overlay, below right = 3mm and 1mm of a] (text)
        {$D\textnormal{-area unit}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\mycomp}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{red}\bv{F}(\bv{X}(s, t))\cdot\dfrac{\bv{N}}{\norm{\bv{N}}}}$};
      \onslide<7->{
        \node[overlay, below left= 1mm and -10mm of a] (text) {$\comp_{\bv{N}}(\bv{F})$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mydS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<8->{beamblue}\norm{\bv{N}}\,dA}$};
      \onslide<8->{
        \node[overlay, below right= 1mm and -5mm of a] (text) {$dS$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units allows us to interpret vector surface integrals.
    \[
      \iint_{\bv{X}}\bv{F}\cdot d\bv{S}
      = \iint_D \myF\cdot\myN\,\mydA
      = \onslide<5->{\textnormal{flux through the surface}}
    \]
    \onslide<6->{Rewriting the integral also gives our interpretation.}
    \[
      \onslide<6->{\iint_{\bv{X}}\bv{F}\cdot d\bv{S} = \iint_D \mycomp\mydS}
    \]
  \end{block}

\end{frame}

\subsection{Examples}


\begin{sagesilent}
  var('x y z')
  X = vector([x, y, 12-4*x-2*y])
  Tx = X.diff(x)
  Ty = X.diff(y)
  N = Tx.cross_product(Ty)
  f = x*z
  d = SR(f(z=X[2]))
  F = vector([z, 1, -2])
  Fs = F(x=X[0], y=X[1], z=X[2])
  flux = integral(integral(Fs*N, y, 0, 6-2*x), x, 0, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \coordinate (x) at ({cos(210)}, {sin(210)});
        \coordinate (y) at ({cos(-15)}, {sin(-15)});
        \coordinate (z) at ({cos(90)}, {sin(90)});

        \pgfmathsetmacro{\coordstretch}{1.50}
        \draw[->] (O) -- ($ \coordstretch*(x) $) node[pos=1.15, sloped] {$x$};
        \draw[->] (O) -- ($ \coordstretch*(y) $) node[pos=1.15, sloped] {$y$};
        \draw[->] (O) -- ($ \coordstretch*(z) $) node[above] {$z$};

        \draw (O) -- (x) node[pos=1.0, sloped, above] {$3$};
        \draw (O) -- (y) node[pos=1.0, sloped, above] {$6$};
        \node[right] at (z) {$12$};

        \onslide<3->{\filldraw[greenfilldraw] (O) -- (x) -- (y) -- cycle;}

        \onslide<1->{\filldraw[bluefilldraw] (x) -- (y) -- (z) -- cycle;}

        \onslide<3->{\draw[beamgreen] (x) -- (y);}

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);
        \coordinate (x) at (1, 0);
        \coordinate (y) at (0, 1);

        \pgfmathsetmacro{\coordstretch}{1.50}
        \draw[<->] ($ -0.5*(x) $) -- ($ \coordstretch*(x) $) node[right] {$x$};
        \draw[<->] ($ -0.5*(y) $) -- ($ \coordstretch*(y) $) node[above] {$y$};

        \filldraw[greenfilldraw] (O) -- (x) -- (y);

        \node at ($ 0.25*(x)+0.25*(y) $) {$D$};

        \draw ($ (x)+(0, 3pt)$) -- ++(0, -6pt) node[below] {$3$};
        \draw ($ (y)+(3pt, 0)$) -- ++(-6pt, 0) node[left] {$6$};

        \node[above right, beamgreen] at ($ 0.5*(x)+0.5*(y) $)
        {$\scriptstyle y=6-2\,x$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{beamblue}12-4\,x-2\,y}$};
      \onslide<7->{
        \node[overlay, above left= 2mm and 1mm of a] (text) {$z=12-4\,x-2\,y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myX}{
    \begin{aligned}
      \onslide<5->{\bv{X}(x, y)} &\onslide<5->{=} \onslide<6->{\langle x, y, \myz\rangle}  \\
      \onslide<8->{\bv{T}_x}     &\onslide<8->{=} \onslide<9->{\sage{Tx}} \\
      \onslide<10->{\bv{T}_y}     &\onslide<10->{=} \onslide<11->{\sage{Ty}} \\
      \onslide<12->{\bv{N}}       &\onslide<12->{=} \onslide<13->{\sage{N}}
    \end{aligned}
  }
  \newcommand{\myfz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<1->{beamblue}(12-4\,x-2\,y)}$};
      \onslide<1->{
        \node[overlay, below left= 6mm  and -11mm of a] (text) {$z$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<16->{red}\langle 12-4\,x-2\,y, 1, -2\rangle}$};
      \onslide<16->{
        \node[overlay, above right= 1mm and -15mm of a] (text) {$\bv{F}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<17->{beamblue}\sage{N}}$};
      \onslide<17->{
        \node[overlay, above right= 1mm and -2mm of a] (text) {$\bv{N}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Let $S$ be the ``first octant'' part of $4\,x+2\,y+z=12$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myS} && \onslide<4->{\myD} && \myX
      \end{align*}
    \end{gather*}
    \onslide<14->{The flux of $\bv{F}=\sage{F}\,\si{\metre\per\second}$ is}
    \begin{gather*}
      \begin{align*}
        \onslide<14->{\iint_{\bv{X}}\bv{F}\cdot d\bv{S}}
        &\onslide<14->{=} \onslide<15->{\int_0^3\int_{0}^{6-2\,x} \myF\cdot\myN\,dy\,dx} \\
        &\onslide<15->{=} \onslide<18->{\int_0^3\int_{0}^{6-2\,x}
          {\color<1->{red}(12-4\,x-2\,y)}\cdot{\color<1->{beamblue}4}
          +
          {\color<1->{red}1}\cdot{\color<1->{beamblue}2}
          +
          {\color<1->{red}(-2)}\cdot{\color<1->{beamblue}1}
          \,dy\,dx} \\
        &\onslide<18->{=} \onslide<19->{\sage{flux}\,\si{\metre\cubed\per\second}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([1, 2, z])
  R.<varphi, theta> = QQ[]
  X = vector([sin(varphi)*cos(theta), sin(varphi)*sin(theta), cos(varphi)])
  Tvarphi = X.diff(varphi)
  Ttheta = X.diff(theta)
  N = Tvarphi.cross_product(Ttheta).simplify_trig()
  Fs = F(x=X[0], y=X[1], z=X[2])
  flux = integral(integral(Fs*N, varphi, 0, pi), theta, 0, 2*pi)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \draw[beamblue] (1, 0) arc(0:180:{1} and {1/8});
        \filldraw[bluefilldraw] (O) circle (1);
        \draw[beamblue] (1, 0) arc(0:-180:{1} and {1/8});

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{red}\sage{X[0]}}$};
      \onslide<5->{
        \node[overlay, above left= 1mm and -1mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<6->{beamgreen}\sage{X[1]}}$};
      \onslide<6->{
        \node[overlay, above left= 1mm and -1mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{beamblue}\sage{X[2]}}$};
      \onslide<7->{
        \node[overlay, above left= 1mm and 1mm of a] (text) {$z$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myN}{\langle \cos(\theta)\sin^2(\varphi), \sin(\theta)\sin^2(\varphi), \cos(\varphi)\sin(\varphi)\rangle}
  \newcommand{\myParam}{
    \begin{aligned}
      \onslide<3->{\bv{X}(\varphi, \theta)} &\onslide<3->{=} \onslide<4->{\langle\myx, \myy, \myz\rangle} \\
      \onslide<8->{\bv{T}_{\varphi}} &\onslide<8->{=} \onslide<9->{\sage{Tvarphi}} \\
      \onslide<10->{\bv{T}_{\theta}} &\onslide<10->{=} \onslide<11->{\sage{Ttheta}} \\
      \onslide<12->{\bv{N}} &\onslide<12->{=} \onslide<13->{\myN}
    \end{aligned}
  }
  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<16->{red}\sage{Fs}}$};
      \onslide<16->{
        \node[overlay, above right= 1mm and -5mm of a] (text) {$\bv{F}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myNlabel}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<17->{beamblue}\myN}$};
      \onslide<17->{
        \node[overlay, above right= 4mm and -32mm of a] (text) {$\bv{N}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Let $S$ be the unit sphere and consider
    $\bv{F}=\sage{F}\,\si{\metre\per\second}$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\myS} && \myParam
      \end{align*}
    \end{gather*}
    \onslide<14->{The flux of $\bv{F}$ through $S$ is}
    \begin{gather*}
      \begin{align*}
        \onslide<14->{\iint_{\bv{X}}\bv{F}\cdot d\bv{S}}
        &\onslide<14->{=} \onslide<15->{\int_{0}^{2\,\pi}\int_0^{\pi}\myF\cdot\myNlabel\,d\varphi\,d\theta} \\
        &\onslide<15->{=} \onslide<18->{\int_{0}^{2\,\pi}\int_0^{\pi}
          {\color<1->{red}1}\cdot{\color<1->{beamblue}\cos(\theta)\sin^2(\varphi)}
          +
          {\color<1->{red}2}\cdot{\color<1->{beamblue}\sin(\theta)\sin^2(\varphi)}
          +
          {\color<1->{red}\cos(\varphi)}\cdot{\color<1->{beamblue}\cos(\varphi)\sin(\varphi)}
          \,d\varphi\,d\theta} \\
        &\onslide<18->{=} \onslide<19->{\sage{flux}\,\si{\metre\cubed\per\second}}
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}



\end{document}

\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\tdplotsetmaincoords{60}{120}

\title{Lines and Planes}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Lines in $\mathbb{R}^n$}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myLa}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , axis/.style={<->, ultra thick}
      , scale=3/4
      ]

      \coordinate (xmin) at (-3, 0);
      \coordinate (xmax) at (3, 0);
      \coordinate (ymin) at (0, -2);
      \coordinate (ymax) at (0, 2);

      \onslide<+->{
        \draw[axis] (xmin) -- (xmax) node [right] {$x$};
        \draw[axis] (ymin) -- (ymax) node [above] {$y$};
      }

      \onslide<+->{\draw[axis, beamblue] (-3, -3/2) -- (2, 2);}
      \onslide<+->{\draw[axis, red] (-2, -2) -- (-2, 2);}
      \onslide<+->{\draw[axis, beamgreen] (-3, 1) -- (3, -1);}
    \end{tikzpicture}
  }
  \newcommand{\myLb}{
    \begin{tikzpicture}[
      , tdplot_main_coords
      , axis/.style={<->, ultra thick}
      , vector/.style={->, ultra thick}
      , line join=round
      , line cap=round
      , scale=2
      ]

      \coordinate (O) at (0, 0, 0);

      \onslide<+->{
        \draw[axis] (-1/2, 0, 0) -- (1, 0, 0) node[anchor=north east] {$x$};
        \draw[axis] (0, -1/2, 0) -- (0, 1, 0) node[anchor=north west] {$y$};
        \draw[axis] (0, 0, -1/2) -- (0, 0, 1) node[anchor=south] {$z$};
      }

      \onslide<+->{\draw[axis, beamblue] (0, -1, 1/2) -- (0, 1, 1/2);}
      \onslide<+->{\draw[axis, red] (-1, 0, 1/2) -- (1, 0, 3/4);}
      \onslide<+->{\draw[axis, beamgreen] (-3/2, 0, 0) -- (1/2, 1/2, 0);}
    \end{tikzpicture}
  }

  \begin{definition}<+->
    A \emph{line in $\mathbb{R}^n$} is a flat one-dimensional figure that
    extends infinitely.
    \begin{align*}
      \myLa && \myLb
    \end{align*}
  \end{definition}

\end{frame}

\subsection{Explicit Parameterizations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{parameterization} of a line $\mathcal{L}$ is a function
    $\bv{L}:\mathbb{R}\to\mathbb{R}^n$ given by
    \[
      \bv{L}(t) = \bv{P}+t\cdot\bv{v}
    \]
    where $P$ is any point on $\mathcal{L}$ and $\bv{v}$ is parallel to
    $\mathcal{L}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, red, ->}
        , axis/.style={ultra thick, beamblue, <->}
        ]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (3, 1);

        \onslide<2->{
          \draw[axis] ($ -1*(v) $) -- ($ 1.25*(v) $) node[pos=1.025] {$\mathcal{L}$};
        }

        \onslide<4->{
          \draw[vector] (O) -- (v) node[midway, above, sloped] {$\bv{v}$};
        }

        \onslide<3->{
          \node[myDot, label=below:{$P$}] at (O) {};
        }

      \end{tikzpicture}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{P}$};
      \onslide<8->{
        \node[overlay, above left= 5mm and 4mm of a] (text) {``initial position''};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myv}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{v}$};
      \onslide<9->{
        \node[overlay, above right= 1mm and 4mm of a] (text) {``velocity vector''};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{block}{Interpretation}<+-> A parameterization
    $\bv{L}(t)=\myP+t\cdot\myv$ describes \emph{motion along $\mathcal{L}$}.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, red, ->}
        , axis/.style={ultra thick, beamblue, <->}
        ]
        \coordinate (O) at (7, -1);
        \coordinate (l0) at (0, 0);
        \coordinate (l1) at (8, 3);
        \coordinate (P) at ( $ (l0)!3/7!(l1) $ );
        \coordinate (v) at ( $ (l0)!4/7!(l1) $ );

        \draw[axis] (l0) -- (l1) node [pos=1.025] {$\mathcal{L}$};
        \draw[vector] (P) -- (v) node [midway, sloped, above] {$\bv{v}$};
        \node[myDot, label={above:$P$}] at (P) {};

        \foreach \i [count=\j] in {-2,...,3} {
          \pgfmathsetmacro\stretch{\j/7}
          % \pgfmathtruncatemacro\mysnum{\j+2}
          \coordinate (Q) at ( $ (l0)!\stretch!(l1) $ );
          % \onslide<\mysnum->{\draw[ultra thick, red, ->] (O) -- (Q);}
          \onslide<+->{\node[myDot, label={[red]below:{\footnotesize$\bv{L}(\i)$}}] at (Q) {};}
        }

        % \onslide<2->{\node[label={right:$O$}] at (O) {\textbullet};}
      \end{tikzpicture}
    \]
    \onslide<10->{We call $\norm{\bv{v}}$ the \emph{speed} of the parameterization.}
  \end{block}

\end{frame}



\begin{sagesilent}
  P = vector([3, -2, 7])
  v = vector([2, 6, -9])
  var('t')
  L = P+t*v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}<+->
    Consider the line $\mathcal{L}$ given by
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, red, ->}
        , axis/.style={ultra thick, beamblue, <->}
        , rotate=15
        , transform shape
        ]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (3, 0);

        \draw[axis] ($ -1*(v) $) -- ($ 1.25*(v) $) node[pos=1.025] {$\mathcal{L}$};
        \draw[vector] (O) -- (v) node[midway, above, sloped] {$\scriptstyle\bv{v}=\sage{v}$};

        \node[myDot, label=below:{$\scriptstyle P\sage{tuple(P)}$}] at (O) {};

      \end{tikzpicture}
    \]
    \onslide<+->We may parameterize $\mathcal{L}$ with
    \[
      \bv{L}(t)
      = \sage{P}+t\cdot\sage{v}
      = \sage{L}
    \]
    \onslide<+->The speed of this parameterization is
    $\norm{\bv{v}}=\norm{\sage{v}}=\sage{v.norm()}$.
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{alertblock}{Warning}<+->
    There are \emph{many} ways to parameterize the same line.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, red, ->}
        , axis/.style={ultra thick, beamblue, <->}
        , rotate=15
        , transform shape
        ]
        \coordinate (l0) at (0, 0);
        \coordinate (l1) at (8, 0);


        \draw[axis] (l0) -- (l1) node [right] {$\mathcal{L}$};


        \foreach \i [count=\j] in {-2,...,3} {
          \pgfmathsetmacro\stretch{\j/7}
          \coordinate (P) at ( $ (l0)!\stretch!(l1) $ );
          \onslide<+->{\node[myDot, color=red, label={[red]below:{$\scriptstyle\bv{L}_1(\i)$}}] at (P) {};}
        }

        \foreach \i [count=\j] in {-3,...,1} {
          \pgfmathsetmacro\stretch{\j/6}
          \coordinate (P) at ( $ (l1)!\stretch!(l0)-(l0) $ );
          \onslide<+->{\node[myDot, color=beamgreen, label={[beamgreen]above:{$\scriptstyle\bv{L}_2(\i)$}}] at (P) {};}
        }
      \end{tikzpicture}
    \]
    \onslide<+->Two parameterizations
    \begin{align*}
      \bv{L}_1(t) &= \bv{P}_1+t\cdot\bv{v}_1 & \bv{L}_2(t) &= \bv{P}_2+t\cdot\bv{v}_2
    \end{align*}
    define the same line if each of $\Set{\bv{v}_1, \bv{v}_2, \vv{P_1P_2}}$ are
    parallel.
  \end{alertblock}

\end{frame}


\begin{sagesilent}
  var('t')
  v1 = vector([-6, 1])
  v2 = -3 * v1
  P1 = vector([-1, 1])
  P2 = 4 * v1 + P1
  L1 = P1+t*v1
  L2 = P2+t*v2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPa}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{P1}$};
      \onslide<4->{
        \node[overlay, below left= 0mm and 0mm of a] (text) {$\bv{P}_1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myva}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v1}$};
      \onslide<5->{
        \node[overlay, below left= 0mm and 0mm of a] (text) {$\bv{v}_1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myPb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{P2}$};
      \onslide<6->{
        \node[overlay, below right= 0mm and 0mm of a] (text) {$\bv{P}_2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myvb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v2}$};
      \onslide<7->{
        \node[overlay, below right= 0mm and 0mm of a] (text) {$\bv{v}_2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the lines $\mathcal{L}_1$ and $\mathcal{L}_2$ parameterized by
    \begin{align*}
      \bv{L}_1(t) &= \sage{L1}                       & \bv{L}_2(t) &= \sage{L2}         \\
                  &\onslide<2->{= \myPa+t\cdot\myva} &             &\onslide<3->{= \myPb+t\cdot\myvb}
    \end{align*}
    \onslide<8->{Note that $\vv{P_1P_2}=\sage{P2}-\sage{P1}=\sage{P2-P1}$ so that}
    \begin{align*}
      \onslide<8->{\bv{v}_2} &\onslide<8->{= -3\cdot\bv{v}_1} & \onslide<9->{\vv{P_1P_2}} &\onslide<9->{= 4\cdot \bv{v}_1}
    \end{align*}
    \onslide<10->{Each of $\Set{\bv{v}_1, \bv{v}_2, \vv{P_1P_2}}$ are parallel,
      so $\mathcal{L}_1=\mathcal{L}_2$.}
  \end{example}

\end{frame}


\subsection{Implicit Equations}

\begin{sagesilent}
  set_random_seed(8)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3, upper_bound=5)
  b = A.column_space().random_element()
  M = A.augment(b, subdivide=True)
  P = vector([-5, -11, 0, 1])
  v = vector([-3, -2, 1, 0])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mySystem}{
    \begin{array}{rcrcrcrcr}
      2\,x_1 &-& x_2 &+& 4\,x_3 & &        &=&  1 \\
      x_1 & &     &+& 3\,x_3 &-& 2\,x_4 &=& -3 \\
      -2\,x_1 &-& x_2 &-& 4\,x_3 &-& 1\,x_4 &=&  0
    \end{array}
  }
  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{P}$};
      \onslide<1->{
        \node[overlay, below left= 0mm and -4mm of a] (text) {$P$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myv}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v}$};
      \onslide<1->{
        \node[overlay, below left= 1mm and -4mm of a] (text) {$\bv{v}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{example}
    Consider the system of equations
    \begin{gather*}
      \begin{align*}
        \mySystem && \onslide<2->{\rref\sage{M}=\sage{M.rref()}}
      \end{align*}
    \end{gather*}
    \onslide<3->{The system is consistent and the solutions are given by}
    \begin{gather*}
      \onslide<4->{\bv{L}(t) =}
      \onslide<5->{\langle -5-3\,t, -11-2\,t, t, 1\rangle =}
      \onslide<6->{\myP+t\cdot\myv}
    \end{gather*}
    \onslide<7->{The solutions are given by the line through $P$ with velocity
      $\bv{v}$.}
  \end{example}

  \begin{block}{Observation}<8->
    This system $A\bv{x}=\bv{b}$ is solved by $\bv{x}=\bv{P}$ and
    $\Null(A)=\Span\Set{\bv{v}}$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider a line $\mathcal{L}$ parameterized by
    \[
      \bv{L}(t) = \bv{P}+t\cdot\bv{v}
    \]
    Then $\mathcal{L}$ is the solution space of a system $A\bv{x}=\bv{b}$ where
    \begin{align*}
      \bv{b} &= A\bv{P} & \Null(A) &= \Span\Set{\bv{v}}
    \end{align*}
    Any basis of $\Span\Set{\bv{v}}^\perp$ can be chosen as the rows of $A$.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  P = vector([1, 2, -1])
  v = vector([2, -1, 3])
  v1 = vector([1, 2, 0])
  v2 = vector([0, 3, 1])
  A = matrix([v1, v2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the line $\mathcal{L}$ given by
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, red, ->}
        , axis/.style={ultra thick, beamblue, <->}
        , rotate=5
        , transform shape
        ]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (3, 0);

        \draw[axis] ($ -1*(v) $) -- ($ 1.25*(v) $) node[pos=1.025] {$\mathcal{L}$};
        \draw[vector] (O) -- (v) node[midway, above, sloped] {$\scriptstyle\bv{v}=\sage{v}$};

        \node[myDot, label=below:{$\scriptstyle P\sage{tuple(P)}$}] at (O) {};

      \end{tikzpicture}
    \]
    \onslide<2->{The orthogonal complement of $\Span\Set{\bv{v}}$ is}
    \[
      \onslide<2->{\Span\Set{\sage{v}}^\perp =}
      \onslide<3->{\Span\Set{\sage{v1}, \sage{v2}}}
    \]
    \onslide<4->{The line is the solution set of $A\bv{x}=\bv{b}$ where}
    \begin{align*}
      \onslide<4->{A} &\onslide<4->{= \sage{A}} & \onslide<5->{\bv{b}} &\onslide<5->{= \overset{A}{\sage{A}}\overset{\bv{P}}{\sage{matrix.column(P)}} = \sage{A*matrix.column(P)}}
    \end{align*}
  \end{example}

\end{frame}


\section{Planes in $\mathbb{R}^n$}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{plane} in $\mathbb{R}^n$ is an infinite two-dimensional flat surface.
  \end{definition}
  \[
    \begin{tikzpicture}[
      , line cap=round
      , line join=round
      , scale=1/4
      , ultra thick
      , tdplot_main_coords
      ]
      % axes extrema
      \def\xmax{16}
      \def\ymax{16}
      \def\zmax{16}

      % axes coordinates
      \coordinate (O) at (0, 0, 0);
      \coordinate (xmax) at (\xmax, 0, 0);
      \coordinate (ymax) at (0, \ymax, 0);
      \coordinate (zmax) at (0, 0, \zmax);

      % axes
      \draw[thick,->] (O) -- (xmax) node[anchor=north east]{$x$};
      \draw[thick,->] (O) -- (ymax) node[anchor=north west]{$y$};
      \draw[thick,->] (O) -- (zmax) node[anchor=south]{$z$};

      % plane coordinates
      \coordinate (P) at (18, -6, 7);
      \coordinate (Q) at (-14, -6, 4);
      \coordinate (R) at (16, 7, 2);
      \coordinate (S) at ($ (Q)+(R)-(P) $);

      % plane
      \filldraw[fill=beamblue!45]
      (P) -- (Q) -- (S) -- (R) -- cycle;

      % dashed axes
      \draw[thick, dashed,->] (O) -- (xmax) node[anchor=north east]{$x$};
      \draw[thick, dashed,->] (O) -- (ymax) node[anchor=north west]{$y$};
      \draw[thick, dashed,->] (O) -- (zmax) node[anchor=south]{$z$};

      % redraw plane outline
      \draw (P) -- (Q) -- (S) -- (R) -- cycle;
    \end{tikzpicture}
  \]
\end{frame}


\subsection{Explicit Parameterizations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{parameterization} of a plane $\mathcal{P}$ is a function
    $\bv{L}:\mathbb{R}\to\mathbb{R}^n$ given by
    \[
      \bv{L}(t_1, t_2) = \bv{P} + t_1\cdot\bv{v}_1 + t_2\cdot\bv{v}_2
    \]
    where $P$ is on $\mathcal{P}$ and $\Set{\bv{v}_1, \bv{v}_2}$ are independent
    and parallel to $\mathcal{P}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, beamblue, ->}
        , scale=3/4
        , transform shape
        ]

        \coordinate (O) at (0, 0);

        \coordinate (tv1) at (3.5, 0);
        \coordinate (tv2) at (2, 2);
        \coordinate (v1) at ($ 0.5*(tv1) $);
        \coordinate (v2) at ($ 0.5*(tv2) $);
        \coordinate (Q) at ($ (tv1)+(tv2) $);

        \onslide<2->{
          \filldraw[fill=beamblue!45] ($ -.40*(tv1)-.40*(tv2) $) -- ($ -.40*(tv2)+1.40*(tv1) $) -- ($ 1.40*(tv1)+1.40*(tv2) $) -- ($ -.40*(tv1)+1.40*(tv2) $) -- cycle;
        }

        \onslide<7->{
          \draw[thick, dashed, ->, beamblue] (O) -- (tv1) node[pos=0.75, below, sloped] {$t_1\cdot\bv{v}_1$};
        }

        \onslide<8->{
          \draw[thick, dashed, ->, red] (O) -- (tv2) node[pos=0.75, above, sloped] {$t_2\cdot\bv{v}_2$};
        }

        \onslide<9->{
          \draw[thick, dashed, ->, red] (tv1) -- (Q);
        }

        \onslide<10->{
          \draw[thick, dashed, ->, beamblue] (tv2) -- (Q);
        }

        \onslide<4->{
          \draw[vector, beamblue] (O) -- (v1) node[midway, below, sloped] {$\bv{v}_1$};
        }

        \onslide<5->{
          \draw[vector, red] (O) -- (v2) node[midway, above, sloped] {$\bv{v}_2$};
        }

        \onslide<3->{
          \node[myDot, label=below:{$P$}] at (O) {};
        }

        \onslide<6->{
          \node[myDot, label=right:{$Q$}] at (Q) {};
        }

      \end{tikzpicture}
    \]
    \onslide<11->{A point $Q$ is on $\mathcal{P}$ if
      $\vv{PQ}\in\Span\Set{\bv{v}_1, \bv{v}_2}$.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(1831)
  A = random_matrix(ZZ, 3, 2, algorithm='echelonizable', rank=2, upper_bound=5)
  P = A*vector([-1, 2])
  v1, v2 = A.columns()
  var('t1 t2')
  L = P+t1*v1+t2*v2
  x, y, z = L
  Q, = A.left_kernel().basis()
  M = A.augment(Q-P, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{P}$};
      \onslide<3->{
        \node[overlay, above right= 1mm and -4mm of a] (text) {$\bv{P}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myva}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v1}$};
      \onslide<4->{
        \node[overlay, above right= 1mm and -4mm of a] (text) {$\bv{v}_1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myvb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v2}$};
      \onslide<5->{
        \node[overlay, above right= 1mm and -4mm of a] (text) {$\bv{v}_2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the plane $\mathcal{P}$ parameterized in coordinates by
    \begin{gather*}
      \begin{align*}
        x(t_1, t_2) &= \sage{x} & y(t_1, t_2) &= \sage{y} & z(t_1, t_2) &= \sage{z}
      \end{align*}
    \end{gather*}
    \onslide<2->{Our parameterization is given by}
    \[
      \onslide<2->{\bv{L}(t_1, t_2) = \myP + t_1\cdot\myva + t_2\cdot\myvb}
    \]
    \onslide<6->{To check if $Q\sage{tuple(Q)}$ is on $\mathcal{P}$, we check if
      $\vv{PQ}\in\Span\Set{\bv{v}_1, \bv{v}_2}$.}
    \[
      \onslide<7->{\rref\sage{M} = \sage{M.rref()}}
    \]
    \onslide<8->{The relevant system is \emph{inconsistent}, so $Q$ is not on
      $\mathcal{P}$.}
  \end{example}

\end{frame}


\subsection{Implicit Equations}

\begin{sagesilent}
  set_random_seed(80417)
  A = random_matrix(ZZ, 2, 4, algorithm='echelonizable', rank=2, upper_bound=10)
  b = A.column_space().random_element()
  M = A.augment(b, subdivide=True)
  P = vector([1, -2, 0, 0])
  v1 = vector([2, 3, 1, 0])
  v2 = vector([3, -2, 0, 1])
  var('t1 t2')
  L = P+t1*v1+t2*v2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mySystem}{
    \begin{array}{rcrcrcrcr}
      -2\,x_1 &-& x_2 &+& 7\,x_3 &+& 4\,x_4 &=& 0 \\
      x_1 & &     &-& x\,x_3 &-& 3\,x_4 &=& 1
    \end{array}
  }
  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{P}$};
      \onslide<6->{
        \node[overlay, below left= 1mm and -4mm of a] (text) {$\bv{P}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myva}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v1}$};
      \onslide<7->{
        \node[overlay, below left= 1mm and -4mm of a] (text) {$\bv{v}_1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myvb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{v2}$};
      \onslide<8->{
        \node[overlay, below left= 1mm and -4mm of a] (text) {$\bv{v}_2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{example}
    Consider the system of equations
    \begin{gather*}
      \begin{align*}
        \mySystem && \onslide<2->{\rref\sage{M}} &\onslide<2->{= \sage{M.rref()}}
      \end{align*}
    \end{gather*}
    \onslide<3->{The system is consistent and the solutions are given by}
    \begin{align*}
      \onslide<3->{\bv{L}(t_1, t_2)}
      &\onslide<3->{=} \onslide<4->{\langle 1+2\,t_1+3\,t_2, -2+3\,t_1-2\,t_2, t_1, t_2\rangle} \\
      &\onslide<4->{=} \onslide<5->{\myP+t_1\cdot\myva+t_2\cdot\myvb}
    \end{align*}
    \onslide<9->{The solutions live on the plane through $P$ and parallel to
      $\Set{\bv{v}_1, \bv{v}_2}$.}
  \end{example}

  \begin{block}{Observation}<10->
    This $A\bv{x}=\bv{b}$ is solved by $\bv{x}=\bv{P}$ and
    $\Null(A)=\Span\Set{\bv{v}_1, \bv{v}_2}$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider a plane $\mathcal{P}$ parameterized by
    \[
      \bv{L}(t_1, t_2) = \bv{P}+t_1\cdot\bv{v}_1+t_2\cdot\bv{v}_2
    \]
    Then $\mathcal{P}$ is the solution space of a system $A\bv{x}=\bv{b}$ where
    \begin{align*}
      \bv{b} &= A\bv{P} & \Null(A) &= \Span\Set{\bv{v}_1, \bv{v}_2}
    \end{align*}
    Any basis of $\Span\Set{\bv{v}_1, \bv{v}_2}^\perp$ can be chosen as the rows
    of $A$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider a plane $\mathcal{P}$ in $\mathbb{R}^3$.
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[
        , line cap=round
        , line join=round
        , scale=1/3
        , tdplot_main_coords
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, beamblue, ->}
        ]
        % the origin
        \coordinate (O) at (0, 0, 0);

        % standard basis
        \coordinate (e1) at (1, 0, 0);
        \coordinate (e2) at (0, 1, 0);
        \coordinate (e3) at (0, 0, 1);

        % plane coordinates
        \def\xmax{18}
        \def\ymax{12}
        \coordinate (X) at ($ \xmax*(e1)  $);
        \coordinate (Y) at ($ \ymax*(e2)  $);

        % plane
        \filldraw[fill=beamblue!45]
        (O) -- (X) -- ($ (X)+(Y) $) -- (Y) -- cycle;

        % coordinate of point P on the plane
        \pgfmathsetmacro\px{(2/5)*\xmax}
        \pgfmathsetmacro\py{(2/3)*\ymax}
        \coordinate (P) at (\px, \py, 0);

        % coordinate of point Q on the plane
        \pgfmathsetmacro\px{(4/5)*\xmax}
        \pgfmathsetmacro\py{(1/4)*\ymax}
        \coordinate (Q) at (\px, \py, 0);

        % compute normal vector n
        \coordinate (n) at ($ 0.4*\xmax*(e3)+(P) $);

        % n is orthogonal to plane
        \def\perplen{1}
        \coordinate (s1) at ($ (P)!\perplen!($ (P)+(e3) $) $);
        \coordinate (s2) at ($ (P)!\perplen!($ (s1)+(e2) $) $);
        \coordinate (s3) at ($ (P)!\perplen!($ (P)+(e2) $) $);
        \onslide<3->{
          \draw[very thick] (s1) -- (s2) -- (s3);
        }

        % draw N
        \onslide<3->{
          \draw[ultra thick, red, ->] (P) -- (n) node [midway, right] {$\bv{N}=\onslide<4->{\bv{v}_1\times\bv{v}_2}$};
        }

        % draw PQ
        \draw[ultra thick, beamblue, ->] (P) -- ($ 0.25*(Q) $) node [midway, above, sloped] {$\bv{v}_1$};
        \draw[ultra thick, beamblue, ->] (P) -- (Q) node [midway, below, sloped] {$\bv{v}_2$};

        % draw point P
        \node[myDot, label={below:{$P$}}] at (P) {};
      \end{tikzpicture}
    \]
    \onslide<2->{The orthogonal complement of $\Span\Set{\bv{v}_1, \bv{v}_2}$
      is}
    \[
      \onslide<2->{\Span\Set{\bv{v}_1, \bv{v}_2}^\perp =} \onslide<5->{\Span\Set{\bv{N}=\bv{v}_1\times\bv{v}_2}}
    \]
    \onslide<6->{The plane is the solution set of
      $\bv{N}\cdot\bv{x}=\bv{N}\cdot\bv{P}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{point-normal forms} of a plane $\mathcal{P}$ in $\mathbb{R}^3$ are the equations
    \begin{align*}
      \bv{N}\cdot\bv{x} &= \bv{N}\cdot\bv{P} & \bv{N}\cdot(\bv{x}-\bv{P}) &= 0
    \end{align*}

    where $\bv{N}$ is orthogonal to $\mathcal{P}$ and $P$ is a point on $\mathcal{P}$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  N = vector([3, 2, -1])
  P = vector([1, 2, -2])
  var('x y z')
  vx = vector([x, y, z])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the plane $\mathcal{P}$ given by
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[
        , line cap=round
        , line join=round
        , scale=1/3
        , tdplot_main_coords
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, beamblue, ->}
        , scale=0.75
        % , transform shape
        ]
        % the origin
        \coordinate (O) at (0, 0, 0);

        % standard basis
        \coordinate (e1) at (1, 0, 0);
        \coordinate (e2) at (0, 1, 0);
        \coordinate (e3) at (0, 0, 1);

        % plane coordinates
        \def\xmax{18}
        \def\ymax{12}
        \coordinate (X) at ($ \xmax*(e1)  $);
        \coordinate (Y) at ($ \ymax*(e2)  $);

        % plane
        \filldraw[fill=beamblue!45]
        (O) -- (X) -- ($ (X)+(Y) $) -- (Y) -- cycle;

        % coordinate of point P on the plane
        \pgfmathsetmacro\px{(2/5)*\xmax}
        \pgfmathsetmacro\py{(2/3)*\ymax}
        \coordinate (P) at (\px, 0.75*\py, 0);

        % coordinate of point Q on the plane
        \pgfmathsetmacro\px{(4/5)*\xmax}
        \pgfmathsetmacro\py{(1/4)*\ymax}
        \coordinate (Q) at (\px, \py, 0);

        % compute normal vector n
        \coordinate (n) at ($ 0.4*\xmax*(e3)+(P) $);

        % n is orthogonal to plane
        \def\perplen{1}
        \coordinate (s1) at ($ (P)!\perplen!($ (P)+(e3) $) $);
        \coordinate (s2) at ($ (P)!\perplen!($ (s1)+(e2) $) $);
        \coordinate (s3) at ($ (P)!\perplen!($ (P)+(e2) $) $);
        \draw[very thick] (s1) -- (s2) -- (s3);

        % draw N
        \draw[ultra thick, red, ->] (P) -- (n) node [midway, right] {$\bv{N}=\sage{N}$};

        % draw PQ
        % \draw[ultra thick, beamblue, ->] (P) -- ($ 0.25*(Q) $) node [midway, above, sloped] {$\bv{v}_1$};
        % \draw[ultra thick, beamblue, ->] (P) -- (Q) node [midway, below, sloped] {$\bv{v}_2$};

        % draw point P
        \node[myDot, label={below:{$P\sage{tuple(P)}$}}] at (P) {};
      \end{tikzpicture}
    \]
    \pause The point-normal forms $\bv{N}\cdot\bv{x}=\bv{N}\cdot\bv{P}$ and
    $\bv{N}\cdot(\bv{x}-\bv{P})=0$ are
    \begin{align*}
      \sage{N*vx} &= \sage{N*P} & 3\cdot(x-1) + 2\cdot(y-2) - (z+2) &= 0
    \end{align*}
  \end{example}

\end{frame}


\begin{sagesilent}
  set_random_seed(1320)
  P, Q, R = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=4)
  N = (Q-P).cross_product(R-P)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\mathcal{P}$ passing through $P\sage{tuple(P)}$,
    $Q\sage{tuple(Q)}$, $R\sage{tuple(R)}$.
    \[
      \begin{tikzpicture}[
        , line cap=round
        , line join=round
        , tdplot_main_coords
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, beamblue, ->}
        % , transform shape
        % , scale=2
        ]

        \pgfmathsetmacro{\myScale}{5}
        \coordinate (O) at (0, 0, 0);
        \coordinate (e1) at (\myScale, 0, 0);
        \coordinate (e2) at (0, \myScale, 0);
        \coordinate (e3) at (0, 0, \myScale);

        \filldraw[fill=beamblue!45] (O) -- (e1) -- ($ (e1)+(e2) $) -- (e2) -- cycle;

        \coordinate (P) at ($ 0.75*(e1)+0.25*(e2) $);
        \begin{scope}[shift={(P)}]
          \coordinate (Q) at ( 0, 2.5, 0);
          \coordinate (R) at (-2.5, 0, 0);
          \coordinate (N) at (0, 0, 4);

          \onslide<4->{
            \draw (0, 0, 0.25) -- (0, 0.25, 0.25) -- (0, 0.25, 0);
          }

          \onslide<2->{
            \draw[vector] (P) -- (Q) node[midway, below, sloped] {$\vv{PQ}$};
          }

          \onslide<3->{
            \draw[vector] (P) -- (R) node[midway, above, sloped] {$\vv{PR}$};
          }

          \onslide<4->{
            \draw[vector, red] (P) -- (N) node[pos=0.85, right] {$\begin{aligned}\scriptstyle \bv{N}&\scriptstyle=\onslide<5->{\vv{PQ}\times\vv{PR}}\\ &\scriptstyle\onslide<6->{= \sage{Q-P}\times\sage{R-P}}\\ &\scriptstyle\onslide<7->{= \sage{N}}\end{aligned}$};
          }

          \node[myDot, label=left:{$P$}] at (P) {};
          \node[myDot, label=right:{$Q$}] at (Q) {};
          \node[myDot, label=above:{$R$}] at (R) {};
        \end{scope}

      \end{tikzpicture}
    \]
    \onslide<8->{Our plane is $-4\cdot(x-1) - 2\cdot(y-2) + 3\cdot(z-3) = 0$.}
  \end{example}

\end{frame}


\section{Distance Problems}
\subsection{A Point and a Line}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider a line $\mathcal{L}$ parameterized by $\bv{L}(t)=\bv{P}+t\cdot\bv{v}$ and a point $Q$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , vector/.style={ultra thick, ->}
        , axis/.style={ultra thick, beamblue, <->}
        , rotate=15
        , transform shape
        ]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (2, 0);
        \coordinate (Q) at (3, 2);
        \coordinate (d) at ($ (O)!(Q)!(v) $);

        \begin{scope}[shift={(d)}]
          \pgfmathsetmacro{\myPerp}{0.25}
          \onslide<6->{
            \draw[ultra thick] (0, \myPerp) -- (\myPerp, \myPerp) -- (\myPerp, 0);
          }
        \end{scope}

        \onslide<6->{
          \draw[dashed, thick] (d) -- (Q) node[midway, right] {$d$};
        }

        \onslide<8->{
          \draw pic["$\theta$", draw=black, ultra thick, angle eccentricity=1.25, angle radius=1cm] {angle=v--O--Q};
        }

        \onslide<2->{
          \draw[axis] ($ -0.5*(v) $) -- ($ 2*(v) $) node[right] {$\mathcal{L}$};
        }

        \onslide<4->{
          \draw[vector, red] (O) -- (v) node[midway, sloped, below] {$\bv{v}$};
        }

        \onslide<7->{
          \draw[vector, beamgreen] (O) -- (Q) node[midway, sloped, above] {$\vv{PQ}$};
        }

        \onslide<3->{
          \node[myDot, label=below:{$P$}] at (O) {};
        }

        \onslide<5->{
          \node[myDot, label=right:{$Q$}] at (Q) {};
        }
      \end{tikzpicture}
    \]
    \onslide<9->{The minimum distance from $Q$ to $\mathcal{L}$ is}
    \[
      \onslide<10->{d =}
      \onslide<11->{\norm{\vv{PQ}}\cdot\sin(\theta) =}
      \onslide<12->{\norm{\vv{PQ}}\cdot\frac{\norm{\bv{v}\times\vv{PQ}}}{\norm{\bv{v}}\cdot\norm{\vv{PQ}}} =}
      \onslide<13->{\frac{\norm{\bv{v}\times\vv{PQ}}}{\norm{\bv{v}}}}
    \]
  \end{example}

\end{frame}


\subsection{A Point and a Plane}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the plane $\mathcal{P}$ given by $\bv{N}\cdot(\bv{x}-\bv{P})=0$ and a point $Q$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , vector/.style={ultra thick, ->}
        , myDot/.style={circle, fill, inner sep=2pt}
        ]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (3, 0);
        \coordinate (w) at (2, 1);

        \coordinate (P) at ($ .35*(v)+.25*(w) $);
        \coordinate (Q) at ($ 1.25*(v)+(0, 3) $);
        \coordinate (p) at (3.75, 0.65);

        \onslide<2->{
          \filldraw[fill=beamblue!45] (O) -- (v) -- ($ (v)+(w) $) -- (w) -- cycle;
        }

        \begin{scope}[shift={(p)}]
          \onslide<4->{
            \draw (0, 0.25) -- (0.25, 0.25) -- (0.25, 0);
          }
        \end{scope}

        \onslide<7->{
          \draw[overlay, thick, dashed, red] (p) -- (Q) node[right, pos=0.75] {$d=\onslide<8->{\abs{\comp_{\bv{N}}(\vv{PQ})}}$};
        }

        \onslide<6->{
          \draw[vector, beamblue] (P) -- (Q) node[midway, above, sloped] {$\vv{PQ}$};
        }

        \onslide<4->{
          \draw[vector, red] (p) -- ($ (p)!0.65!(Q) $) node[midway, right] {$\bv{N}$};
        }

        \onslide<3->{
          \node[myDot, label=left:{$P$}] at (P) {};
        }

        \onslide<5->{
          \node[myDot, label=right:{$Q$}] at (Q) {};
        }

      \end{tikzpicture}
    \]
    \onslide<9->{The minimum distance from $\mathcal{P}$ to $Q$ is
      $d=\abs{\comp_{\bv{N}}(\vv{PQ})}$.}
  \end{example}

\end{frame}


\end{document}

\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\tikzset{
  , myfill/.style={fill=blue!40!white, fill opacity=.6}
  , mydraw/.style={draw=blue!70!black}
  , bluefilldraw/.style={fill=beamblue!40!white, fill opacity=.6, draw=beamblue}
  , redfilldraw/.style={draw=red, fill=red!40, fill opacity=0.6}
  , greenfill/.style={fill=beamgreen!40, fill opacity=0.6}
  , greenfilldraw/.style={draw=beamgreen, fill=beamgreen!40, fill opacity=0.6}
}

\title{Gauss' Divergence Theorem}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Boundaries of Solids}
\subsection{Terminology}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $D$ be a solid domain in $\mathbb{R}^3$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\r}{1}
        \pgfmathtruncatemacro{\n}{12}

        \foreach \i in {1,2,...,\n} {
          % \coordinate (c\i) at ({\R*cos(\i*360/\n)}, {\r*sin(\i*360/\n)});
          \coordinate (c\i) at (\i*360/\n: {\R} and {\r});
          % \node[] at (c\i) {\i};
        };

        % \draw[beamblue] (c4) to[bend left] (c8);
        \draw[beamblue] (\R, 0) arc(0:180:{\R} and {\R/8});

        \filldraw[bluefilldraw, postaction={decorate}]
        plot[smooth cycle] coordinates{
          ($ 0.75*(c1) $)
          (c2)
          ($ 0.5*(c3) $)
          (c4)
          (c5)
          (c6)
          (c7)
          (c8)
          ($ 0.75*(c9) $)
          (c10)
          (c11)
          ($ 1.00*(c12) $)
        };

        % \draw[beamblue] (c4) to[bend right] (c8);
        \draw[beamblue] (\R, 0) arc(0:-180:{\R} and {\R/8});
        % \node[left] at (O) {$D$};
      \end{tikzpicture}
    \]
    \onslide<2->{The \emph{boundary $\partial D$} of $D$ is the outer shell of
      $D$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1};
        \pgfmathsetmacro{\r}{\R/6};

        \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
        \filldraw[bluefilldraw] (O) circle(\R);
        \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

      \end{tikzpicture}
  }
  \begin{example}
    Suppose $D$ is the ``unit ball'' $x^2+y^2+z^2\leq 1$.
    \[
      \onslide<2->{\myD}
    \]
    \onslide<3->{The boundary $\partial D$ is the ``unit sphere''
      $x^2+y^2+z^2=1$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myClosed}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\r}{1}
        \pgfmathtruncatemacro{\n}{12}

        \foreach \i in {1,2,...,\n} {
          % \coordinate (c\i) at ({\R*cos(\i*360/\n)}, {\r*sin(\i*360/\n)});
          \coordinate (c\i) at (\i*360/\n: {\R} and {\r});
          % \node[] at (c\i) {\i};
        };

        % \draw[beamblue] (c4) to[bend left] (c8);
        \draw[beamblue] (\R, 0) arc(0:180:{\R} and {\R/8});

        \filldraw[bluefilldraw, postaction={decorate}]
        plot[smooth cycle] coordinates{
          ($ 0.75*(c1) $)
          (c2)
          ($ 0.5*(c3) $)
          (c4)
          (c5)
          (c6)
          (c7)
          (c8)
          ($ 0.75*(c9) $)
          (c10)
          (c11)
          ($ 1.00*(c12) $)
        };

        % \draw[beamblue] (c4) to[bend right] (c8);
        \draw[beamblue] (\R, 0) arc(0:-180:{\R} and {\R/8});
        % \node[left] at (O) {$D$};

        \node[below] at (current bounding box.south)
        {$S$ \emph{closed} ($\partial S=\varnothing$)};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNotClosed}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\r}{1}
        \pgfmathtruncatemacro{\n}{12}
        \pgfmathsetmacro{\pitch}{\R/10}

        \foreach \i in {1,2,...,\n} {
          % \coordinate (c\i) at ({\R*cos(\i*360/\n)}, {\r*sin(\i*360/\n)});
          \coordinate (c\i) at (\i*360/\n: {\R} and {\r});
          % \node[] at (c\i) {\i};
        };

        \filldraw[bluefilldraw]
        (\R, 0) arc(0:180:{\R} and {\pitch}) --
        plot[smooth] coordinates{
          % ($ 0.75*(c1) $)
          % (c2)
          % ($ 0.5*(c3) $)
          % (c4)
          % (c5)
          (c6)
          (c7)
          (c8)
          ($ 0.75*(c9) $)
          (c10)
          (c11)
          ($ 1.00*(c12) $)
        };

        \filldraw[bluefilldraw]
        (\R, 0) arc(0:-180:{\R} and {\pitch}) --
        plot[smooth] coordinates{
          % ($ 0.75*(c1) $)
          % (c2)
          % ($ 0.5*(c3) $)
          % (c4)
          % (c5)
          (c6)
          (c7)
          (c8)
          ($ 0.75*(c9) $)
          (c10)
          (c11)
          ($ 1.00*(c12) $)
        };

        \onslide<4->{
          \draw[red] (O) circle ({\R} and {\pitch});
          \coordinate (b) at (0, \pitch);
          \draw[red, overlay, <-, thick, shorten <=2pt]
          (b.north) |- ++(1/2, 3mm) node[right] {$\partial S$};
        }

        \node[below] at (current bounding box.south)
        {$S$ not closed ($\partial S\neq\varnothing$)};
      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    A surface $S$ is \emph{closed} if $\partial S=\varnothing$.
    \begin{align*}
      \onslide<2->{\myClosed} && \onslide<3->{\myNotClosed}
    \end{align*}
    \onslide<5->{The symbol $\oiint_{S}$ indicates $S$ consists of one or more
      closed surfaces.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Convention}
    We work with $D$ for which $\partial D$ is a \emph{union of closed
      surfaces}.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $D$ is $x^2+y^2\leq z\leq 4$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\r}{\R/8}
        \pgfmathsetmacro{\H}{sqrt(\R)}

        \filldraw[bluefilldraw]
        (-\R, \H) parabola[bend at end]
        (O) parabola
        (\R, \H) arc(0:180:{\R} and {\r})
        -- cycle;

        \filldraw[bluefilldraw]
        (-\R, \H) parabola[bend at end]
        (O) parabola
        (\R, \H) arc(0:-180:{\R} and {\r})
        -- cycle;

        \filldraw[bluefilldraw] (0, \H) circle ({\R} and {\r});

        \onslide<2->{
          \filldraw[greenfilldraw]
          (-\R, \H) parabola[bend at end]
          (O) parabola
          (\R, \H) arc(0:180:{\R} and {\r})
          -- cycle;

          \filldraw[greenfilldraw]
          (-\R, \H) parabola[bend at end]
          (O) parabola
          (\R, \H) arc(0:-180:{\R} and {\r})
          -- cycle;
        }

        \filldraw[bluefilldraw] (0, \H) circle ({\R} and {\r});

        \onslide<2->{
          \coordinate (bottom) at (\R/2, \H/2);
          \draw[thick, beamgreen, <-, overlay]
          (bottom.south) |- ++(1/2, -2mm) node[right] {$S_1$};
        }

        \onslide<3->{
          \filldraw[redfilldraw] (0, \H) circle ({\R} and {\r});
          \coordinate (top) at (0, \H);
          \draw[thick, red, <-, overlay]
          (top.north) |- ++(1/4, 5mm) node[right] {$S_2$};
        }

      \end{tikzpicture}
    \]
    \onslide<4->{Here, we write $\partial D=S_1\cup S_2$.}
  \end{example}

\end{frame}


\subsection{Orientations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPos}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\r}{\R/8}
        \pgfmathsetmacro{\t}{45}

        \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
        \filldraw[bluefilldraw] (O) circle(\R);

        \draw[->, overlay, beamgreen]
        (\t:\R) -- ++(\t:0.75*\R) node[right] {$\bv{N}$};

        \filldraw[bluefilldraw] (O) circle (\R);
        \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{beamgreen}$\bv{N}$ points \emph{away from}
              D}}{{\color{beamgreen}$\partial D$ \emph{positively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNeg}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\r}{\R/8}
        \pgfmathsetmacro{\t}{45}

        \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
        \filldraw[bluefilldraw] (O) circle(\R);

        \draw[->, overlay, red]
        (\t:\R) -- ++(\t:-0.75*\R) node[left] {$\bv{N}$};

        \filldraw[bluefilldraw] (O) circle (\R);
        \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{red}$\bv{N}$ points \emph{into}
              D}}{{\color{red}$\partial D$ \emph{negatively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    The location of $D$ relative to $\bv{N}$ determines the \emph{orientation of
      $\partial D$}.
    \begin{align*}
      \myPos && \myNeg
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPos}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\Ra}{2}
        \pgfmathsetmacro{\ra}{\Ra/4}

        \pgfmathsetmacro{\Rb}{1}
        \pgfmathsetmacro{\rb}{\Rb/4}
        \pgfmathsetmacro{\t}{45}

        \draw[beamblue] (\Ra, 0) arc (0:180:{\Ra} and {\ra});
        \filldraw[bluefilldraw] (O) circle(\Ra);

        \draw[beamblue] (\Rb, 0) arc (0:180:{\Rb} and {\rb});
        \filldraw[bluefilldraw] (O) circle(\Rb);

        \draw[->, overlay, beamgreen]
        (\t:\Ra) -- ++(\t:0.5) node[right] {$\bv{N}$};

        \draw[->, overlay, beamgreen]
        (\t:\Rb) -- ++(\t:-0.5) node[left] {$\bv{N}$};

        \filldraw[bluefilldraw] (O) circle(\Rb);
        \draw[beamblue] (\Rb, 0) arc (0:-180:{\Rb} and {\rb});

        \filldraw[bluefilldraw] (O) circle (\Ra);
        \draw[beamblue] (\Ra, 0) arc (0:-180:{\Ra} and {\ra});

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{beamgreen}$\bv{N}$ points \emph{away from}
              D}}{{\color{beamgreen}$\partial D$ \emph{positively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNeg}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\Ra}{2}
        \pgfmathsetmacro{\ra}{\Ra/4}

        \pgfmathsetmacro{\Rb}{1}
        \pgfmathsetmacro{\rb}{\Rb/4}
        \pgfmathsetmacro{\ta}{30}
        \pgfmathsetmacro{\tb}{60}

        \draw[beamblue] (\Ra, 0) arc (0:180:{\Ra} and {\ra});
        \filldraw[bluefilldraw] (O) circle(\Ra);

        \draw[beamblue] (\Rb, 0) arc (0:180:{\Rb} and {\rb});
        \filldraw[bluefilldraw] (O) circle(\Rb);

        \draw[->, overlay, red]
        (\ta:\Ra) -- ++(\ta:-0.5) node[left] {$\bv{N}$};

        \draw[->, overlay, red]
        (\tb:\Rb) -- ++(\tb:0.5) node[above] {$\bv{N}$};

        \filldraw[bluefilldraw] (O) circle(\Rb);
        \draw[beamblue] (\Rb, 0) arc (0:-180:{\Rb} and {\rb});

        \filldraw[bluefilldraw] (O) circle (\Ra);
        \draw[beamblue] (\Ra, 0) arc (0:-180:{\Ra} and {\ra});

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{red}$\bv{N}$ points \emph{into}
              D}}{{\color{red}$\partial D$ \emph{negatively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Each part of $\partial D$ must be inspected to determine orientation.
    \begin{align*}
      \myPos && \myNeg
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Convention}
    Unless otherwise stated, we assume $\partial D$ is \emph{positively
      oriented}.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\M}{4}
        \pgfmathsetmacro{\m}{3*\M/5}
        \pgfmathsetmacro{\pitch}{\M/6}

        \pgfmathsetmacro{\R}{\M/3}
        \pgfmathsetmacro{\r}{\R/4}
        \pgfmathsetmacro{\t}{120}


        \draw[beamblue] (\M, 0) arc (0:180:{\M} and {\pitch});
        \filldraw[bluefilldraw] (O) circle({\M} and {\m});

        \begin{scope}[shift={(-3*\R/2, 0)}]
          \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
          \filldraw[bluefilldraw] (0, 0) circle(\R);

          \draw[->, overlay, beamgreen]
          (\t:\R) -- ++(\t:-0.5) node[right] {$\bv{N}_2$};

          \filldraw[bluefilldraw] (0, 0) circle(\R);
          \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

          \coordinate (b) at (0, -\R);
          \draw[beamgreen, thick, <-, shorten <=2pt]
          (b.south) |- ++(1/2, -3mm) node[right] {$S_2$};
        \end{scope}

        \begin{scope}[shift={(3*\R/2, 0)}]
          \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
          \filldraw[bluefilldraw] (0, 0) circle(\R);

          \draw[->, overlay, red]
          (\t:\R) -- ++(\t:0.5) node[right] {$\bv{N}_3$};

          \filldraw[bluefilldraw] (0, 0) circle(\R);
          \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

          \coordinate (b) at (0, -\R);
          \draw[red, thick, <-, shorten <=2pt]
          (b.south) |- ++(-1/2, -3mm) node[left] {$S_3$};
        \end{scope}

        \draw[->, red] (0, \m) -- ++(0, -1/2) node[below] {$\bv{N}_1$};

        \coordinate(right) at (\M, 0);
        \draw[red, thick, <-, shorten <=2pt]
        (right.east) -| ++(3mm, 1/2) node[above] {$S_1$};

        \filldraw[bluefilldraw] (O) circle({\M} and {\m});
        \draw[beamblue] (\M, 0) arc (0:-180:{\M} and {\pitch});

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    Assuming positive orientation defines \emph{boundary surface arithmetic}.
    \[
      \myD
    \]
    Here, we have $\partial D=-S_1+S_2-S_3$
  \end{block}

\end{frame}

\subsection{Flux}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A fluid flow $\bv{F}$ (\SI{}{\metre\per\second}) flows through a surface
    $S$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=.60
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \filldraw[bluefilldraw] (0, 0)
        to[bend left]  (3, -1)
        to[bend left]  (5,  2)
        to[bend right] (2,  3)
        to[bend right] (0,  0)
        --cycle;

        \begin{scope}[shift={(5/2, 2)}]
          \coordinate (O) at (0, 0);
          \coordinate (N) at (0, 3);
          \coordinate (F) at (1, 2);
          \coordinate (p) at ($ (O)!(F)!(N) $);

          \draw[->, red] (O) -- (1, 2) node[right] {$\bv{F}$};

          \node[myDot, label=below:{$P$}] at (O) {};
        \end{scope}

      \end{tikzpicture}
    \]
    \onslide<2->{\emph{Flux} (in \SI{}{\metre\cubed\per\second}) is the rate at
      which fluid volume flows through $S$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\M}{4}
        \pgfmathsetmacro{\m}{3*\M/5}
        \pgfmathsetmacro{\pitch}{\M/6}

        \draw[beamblue] (\M, 0) arc (0:180:{\M} and {\pitch});
        \filldraw[bluefilldraw] (O) circle({\M} and {\m});


        \onslide<3->{
          \pgfmathsetseed{1}
          \pgfmathsetmacro{\t}{360*rand}
          \draw[thick, beamgreen, arrows={-stealth}]
          (O) -- ++({0.25*cos(\t)}, {0.25*sin(\t)});

          \pgfmathtruncatemacro{\nlayers}{9}
          \foreach \s in {1,2,...,\nlayers} {
            \pgfmathtruncatemacro{\N}{10*\s}
            \foreach \i in {1,2,...,\N} {
              \pgfmathsetmacro{\t}{360/\N*\i}
              \pgfmathsetmacro{\k}{65*rand}
              \coordinate (p) at ({\M*\s*cos(\t)/(\nlayers)}, {\m*\s*sin(\t)/(\nlayers)});
              \draw[thick, beamgreen, arrows={-stealth}]
              (p) -- ++({0.25*cos(\t+\k)}, {0.25*sin(\t+\k)});
            }
          }
        }

        \filldraw[bluefilldraw] (O) circle({\M} and {\m});
        \draw[beamblue] (\M, 0) arc (0:-180:{\M} and {\pitch});

      \end{tikzpicture}
  }
  \begin{block}{Question}
    Suppose $\bv{F}$ is a vector field inside a domain $D$.
    \[
      \onslide<2->{\myD}
    \]
    \onslide<4->{The \emph{flux out of $D$} is
      $\oiint_{\partial D}\bv{F}\cdot d\bv{S}$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Interpretation}
    The flux out of $D$ is the rate that fluid volume enters or exists $D$.
    \begin{align*}
      \underset{\textnormal{{\color<2->{red}\onslide<2->{net flow in}}}}{{\color<2->{red}\oiint_{\partial D}\bv{F}\cdot d\bv{S}<0}}
      && \underset{\textnormal{{\color<4->{beamgreen}\onslide<4->{net flow out}}}}{{\color<4->{beamgreen}\onslide<3->{\oiint_{\partial D}\bv{F}\cdot d\bv{S}>0}}}
      && \underset{\textnormal{{\color<6->{beamblue}\onslide<6->{zero net flow}}}}{{\color<6->{beamblue}\onslide<5->{\oiint_{\partial D}\bv{F}\cdot d\bv{S}=0}}}
    \end{align*}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\M}{4}
        \pgfmathsetmacro{\m}{3*\M/5}
        \pgfmathsetmacro{\pitch}{\M/6}

        \pgfmathsetmacro{\R}{\M/3}
        \pgfmathsetmacro{\r}{\R/4}
        \pgfmathsetmacro{\t}{120}


        \draw[beamblue] (\M, 0) arc (0:180:{\M} and {\pitch});
        \filldraw[bluefilldraw] (O) circle({\M} and {\m});

        \begin{scope}[shift={(-3*\R/2, 0)}]
          \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
          \filldraw[bluefilldraw] (0, 0) circle(\R);

          \draw[->, overlay, beamgreen]
          (\t:\R) -- ++(\t:-0.5) node[right] {$\bv{N}_2$};

          \filldraw[bluefilldraw] (0, 0) circle(\R);
          \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

          \coordinate (b) at (0, -\R);
          \draw[beamgreen, thick, <-, shorten <=2pt]
          (b.south) |- ++(1/2, -3mm) node[right] {$S_2$};
        \end{scope}

        \begin{scope}[shift={(3*\R/2, 0)}]
          \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
          \filldraw[bluefilldraw] (0, 0) circle(\R);

          \draw[->, overlay, red]
          (\t:\R) -- ++(\t:0.5) node[left] {$\bv{N}_3$};

          \filldraw[bluefilldraw] (0, 0) circle(\R);
          \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

          \coordinate (b) at (0, -\R);
          \draw[red, thick, <-, shorten <=2pt]
          (b.south) |- ++(-1/2, -3mm) node[left] {$S_3$};
        \end{scope}

        \draw[->, red] (0, \m) -- ++(0, -1/2) node[below] {$\bv{N}_1$};

        \coordinate(right) at (\M, 0);
        \draw[red, thick, <-, shorten <=2pt]
        (right.east) -| ++(3mm, 1/2) node[above] {$S_1$};

        \filldraw[bluefilldraw] (O) circle({\M} and {\m});
        \draw[beamblue] (\M, 0) arc (0:-180:{\M} and {\pitch});

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    Flux integrals respect boundary arithmetic.
    \[
      \myD
    \]
    Here,
    $
    \oiint_{\partial D}\bv{F}\cdot d\bv{S}
    =
    -\oiint_{S_1}\bv{F}\cdot d\bv{S}
    +\oiint_{S_2}\bv{F}\cdot d\bv{S}
    -\oiint_{S_3}\bv{F}\cdot d\bv{S}
    $.
  \end{block}

\end{frame}



\begin{sagesilent}
  var('x y z')
  F = vector([-4*y*z, 4*x*z, -3*z])
  var('rho varphi theta')
  X = vector([rho*sin(varphi)*cos(theta), rho*sin(varphi)*sin(theta), rho*cos(varphi)])
  Tvarphi = X.diff(varphi)
  Ttheta = X.diff(theta)
  N = (Tvarphi.cross_product(Ttheta)).simplify_trig()
  Fs = F(x=X[0], y=X[1], z=X[2])
  FN = (Fs*N).simplify_trig()
  flux = integral(integral(FN, varphi, 0, pi), theta, 0, 2*pi)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\Ra}{2}
        \pgfmathsetmacro{\ra}{\Ra/6}
        \pgfmathsetmacro{\ta}{30}

        \pgfmathsetmacro{\Rb}{1}
        \pgfmathsetmacro{\rb}{\Rb/6}
        \pgfmathsetmacro{\tb}{60}

        \draw[beamblue] (\Ra, 0) arc(0:180:{\Ra} and {\ra});
        \filldraw[bluefilldraw] (O) circle(\Ra);

        \draw[beamblue] (\Rb, 0) arc(0:180:{\Rb} and {\rb});
        \filldraw[bluefilldraw] (O) circle(\Rb);

        \onslide<6->{
          \draw[red, ->]
          ({\Rb*cos(\tb)}, {\Rb*sin(\tb)}) -- ++({0.5*cos(\tb)}, {0.5*sin(\tb)})
          node[above left] {$\bv{N}_1$};
        }

        \filldraw[bluefilldraw] (O) circle(\Rb);
        \draw[beamblue] (\Rb, 0) arc(0:-180:{\Rb} and {\rb});

        \onslide<3->{
          \coordinate (inner) at (0, -\Rb);
          \draw[thick, red, <-, shorten <=2pt] (inner.south) |- ++(1/3, -3mm)
          node[right] {$\bv{X}_1$};
        }

        \onslide<7->{
          \draw[beamgreen, ->, overlay]
          ({\Ra*cos(\ta)}, {\Ra*sin(\ta)}) -- ++({0.5*cos(\ta)}, {0.5*sin(\ta)})
          node[above] {$\bv{N}_2$};
        }

        \filldraw[bluefilldraw] (O) circle(\Ra);
        \draw[beamblue] (\Ra, 0) arc(0:-180:{\Ra} and {\ra});

        \onslide<4->{
          \coordinate (outer) at (0, -\Ra);
          \draw[overlay, thick, red, <-, shorten <=2pt]
          (outer.south) |- ++(1/3, -3mm)
          node[right] {$\bv{X}_2$};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myFN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<10->{red}\sage{FN}}$};
      \onslide<10->{
        \node[overlay, below left= 2mm and -16mm of a] (text) {$\bv{F}\cdot\bv{N}_\rho$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myFlux}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<14->{red}\displaystyle-\oiint_{\bv{X}_1} \bv{F}\cdot d\bv{S}}$};
      \onslide<14->{
        \node[overlay, below left= 1mm and -4mm of a] (text) {$\bv{N}_1$ wrong direction};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myCalc}{
    \begin{aligned}
      \onslide<2->{\bv{X}_{\rho}} &\onslide<2->{=} \onslide<2->{\sage{X}} \\
      \onslide<5->{\bv{N}_{\rho}} &\onslide<5->{=} \onslide<5->{\sage{N}} \\
      \onslide<8->{\oiint_{\bv{X}_\rho} \bv{F}\cdot d\bv{S}} &\onslide<8->{=} \onslide<9->{\int_{0}^{2\,\pi}\int_{0}^\pi\myFN\,d\varphi\,d\theta =} \onslide<11->{\sage{flux}\,\si{\metre\cubed\per\second}}
    \end{aligned}
  }
  \begin{example}
    Let $D$ be $1\leq\rho\leq2$ and consider
    $\bv{F}=\sage{F}\,\si{\metre\per\second}$.
    \begin{gather*}
      \begin{align*}
        \myD && \myCalc
      \end{align*}
    \end{gather*}
    \onslide<12->{The flux of $\bv{F}$ out of $D$ is then}
    \[
      \onslide<12->{\oiint_{\partial D}\bv{F}\cdot d\bv{S}
      =} \onslide<13->{\myFlux+\oiint_{\bv{X}_2} \bv{F}\cdot d\bv{S}
      =} \onslide<15->{\sage{-flux(rho=1)+flux(rho=2)}\,\si{\metre\cubed\per\second}}
    \]
  \end{example}

\end{frame}


\section{The Divergence Theorem}
\subsection{Divergence}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\M}{4}
        \pgfmathsetmacro{\m}{3*\M/5}
        \pgfmathsetmacro{\pitch}{\M/6}

        \draw[beamblue] (\M, 0) arc (0:180:{\M} and {\pitch});
        \filldraw[bluefilldraw] (O) circle({\M} and {\m});

        \onslide<3->{
          \pgfmathsetseed{1}
          \pgfmathsetmacro{\t}{360*rand}
          \draw[thick, beamgreen, arrows={-stealth}]
          (O) -- ++({0.25*cos(\t)}, {0.25*sin(\t)});

          \pgfmathtruncatemacro{\nlayers}{9}
          \foreach \s in {1,2,...,\nlayers} {
            \pgfmathtruncatemacro{\N}{10*\s}
            \foreach \i in {1,2,...,\N} {
              \pgfmathsetmacro{\t}{360/\N*\i}
              \pgfmathsetmacro{\k}{65*rand}
              \coordinate (p) at ({\M*\s*cos(\t)/(\nlayers)}, {\m*\s*sin(\t)/(\nlayers)});
              \draw[thick, beamgreen, arrows={-stealth}]
              (p) -- ++({0.25*cos(\t+\k)}, {0.25*sin(\t+\k)});
            }
          }
        }

        \filldraw[bluefilldraw] (O) circle({\M} and {\m});
        \draw[beamblue] (\M, 0) arc (0:-180:{\M} and {\pitch});

      \end{tikzpicture}
  }
  \begin{block}{Question}
    Suppose $\bv{F}$ is a vector field inside a domain $D$.
    \[
      \onslide<2->{\myD}
    \]
    \onslide<4->{Can information ``inside'' $D$ help calculate flux
      $\oiint_{\partial D}\bv{F}\cdot d\bv{S}$?}
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{divergence} of $\bv{F}\in\mathfrak{X}(\mathbb{R}^n)$ is the
    symbolic dot product
    \[
      \vdiv(\bv{F})
      = \nabla\cdot\bv{F}
      = \pdv{F_1}{x_1}+\pdv{F_2}{x_2}+\dotsb+\pdv{F_n}{x_n}
    \]
    \onslide<2->{Note that $\vdiv(\bv{F})$ is a \emph{scalar}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mysource}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \draw[beamgreen] (O) circle (2 and 3/2);

        \node at (O) {\stackanchor{source $\vdiv(\bv{F})>0$}{``net flow out''}};

        \pgfmathsetmacro{\myx}{2*cos(45)}
        \pgfmathsetmacro{\myy}{3*sin(45)/2}
        \draw[beamblue, ->] (2, 0) -- ++(1/2, 0);
        \draw[beamblue, ->] (\myx, \myy) -- ++(\myx/4, \myy*8/18);
        \draw[beamblue, ->] (0, 3/2) -- ++(0, 1/2);
        \draw[beamblue, ->] (-\myx, \myy) -- ++(-\myx/4, \myy*8/18);
        \draw[beamblue, ->] (-2, 0) -- ++(-1/2, 0);
        \draw[beamblue, ->] (-\myx, -\myy) -- ++(-\myx/4, -\myy*8/18);
        \draw[beamblue, ->] (0, -3/2) -- ++(0, -1/2);
        \draw[beamblue, ->] (\myx, -\myy) -- ++(\myx/4, -\myy*8/18);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\mysink}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \draw[red] (O) circle (2 and 3/2);

        \node at (O) {\stackanchor{sink $\vdiv(\bv{F})<0$}{``net flow in''}};

        \pgfmathsetmacro{\myx}{2*cos(45)}
        \pgfmathsetmacro{\myy}{3*sin(45)/2}
        \draw[beamblue, <-] (2, 0) -- ++(1/2, 0);
        \draw[beamblue, <-] (\myx, \myy) -- ++(\myx/4, \myy*8/18);
        \draw[beamblue, <-] (0, 3/2) -- ++(0, 1/2);
        \draw[beamblue, <-] (-\myx, \myy) -- ++(-\myx/4, \myy*8/18);
        \draw[beamblue, <-] (-2, 0) -- ++(-1/2, 0);
        \draw[beamblue, <-] (-\myx, -\myy) -- ++(-\myx/4, -\myy*8/18);
        \draw[beamblue, <-] (0, -3/2) -- ++(0, -1/2);
        \draw[beamblue, <-] (\myx, -\myy) -- ++(\myx/4, -\myy*8/18);

      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    Points can be classified as \emph{sources} or \emph{sinks} of $\bv{F}$.
    \begin{align*}
      \onslide<2->{\mysource} && \onslide<3->{\mysink}
    \end{align*}
  \end{definition}

\end{frame}


\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Divergence Theorem]
    $\displaystyle\iiint_{D}\vdiv(\bv{F})\,dV=\oiint_{\partial D}\bv{F}\cdot d\bv{S}$
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDivergence}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\vdiv(\bv{F})}$};
      \onslide<2->{
        \node[overlay, below left= 4mm and -3mm of a] (text) {$\dfrac{1}{\operatorname{time}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mydV}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}dV}$};
      \onslide<3->{
        \node[overlay, below left= 14mm and 0mm of a] (text) {$\operatorname{distance}^3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\bv{F}}$};
      \onslide<4->{
        \node[overlay, below right= 8mm and 0mm of a] (text) {$\dfrac{\operatorname{distance}}{\operatorname{time}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\mydS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{beamgreen}d\bv{S}}$};
      \onslide<5->{
        \node[overlay, below right= 3mm and 0mm of a] (text) {$\operatorname{distance}^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units helps make sense of the divergence theorem.
    \[
      \iiint_{D} \myDivergence\,\mydV
      =
      \oiint_{\partial D}\myF\cdot\mydS
      \onslide<6->{= \textnormal{flux out of }D}
    \]
  \end{block}

\end{frame}


\subsection{Examples}
\begin{sagesilent}
  var('x y z')
  F = vector([-4*y*z, 4*x*z, -3*z])
  divF = F.div([x, y, z])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , scale=4/5
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\Ra}{2}
        \pgfmathsetmacro{\ra}{\Ra/6}
        \pgfmathsetmacro{\ta}{30}

        \pgfmathsetmacro{\Rb}{1}
        \pgfmathsetmacro{\rb}{\Rb/6}
        \pgfmathsetmacro{\tb}{60}

        \draw[beamblue] (\Ra, 0) arc(0:180:{\Ra} and {\ra});
        \filldraw[bluefilldraw] (O) circle(\Ra);

        \draw[beamblue] (\Rb, 0) arc(0:180:{\Rb} and {\rb});
        \filldraw[bluefilldraw] (O) circle(\Rb);

        \filldraw[bluefilldraw] (O) circle(\Rb);
        \draw[beamblue] (\Rb, 0) arc(0:-180:{\Rb} and {\rb});

        \filldraw[bluefilldraw] (O) circle(\Ra);
        \draw[beamblue] (\Ra, 0) arc(0:-180:{\Ra} and {\ra});

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myDiv}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<1->{red}\sage{divF}}$};
      \onslide<1->{
        \node[overlay, below right= 0mm and 0mm of a] (text) {$\vdiv(\bv{F})=\pdv*{\Set{\sage{F[0]}}}{x}+\pdv*{\Set{\sage{F[1]}}}{y}+\pdv*{\Set{\sage{F[2]}}}{z}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    Let $D$ be $1\leq\rho\leq2$ and consider
    $\bv{F}=\sage{F}\,\si{\metre\per\second}$.
    \[
      \myD
    \]
    \onslide<2->{According to the divergence theorem, the flux out of $D$ is}
    \[
      \onslide<3->{\oiint_{\partial D}\bv{F}\cdot d\bv{S}
        =} \onslide<4->{\iiint_{D}\myDiv\,dV
        =} \onslide<5->{-3\cdot\volume(D)
        =} \onslide<6->{-28\,\pi\,\si{\metre\cubed\per\second}}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z r theta')
  F = vector([x*y, 2*x*z, -2*z^2])
  divF = F.div([x, y, z])
  divFs = divF(x=r*cos(theta), y=r*sin(theta))
  f = divFs*r
  flux = integral(integral(integral(f, z, r, 1), r, 0, 1), theta, 0, 2*pi)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\r}{\R/6}

        \pgfmathsetmacro{\H}{\R}

        \filldraw[bluefilldraw]
        (O) --
        (\R, \H) arc(0:180:{\R} and {\r})
        --
        (O) --cycle;

        \filldraw[bluefilldraw]
        (O) --
        (\R, \H) arc(0:-180:{\R} and {\r})
        --
        (O) --cycle;

        \filldraw[bluefilldraw]
        (0, \H) circle({\R} and {\r});

        \draw[<-, thick, red, shorten <=2pt]
        (\R, \H) -| ++(1/2, 1mm) node[above] {$z=1$};

        \draw[<-, thick, red, shorten <=2pt]
        (\R/3, \H/3) -| ++(1, -1mm) node[below] {$z=\sqrt{x^2+y^2}$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myCalc}{
    \begin{aligned}
      \bv{F} &= \sage{F}\,\si{\metre\per\second} \\
      \onslide<2->{\vdiv(\bv{F})} &\onslide<2->{=} \onslide<2->{\pdv*{\Set{\sage{F[0]}}}{x}+\pdv*{\Set{\sage{F[1]}}}{y}+\pdv*{\Set{\sage{F[2]}}}{z}} \\
      &\onslide<2->{=} \onslide<3->{\sage{divF}}
    \end{aligned}
  }
  \begin{example}
    Consider the depicted solid cone $D$ and the vector field $\bv{F}$ given by
    \begin{gather*}
      \begin{align*}
        \myD && \myCalc
      \end{align*}
    \end{gather*}
    \onslide<4->{According to the divergence theorem, the flux out of $D$ is}
    \begin{gather*}
      \onslide<4->{\oiint_{\partial D}\bv{F}\cdot d\bv{S}
        =} \onslide<5->{\iiint_{D}\sage{divF}\,dV
        =} \onslide<6->{\int_{0}^{2\,\pi}\int_{0}^{1}\int_{r}^{1}(\sage{divFs})\cdot r\,dz\,dr\,d\theta
        =} \onslide<7->{\sage{flux}\,\si{\metre\cubed\per\second}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector((-2*y*z, -y**2, z))
  divF = F.div([x, y, z])
  divFs = divF(x=r*cos(theta), y=r*sin(theta))
  f = divFs*r
  flux = integral(integral(integral(f, z, 0, 4-r**2), r, 0, 2), theta, 0, 2*pi)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\r}{\R/6}
        \pgfmathsetmacro{\H}{\R*\R}

        \filldraw[bluefilldraw]
        (-\R, 0) parabola[bend at end]
        (0, \H) parabola
        (\R, 0) arc (0:180:{\R} and {\r})
        -- cycle;

        \filldraw[bluefilldraw]
        (O) circle ({\R} and {\r});

        \filldraw[bluefilldraw]
        (-\R, 0) parabola[bend at end]
        (0, \H) parabola
        (\R, 0) arc (0:-180:{\R} and {\r})
        -- cycle;

        \draw[<-, red, overlay, thick, shorten <=2pt]
        (0, \H) |- ++(1/2, 3mm) node[right] {$z=4-x^2-y^2$};

        \draw[<-, red, overlay, thick, shorten <=2pt]
        (\R, 0) -- ++(1/3, 0) node[right] {$z=0$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myCalc}{
    \begin{aligned}
      \bv{F} &= \sage{F}\,\si{\metre\per\second} \\
      \onslide<2->{\vdiv(\bv{F})} &\onslide<2->{=} \onslide<2->{\pdv*{\Set{\sage{F[0]}}}{x}+\pdv*{\Set{\sage{F[1]}}}{y}+\pdv*{\Set{\sage{F[2]}}}{z}} \\
      &\onslide<2->{=} \onslide<3->{\sage{divF}}
    \end{aligned}
  }
  \begin{example}
    Consider the solid paraboloid $D$ and the vector field $\bv{F}$ given by
    \begin{gather*}
      \begin{align*}
        \myD && \myCalc
      \end{align*}
    \end{gather*}
    \onslide<4->{According to the divergence theorem, the flux out of $D$ is}
    \begin{gather*}
      \onslide<4->{\oiint_{\partial D}\bv{F}\cdot d\bv{S}
        =} \onslide<5->{\iiint_{D}\sage{divF}\,dV
        =} \onslide<6->{\int_{0}^{2\,\pi}\int_{0}^{2}\int_{0}^{4-r^2}(\sage{divFs})\cdot r\,dz\,dr\,d\theta
        =} \onslide<7->{\sage{flux}\,\si{\metre\cubed\per\second}}
    \end{gather*}
  \end{example}

\end{frame}

\end{document}

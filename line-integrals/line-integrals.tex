\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{Line Integrals}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Scalar Line Integrals}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Theme}
    Consider a wire $\mathcal{W}$ in $\mathbb{R}^n$ connecting endpoints $P$ and
    $Q$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \coordinate (P) at (-4, 0);
        \coordinate (Q) at (4, 1);

        \begin{scope}[
          decoration={
            , markings
            , mark=at position 0.2 with \coordinate (Ca);
            , mark=at position 0.4 with \coordinate (Cb);
            , mark=at position 0.6 with \coordinate (Cc);
            , mark=at position 0.8 with \coordinate (Cd);
          }
          ]
          \draw[postaction={decorate}, beamblue] plot [smooth]
          coordinates {(P) (-1, 2) (1, 0) (Q)};
        \end{scope}

        \node[myDot, label=left:{$P$}] at (P) {};
        \node[myDot, label=right:{$Q$}] at (Q) {};


        \onslide<3->
        \node[overlay, red, below left= 1mm and 3mm of P]
        (text) {$\scriptstyle f(P)=2\,\si{\kg\per\metre}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P.south) |- (text.east);

        \onslide<4->
        \node[overlay, red, below= 5mm of Q]
        (text) {$\scriptstyle f(Q)=13\,\si{\kg\per\metre}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (Q.south) -- (text.north);

        \onslide<5->
        \node[myDot, label=below:{$P_1$}] at (Ca) {};
        \node[overlay, red, above left= 1mm and 10mm of Ca]
        (text) {$\scriptstyle f(P_1)=3\,\si{\kg\per\metre}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (Ca.north) |- (text.east);

        \onslide<6->
        \node[myDot, label=below:{$P_2$}] at (Cb) {};
        \node[overlay, red, right= 6mm of Cb]
        (text) {$\scriptstyle f(P_2)=7\,\si{\kg\per\metre}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (Cb.east) -- (text.west);

        \onslide<7->
        \node[myDot, label=below:{$P_3$}] at (Cc) {};
        \node[overlay, red, left= 6mm of Cc]
        (text) {$\scriptstyle f(P_3)=5\,\si{\kg\per\metre}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (Cc.west) -- (text.east);

        \onslide<8->
        \node[myDot, label=below:{$P_4$}] at (Cd) {};
        \node[overlay, red, above= 5mm of Cd]
        (text) {$\scriptstyle f(P_4)=9\,\si{\kg\per\metre}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (Cd.north) -- (text.south);

      \end{tikzpicture}
    \]
    \onslide<2->{Suppose $f\in\mathscr{C}(\mathbb{R}^n)$ measures density
      (\SI{}{\kg\per\metre}) at every point of $\mathcal{W}$.}
  \end{block}

  \begin{definition}<9->
    The \emph{scalar line integral of $f$ along $\mathcal{W}$} is
    \[
      \int_{\mathcal{W}} f\, ds=\textnormal{mass of $\mathcal{W}$ (in \SI{}{\kg})}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{scalar line integral} of $f\in\mathscr{C}(\mathbb{R}^n)$ along
    $\bv{x}:[a, b]\to\mathbb{R}^n$ is
    \[
      \int_{\bv{x}} f\,{\color<4->{red}ds} = \int_a^b f(\bv{x}(t))\cdot{\color<6->{beamgreen}\norm{\bv{x}^\prime(t)}\,dt}
    \]
    \onslide<2->{Here, we adopt the notation ${\color<3->{red}ds}={\color<5->{beamgreen}\norm{\bv{x}^\prime(t)}\,dt}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}f(\bv{x}(t))}$};
      \onslide<2->{
        \node[overlay, below left = 6mm and -4mm of a] (text)
        {$\dfrac{\textnormal{mass units}}{\textnormal{distance unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myNorm}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamblue}\norm{\bv{x}^\prime(t)}}$};
      \onslide<3->{
        \node[overlay, below = 6mm of a] (text)
        {$\dfrac{\textnormal{distance units}}{\textnormal{time unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \newcommand{\mydt}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamgreen}dt}$};
      \onslide<4->{
        \node[overlay, below right = 3mm and 1mm of a] (text)
        {$\textnormal{time unit}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units allows us to interpret scalar line integrals.
    \[
      \int_{\bv{x}}f\,ds
      = \int_a^b \myf\cdot\myNorm\,\mydt
      = \onslide<5->{\textnormal{mass of the curve}}
    \]
  \end{block}

\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y t')
  r = vector([cos(t), sin(t)])
  f = x**2*y
  a, b = 0, pi
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myUsub}{
    \begin{array}{rclrcl}
      \onslide<6->{{\color{red}u}}       &\onslide<6->{{\color{red}=}}      & \onslide<7->{{\color{red}\cos(t)}}            & \onslide<12->{{\color{beamgreen}u(\pi)}} & \onslide<12->{{\color{beamgreen}=}} & \onslide<13->{{\color{beamgreen}-1}} \\
      \onslide<9->{{\color{beamblue}du}} &\onslide<9->{{\color{beamblue}=}} & \onslide<10->{{\color{beamblue}-\sin(t)\,dt}} & \onslide<15->{{\color{beamgreen}u(0)}}   & \onslide<15->{{\color{beamgreen}=}} & \onslide<16->{{\color{beamgreen}1}}
    \end{array}
  }
  \newcommand{\myCirc}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        % \draw[<->] (-1.5, 0) -- (1.5, 0);
        % \draw[<->] (0, -.5) -- (0, 1.5);

        \draw[beamblue] plot[smooth, domain=0:180] ({cos(\x)}, {sin(\x)});

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Suppose $f$ measures density (in \SI{}{\kg\per\metre}) along $\bv{x}(t)$
    where
    \begin{align*}
      f(x, y) &= \sage{f} & \bv{x}(t) &= \sage{r} && \myCirc
    \end{align*}
    and $0\leq t\leq\pi$. \onslide<2->{The mass of the curve is}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\int_{\bv{x}} f\,ds}
        &\onslide<2->{=} \onslide<3->{\int_{\sage{a}}^{\sage{b}}f(\bv{x}(t))\cdot\norm{\bv{x}^\prime(t)}\,dt}                                                                                            \\
        &\onslide<3->{=} \onslide<4->{\int_{\sage{a}}^{\sage{b}}\sage{f}\,\norm{\sage{r.diff(t)}}\,dt}                                                                                                   \\
        &\onslide<4->{=} \onslide<5->{\int_{{\color<17->{beamgreen}\sage{a}}}^{{\color<14->{beamgreen}\sage{b}}}{\color<8->{red}\cos^{{\color{black}2}}(t)\,}{\color<11->{beamblue} \sin(t)\,dt}} && \onslide<6->{\myUsub} \\
        &\onslide<5->{=} \onslide<18->{\int_{1}^{-1}-u^2\,du}                                                                                                                                              \\
        &\onslide<18->{=} \onslide<19->{\sfrac{2}{3}\,\si{\kg}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z t')
  p = (-2, -2, 1)
  q = (-1, -2, -1)
  P = vector(p)
  Q = vector(q)
  f = (x+2*z)*exp(x-y)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myParts}{
    \begin{array}{rclrcl}
      \onslide<9->{{\color{red}u}}        & \onslide<9->{{\color{red}=}}       & \onslide<10->{{\color{red}t}}                  & \onslide<14->{{\color<20->{beamgreen}du}} & \onslide<14->{{\color<20->{beamgreen}=}} & \onslide<15->{{\color<20->{beamgreen}{dt}}}                \\
      \onslide<9->{{\color{beamgreen}dv}} & \onslide<9->{{\color{beamgreen}=}} & \onslide<12->{{\color{beamgreen}e^t\,dt}} & \onslide<16->{{\color<22->{beamblue}v}}   & \onslide<16->{{\color<22->{beamblue}=}}  & \onslide<17->{{\color<22->{beamblue}e^t}}
    \end{array}
  }
  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\sage{P}}$};
      \onslide<2->{
        \node[overlay, above left= 1mm and -3mm of a] (text) {$\bv{P}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myPQ}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}\sage{Q-P}}$};
      \onslide<3->{
        \node[overlay, above left= 0mm and -3mm of a] (text) {$\vv{PQ}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\mySeg}{
    \begin{aligned}
      \bv{x}(t)
      &= \myP+t\cdot\myPQ \\
      &\onslide<4->{= \sage{P+t*(Q-P)}}
    \end{aligned}
  }
  \newcommand{\myLine}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , rotate=15
        , xscale=2.5
        ]

        \coordinate (P) at (0, 0);
        \coordinate (Q) at (1, 0);

        \draw[beamblue] (P) -- (Q);
        \node[myDot, label=below:{$P$}] at (P) {};
        \node[myDot, label=below:{$Q$}] at (Q) {};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    The segment connecting $P\sage{p}$ to $Q\sage{q}$ is
    \begin{align*}
      \mySeg && \myLine
    \end{align*}
    for $0\leq t\leq 1$. \onslide<5->{Suppose density is $f(x, y, z)=(x+2\,z)\cdot e^{x-y}$.}
    \begin{gather*}
      \begin{align*}
        \onslide<6->{\int_{\bv{x}}f\,ds} &\onslide<6->{=}  \onslide<7->{\int_0^1 (x+2\,z)\cdot e^{x-y}\cdot\norm{\sage{Q-P}}\,dt}                                                                                                                                                                 \\
                                         &\onslide<7->{=}  \onslide<8->{\sqrt{5}\int_0^1-3\,{\color<11->{red}t}\cdot {\color<13->{beamgreen}e^t\,dt}}                                                                                                                           && \myParts  \\
                                         &\onslide<8->{=}  \onslide<18->{-3\sqrt{5}\cdot\Set*{\left.{\color<19->{red}t}\,{\color<23->{beamblue}e^t}\right\rvert_{0}^{1}-\int_0^1 {\color<23->{beamblue}e^t}\,{\color<21->{beamgreen}dt}}}              \\
                                         &\onslide<18->{=} \onslide<24->{-3\sqrt{5}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\section{Vector Line Integrals}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{F}$};
      \onslide<+->{
        \node[overlay, above right= 1mm and 4mm of a] (text) {``Newton'' $\si{\newton}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myd}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{d}$};
      \onslide<+->{
        \node[overlay, above left= 1mm and 4mm of a] (text) {``metre'' $\si{\metre}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myv}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\bv{v}$};
      \onslide<+->{
        \node[overlay, below right= 0mm and 4mm of a] (text) {$\si{\metre\per\second}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myW}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\work=\bv{F}\cdot\bv{d}$};
      \onslide<+->{
        \node[overlay, right= 6mm of a] (text) {``joule'' $\si{\joule}=\si{\newton\cdot\metre}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -- (text.west);
      }
    }
  }
  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\power=\bv{F}\cdot\bv{v}$};
      \onslide<+->{
        \node[overlay, right= 3mm of a] (text) {``watt'' $\si{\watt}=\si{\newton\cdot\metre\per\second=\joule\per\second}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -- (text.west);
      }
    }
  }
  \begin{definition}<+->
    The \emph{work} of a force $\myF$ acting on a particle with displacement
    $\myd$ is
    \[
      \myW
    \]
    \onslide<+->The \emph{power} of $\bv{F}$ along a velocity $\myv$ is
    \[
      \myP
    \]
    \onslide<+->Power is also called \emph{work density}.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Theme}
    Consider a particle's path $\mathcal{P}$ in $\mathbb{R}^n$ from $P$ to $Q$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \coordinate (c1) at (-4, 0);
        \coordinate (c2) at (-1, 2);
        \coordinate (c3) at (1, 0);
        \coordinate (c4) at (4, 1);

        \path[name path={curve}] plot[smooth]
        coordinates {(c1) (c2) (c3) (c4)};

        \begin{scope}[
          decoration={
            , markings
            , mark=at position 0.2 with \coordinate (p1);
            , mark=at position 0.4 with \coordinate (p2);
            , mark=at position 0.6 with \coordinate (p3);
            , mark=at position 0.8 with \coordinate (p4);
          }
          ]
          \draw[postaction={decorate}, blue] plot [smooth]
          coordinates {(c1) (c2) (c3) (c4)};
        \end{scope}

        \path[name path={circle1}] (p1) circle (0.1);
        \node[
        , coordinate
        , name intersections={of={circle1} and curve}
        ]
        (i1) at  (intersection-1) {};
        \begin{scope}[shift={(p1)}]

          \onslide<14->{
          \draw[red, ->]
          (p1) -- ($ (p1)!1cm!(i1) $)
          node[pos=0.75, above, sloped]
          {$\scriptstyle\bv{x}^\prime$};
          }

          \onslide<9->{
          \draw[beamgreen, ->] (p1) -- ++(1, 0)
          node[below] {$\scriptstyle\bv{F}$};
          }

          \onslide<3->{
          \node[
          , myDot
          % , label=below:{$P_1$}
          ] at (p1) {};
          }
        \end{scope}

        \path[name path={circle2}] (p2) circle (0.1);
        \node[
        , coordinate
        , name intersections={of={circle2} and curve}
        ]
        (i2) at  (intersection-2) {};
        \begin{scope}[shift={(p2)}]
          \onslide<15->{
          \draw[red, ->]
          (p2) -- ($ (p2)!1cm!(i2) $)
          node[pos=0.75, above, sloped]
          {$\scriptstyle\bv{x}^\prime$};
          }

          \onslide<10->{
          \draw[beamgreen, ->]
          (p2) -- ++(3/4, 3/8) node[pos=0.65, above, sloped]
          {$\scriptstyle\bv{F}$};
          }

          \onslide<4->{
          \node[
          , myDot
          % , label=below:{$P_2$}
          ] at (p2) {};
          }
        \end{scope}

        \path[name path={circle3}] (p3) circle (0.1);
        \node[
        , coordinate
        , name intersections={of={circle3} and curve}
        ]
        (i3) at  (intersection-2) {};
        \begin{scope}[shift={(p3)}]
          \onslide<16->{
          \draw[red, ->]
          (p3) -- ($ (p3)!1cm!(i3) $)
          node[right]
          {$\scriptstyle\bv{x}^\prime$};
          }

          \onslide<11->{
          \draw[beamgreen, ->] (p3) -- ++(-1, -0.25)
          node[left] {$\scriptstyle \bv{F}$};
          }

          \onslide<5->{
          \node[
          , myDot
          % , label=right:{$P_3$}
          ] at (p3) {};
          }
        \end{scope}


        \path[name path={circle4}] (p4) circle (0.1);
        \node[
        , coordinate
        , name intersections={of={circle4} and curve}
        ]
        (i4) at  (intersection-1) {};
        \begin{scope}[shift={(p4)}]
          \onslide<17->{
          \draw[red, ->]
          (p4) -- ($ (p4)!1cm!(i4) $)
          node[pos=1.10, below, sloped]
          {$\scriptstyle\bv{x}^\prime$};
          }

          \onslide<12->{
          \draw[beamgreen, ->] (p4) -- ++(1/3, 1)
          node[right] {$\scriptstyle \bv{F}$};
          }

          \onslide<6->{
          \node[
          , myDot
          % , label=below:{$P_4$}
          ] at (p4) {};
          }
        \end{scope}

        \onslide<2->{
          \node[myDot, label=left:{$P$}] at (c1) {};
        }

        \onslide<7->{
          \node[myDot, label=right:{$Q$}] at (c4) {};
        }

      \end{tikzpicture}
    \]
    \onslide<8->{Suppose $\bv{F}\in\mathfrak{X}(\mathbb{R}^n)$ exerts a force
      (in \SI{}{\newton}) at every point of $\mathcal{P}$.}
  \end{block}

  \begin{definition}<13->
    The \emph{vector line integral of $\bv{F}$ along $\mathcal{P}$} is
    \[
      \int_{\mathcal{P}}\bv{F}\cdot d\bv{s} = \textnormal{work (in \SI{}{\joule})}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{vector line integral of $\bv{F}\in\mathfrak{X}(\mathbb{R}^n)$
      along $\bv{x}:[a, b]\to\mathbb{R}^n$} is
    \[
      \int_{\bv{x}}\bv{F}\cdot {\color<4->{red}d\bv{s}}
      =
      \int_a^b\bv{F}(\bv{x}(t))\cdot{\color<6->{beamgreen}\bv{x}^\prime(t)\,dt}
    \]
    \onslide<2->{Here, we adopt the notation ${\color<3->{red}d\bv{s}}={\color<5->{beamgreen}\bv{x}^\prime(t)\,dt}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\bv{F}(\bv{x}(t))}$};
      \onslide<2->{
        \node[overlay, below left = 6mm and -4mm of a] (text)
        {force units};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myV}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamblue}\bv{x}^\prime(t)}$};
      \onslide<3->{
        \node[overlay, below = 6mm of a] (text)
        {$\dfrac{\textnormal{distance units}}{\textnormal{time unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \newcommand{\mydt}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamgreen}dt}$};
      \onslide<4->{
        \node[overlay, below right = 3mm and 1mm of a] (text)
        {$\textnormal{time unit}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units allows us to interpret vector line integrals.
    \[
      \int_{\bv{x}}\bv{F}\cdot d\bv{s}
      = \int_a^b \myF\cdot\myV\,\mydt
      = \onslide<5->{\textnormal{work done}}
    \]
  \end{block}


\end{frame}


\subsection{Examples}
\begin{sagesilent}
  var('x y t')
  r = vector([t, t**2])
  v = r.diff(t)
  F = vector([1/(x*y), 1/(x+y)])
  F0 = F(x=r[0], y=r[1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      \bv{F}(x, y) &= \sage{F} & \bv{x}(t) &= \sage{r} && 1\leq t\leq4
    \end{align*}
    \onslide<2->{The work is}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\int_{\bv{x}}\bv{F}\cdot d\bv{s}} &\onslide<2->{=} \onslide<3->{\int_1^4\bv{F}(\bv{x}(t))\cdot\bv{x}^\prime(t)\,dt} & &\onslide<6->{=} \onslide<7->{\int_1^4\sage{F0[0]*v[0]}+\frac{2}{t+1}\,dt} \\
                                                       &\onslide<3->{=} \onslide<4->{\int_1^4\sage{F}\cdot\sage{v}\,dt}                  & &\onslide<7->{=} \onslide<8->{\Set*{-\frac{1}{2\,t^2}+2\,\log(t+1)}_1^4}      \\
                                                       &\onslide<4->{=} \onslide<5->{\int_1^4\sage{F[0]*v[0]}+\sage{F[1]*v[1]}\,dt}      & &\onslide<8->{=} \onslide<9->{\frac{15}{32}+2\,\log(5)-2\,\log(2)}         \\
                                                       &\onslide<5->{=} \onslide<6->{\int_1^4\sage{F0[0]*v[0]}+\sage{F0[1]*v[1]}\,dt}    & &
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z t')
  F = vector([y*z, x*z, x*y])
  r = vector([t, t**2, t**3])
  a, b = 0, 1
  v = r.diff(t)
  F0 = F(x=r[0], y=r[1], z=r[2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      \bv{F}(x, y, z) &= \sage{F} & \bv{x}(t) &= \sage{r} && \sage{a}\leq t\leq\sage{b}
    \end{align*}
    \onslide<2->{The work is}
    \begin{align*}
      \onslide<2->{\int_{\bv{x}}\bv{F}\cdot d\bv{s}} &\onslide<2->{=} \onslide<3->{\int_0^1\bv{F}(\bv{x}(t))\cdot\bv{x}^\prime(t)\,dt}                & &\onslide<6->{=} \onslide<7->{\int_0^1\sage{F0*v}\,dt}    \\
                                                     &\onslide<3->{=} \onslide<4->{\int_0^1\sage{F}\cdot\sage{v}\,dt}                                 & &\onslide<7->{=} \onslide<8->{\left. t^6\right\rvert_0^1} \\
                                                     &\onslide<4->{=} \onslide<5->{\int_0^1\sage{F0}\cdot\sage{v}\,dt}                                & &\onslide<8->{=} \onslide<9->{1}                          \\
                                                     &\onslide<5->{=} \onslide<6->{\int_0^1\sage{F0[0]*v[0]}+\sage{F0[1]*v[1]}+\sage{F0[2]*v[2]}\,dt} & &
    \end{align*}
  \end{example}
\end{frame}

\subsection{Differential Form Notation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mydx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}dx}$};
      \onslide<2->{
        \node[overlay, below right= 1mm and -1mm of a] (text) {$x_1^\prime\,dt$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\mydy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamblue}dy}$};
      \onslide<3->{
        \node[overlay, above right= 1mm and -1mm of a] (text) {$x_2^\prime\,dt$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\mydz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{beamgreen}dz}$};
      \onslide<4->{
        \node[overlay, below right= 1mm and -1mm of a] (text) {$x_3^\prime\,dt$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{definition}
    For $\bv{F}=\langle F_1, F_2, F_3\rangle$ it is common to write
    \[
      \int_{\bv{x}}\bv{F}\cdot d\bv{s}
      =
      \int_{\bv{x}} F_1\,\mydx + F_2\,\mydy + F_3\,\mydz
    \]
    \onslide<5->{The expression $F_1\,dx + F_2\,dy + F_3\,dz$ is called a
      \emph{differential form}.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([x**2, x*y, 1])
  r = vector([t, t**2, 1])
  v = r.diff(t)
  F0 = F(x=r[0], y=r[1], z=r[2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\bv{x}(t)=\sage{r}$ for $0\leq t\leq 1$. \onslide<2->{Then}
    \begin{align*}
      \onslide<2->{\int_{\bv{x}}\sage{F[0]}\,dx+\sage{F[1]}\,dy+\sage{F[2]}\,dz} &\onslide<2->{=} \onslide<3->{\int_0^1\sage{F}\cdot d\bv{s}}     \\
                                                                                 &\onslide<3->{=} \onslide<4->{\int_0^1\sage{F}\cdot\sage{v}\,dt} \\
                                                                                 &\onslide<4->{=} \onslide<5->{\int_0^1\sage{F*v}\,dt}            \\
                                                                                 &\onslide<5->{=} \onslide<6->{\int_0^1\sage{F0*v}\,dt}           \\
                                                                                 &\onslide<6->{=} \onslide<7->{\sage{integral(F0*v, t, 0, 1)}}
    \end{align*}
  \end{example}

\end{frame}



\end{document}

\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}

\title{Parameterized Curves}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Parameterized Curves}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{parameterized curve in $\mathbb{R}^n$} is a function
    $\bv{x}:\mathbb{R}\to\mathbb{R}^n$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('t')
  x = vector([cos(t), sin(t)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The unit circle is parameterized by $\bv{x}(t)=\sage{x}$. \pause
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-4, 0) -- (4, 0);
        \draw[<->] (0, -3) -- (0, 3);

        \draw[beamblue] (O) circle (2);
        \draw[beamblue, ->] plot[domain=0:45]
        ({2*cos(\x)}, {2*sin(\x)});

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  var('t')
  x = vector([cos(3*t), sin(2*t)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    We call $\bv{x}(t)=\sage{x}$ a \emph{Lissajous curve}. \pause
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-4, 0) -- (4, 0);
        \draw[<->] (0, -3) -- (0, 3);

        \draw[beamblue] plot[samples=200, domain=0:360]
        ({2*cos(3*\x)}, {2*sin(2*\x)});
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  var('t')
  x = vector([cos(t), sin(t), t])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The parameterization $\bv{x}(t)=\sage{x}$ describes a \pause
    \emph{helix}.\pause
    \[
      \tdplotsetmaincoords{70}{135}
      \begin{tikzpicture}[
        , tdplot_main_coords
        , ultra thick
        , scale=1
        , line cap=round
        , line join=round
        ]
        % \draw[thick,<->] (-2,0,0) -- (2,0,0) node[anchor=north east]{$x$};
        % \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
        % \draw[thick,<->] (0,0,-1) -- (0,0,2) node[anchor=south]{$z$};

        \newcommand{\myhelix}[3][beamblue, <->]{
          \draw[####1, domain=####2:####3, samples=500, variable=\t]
          plot ({cos(4*\t r)}, {sin(4*\t r)}, {\t/2});
        }

        \myhelix{0}{2*pi}
        \draw[<->] (0, 0, -1) -- (0, 0, 4) node[above] {$z$};

        \myhelix[beamblue]{0}{pi/12}
        \myhelix[beamblue]{pi/2}{10*pi/15}
        \myhelix[beamblue]{pi}{10*pi/9}
        \myhelix[beamblue]{20*pi/13}{20*pi/12}
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\subsection{Velocity}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}<+->
    Consider a parameterized curve $\bv{x}:\mathbb{R}\to\mathbb{R}^n$ of the
    form
    \[
      \bv{x}(t)
      =
      \langle
      x_1(t), x_2(t), \dotsc, x_n(t)
      \rangle
    \]
    \onslide<+->{The Jacobian derivative is then}
    \[
      \onslide<+->{D\bv{x}=}
      \onslide<+->{\bv{x}^\prime(t)=}
      \onslide<+->{
        \left[
          \begin{array}{c}
            x_1^\prime(t) \\
            x_2^\prime(t) \\
            \vdots        \\
            x_n^\prime(t)
          \end{array}
        \right]
      }
    \]
    \onslide<+->{This vector $\bv{x}^\prime(t)$ is called the \emph{velocity vector}.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}<+->
    The \emph{velocity vector} of a curve $\bv{x}(t)$ is
    \[
      \bv{x}^\prime(t) = \langle x_1^\prime(t), x_2^\prime(t), \dotsc, x_n^\prime(t)\rangle
    \]
    \onslide<+->{The \emph{speed} of the curve is $\norm{\bv{x}^\prime(t)}$.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('t')
  x = vector([cos(3*t), sin(2*t)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the Lissajous curve and its velocity vector
    \begin{align*}
      \bv{x}(t) &= \sage{x} & \bv{x}^\prime(t) &= \onslide<2->{\sage{x.diff(t)}}
    \end{align*}
    \onslide<3->{This gives
      $\bv{x}(\sfrac{\pi}{12})=\langle\sfrac{\sqrt{2}}{2}, \sfrac{1}{2}\rangle$
      and
      $\bv{x}^\prime(\sfrac{\pi}{12})=\langle-\sfrac{3}{\sqrt{2}},
      \sqrt{3}\rangle$.}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \onslide<4->{
          \draw[<->] (-3, 0) -- (3, 0);
          \draw[<->] (0, -2) -- (0, 2);
        }

        \pgfmathsetmacro{\myrta}{sqrt(2)}
        \pgfmathsetmacro{\myrtb}{sqrt(3)}

        \coordinate (p) at (\myrta/2, 1/2);
        \coordinate (v) at (-3/\myrtb, \myrtb);

        \onslide<5->{
          \draw[beamblue] plot[samples=200, domain=0:360]
          ({cos(3*\x)}, {sin(2*\x)});
        }

        \onslide<7->{
          \draw[beamgreen, ->] (p) -- ++(v) node[left] {$\bv{x}^\prime(\sfrac{\pi}{12})$};
        }

        \onslide<6->{
          \node[myDot] at (p) {};

          \node[overlay, red, above right= 2mm and 6mm of p] (text) {$\bv{x}(\sfrac{\pi}{12})$};
          \draw[overlay, red, <-, thick, shorten <=2pt] (p.east) -| (text.south);
        }
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The local linearization of $\bv{x}(t)$ at $t=t_0$ is
    \[
      \bv{L}_{t_0}(t) = \bv{x}(t_0)+\bv{x}^\prime(t_0)\cdot(t-t_0)
    \]
    This is the \emph{line tangent to $\bv{x}(t)$ at $t=t_0$}.
  \end{block}

\end{frame}


\begin{sagesilent}
  var('t')
  x = vector([cos(3*t), sin(2*t)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The line tangent to our Lissajous curve at $t=\sfrac{\pi}{12}$ is
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \draw[<->] (-3, 0) -- (3, 0);
        \draw[<->] (0, -2) -- (0, 2);

        \pgfmathsetmacro{\myrta}{sqrt(2)}
        \pgfmathsetmacro{\myrtb}{sqrt(3)}

        \coordinate (p) at (\myrta/2, 1/2);
        \coordinate (v) at (-3/\myrtb, \myrtb);

        \draw[beamblue] plot[samples=200, domain=0:360]
        ({cos(3*\x)}, {sin(2*\x)});

        \onslide<2->{
          \draw[red, ->] (p) -- ++($ 1.25*(v) $);
          \draw[red, ->] (p) -- ++($ -1*(v) $) node[right] {$\bv{L}_{\sfrac{\pi}{12}}(t)$};
        }

        \draw[beamgreen, ->] (p) -- ++(v);% node[below left] {$\bv{x}^\prime(\sfrac{\pi}{12})$};

        \node[myDot] at (p) {};

        % \node[overlay, above right= 2mm and 6mm of p] (text) {$\bv{x}(\sfrac{\pi}{12})$};
        % \draw[overlay, <-, thick, shorten <=2pt] (p.east) -| (text.south);
      \end{tikzpicture}
    \]
    \onslide<3->{Here,
      $\bv{L}_{\sfrac{\pi}{12}}(t)=\bv{x}(\sfrac{\pi}{12})+\bv{x}^\prime(\sfrac{\pi}{12})\cdot(t-\sfrac{\pi}{12})$.}
  \end{example}

\end{frame}


\subsection{Acceleration}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Given position $x(t)$, velocity and acceleration are
    \begin{align*}
      \onslide<2->{v(t)} &\onslide<2->{=} \onslide<3->{x^\prime(t)} & \onslide<4->{a(t)} &\onslide<4->{=} \onslide<5->{x^{\prime\prime}(t)}
    \end{align*}
    \onslide<6->{Position, velocity, and acceleration are related by the formulas}
    \begin{align*}
      \onslide<6->{x(t)} &\onslide<6->{=} \onslide<7->{\int_{t_0}^t x^\prime(u)\,du + x(t_0)} & \onslide<8->{x^\prime(t)} &\onslide<8->{=} \onslide<9->{\int_{t_1}^tx^{\prime\prime}(u)\,du + x^\prime(t_1)}
    \end{align*}
    \onslide<10->{This is the idea behind the \emph{Fundamental Theorem of
        Calculus}.}
  \end{block}

\end{frame}


\begin{sagesilent}
  var('t u')
  a = 12*t-6
  vt0 = 1
  v0 = 3
  xt0 = 0
  x0 = -2
  inta = integral(a(t=u), u, vt0, t)
  v = inta+v0
  intv = integral(v(t=u), u, xt0, t)
  x = intv+x0
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myvelint}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\displaystyle\int_{\sage{vt0}}^{t}\sage{a(t=u)}\,du}$};
      \onslide<4->{
        \node[overlay, above right= 3mm and -8mm of a] (text) {$\int_{t_1}^t x^{\prime\prime}(u)\,du$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myvelinit}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{beamblue}\sage{v0}}$};
      \onslide<5->{
        \node[overlay, above right= 1mm and 3mm of a] (text) {$x^\prime(t_1)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myposint}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<9->{red}\displaystyle\int_{\sage{xt0}}^{t} \sage{v(t=u)}\,du}$};
      \onslide<9->{
        \node[overlay, below left= 2mm and -10mm of a] (text) {$\int_{t_0}^t x^\prime(u)\,du$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myposinit}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<10->{beamblue}\sage{abs(x0)}}$};
      \onslide<10->{
        \node[overlay, below right= 4mm and 3mm of a] (text) {$x(t_0)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the data
    \begin{align*}
      x(\sage{xt0}) &= \sage{x0}\,\si{\metre} & x^\prime(\sage{vt0}) &= \sage{v0}\,\si{\metre\per\second} & x^{\prime\prime}(t) &= \sage{a}\,\si{\metre\per\second\squared}
    \end{align*}
    \onslide<2->{Velocity is given by}
    \begin{gather*}
      \onslide<2->{x^{\prime}(t)%        =} \onslide<3->{\int_{\sage{vt0}}^{t}x^{\prime\prime}(u)\,du+x^\prime(\sage{vt0})
        =} \onslide<3->{\myvelint{\color<5->{beamblue}\ +\ }\myvelinit%        =} \onslide<4->{\sage{inta}+\sage{v0}
        =} \onslide<6->{\sage{v}\,\si{\metre\per\second}}
    \end{gather*}
    \onslide<7->{Position is then given by}
    \begin{gather*}
      \onslide<7->{x(t)%        =} \onslide<6->{\int_{\sage{xt0}}^{t} x^\prime(u)\,du+x(\sage{xt0})
        =} \onslide<8->{\myposint{\color<10->{beamblue}\ -\ }\myposinit%        =} \onslide<8->{\sage{intv}-\sage{abs(x0)}
        =} \onslide<11->{\sage{x}\,\si{\metre}}
    \end{gather*}
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{acceleration vector} of a curve $\bv{x}(t)$ is the vector $\bv{x}^{\prime\prime}(t)$.
  \end{definition}

  \begin{theorem}<2->
    Position, velocity, and acceleration are related by the formulas
    \begin{align*}
      \onslide<2->{\bv{x}(t)} &\onslide<2->{=} \onslide<3->{\int_{t_0}^t\bv{x}^\prime(u)\,du + \bv{x}(t_0)} & \onslide<4->{\bv{x}^\prime(t)} &\onslide<4->{=} \onslide<5->{\int_{t_1}^t\bv{x}^{\prime\prime}(u)\,du+\bv{x}^\prime(t_1)}
    \end{align*}
    \onslide<6->{Here, the integrals are taken coordinate-by-coordinate.}
  \end{theorem}

\end{frame}



\begin{sagesilent}
  var('t u')
  x0 = vector([0, 6])
  v0 = vector([4, -2])
  a0 = vector([0, -8])
  vt0 = 0
  xt0 = 2
  vint = integral(a0, u, vt0, t)
  v = vint+v0
  xint = integral(v(t=u), u, xt0, t)
  x = xint+x0
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myvint}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\left\langle\displaystyle \int_{\sage{vt0}}^t 0\,du, \int_{\sage{vt0}}^t -8\,du\right\rangle}$};
      \onslide<4->{
        \node[overlay, below left= 2mm and -10mm of a] (text) {$\int_{t_1}^t\bv{x}^{\prime\prime}(u)\,du$};
        \draw[<-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myvinit}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{beamblue}\sage{v0}}$};
      \onslide<5->{
        \node[overlay, below= 4mm of a] (text) {$\bv{x}^{\prime}(t_1)$};
        \draw[<-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \newcommand{\myposint}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<10->{red}\left\langle\displaystyle\int_{\sage{xt0}}^{t}\sage{v[0](t=u)}\,du, \int_{\sage{xt0}}^t\sage{v[1](t=u)}\,du\right\rangle}$};
      \onslide<10->{
        \node[overlay, below left= 2mm and -10mm of a] (text) {$\int_{t_0}^t\bv{x}^{\prime}(u)\,du$};
        \draw[<-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myposinit}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<11->{beamblue}\sage{x0}}$};
      \onslide<11->{
        \node[overlay, below= 4mm of a] (text) {$\bv{x}(t_0)$};
        \draw[<-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \begin{example}
    Consider a particle's path $\bv{x}(t)$ satisfying
    \begin{align*}
      \bv{x}(\sage{xt0}) &= \sage{x0}\,\si{\metre} & \bv{x}^\prime(\sage{vt0}) &= \sage{v0}\,\si{\metre\per\second} & \bv{x}^{\prime\prime}(t) &= \sage{a0}\,\si{\metre\per\second\squared}
    \end{align*}
    \onslide<2->{We can compute velocity $\bv{x}^\prime(t)$ coordinate-by-coordinate}
    \begin{gather*}
      \onslide<2->{\bv{x}^\prime(t)%        =} \onslide<3->{\int_{\sage{vt0}}^t \bv{x}^{\prime\prime}(u)\,du+\bv{x}^\prime(0)
        =} \onslide<3->{\myvint{\color<5->{beamblue}\ +\ }\myvinit%        =} \onslide<5->{\langle0, -t\rangle+\sage{v0}
        =} \onslide<6->{\sage{v}\,\si{\metre\per\second}}
    \end{gather*}
    \onslide<7->{We can then compute position $\bv{x}(t)$ with}
    \begin{gather*}
      \onslide<8->{\bv{x}(t)%        =} \onslide<9->{\int_{\sage{xt0}}^t\bv{x}^\prime(u)\,du+\bv{x}(0)
        =} \onslide<9->{\myposint{\color<11->{beamblue}\ +\ }\myposinit%        =} \onslide<11->{\sage{xint}+\sage{x0}
        =} \onslide<12->{\sage{x}\,\si{\metre}}
    \end{gather*}
  \end{example}

\end{frame}


\section{Arclength}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Recall the notions of \emph{displacement} and \emph{distance} over $a\leq t\leq b$.
    \begin{align*}
      \onslide<2->{\operatorname{displacement}} &\onslide<2->{=} \onslide<3->{\int_a^b\operatorname{velocity}\,dt} & \onslide<4->{\operatorname{distance}} &\onslide<4->{=} \onslide<5->{\int_a^b\operatorname{speed}\,dt}
    \end{align*}
    \onslide<6->{These equations can be written as}
    \begin{align*}
      \onslide<7->{x(b)-x(a)} &\onslide<7->{=} \onslide<8->{\int_a^b x^\prime(t)\,dt} & \onslide<9->{L(x)} &\onslide<9->{=} \onslide<10->{\int_a^b\abs{x^\prime(t)}\,dt}
    \end{align*}
    \onslide<11->{assuming $x(t)$ is our position function.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we compute the length of a curve?
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        % curve controls
        \coordinate (start) at (0, 0);
        \coordinate (step1) at (3, 2);
        \coordinate (step2) at (5, 0);
        \coordinate (end) at   (8, 1);

        \begin{scope}[
          decoration={
            , markings
            , mark=at position 0.01 with \arrow{<};
            , mark=at position 0.2 with \coordinate (xa);
            % , mark=at position 0.4 with \coordinate (P2);
            % , mark=at position 0.6 with \coordinate (P3);
            , mark=at position 0.8 with \coordinate (xb);
            , mark=at position 0.999 with \arrow{>};
          }
          ]
          \draw[beamblue, postaction={decorate}] plot [smooth]
          coordinates {(start) (step1) (step2) (end)} node[right] {$\bv{x}(t)$};
        \end{scope}

        \coordinate (nw) at ($ (xa)!(current bounding box.north west)!(xa) $);
        \coordinate (se) at ($ (xb)!(current bounding box.south east)!(xb) $);

        \begin{scope}
          \clip (nw) rectangle (se);
          \onslide<3->{\draw[red] plot [smooth] coordinates {(start) (step1) (step2) (end)};}
        \end{scope}

        \onslide<2->{
          \node[myDot, label=below:{$\bv{x}(a)$}] at (xa) {};
          \node[myDot, label=below:{$\bv{x}(b)$}] at (xb) {};
        }

      \end{tikzpicture}
    \]
    \onslide<4->{To find this length, we should integrate speed!}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{arclength} of $\bv{x}:[a, b]\to\mathbb{R}^n$ is
    \[
      L(\bv{x})
      = \int_a^b\norm{\bv{x}^\prime(t)}\,dt
      = \int_a^b\sqrt{[x_1^\prime(t)]^2+\dotsb+[x_n^\prime(t)]^2}\,dt
    \]
    assuming $\bv{x}(t)=\langle x_1(t), x_2(t), \dotsc, x_n(t)\rangle$.
  \end{definition}

\end{frame}


\subsection{Example}
\begin{sagesilent}
  var('t')
  x = vector([t*sin(t)+cos(t), -t*cos(t)+sin(t)])
  v = x.diff(t)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myInvolute}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=1/8
        ]

        \coordinate (O) at (0, 0);
        \onslide<7->{
          \draw[beamblue, ->] plot[samples=400, domain=0:6*pi] ({\x*sin(\x r)+cos(\x r)}, {-\x*cos(\x r)+sin(\x r)});
        }

        \coordinate (x0) at (1, 0);
        \coordinate (xb) at (-11*pi/2, -1);

        \coordinate (O) at (0, 0);
        \onslide<14->{
          \draw[red] plot[samples=400, domain=0:11*pi/2] ({\x*sin(\x r)+cos(\x r)}, {-\x*cos(\x r)+sin(\x r)});
        }

        \onslide<8->{
          \node[myDot, label=below:{$\bv{x}(0)$}] at (x0) {};
        }
        \onslide<9->{
          \node[myDot, label=left:{$\bv{x}(b)$}] at (xb) {};
        }

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myArc}{
    \onslide<10->{L(\bv{x})
      =} \onslide<11->{\int_0^b t\,dt
      =} \onslide<12->{\left.\frac{t^2}{2}\right\rvert_{t=0}^{t=b}
      =} \onslide<13->{\frac{b^2}{2}}
  }
  \begin{example}
    The speed of $\bv{x}(t)=\sage{x}$ is
    \begin{gather*}
      \norm{\bv{x}^\prime(t)}
      = \onslide<2->{\norm{\sage{v}}
        =} \onslide<3->{\sqrt{t^2\cos^2(t)+t^2\sin^2(t)}
        =} \onslide<4->{\sqrt{t^2}
        =} \onslide<5->{t}
    \end{gather*}
    \onslide<6->{The arclength of $\bv{x}$ from $t=0$ to $t=b$ is then}
    \begin{align*}
      \myInvolute && \myArc
    \end{align*}
  \end{example}

\end{frame}


\section{Curvature}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myFlat}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      , axis/.style={<->}
      , vector/.style={->}
      ]

      \coordinate (O) at (0, 0);
      \draw[beamblue, <->] (-2, -1/2) -- (2, 1/2);

      \onslide<2->{
        \node[overlay, below right= 0mm and 3mm of O, red] (text) {``flat''};
        \draw[overlay, <-, thick, shorten <=2pt, red] (O.south) |- (text.west);
      }

    \end{tikzpicture}
  }
  \newcommand{\myBendy}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      , axis/.style={<->}
      , vector/.style={->}
      ]

      \coordinate (O) at (0, 0);
      \draw[beamblue, <->] plot[smooth, domain=-1:1] ({\x}, {-\x*\x});

      \onslide<3->{
        \node[overlay, above right= 0mm and 3mm of O, red] (text) {``bendy''};
        \draw[overlay, <-, thick, shorten <=2pt, red] (O.north) |- (text.west);
      }

    \end{tikzpicture}
  }
  \begin{block}{Question}
    These two curves are clearly different.
    \begin{align*}
      \myFlat && \myBendy
    \end{align*}
    \onslide<4->{How can we measure how ``bendy'' a curve is?}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{unit tangent} and \emph{principal unit normal} vectors are
    \begin{align*}
      \bv{T}(t) &= \frac{\bv{x}^\prime(t)}{\norm{\bv{x}^\prime(t)}} & \bv{N}(t) &= \frac{\bv{T}^\prime(t)}{\norm{\bv{T}^\prime(t)}}
    \end{align*}
    \onslide<2->{Each of these vectors is a unit vector, so
      $\norm{\bv{T}}=\norm{\bv{N}}=\onslide<3->{1}$\onslide<3->{.}}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myLen}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{red}\Set{\bv{T}\cdot\bv{T}}}$};
      \onslide<7->{
        \node[overlay, above right= 0mm and 4mm of a] (text) {$\bv{T}\cdot\bv{T}=\norm{\bv{T}}^2=1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{theorem}
    $\bv{T}\perp\bv{N}$
  \end{theorem}
  \begin{proof}<2->
    $
    \begin{aligned}
      \onslide<2->{\bv{T}\cdot\bv{N}}
      &\onslide<2->{=} \onslide<3->{\bv{T}\cdot\frac{\bv{T}^\prime}{\norm{\bv{T}^\prime}}} \\
      &\onslide<3->{=} \onslide<4->{\frac{1}{\norm{\bv{T}^\prime}} \bv{T}\cdot\bv{T}^\prime} \\
      &\onslide<4->{=} \onslide<5->{\frac{1}{\norm{\bv{T}^\prime}} \cdot\frac{1}{2}\cdot \Set{\bv{T}\cdot\bv{T}^\prime+\bv{T}^\prime\cdot\bv{T}}} \\
      &\onslide<5->{=} \onslide<6->{\frac{1}{\norm{\bv{T}^\prime}} \cdot\frac{1}{2}\cdot \odv*{\myLen}{t}} \\
      &\onslide<6->{=} \onslide<8->{0\qedhere}
    \end{aligned}
    $
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Acceleration is a linear combination of $\bv{T}$ and $\bv{N}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , scale=2
        , rotate=25
        , nodes={rotate=25}
        ]

        \onslide<2->{
          \draw[beamblue, <->] plot[smooth, domain=-1/2:3/2] ({\x*\x}, {-\x}) node[right] {$\bv{x}(t)$};
        }

        \pgfmathsetmacro{\t}{1/2}

        \pgfmathsetmacro{\x}{\t*\t}
        \pgfmathsetmacro{\y}{-\t}

        \pgfmathsetmacro{\Tx}{2*\t/sqrt(4*\t*\t+1)}
        \pgfmathsetmacro{\Ty}{-1/sqrt(4*\t*\t+1)}
        \pgfmathsetmacro{\aN}{sqrt(2)}
        \pgfmathsetmacro{\aT}{sqrt(2)}

        \begin{scope}[shift={(\x, \y)}]
          \coordinate (p) at (0, 0);
          \coordinate (T) at (\Tx, \Ty);
          \coordinate (N) at (-\Ty, \Tx);
          \coordinate (a) at (2, 0);

          \onslide<7->{
            \draw[dotted, thick] (p) -- ($ \aT*(T) $);
            \draw[dotted, thick] (p) -- ($ \aN*(N) $);
            \draw[dotted, thick] ($ \aT*(T) $) -- (a) node[midway, below, sloped] {$a_{\bv{N}}$};
            \draw[dotted, thick] ($ \aN*(N) $) -- (a) node[midway, above, sloped] {$a_{\bv{T}}$};
          }

          \onslide<5->{
            \draw[beamgreen, ->] (p) -- ++(T) node[midway, below, sloped] {$\bv{T}$};
          }

          \onslide<6->{
            \draw[beamgreen, ->] (p) -- ++(N) node[midway, above, sloped] {$\bv{N}$};
          }

          \onslide<4->{
            \draw[red, ->] (p) -- ++(a) node[midway, above, sloped]
            {$\scriptstyle\bv{a}=a_{\bv{T}}\cdot\bv{T}+a_{\bv{N}}\cdot\bv{N}$};
          }

          \onslide<3->{
            \node[myDot] at (p) {};
          }
        \end{scope}

      \end{tikzpicture}
    \]
    \onslide<8->{For curves in $\mathbb{R}^3$, we have}
    \begin{align*}
      \onslide<9->{\overset{{\color{beamgreen}\textnormal{\stackanchor{``tangential component''}{(rate of change of \emph{speed})}}}}{a_{\bv{T}} = \frac{\bv{x}^\prime\cdot\bv{x}^{\prime\prime}}{\norm{\bv{x}^\prime}}}} && \onslide<10->{\overset{{\color{beamgreen}\textnormal{\stackanchor{``normal component''}{(rate of change of \emph{direction})}}}}{a_{\bv{N}} = \frac{\norm{\bv{x}^\prime\times\bv{x}^{\prime\prime}}}{\norm{\bv{x}^\prime}}}}
    \end{align*}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{curvature} of $\bv{x}(t)$ is
    \[
      \kappa = \frac{a_{\bv{N}}}{\norm{\bv{x}^\prime}^2} = \frac{\norm{\bv{x}^\prime\times\bv{x}^{\prime\prime}}}{\norm{\bv{x}^\prime}^3}
    \]
    \onslide<2->{Note that $\kappa$ is a \onslide<3->{\emph{scalar}.}}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Interpretation}
    Curvature measures the degree to which $\bv{x}(t)$ ``lifts off'' $\bv{T}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , scale=2
        , rotate=25
        , nodes={rotate=25}
        ]

        \draw[beamblue, <->] plot[smooth, domain=-1/2:3/2] ({\x*\x}, {-\x}) node[right] {$\bv{x}(t)$};

        \pgfmathsetmacro{\t}{1/2}

        \pgfmathsetmacro{\x}{\t*\t}
        \pgfmathsetmacro{\y}{-\t}

        \pgfmathsetmacro{\Tx}{2*\t/sqrt(4*\t*\t+1)}
        \pgfmathsetmacro{\Ty}{-1/sqrt(4*\t*\t+1)}
        \pgfmathsetmacro{\aN}{sqrt(2)}
        \pgfmathsetmacro{\aT}{sqrt(2)}

        \begin{scope}[shift={(\x, \y)}]
          \coordinate (p) at (0, 0);
          \coordinate (T) at (\Tx, \Ty);
          \coordinate (N) at (-\Ty, \Tx);
          \coordinate (a) at (2, 0);

          \draw[dotted, thick] (p) -- ($ \aT*(T) $);
          \draw[dotted, thick] (p) -- ($ \aN*(N) $);
          \draw[dotted, thick] ($ \aT*(T) $) -- (a) node[pos=0.65, below, sloped] {$\scriptstyle a_{\bv{N}}=\norm{\bv{v}}^2\cdot\kappa$};
          \draw[dotted, thick] ($ \aN*(N) $) -- (a) node[midway, above, sloped] {$a_{\bv{T}}$};

          \draw[beamgreen, ->] (p) -- ++(T) node[midway, below, sloped] {$\bv{T}$};
          \draw[beamgreen, ->] (p) -- ++(N) node[midway, above, sloped] {$\bv{N}$};
          \draw[red, ->] (p) -- ++(a) node[midway, above, sloped]
          {$\scriptstyle\bv{a}=a_{\bv{T}}\cdot\bv{T}+a_{\bv{N}}\cdot\bv{N}$};

          \node[myDot] at (p) {};
        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}


\end{frame}


\subsection{Example}

\begin{sagesilent}
  var('R t')
  x = vector([R*cos(t), R*sin(t), 0])
  v = x.diff(t)
  a = x.diff(t, 2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myca}{
    \left|
      \begin{array}{rr}
        R\cos(t) & 0 \\
        -R\sin(t) & 0
      \end{array}
    \right|
  }
  \newcommand{\mycb}{
    \left|
      \begin{array}{rr}
        -R\sin(t) & 0 \\
        -R\cos(t) & 0
      \end{array}
    \right|
  }
  \newcommand{\mycc}{
    \left|
      \begin{array}{rr}
        -R\sin(t) & R\cos(t) \\
        -R\cos(t) & -R\sin(t)
      \end{array}
    \right|
  }
  \begin{example}
    The circle $\bv{x}(t)=\sage{x}$ satisfies
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\bv{x}^\prime} &\onslide<2->{=} \onslide<3->{\sage{v}} & \onslide<4->{\bv{x}^{\prime\prime}} &\onslide<4->{=} \onslide<5->{\sage{a}}
      \end{align*}
    \end{gather*}
    \onslide<6->{The cross product $\bv{x}^\prime\times\bv{x}^{\prime\prime}$ is}
    \begin{gather*}
      \onslide<6->{\bv{x}^\prime\times\bv{x}^{\prime\prime}
      =}
    \onslide<7->{\left\langle
        \myca, -\mycb, \mycc
      \right\rangle
      =} \onslide<8->{\langle0, 0, R^2\rangle}
    \end{gather*}
    \onslide<9->{The curvature of the circle of radius $R$ is thus}
    \[
      \onslide<10->{\kappa
        =} \onslide<11->{\frac{\norm{\bv{x}^\prime\times\bv{x}^{\prime\prime}}}{\norm{\bv{x}^\prime}^3}
        =} \onslide<12->{\oldfrac{\norm{\langle0, 0, R^2\rangle}}{\norm{\sage{v}}^3}
        =} \onslide<13->{\frac{1}{R}}
    \]
    \onslide<14->{Larger circles have smaller curvature!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Interpretation}
    Curvature defines the ``circle of best fit'' to a curve.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , scale=2
        , rotate=25
        , nodes={rotate=25}
        ]

        \onslide<2->{\draw[beamblue, <->] plot[smooth, domain=-1:3/2] ({\x*\x}, {-\x}) node[right] {$\bv{x}(t)$};}
        \onslide<5->{
          \node[myDot] at (1/2, 0) {};
          \draw[dotted, thick] (0, 0) -- (1/2, 0) node[midway, above, sloped] {$\frac{1}{\kappa}$};
        }
        \onslide<4->{\draw[beamgreen] (1/2, 0) circle (1/2);}
        \onslide<3->{
          \node[myDot] at (0, 0) {};
        }

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}

\end{document}

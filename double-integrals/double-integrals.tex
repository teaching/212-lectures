\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}

\tikzset{
  , myfill/.style={fill=beamblue, fill opacity=.4}
  , mydraw/.style={draw=blue!70!black}
}

\title{Double Integrals}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Double Integrals}
\subsection{Motivation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    A sheet of material conforms to the shape of a domain $D$ in $\mathbb{R}^2$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=.60
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \filldraw[myfill, draw=beamblue] (0, 0)
        to[bend left]  (3, -1)
        to[bend left]  (5,  2)
        to[bend right] (2,  3)
        to[bend right] (0,  0)
        --cycle;

        \coordinate (P1) at (1, 1);
        \coordinate (P2) at (5/2, 1/2);
        \coordinate (P3) at (2, 2);
        \coordinate (P4) at (7/2, 2);

        \onslide<3->
        \node[myDot, label=left:{$\scriptstyle P_1$}] at (P1) {};
        \node[overlay, red, below left= 0.5mm and 16mm of P1]
        (text) {$\scriptstyle f(P_1)=7\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P1.south) |- (text.east);

        \onslide<4->
        \node[myDot, label=right:{$\scriptstyle P_2$}] at (P2) {};
        \node[overlay, red, below right= 1mm and 16mm of P2]
        (text) {$\scriptstyle f(P_2)=17\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P2.south) |- (text.west);

        \onslide<5->
        \node[myDot, label=left:{$\scriptstyle P_3$}] at (P3) {};
        \node[overlay, red, above left= 1mm and 16mm of P3]
        (text) {$\scriptstyle f(P_3)=3\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P3.south) |- (text.east);

        \onslide<6->
        \node[myDot, label=right:{$\scriptstyle P_4$}] at (P4) {};
        \node[overlay, red, above right= 1mm and 16mm of P4]
        (text) {$\scriptstyle f(P_4)=21\,\si{\kg\per\metre\squared}$};
        \draw[overlay, red, <-, thick, shorten <=4pt]
        (P4.north) |- (text.west);

      \end{tikzpicture}
    \]
    \onslide<2->{Suppose $f\in\mathscr{C}(\mathbb{R}^2)$ measures density
      (\SI{}{\kg\per\metre\squared}) at every point of $D$.}
  \end{example}

  \begin{definition}<7->
    The \emph{double integral of $f$ over $D$} is
    \[
      \iint_{D}f\,dA=\textnormal{mass of $D$ (in \SI{}{\kg})}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}f}$};
      \onslide<2->{
        \node[overlay, below left = 4mm and 2mm of a] (text)
        {$\dfrac{\textnormal{mass units}}{\textnormal{area unit}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mydA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}dA}$};
      \onslide<3->{
        \node[overlay, below right = 3mm and 1mm of a] (text)
        {$\textnormal{area unit}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units allows us to interpret double integrals.
    \[
      \iint_D \myf\,\mydA
      = \onslide<4->{\textnormal{mass of }D}
    \]
  \end{block}

\end{frame}


\subsection{Iterated Integrals}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we \emph{calculate} a double integral?
  \end{block}

  \begin{block}{Answer}<2->
    Integrate variable-by-variable!
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      , axis/.style={<->}
      , vector/.style={->}
      , scale=3/4
      ]

      \coordinate (O) at (0, 0);

      \pgfmathsetmacro{\xlen}{6}
      \pgfmathsetmacro{\ylen}{9*\xlen/16}
      \pgfmathsetmacro{\xmin}{1/2}
      \pgfmathsetmacro{\xmax}{\xmin+\xlen}
      \pgfmathsetmacro{\ymin}{1/2}
      \pgfmathsetmacro{\ymax}{\ymin+\ylen}
      \pgfmathsetmacro{\myx}{\xmin+0.618*(\xmax-\xmin)}

      \draw[<->] (\xmin-1.25, 0) -- (\xmax+1/2, 0);
      \draw[<->] (0, \ymin-1.25) -- (0, \ymax+1/2);

      \draw (\xmin, 3pt) -- (\xmin, -3pt) node[below] {$a$};
      \draw (\xmax, 3pt) -- (\xmax, -3pt) node[below] {$b$};
      \draw (3pt, \ymin) -- (-3pt, \ymin) node[left] {$c$};
      \draw (3pt, \ymax) -- (-3pt, \ymax) node[left] {$d$};

      \onslide<3->{
        \draw[red] (\myx, 3pt) -- (\myx, -3pt) node[below] {$x$};
      }

      \filldraw[myfill, mydraw] (\xmin, \ymin) rectangle (\xmax, \ymax);

      \onslide<4->{
        \draw[red, dashed] (\myx, \ymin) -- (\myx, \ymax);
      }

      \coordinate (a) at (\myx, \ymax);
      \onslide<5->{
        \node[overlay, red, above right= 0mm and 4mm of a] (text) {$\operatorname{density}=\int_c^df(x, y)\,dy$};
        \draw[overlay, red, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }

    \end{tikzpicture}
  }
  \begin{definition}
    Suppose $f\in\mathscr{C}(\mathbb{R}^2)$ measures density throughout a
    rectangle $D$.
    \[
      \onslide<2->{\myD}
    \]
    \onslide<6->{The \emph{iterated integral with $x$-slicing} computes mass with}
    \[
      \onslide<7->{
        \iint_D f\,dA
        = \int_a^b{\color{red}\int_c^df(x, y)\,dy}\,dx
      }
    \]
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = x*y**2
  x0, x1 = 1, 5
  y0, y1 = 0, 2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myInner}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\displaystyle\int_{\sage{y0}}^{\sage{y1}}\sage{f}\,dy}$};
      \onslide<4->{
        \node[overlay, above right= 0mm and 8mm of a] (text) {$x$ constant};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Suppose density throughout $D=[\sage{x0}, \sage{x1}]\times[\sage{y0}, \sage{y1}]$ is
    \[
      f(x, y) = \sage{f}\,\si{\kilogram\per\metre\squared}
    \]
    \onslide<2->{We may compute the mass of $D$ using $x$-slicing.}
    \begin{align*}
      \onslide<2->{\iint_D f\,dA}
      &\onslide<2->{=} \onslide<3->{\int_{\sage{x0}}^{\sage{x1}}\myInner\,dx}                                                              \\
      &\onslide<3->{=} \onslide<5->{\int_{\sage{x0}}^{\sage{x1}}{\color{red}\Set*{\sage{integral(f, y)}}_{y=\sage{y0}}^{y=\sage{y1}}}\,dx} \\
      &\onslide<5->{=} \onslide<6->{\int_{\sage{x0}}^{\sage{x1}}{\color{red}\sage{integral(f, y, y0, y1)}}\,dx}                            \\
      &\onslide<6->{=} \onslide<7->{\sage{integral(integral(f, y, y0, y1), x, x0, x1)}\,\si{\kilogram}}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}
      , axis/.style={<->}
      , vector/.style={->}
      , scale=3/4
      ]

      \coordinate (O) at (0, 0);

      \pgfmathsetmacro{\xlen}{6}
      \pgfmathsetmacro{\ylen}{9*\xlen/16}
      \pgfmathsetmacro{\xmin}{1/2}
      \pgfmathsetmacro{\xmax}{\xmin+\xlen}
      \pgfmathsetmacro{\ymin}{1/2}
      \pgfmathsetmacro{\ymax}{\ymin+\ylen}
      \pgfmathsetmacro{\myy}{\ymin+0.618*(\ymax-\ymin)}

      \draw[<->] (\xmin-1.25, 0) -- (\xmax+1/2, 0);
      \draw[<->] (0, \ymin-1.25) -- (0, \ymax+1/2);

      \draw (\xmin, 3pt) -- (\xmin, -3pt) node[below] {$a$};
      \draw (\xmax, 3pt) -- (\xmax, -3pt) node[below] {$b$};
      \draw (3pt, \ymin) -- (-3pt, \ymin) node[left] {$c$};
      \draw (3pt, \ymax) -- (-3pt, \ymax) node[left] {$d$};

      \onslide<3->{
        \draw[red] (3pt, \myy) -- (-3pt, \myy) node[left] {$y$};
      }

      \filldraw[myfill, mydraw] (\xmin, \ymin) rectangle (\xmax, \ymax);

      \onslide<4->{
        \draw[red, dashed] (\xmin, \myy) -- (\xmax, \myy);
      }

      \coordinate (a) at (\xmax, \myy);
      \onslide<5->{
        \node[overlay, red, above right= 10mm and -12mm of a] (text) {$\operatorname{density}=\int_a^b f(x, y)\,dx$};
        \draw[overlay, red, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }

    \end{tikzpicture}
  }
  \begin{definition}
    Suppose $f\in\mathscr{C}(\mathbb{R}^2)$ measures density throughout a
    rectangle $D$.
    \[
      \onslide<2->{\myD}
    \]
    \onslide<6->{The \emph{iterated integral with $y$-slicing} computes mass with}
    \[
      \onslide<7->{
        \iint_D f\,dA
        = \int_c^d{\color{red}\int_a^b f(x, y)\,dx}\,dy
      }
    \]
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y')
  f = x*y**2
  x0, x1 = 1, 5
  y0, y1 = 0, 2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myInner}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\displaystyle\int_{\sage{x0}}^{\sage{x1}}\sage{f}\,dx}$};
      \onslide<4->{
        \node[overlay, above right= 0mm and 8mm of a] (text) {$y$ constant};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Suppose again density throughout $D=[\sage{x0}, \sage{x1}]\times[\sage{y0}, \sage{y1}]$ is
    \[
      f(x, y) = \sage{f}\,\si{\kilogram\per\metre\squared}
    \]
    \onslide<2->{We may compute the mass of $D$ using $y$-slicing.}
    \begin{align*}
      \onslide<2->{\iint_D f\,dA}
      &\onslide<2->{=} \onslide<3->{\int_{\sage{y0}}^{\sage{y1}}\myInner\,dy}                                                              \\
      &\onslide<3->{=} \onslide<5->{\int_{\sage{y0}}^{\sage{y1}}{\color{red}\Set*{\sage{integral(f, x)}}_{x=\sage{x0}}^{x=\sage{x1}}}\,dy} \\
      &\onslide<5->{=} \onslide<6->{\int_{\sage{y0}}^{\sage{y1}}{\color{red}\sage{integral(f, x, x0, x1)}}\,dy}                            \\
      &\onslide<6->{=} \onslide<7->{\sage{integral(integral(f, y, y0, y1), x, x0, x1)}\,\si{\kilogram}}
    \end{align*}
  \end{example}

\end{frame}



\begin{sagesilent}
  var('x y')
  f = x*sin(x*y)
  x0, x1 = 1, 2
  y0, y1 = 0, pi
  i = integral(integral(f, y, y0, y1), x, x0, x1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose density throughout
    $D=[\sage{x0}, \sage{x1}]\times[\sage{y0}, \sage{y1}]$ is
    \[
      f(x, y) = \sage{f}\,\si{\kilogram\per\metre\squared}
    \]
    \onslide<2->{We may compute the mass of $D$ using $x$-slicing.}
    \begin{align*}
      \onslide<2->{\iint_D f\,dA}
      &\onslide<2->{=} \onslide<3->{\int_{\sage{x0}}^{\sage{x1}} {\color<1->{red}\int_{\sage{y0}}^{\sage{y1}}\sage{f}\,dy}\,dx}                 \\
      &\onslide<3->{=} \onslide<4->{\int_{\sage{x0}}^{\sage{x1}} {\color<1->{red}\Set*{\sage{integral(f, y)}}_{y=\sage{y0}}^{y=\sage{y1}}}\,dx} \\
      &\onslide<4->{=} \onslide<5->{\int_{\sage{x0}}^{\sage{x1}} {\color<1->{red}\sage{expand(integral(f, y, y0, y1))}}\,dx}                    \\
      &\onslide<5->{=} \onslide<6->{\Set*{-\frac{\sin(\pi x)}{\pi}+x}_{x=\sage{x0}}^{x=\sage{x1}}}                                              \\
      &\onslide<6->{=} \onslide<7->{\sage{i}}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Nonrectangular Regions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we compute $\iint_D f\,dA$ if $D$ is not rectangular?
  \end{block}

  \begin{block}{Answer}<2->
    Our slicing method will depend on the shape of $D$.
  \end{block}

\end{frame}


% got this example from https://www.analyzemath.com/high_school_math/grade_11/parabola_problems.html
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Suppose $D$ is enclosed between the graphs of $y_1(x)$ and $y_2(x)$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , declare function = {
          % ft(\x) = -\x*\x+6*\x-4;
          % fb(\x) = \x*\x-4*\x+4;
          ft(\x) = -\x*\x/8+3*\x/2-2;
          fb(\x) = \x*\x/8-\x+2;
        }
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\xa}{2}
        \pgfmathsetmacro{\xb}{8}
        \pgfmathsetmacro{\ya}{1/2}
        \pgfmathsetmacro{\yb}{2}

        \pgfmathsetmacro{\mytop}{5/2}
        \pgfmathsetmacro{\mybottom}{0}

        \pgfmathsetmacro{\myx}{6}

        \coordinate (P) at (\xa, \ya);
        \coordinate (Q) at (\xb, \yb);

        \draw[<->] (\xa-1, -1/2) -- (\xb+1/2, -1/2);
        \draw[<->] (\xa-1/2, \mybottom-1) -- (\xa-1/2, \mytop+1/2);

        \begin{scope}[shift={(0, -1/2)}]
          \draw (\xa, 3pt) -- (\xa, -3pt) node[below] {$a$};
          \draw (\xb, 3pt) -- (\xb, -3pt) node[below] {$b$};

          \onslide<4->{
            \draw[red] (\myx, 3pt) -- (\myx, -3pt) node[below] {$x$};
          }
        \end{scope}

        \filldraw[myfill, mydraw]
        (P)
        -- plot[smooth, domain=\xa:\xb] ({\x}, {ft(\x)})
        -- (Q)
        -- plot[smooth, domain=\xb:\xa] ({\x}, {fb(\x)})
        -- (P)
        -- cycle;

        \node[myDot] at (P) {};
        \node[myDot] at (Q) {};

        \onslide<5->{
          \draw[red, dashed] (\myx, {fb(\myx)}) -- (\myx, {ft(\myx)});
        }

        \coordinate (a) at (\myx, \mytop);
        \onslide<6->{
          \node[overlay, red, above right= -4mm and 4mm of a] (text) {$\operatorname{density}=\displaystyle\int_{y_1(x)}^{y_2(x)} f(x, y)\,dy$};
          \draw[overlay, red, <-, thick, shorten <=2pt] (a.north) |- (text.west);
        }

        \pgfmathsetmacro{\myx}{4}
        \coordinate (a) at (\myx, {ft(\myx)});
        \onslide<3->{
        \node[overlay, beamgreen, above left= 0mm and 4mm of a] (text) {$y_2(x)$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.north) |- (text.east);
        }

        \coordinate (a) at (\myx, {fb(\myx)});
        \onslide<2->{
        \node[overlay, beamgreen, below left= -1mm and 4mm of a] (text) {$y_1(x)$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.south) |- (text.east);
        }

      \end{tikzpicture}
    \]
    \onslide<7->{The \emph{iterated integral with $x$-slicing} computes mass
      with}
    \[
      \onslide<8->{\iint_Df\,dA = \int_a^b{\color{red}\int_{y_1(x)}^{y_2(x)}f(x, y)\,dy}\,dx}
    \]
  \end{definition}

\end{frame}



\begin{sagesilent}
  var('x y')
  y1 = x**2
  y2 = x+2
  x0, x1 = -1, 2
  f = x+2*y
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the region $D$ bounded by $y=\sage{y1}$ and $y=\sage{y2}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , declare function = {
          y1(\x) = \x*\x;
          y2(\x) = \x+2;
        }
        , yscale=3/4
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\xa}{-1}
        \pgfmathsetmacro{\ya}{ 1}
        \pgfmathsetmacro{\xb}{ 2}
        \pgfmathsetmacro{\yb}{ 4}

        \coordinate (P) at (\xa, \ya);
        \coordinate (Q) at (\xb, \yb);

        \draw[<->] (\xa-1, 0) -- (\xb+1, 0);
        \draw[<->] (0, -1/2) -- (0, 9/2);

        \onslide<5->{
          \draw[shift only] (\xa, 3pt) -- (\xa, -3pt) node[below] {$\sage{x0}$};
        }

        \onslide<6->{
          \draw[shift only] (\xb, 3pt) -- (\xb, -3pt) node[below] {$\sage{x1}$};
        }

        \filldraw[myfill, mydraw]
        (P)
        -- plot[smooth, domain=\xa:\xb] ({\x}, {y1(\x)})
        -- (Q)
        -- plot[smooth, domain=\xb:\xa] ({\x}, {y2(\x)})
        -- (P)
        -- cycle;

        \node[myDot] at (P) {};
        \node[myDot] at (Q) {};

        \onslide<4->{
        \node[overlay, red, above right= -4mm and 8mm of Q] (text) {$\begin{aligned} 0 &= \sage{y1}-(\sage{y2})\\ &=\sage{factor(y1-y2)}\end{aligned}$};
        \draw[overlay, red, <-, thick, shorten <=2pt] (Q.north) |- (text.west);
        }

        \pgfmathsetmacro{\myx}{-1/2}
        \coordinate (a) at (\myx, {y2(\myx)});
        \onslide<3->{
        \node[overlay, beamgreen, left= 4mm of a] (text) {$y_2(x)=\sage{y2}$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.west) -- (text.east);
        }

        \pgfmathsetmacro{\myx}{1}
        \coordinate (a) at (\myx, {y1(\myx)});
        \onslide<2->{
        \node[overlay, beamgreen, right= 4mm of a] (text) {$y_1(x)=\sage{y1}$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.east) -- (text.west);
        }

      \end{tikzpicture}
    \]
    \onslide<7->{The mass of this region can be calculated using $x$-slicing.}
    \[
      \onslide<8->{\iint_D f\,dA = \int_{\sage{x0}}^{\sage{x1}} {\color{red}\int_{\sage{y1}}^{\sage{y2}} f\,dy}\,dx}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose density is $f(x, y)=\sage{f}\,\si{\ampere\per\metre\squared}$ in the previous example.
    \begin{align*}
      \onslide<2->{\iint_Df\,dA}
      &\onslide<2->{=} \onslide<3->{\int_{\sage{x0}}^{\sage{x1}} {\color{red}\int_{\sage{y1}}^{\sage{y2}} \sage{f}\,dy}\,dx}                \\
      &\onslide<3->{=} \onslide<4->{\int_{\sage{x0}}^{\sage{x1}} {\color{red}\Set*{\sage{integral(f, y)}}_{y=\sage{y1}}^{y=\sage{y2}}}\,dx} \\
      &\onslide<4->{=} \onslide<5->{\int_{\sage{x0}}^{\sage{x1}} {\color{red}\Set{x\cdot(x+2)+(x+2)^2}-\Set{x\cdot(x^2)+(x^2)^2}}\,dx}      \\
      &\onslide<5->{=} \onslide<6->{\int_{\sage{x0}}^{\sage{x1}} {\color{red}\sage{integral(f, y, y1, y2)}}\,dx}                            \\
      &\onslide<6->{=} \onslide<7->{\sage{integral(integral(f, y, y1, y2), x, x0, x1)}\,\si{\ampere}}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Suppose $D$ is enclosed between the graphs of $x_1(y)$ and $x_2(y)$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , declare function = {
          x1(\x) = \x*\x-4*\x+1;
          x2(\x) = -\x*\x+6*\x-7;
        }
        , yscale=0.90
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\xa}{ 1}
        \pgfmathsetmacro{\ya}{-2}
        \pgfmathsetmacro{\xb}{ 4}
        \pgfmathsetmacro{\yb}{ 1}

        \coordinate (P) at (\ya, \xa);
        \coordinate (Q) at (\yb, \xb);

        \pgfmathsetmacro{\xmin}{-3}
        \pgfmathsetmacro{\xmax}{ 2}
        \pgfmathsetmacro{\ymin}{\xa}
        \pgfmathsetmacro{\ymax}{\xb}

        \pgfmathsetmacro{\myy}{2}

        \draw[<->] (\xmin-1, \ymin-1/2) -- (\xmax+1/2, \ymin-1/2);
        \draw[<->] (\xmin-1/2, \ymin-1) -- (\xmin-1/2, \ymax+1/2);

        \begin{scope}[shift={(\xmin-1/2, 0)}]
          \onslide<4->{
            \draw[red] (3pt, \myy) -- (-3pt, \myy) node[left] {$y$};
          }

          \draw (3pt, \xa) -- (-3pt, \xa) node[left] {$c$};
          \draw (3pt, \xb) -- (-3pt, \xb) node[left] {$d$};
        \end{scope}

        \filldraw[myfill, mydraw]
        (P)
        -- plot[smooth, domain=\xa:\xb] ({x1(\x)}, {\x})
        -- (Q)
        -- plot[smooth, domain=\xb:\xa] ({x2(\x)}, {\x})
        -- (P)
        -- cycle;

        \onslide<5->{
          \draw[dashed, red] ({x1(\myy)}, \myy) -- ({x2(\myy)}, \myy);
        }

        \node[myDot] at (P) {};
        \node[myDot] at (Q) {};

        \coordinate (a) at ({x2(\myy)}, \myy);
        \onslide<6->{
        \node[overlay, red, below right= 2mm and -2mm of a] (text) {$\operatorname{density}=\displaystyle\int_{x_1(y)}^{x_2(y)} f(x, y)\,dx$};
        \draw[overlay, red, <-, thick, shorten <=2pt] (a.east) -| (text.north);
        }

        \pgfmathsetmacro{\myy}{3}
        \coordinate (a) at ({x1(\myy)}, \myy);
        \onslide<2->{
        \node[overlay, beamgreen, above left= 0mm and 4mm of a] (text) {$x_1(y)$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.north) |- (text.east);
        }

        \coordinate (a) at ({x2(\myy)}, \myy);
        \onslide<3->{
        \node[overlay, beamgreen, right= 4mm of a] (text) {$x_2(y)$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.east) -- (text.west);
        }
      \end{tikzpicture}
    \]
    \onslide<7->{The \emph{iterated integral with $y$-slicing} computes mass with}
    \[
      \onslide<8->{\iint_D f\,dA = \int_c^d {\color{red}\int_{x_1(y)}^{x_2(y)} f(x, y)\,dx}\,dy}
    \]
  \end{definition}

\end{frame}



\begin{sagesilent}
  var('x y')
  y1 = sqrt(x)
  y2 = 2-x
  x1 = y**2
  x2 = 2-y
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myya}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}y=\sqrt{x}}$};
      \onslide<2->{
        \node[overlay, above left= 0mm and -1mm of a] (text) {$x=y^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myyb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{red}y=2-x}$};
      \onslide<3->{
        \node[overlay, above right= 0mm and -1mm of a] (text) {$x=2-y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the region $D$ bounded by {\myya} and {\myyb}.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , declare function = {
          y1(\x) = sqrt(\x);
          y2(\x) = 2-\x;
        }
        , scale=2
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 1);
        \coordinate (Q) at (2, 0);

        \draw[<->] (-1/4, 0) -- (2+1/4, 0);
        \draw[<->] (0, -1/4) -- (0, 1+1/4);

        \onslide<6->{
          \draw[shift only] (3pt, 2) -- (-3pt, 2) node[left] {$1$};
          }

        \filldraw[myfill, mydraw]
        (O)
        -- plot[samples=200, domain=0:1] ({\x}, {y1(\x)})
        -- (P)
        -- plot[smooth, domain=1:2] ({\x}, {y2(\x)})
        -- (Q)
        -- (O)
        -- cycle;

        \node[myDot] at (P) {};

        \pgfmathsetmacro{\myx}{1/2}
        \coordinate (a) at (\myx, {y1(\myx)});
        \onslide<4->{
        \node[overlay, beamgreen, above right= 6mm and 2mm of a] (text) {$x_1(y)=y^2$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.north) |- (text.west);
        }

        \pgfmathsetmacro{\myx}{3/2}
        \coordinate (a) at (\myx, {y2(\myx)});
        \onslide<5->{
        \node[overlay, beamgreen, right= 4mm of a] (text) {$x_2(y)=2-y$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.east) -- (text.west);
        }

      \end{tikzpicture}
    \]
    \onslide<7->{The mass of this region can be calculated using $y$-slicing.}
    \[
      \onslide<8->{\iint_D f\,dA = \int_{0}^{1} {\color{red} \int_{y^2}^{2-y} f\,dx}\,dy}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  var('x y')
  f = y
  x0, x1 = y**2, 2-y
  y0, y1 = 0, 1
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose density is $f(x, y)=\sage{f}\,\si{\$\per\metre\squared}$ in the previous example.
    \begin{align*}
      \onslide<2->{\iint_D f\,dA}
      &\onslide<2->{=} \onslide<3->{\int_{0}^{1} {\color{red} \int_{y^2}^{2-y} \sage{f}\,dx}\,dy}                \\
      &\onslide<3->{=} \onslide<4->{\int_{0}^{1} {\color{red} \Set*{\sage{integral(f, x)}}_{x=y^2}^{x=2-y}}\,dy} \\
      &\onslide<4->{=} \onslide<5->{\int_{0}^{1} {\color{red} \Set*{(2-y)y-(y^2)y}}\,dy}                         \\
      &\onslide<5->{=} \onslide<6->{\int_{0}^{1} {\color{red} \Set*{2\,y-y^2-y^3}}\,dy}                          \\
      &\onslide<6->{=} \onslide<7->{\Set*{y^2-\frac{y^3}{3}-\frac{y^4}{4}}_{y=0}^{y=1}}                          \\
      &\onslide<7->{=} \onslide<8->{\sage{integral(integral(f, x, x0, x1), y, y0, y1)}\,\si{\$}}
    \end{align*}
  \end{example}

\end{frame}


\section{Example I}
\subsection{$x$-slicing}

\begin{sagesilent}
  var('x y')
  f = x/y
  x0, x1 = 1, 2
  y0, y1 = 1, x**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myusub}{
    \begin{array}{rclrcr}
      u                 &=& x^2   & u(2) &=& 4 \\
      (\frac{1}{2})\,du &=& x\,dx & u(1) &=& 1
    \end{array}
  }
  \newcommand{\myparts}{
    \begin{array}{rclrcl}
      w  &=& \log(u) & dw &=& \frac{1}{u}\,du \\
      dv &=& du      & v  &=& u
    \end{array}
  }
  \begin{example}
    Consider a direct calculation of $\int_{\sage{x0}}^{\sage{x1}}\int_{\sage{y0}}^{\sage{y1}}\sage{f}\,dy\,dx$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\int_{\sage{x0}}^{\sage{x1}}\int_{\sage{y0}}^{\sage{y1}}\sage{f}\,dy\,dx}
        &\onslide<2->{=} \onslide<3->{\int_{\sage{x0}}^{\sage{x1}} \left.\sage{integral(f, y)}\right\rvert_{y=\sage{y0}}^{y=\sage{y1}}\,dx}                          \\
        &\onslide<3->{=} \onslide<4->{\int_{\sage{x0}}^{\sage{x1}} x\log(x^2)\,dx}                                                          & \onslide<5->{\myusub}  \\
        &\onslide<4->{=} \onslide<6->{\frac{1}{2}\int_1^4\log(u)\,du}                                                                       & \onslide<7->{\myparts} \\
        &\onslide<6->{=} \onslide<8->{\frac{1}{2}\Set*{\left.u\log(u)\right\rvert_{u=1}^{u=4}-\int_1^4du}}                                                           \\
        &\onslide<8->{=} \onslide<9->{\frac{1}{2}\Set*{4\,\log(4)-3}}                                                                                                \\
        &\onslide<9->{=} \onslide<10->{2\,\log(4)-\frac{3}{2}}
      \end{align*}
    \end{gather*}
    \onslide<11->{Note that this method uses \emph{$x$-slicing}.}
  \end{example}

\end{frame}


\subsection{$y$-slicing}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , xscale=4
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 1);
        \coordinate (Q) at (2, 1);
        \coordinate (R) at (2, 4);

        \filldraw[myfill, mydraw]
        (P)
        -- (Q)
        -- (R)
        -- plot[smooth, domain=2:1] ({\x}, {\x*\x})
        -- (P)
        -- cycle;

        \coordinate (a) at (3/2, 1);
        \node[overlay, beamgreen, below right= 0mm and 2mm of a] (text) {$y=1$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.south) |- (text.west);

        \coordinate (a) at (2, 2);
        \node[beamgreen, above right= 2mm and 2mm of a] (text) {$x=2$};
        \draw[beamgreen, <-, thick, shorten <=2pt] (a.south) -| (text.south);

        \coordinate (a) at (3/2, 9/4);
        \node[overlay, beamgreen, left= 4mm of a] (text) {$y=x^2$};
        \draw[overlay, beamgreen, <-, thick, shorten <=2pt] (a.west) -| (text.east);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myInt}{
    \begin{aligned}
      \onslide<2->{\int_{\sage{x0}}^{\sage{x1}}\int_{\sage{y0}}^{\sage{y1}}\sage{f}\,dy\,dx}
      &\onslide<2->{=} \onslide<4->{\int_1^4\int_{\sqrt{y}}^{2}\sage{f}\,dx\,dy}                               \\
      &\onslide<4->{=} \onslide<5->{\frac{1}{2}\int_1^4\left.\frac{x^2}{y}\right\rvert_{x=\sqrt{y}}^{x=2}\,dy} \\
      &\onslide<5->{=} \onslide<6->{\frac{1}{2}\int_1^4\frac{4}{y}-1\,dy}                                      \\
      &\onslide<6->{=} \onslide<7->{\frac{1}{2}\Set{4\,\log(y)-y}_{y=1}^{y=4}}                                 \\
      &\onslide<7->{=} \onslide<8->{\frac{1}{2}\Set{(4\,\log(4)-4)-(4\,\log(1)-1)}}                            \\
      &\onslide<8->{=} \onslide<9->{2\,\log(4)-\frac{3}{2}}
    \end{aligned}
  }
  \begin{example}
    Calculating
    $\int_{\sage{x0}}^{\sage{x1}}\int_{\sage{y0}}^{\sage{y1}}\sage{f}\,dy\,dx$
    with $y$-slicing gives
    \begin{gather*}
      \begin{align*}
        \myInt && \onslide<3->{\myD}
      \end{align*}
    \end{gather*}
    \onslide<10->{Here we avoid the $u$-substitution and the integration by
      parts!}
  \end{example}

\end{frame}


\section{Example II}
\subsection{$x$-slicing}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myImpossible}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\displaystyle\int_y^1e^{x^2}\,dx}$};
      \onslide<2->{
        \node[overlay, below right= 0mm and 3mm of a] (text) {no elementary antiderivative!};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the iterated integral
    \[
      \int_0^1\myImpossible\,dy
    \]
  \end{example}

\end{frame}


\subsection{$y$-slicing}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=3
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 0);
        \coordinate (Q) at (1, 1);

        \draw[beamgreen] (O) -- (P) node[midway, below]         {$y=0$};
        \draw[beamgreen] (P) -- (Q) node[midway, right]         {$x=1$};
        \draw[beamgreen] (O) -- (Q) node[midway, sloped, above] {$x=y$};

        \filldraw[myfill, mydraw] (O) -- (P) -- (Q) -- (O) -- cycle;

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\usub}{
    \begin{array}{rclrcr}
      u                 &=& x^2   & u(1) &=& 1 \\
      (\frac{1}{2})\,du &=& x\,dx & u(0) &=& 0
    \end{array}
  }
  \newcommand{\myIntegral}{
    \begin{aligned}
      \onslide<2->{\int_0^1\int_y^1 e^{x^2}\,dx\,dy}
      &\onslide<2->{=} \onslide<4->{\int_0^1\int_0^x e^{x^2}\,dy\,dx}                                            \\
      &\onslide<4->{=} \onslide<5->{\int_0^1 \left.y\,e^{x^2}\right\rvert_{y=0}^{y=x}\,dx}                       \\
      &\onslide<5->{=} \onslide<6->{\int_0^1 x\,e^{x^2}\,dx}                               & \onslide<7->{\usub} \\
      &\onslide<6->{=} \onslide<8->{\frac{1}{2}\int_0^1 e^u\,du}                                                 \\
      &\onslide<9->{=} \onslide<10->{\frac{e-1}{2}}
    \end{aligned}
  }
  \begin{example}
    Calculating $\int_0^1\int_y^1 e^{x^2}\,dx\,dy$ using $x$-slicing gives
    \begin{gather*}
      \begin{align*}
        \myIntegral && \onslide<3->{\myD}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}

\end{document}

\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{The Chain Rule}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{The Chain Rule}
\subsection{Compositions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider a diagram of functions
    \[
      \begin{tikzcd}
        \mathbb{R}^n\ar[r, "\bv{g}"]\onslide*<3->{\ar[rr, bend right, "\bv{f}\circ\bv{g}" below]}\only<1-2>{\ar[rr, white, bend right, "\phantom{\bv{f}\circ\bv{g}}" below]} \pgfmatrixnextcell \mathbb{R}^m\ar[r, "\bv{f}"] \pgfmatrixnextcell \mathbb{R}^{\ell}
      \end{tikzcd}
    \]
    \onslide<2->{The \emph{composition of $\bv{f}$ and $\bv{g}$} is the function}
    \[
      \onslide<2->{\bv{f}\circ\bv{g}:\mathbb{R}^n\to\mathbb{R}^{\ell}}
    \]
    \onslide<2->{defined by $(\bv{f}\circ\bv{g})(\bv{x})=\bv{f}(\bv{g}(\bv{x}))$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    To form the composition $\bv{f}\circ\bv{g}$, we must start with a diagram
    \[
      \begin{tikzcd}
        \mathbb{R}^n\ar[r, "\bv{g}"]\ar[rr, bend right, "\bv{f}\circ\bv{g}" below] \pgfmatrixnextcell \mathbb{R}^m\ar[r, "\bv{f}"] \pgfmatrixnextcell \mathbb{R}^{\ell}
      \end{tikzcd}
    \]
    \onslide<2->{This requires that $\Domain(\bv{f})=\onslide<3->{\Target(\bv{g})}$\onslide<3->{.}}
  \end{block}

  \begin{alertblock}{Warning}<4->
    $\bv{f}\circ\bv{g}\neq\bv{g}\circ{\bv{f}}$
  \end{alertblock}

\end{frame}


\begin{sagesilent}
  var('w x y z s t')
  f = vector([s**2*t, s-t, t**3])
  g = vector([w*x*y, arctan(w**2*z)])
  g1, g2 = g
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}<+->
    Consider the functions
    \begin{align*}
      \bv{g}(w, x, y, z) &= \sage{g} & \bv{f}(s, t) &= \sage{f}
    \end{align*}
    \onslide<+->This data fits into a diagram
    \[
      \begin{tikzcd}
        \mathbb{R}^4\ar[r, "\bv{g}"]\pgfmatrixnextcell \mathbb{R}^2\ar[r, "\bv{f}"]\pgfmatrixnextcell\mathbb{R}^3
      \end{tikzcd}
    \]
    \onslide<+->The composition $\bv{f}\circ\bv{g}$ is given by
    \begin{gather*}
      \bv{f}(\bv{g}(w, x, y, z))
      =
      \sage{f(s=g1, t=g2)}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('K L t r')
  Q = 4*K**(3/4)*L**(1/4)
  fK = 10*t**2/r
  fL = 6*t**2+250*r
  t0, r0 = 10, 1/10
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      Q(K, L) &= \sage{Q} & K(r, t) &= \sage{fK} & L(r, t) &= \sage{fL}
    \end{align*}
    \onslide<2->{To compute $\pdv{Q}{t}(r=\sage{r0}, t=\sage{t0})$, we could use substitution}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{Q(r, t)} &\onslide<2->{= \sage{Q(K=fK, L=fL)}} & \onslide<3->{\pdv{Q}{t}} &\onslide<3->{= \sage{Q(K=fK, L=fL).diff(t)}}
      \end{align*}
    \end{gather*}
    \onslide<4->{This gives
      $\pdv{Q}{t}(r=\sage{r0}, t=\sage{t0})=\sage{Q(K=fK, L=fL).diff(t)(r=r0,
        t=t0).canonicalize_radical()}$.}
  \end{example}

  \begin{alertblock}{Problem}<5->
    This example should be easier. We need a multivariable chain
    rule!
  \end{alertblock}

\end{frame}

\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDfg}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$D(\bv{f}\circ\bv{g})$};
      \onslide<4->{
        \node[overlay, below= 6mm of a] (text) {$\ell\times n$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$D\bv{f}$};
      \onslide<2->{
        \node[overlay, below= 6mm of a] (text) {$\ell\times m$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) -- (text.north);
      }
    }
  }
  \newcommand{\myg}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {$D\bv{g}$};
      \onslide<3->{
        \node[overlay, below right= 2mm and 3mm of a] (text) {$m\times n$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{theorem}[The Chain Rule]
    Consider a diagram of functions
    \[
      \begin{tikzcd}
        \mathbb{R}^n\ar[r, "\bv{g}"] \pgfmatrixnextcell \mathbb{R}^m\ar[r, "\bv{f}"] \pgfmatrixnextcell \mathbb{R}^{\ell}
      \end{tikzcd}
    \]
    Then $\myDfg=\myf\cdot\myg$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myf}[4]{
    \left[
      \begin{array}{c}
        ####1_1(####2_1,\dotsc,####2_{####3})       \\
        ####1_2(####2_1,\dotsc,####2_{####3})       \\
        \vdots                                      \\
        ####1_{####4}(####2_1,\dotsc,####2_{####3})
      \end{array}
    \right]
  }
  \newcommand{\myD}[4]{\renewcommand{\arraystretch}{1.4}
    \left[
      \begin{array}{cccc}
        \pdv{####1_1}{####2_1}       & \pdv{####1_1}{####2_2}       & \cdots & \pdv{####1_1}{####2_{####3}}      \\
        \pdv{####1_2}{####2_1}       & \pdv{####1_2}{####2_2}       & \cdots & \pdv{####1_2}{####2_{####3}}      \\
        \vdots                       & \vdots                       & \ddots & \vdots                            \\
        \pdv{####1_{####4}}{####2_1} & \pdv{####1_{####4}}{####2_2} & \cdots & \pdv{####1_{####4}}{####2_{####3}}
      \end{array}
    \right]
  }
  \begin{block}{Notation}
    Given a diagram
    $\mathbb{R}^n\xrightarrow{\bv{g}}\mathbb{R}^m\xrightarrow{\bv{f}}\mathbb{R}^{\ell}$
    we have
    \begin{gather*}
      \begin{align*}
        \bv{f}(s_1,\dotsc, s_m) &= \myf{f}{s}{m}{\ell} & \bv{g}(t_1,\dotsc,t_n) &= \myf{g}{t}{n}{m}
      \end{align*}
    \end{gather*}
    \onslide<2->{The chain rule $D(\bv{f}\circ\bv{g})=D\bv{f}\cdot D\bv{g}$
      gives the equation}
    \begin{gather*}
      \onslide<2->{\overset{D(\bv{f}\circ\bv{g})}{\myD{f}{t}{n}{\ell}}=\overset{D\bv{f}}{\myD{f}{s}{m}{\ell}}\overset{D\bv{g}}{\myD{g}{t}{n}{m}}}
    \end{gather*}
  \end{block}

\end{frame}

\subsection{Examples}

\begin{sagesilent}
  var('K L t r')
  Q = 4*K**(3/4)*L**(1/4)
  fK = 10*t**2*r**(-1)
  fL = 6*t**2+250*r
  t0, r0 = 10, 1/10
  g = vector([fK, fL])
  K0, L0 = g(r=r0, t=t0)
  DQ = jacobian(Q, (K, L))
  Dg = jacobian(g, (r, t))
  DQ0 = DQ(K=K0, L=L0)
  Dg0 = Dg(r=r0, t=t0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mydiag}{\mathbb{R}^2\xrightarrow{\bv{g}}\mathbb{R}^2\xrightarrow{Q}\mathbb{R}}
  \newcommand{\myDQ}{
    \left[
      \begin{array}{cc}
        \pdv{Q}{r} & \pdv{Q}{t}
      \end{array}
    \right]
  }
  \begin{example}
    We may interpret our previous example diagrammatically
    \begin{align*}
      \mydiag && \bv{g}(r, t) &= \sage{matrix.column(g)} & Q(K, L) &= \sage{Q}
    \end{align*}
    \onslide<2->{Note that $\bv{g}(\sage{r0}, \sage{t0})=\sage{g(r=r0, t=t0)}$
      and the Jacobian data is}
    \begin{align*}
      \onslide<2->{DQ} &\onslide<2->{=} \onslide<3->{\sage{DQ}} & \onslide<4->{D\bv{g}} &\onslide<4->{=} \onslide<5->{\sage{Dg}}
    \end{align*}
    \onslide<6->{The chain rule gives}
    \begin{gather*}
      \onslide<7->{\myDQ
      = \overset{DQ(\sage{K0}, \sage{L0})}{\sage{DQ0}}\overset{D\bv{g}(\sage{r0}, \sage{t0})}{\sage{Dg0}}
      =} \onslide<8->{\sage{DQ0*Dg0}}
    \end{gather*}
    \onslide<9->{when $r=\sage{r0}$ and $t=\sage{t0}$.}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('t x y z')
  r = vector([cos(t), sin(t), t])
  T = x**2*y*z
  Dr = jacobian(r, (t))
  DT = jacobian(T, (x, y, z))
  t0 = pi
  r0 = r(t=t0)
  x0, y0, z0 = r0
  Dr0 = Dr(t=t0)
  DT0 = DT(x=x0, y=y0, z=z0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mydiag}{
    \mathbb{R}\xrightarrow{\bv{r}}\mathbb{R}^3\xrightarrow{T}\mathbb{R}
  }
  \begin{example}
    Consider the data
    \begin{align*}
      \mydiag && \bv{r}(t) &= \sage{r} & T(x, y, z) &= \sage{T}\,\si{\celsius}
    \end{align*}
    \onslide<2->{The chain rule gives}
    \begin{gather*}
      \onslide<2->{\pdv{T}{t} =
        \overset{DT}{\sage{DT}}\overset{D\bv{r}}{\sage{Dr}} =}
      \onslide<3->{-2\,xyz\sin(t)+x^2z\cos(t)+x^2y\,\si{\celsius\per\second}}
    \end{gather*}
    \onslide<4->{When $t=\sage{t0}$ we have $\bv{r}(\sage{t0})=\sage{r0}$ so}
    \begin{gather*}
      \onslide<5->{\pdv{T}{t}(\pi)
      = -2\,(\sage{x0})(\sage{y0})(\sage{z0})\sin(\sage{t0})+(\sage{x0})^2(\sage{z0})\cos(\sage{t0})+(\sage{x0})^2(\sage{y0})
      =} \onslide<6->{\sage{vector(Dr0)*vector(DT0)}\,\si{\celsius\per\second}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('p q c x y')
  w = p*q-c
  fp = x*y
  fq = x-y
  fc = x**2+y**2
  g = vector([fp, fq, fc])
  Dw = jacobian(w, (p, q, c))
  Dg = jacobian(g, (x, y))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mydiag}{\mathbb{R}^2\xrightarrow{\bv{g}}\mathbb{R}^3\xrightarrow{w}\mathbb{R}}
  \newcommand{\myDw}{
    \left[
      \begin{array}{cc}
        \pdv{w}{x} & \pdv{w}{y}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the variables
    \begin{align*}
      w &= \sage{w} & p &= \sage{fp} & q &= \sage{fq} & c &= \sage{fc}
    \end{align*}
    \onslide<2->{This data can be interpreted diagrammatically}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\mydiag} && \onslide<2->{\bv{g}(x, y)} &\onslide<2->{=} \onslide<3->{\sage{g}} & \onslide<4->{w(p, q, c)} &\onslide<4->{=} \onslide<5->{\sage{w}}
      \end{align*}
    \end{gather*}
    \onslide<6->{The chain rule gives}
    \begin{gather*}
      \onslide<7->{\myDw
        =} \onslide<8->{\overset{Dw}{\sage{Dw}}\overset{D\bv{g}}{\sage{Dg}}
        =} \onslide<9->{\sage{Dw*Dg}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z u v w')
  h = exp(5*x-2*y-4*z)
  g = vector([u**2*v, u+2*v*w**2, u-w])
  u0, v0, w0 = 2, 1, -1
  g0 = g(u=u0, v=v0, w=w0)
  x0, y0, z0 = g0
  Dh = jacobian(h, (x, y, z))
  Dg = jacobian(g, (u, v, w))
  Dh0 = Dh(x=x0, y=y0, z=z0)
  Dg0 = Dg(u=u0, v=v0, w=w0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mydiag}{\mathbb{R}^3\xrightarrow{\bv{g}}\mathbb{R}^3\xrightarrow{h}\mathbb{R}}
  \newcommand{\myDh}{
    \left[
      \begin{array}{ccc}
        \pdv{h}{u} & \pdv{h}{v} & \pdv{h}{w}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the diagram $\mydiag$ where
    \begin{align*}
      h(x, y, z) &= \sage{h} & \bv{g}(u, v, w) &= \sage{g}
    \end{align*}
    \onslide<2->{Note that $\bv{g}(\sage{u0}, \sage{v0}, \sage{w0})=\sage{g0}$
      and the Jacobian data is}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{Dh} &\onslide<2->{=} \onslide<3->{\sage{Dh}} & \onslide<4->{D\bv{g}} &\onslide<4->{=} \onslide<5->{\sage{Dg}}
      \end{align*}
    \end{gather*}
    \onslide<6->{The chain rule implies that}
    \[
      \onslide<7->{\myDh
      =
      \overset{Dh(\sage{x0}, \sage{y0}, \sage{z0})}{\sage{Dh0}}
      \overset{D\bv{g}(\sage{u0}, \sage{v0}, \sage{w0})}{\sage{Dg0}}
      =} \onslide<8->{\sage{Dh0*Dg0}}
    \]
    \onslide<9->{when $(u, v, w)=\sage{(u0, v0, w0)}$.}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z s t')
  f = vector([exp(2*x-y+z), x*y*z])
  g = vector([s**2*t, s+2*t**2, s*t])
  s0, t0 = 1, 1
  g0 = g(s=s0, t=t0)
  x0, y0, z0 = g(s=s0, t=t0)
  Df = jacobian(f, (x, y, z))
  Dg = jacobian(g, (s, t))
  Df0 = Df(x=x0, y=y0, z=z0)
  Dg0 = Dg(s=s0, t=t0)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDf}{\renewcommand{\arraystretch}{1.4}
    \left[
      \begin{array}{cc}
        \pdv{f_1}{s} & \pdv{f_1}{t} \\
        \pdv{f_2}{s} & \pdv{f_2}{t}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the diagram
    $\mathbb{R}^2\xrightarrow{\bv{g}}\mathbb{R}^3\xrightarrow{\bv{f}}\mathbb{R}^2$
    where
    \begin{align*}
      \bv{f}(x, y, z) &= \sage{f} & \bv{g}(s, t) &= \sage{g}
    \end{align*}
    \onslide<2->{Note that $\bv{g}(\sage{s0}, \sage{t0})=\sage{g0}$ and the Jacobian data is}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{D\bv{f}} &\onslide<3->{=} \onslide<4->{\sage{Df}} & \onslide<5->{D\bv{g}} &\onslide<5->{=} \onslide<6->{\sage{Dg}}
      \end{align*}
    \end{gather*}
    \onslide<7->{The chain rule implies that when $(s, t)=(1, 1)$ we have
    \[
      \resizebox{.75\hsize}{!}{$
      \myDf
      =
      \overset{D\bv{f}(\sage{x0}, \sage{y0}, \sage{z0})}{\sage{Df0}}
      \overset{D\bv{g}(\sage{s0}, \sage{t0})}{\sage{Dg0}}
      = \sage{Df0*Dg0}
      $
      }
    \]
    }
  \end{example}

\end{frame}

\end{document}

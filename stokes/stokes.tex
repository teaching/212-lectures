\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\tikzset{
  , myfill/.style={fill=blue!40!white, fill opacity=.6}
  , mydraw/.style={draw=blue!70!black}
  , bluefilldraw/.style={fill=beamblue!40!white, fill opacity=.6, draw=beamblue}
  , redfilldraw/.style={draw=red, fill=red!40, fill opacity=0.6}
  , greenfill/.style={fill=beamgreen!40, fill opacity=0.6}
  , greenfilldraw/.style={draw=beamgreen, fill=beamgreen!40, fill opacity=0.6}
}

\title{Stokes' Theorem}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Boundaries in $\mathbb{R}^3$}
\subsection{Terminology}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $S$ be a surface in $\mathbb{R}^3$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , yscale=0.60
        ]

        \coordinate (O) at (0, 0);
        \coordinate (p1) at (3, -1);
        \coordinate (p2) at (5, 2);
        \coordinate (p3) at ($ (p2)-(p1) $);

        \filldraw[bluefilldraw]
        (O)  to[bend left]
        (p1) to[bend left]
        (p2) to[bend right]
        (p3) to[bend right]
        (O) --cycle;

        \onslide<3->{
          \draw[red]
          (O)  to[bend left]
          (p1) to[bend left]
          (p2) to[bend right]
          (p3) to[bend right]
          (O);

          \draw[red] (p3) to[bend right] node[midway, name=pS] {} (O);

          \draw[overlay, thick, red, <-]
          (pS.west) -| ++(-1, 3mm) node[above] {$\partial S$};
        }
      \end{tikzpicture}
    \]
    \onslide<2->{The \emph{boundary $\partial S$} of $S$ is the outer edge of
      $S$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The boundary of a surface is the \emph{union of simple closed curves}.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\Hl}{3}
        \pgfmathsetmacro{\Hr}{4}
        \pgfmathsetmacro{\pitch}{\R/8}
        \pgfmathsetmacro{\D}{abs(\Hr-\Hl)/2}

        \pgfmathsetmacro{\Rt}{sqrt(\R^2+\D^2)}
        \pgfmathsetmacro{\t}{atan(\D/\R)}

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:0:{\R} and {\pitch})
        -- cycle;

        \onslide<2->{\draw[red] (-\R, 0) arc(180:0:{\R} and {\pitch});}

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:-180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:360:{\R} and {\pitch})
        -- cycle;

        \onslide<3->{
          \draw[red] plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:360]
          ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D});

          \draw[red] plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:90]
          ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D}) node[name=C2] {};
          \draw[thick, red, overlay, <-, shorten <=2pt]
          (C2.north) |- ++(-1/2, 3mm) node[left] {$C_2$};
        }

        \onslide<2->{
          \draw[red] (-\R, 0) arc(180:360:{\R} and {\pitch});

          \coordinate (C1) at (0, -\pitch);
          \draw[thick, red, overlay, <-, shorten <=2pt]
          (C1.south) |- ++(1/2, -3mm) node[right] {$C_1$};
        }
      \end{tikzpicture}
    \]
    \onslide<4->{Here, we write $\partial S=C_1\cup C_2$.}
  \end{block}

\end{frame}


\subsection{Orientations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myN}[2]{($ ####1*(-2*\H*####2/\R/\R, 1) $)}
  \newcommand{\myPos}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{3/2}
        \pgfmathsetmacro{\H}{2}
        \pgfmathsetmacro{\r}{\R/8}

        \filldraw[bluefilldraw]
        (-\R, \H) parabola[bend at end]
        (O) parabola
        (\R, \H) arc(0:180:{\R} and {\r});

        \draw[beamgreen, ->]
        (O) -- ++\myN{0.5}{0} node[above] {$\bv{N}$};

        \draw[beamgreen, ->]
        (\R, \H) -- ++\myN{0.5}{\R} node[above] {$\bv{N}$};

        \filldraw[bluefilldraw]
        (-\R, \H) parabola[bend at end]
        (O) parabola
        (\R, \H) arc(0:-180:{\R} and {\r});

        \begin{scope}[
          , decoration={markings, mark=at position 0.5 with {\arrow[beamgreen]{>}}}
          ]
          \draw[beamblue, postaction={decorate}]
          (-\R, \H) arc(180:360:{\R} and {\r});
        \end{scope}

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{beamgreen}$S$ \emph{left} of $\bv{N}$}}{{\color{beamgreen}$\partial S$ \emph{positively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNeg}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{3/2}
        \pgfmathsetmacro{\H}{2}
        \pgfmathsetmacro{\r}{\R/8}

        \filldraw[bluefilldraw]
        (-\R, \H) parabola[bend at end]
        (O) parabola
        (\R, \H) arc(0:180:{\R} and {\r});

        \draw[red, ->]
        (O) -- ++\myN{0.5}{0} node[above] {$\bv{N}$};

        \draw[red, ->]
        (\R, \H) -- ++\myN{0.5}{\R} node[above] {$\bv{N}$};

        \filldraw[bluefilldraw]
        (-\R, \H) parabola[bend at end]
        (O) parabola
        (\R, \H) arc(0:-180:{\R} and {\r});

        \begin{scope}[
          , decoration={markings, mark=at position 0.5 with {\arrow[red]{<}}}
          ]
          \draw[beamblue, postaction={decorate}]
          (-\R, \H) arc(180:360:{\R} and {\r});
        \end{scope}

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{red}$S$ \emph{right} of $\bv{N}$}}{{\color{red}$\partial S$ \emph{negatively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    The location of $S$ relative to $\bv{N}$ and $\bv{T}$ gives the
    \emph{orientation} of $\partial S$.
    \begin{align*}
      \myPos && \myNeg
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPos}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1.5}
        \pgfmathsetmacro{\Hl}{2}
        \pgfmathsetmacro{\Hr}{2.5}
        \pgfmathsetmacro{\pitch}{\R/8}
        \pgfmathsetmacro{\D}{abs(\Hr-\Hl)/2}

        \pgfmathsetmacro{\Rt}{sqrt(\R^2+\D^2)}
        \pgfmathsetmacro{\t}{atan(\D/\R)}

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:0:{\R} and {\pitch})
        -- cycle;

        \draw[beamgreen, ->] (\R, \Hr/2) -- ++(-\R, 0)
        node[left] {$\bv{N}$};

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:-180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:360:{\R} and {\pitch})
        -- cycle;

        \begin{scope}[
          , decoration={markings, mark=at position 0.5 with {\arrow[beamgreen]{>}}}
          ]

          \draw[beamblue, postaction={decorate}]
          plot[rotate around={\t:(0, \Hl+\D)}, domain=-180:0]
          ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D});

          \draw[beamblue, postaction={decorate}]
          (\R, 0) arc(0:-180:{\R} and {\pitch});
        \end{scope}

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{beamgreen}$S$ \emph{left} of $\bv{N}$}}{{\color{beamgreen}$\partial S$ \emph{positively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNeg}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1.5}
        \pgfmathsetmacro{\Hl}{2}
        \pgfmathsetmacro{\Hr}{2.5}
        \pgfmathsetmacro{\pitch}{\R/8}
        \pgfmathsetmacro{\D}{abs(\Hr-\Hl)/2}

        \pgfmathsetmacro{\Rt}{sqrt(\R^2+\D^2)}
        \pgfmathsetmacro{\t}{atan(\D/\R)}

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:0:{\R} and {\pitch})
        -- cycle;

        \draw[red, ->] (\R, \Hr/2) -- ++(-\R, 0)
        node[left] {$\bv{N}$};

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:-180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:360:{\R} and {\pitch})
        -- cycle;

        \begin{scope}[
          , decoration={markings, mark=at position 0.5 with {\arrow[red]{<}}}
          ]

          \draw[beamblue, postaction={decorate}]
          plot[rotate around={\t:(0, \Hl+\D)}, domain=-180:0]
          ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D});

          \draw[beamblue, postaction={decorate}]
          (\R, 0) arc(0:-180:{\R} and {\pitch});
        \end{scope}

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{red}$S$ \emph{right} of $\bv{N}$}}{{\color{red}$\partial S$ \emph{negatively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Each part of $\partial S$ must be inspected to determine the orientation.
    \begin{align*}
      \myPos && \myNeg
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPos}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1.5}
        \pgfmathsetmacro{\Hl}{2}
        \pgfmathsetmacro{\Hr}{2.5}
        \pgfmathsetmacro{\pitch}{\R/8}
        \pgfmathsetmacro{\D}{abs(\Hr-\Hl)/2}

        \pgfmathsetmacro{\Rt}{sqrt(\R^2+\D^2)}
        \pgfmathsetmacro{\t}{atan(\D/\R)}

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:0:{\R} and {\pitch})
        -- cycle;

        \draw[overlay, red, ->] (\R, \Hr/2) -- ++(\R/2, 0)
        node[right] {$\bv{N}$};

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:-180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:360:{\R} and {\pitch})
        -- cycle;

        \begin{scope}[
          , decoration={markings, mark=at position 0.5 with {\arrow[red]{>}}}
          ]

          \draw[beamblue, postaction={decorate}]
          plot[rotate around={\t:(0, \Hl+\D)}, domain=-180:0]
          ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D});

          \draw[beamblue, postaction={decorate}]
          (\R, 0) arc(0:-180:{\R} and {\pitch});
        \end{scope}

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{red}$S$ \emph{right} of $\bv{N}$}}{{\color{red}$\partial S$ \emph{negatively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNeg}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1.5}
        \pgfmathsetmacro{\Hl}{2}
        \pgfmathsetmacro{\Hr}{2.5}
        \pgfmathsetmacro{\pitch}{\R/8}
        \pgfmathsetmacro{\D}{abs(\Hr-\Hl)/2}

        \pgfmathsetmacro{\Rt}{sqrt(\R^2+\D^2)}
        \pgfmathsetmacro{\t}{atan(\D/\R)}

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:0:{\R} and {\pitch})
        -- cycle;

        \draw[overlay, beamgreen, ->] (\R, \Hr/2) -- ++(\R/2, 0)
        node[right] {$\bv{N}$};

        \filldraw[myfill, mydraw]
        (\R, \Hr)
        plot[smooth, rotate around={\t:(0, \Hl+\D)}, domain=0:-180]
        ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D})
        -- (-\R, 0) arc(180:360:{\R} and {\pitch})
        -- cycle;

        \begin{scope}[
          , decoration={markings, mark=at position 0.5 with {\arrow[beamgreen]{<}}}
          ]

          \draw[beamblue, postaction={decorate}]
          plot[rotate around={\t:(0, \Hl+\D)}, domain=-180:0]
          ({\Rt*cos(\x)}, {\pitch*sin(\x)+\Hl+\D});

          \draw[beamblue, postaction={decorate}]
          (\R, 0) arc(0:-180:{\R} and {\pitch});
        \end{scope}

        \node[below] at (current bounding box.south)
        {\stackanchor{{\color{beamgreen}$S$ \emph{left} of $\bv{N}$}}{{\color{beamgreen}$\partial S$ \emph{positively oriented}}}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    The direction of $\bv{N}$ influences orientation.
    \begin{align*}
      \myPos && \myNeg
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Convention}
    Unless otherwise stated, we assume $\partial S$ is \emph{positively
      oriented}.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Assuming positive orientation defines \emph{boundary curve arithmetic}.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\H}{3}
        \pgfmathsetmacro{\r}{\R/8}

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:180:{\R} and {\r}) --
        (-\R, 0) arc(180:0:{\R} and {\r}) --
        cycle;

        \draw[beamgreen] (\R, 0) arc(0:180:{\R} and {\r});

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:-180:{\R} and {\r}) --
        (-\R, 0) arc(-180:0:{\R} and {\r}) --
        cycle;

        \draw[overlay, beamblue, ->]
        (\R, \H/2) -- ++(1, 0) node[right] {$\bv{N}$};

        \draw[beamgreen] (\R, 0) arc(0:-180:{\R} and {\r});
        \draw[->, beamgreen] plot[domain=-180:-45]
        ({\R*cos(\x)}, {\r*sin(\x)});

        \draw[->, red] plot[domain=-180:-45]
        ({\R*cos(\x)}, {\r*sin(\x)+\H});

        \draw[red] (0, \H) circle({\R} and {\r});

        \coordinate (C1) at (0, -\r);
        \draw[thick, beamgreen, overlay, <-, shorten <=2pt]
        (C1.south) |- ++(-1, -3mm) node[left] {$C_1$};

        \coordinate (C2) at (0, \H+\r);
        \draw[thick, red, overlay, <-, shorten <=2pt]
        (C2.north) |- ++(-1, 3mm) node[left] {$C_2$};

      \end{tikzpicture}
    \]
    Here, we have $\partial S=C_1-C_2$.
  \end{block}

\end{frame}


\subsection{Circulation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Suppose $\bv{F}$ is a vector field on a surface $S$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \pgfmathsetmacro{\myx}{1.5}
        \pgfmathsetmacro{\myy}{0.6*\myx}
        \begin{scope}[yscale=\myy, xscale=\myx, shift={(-2, -2)}]
          \coordinate (O) at (0, 0);
          \coordinate (p1) at (3, -1);
          \coordinate (p2) at (5, 2);
          \coordinate (p3) at ($ (p2)-(p1) $);

          \filldraw[bluefilldraw]
          (O)  to[bend left]
          (p1) to[bend left]
          (p2) to[bend right]
          (p3) to[bend right]
          (O) --cycle;
        \end{scope}


        \begin{scope}[scale=1/2]
          \pgfmathsetmacro{\myN}{10}
          \pgfmathsetmacro{\myDx}{11/\myN}
          \pgfmathsetmacro{\myDy}{8/\myN}
          \foreach \i in {0,...,9}
          \foreach \j in {0,...,9}{
            \pgfmathsetmacro{\myx}{-4+\i*\myDx}
            \pgfmathsetmacro{\myy}{-4+\j*\myDy}
            \pgfmathsetmacro{\myFx}{\myx+sin(\myy r)}
            \pgfmathsetmacro{\myFy}{\myy+sin(\myx r)}
            % \node at (\myx, \myy) {x};
            \draw[thick, beamgreen, arrows={-stealth}] (\myx, \myy) -- (\myFx, \myFy);
          }
        \end{scope}

      \end{tikzpicture}
    \]
    The \emph{circulation of $\bv{F}$ around $S$} is
    $\oint_{\partial S}\bv{F}\cdot d\bv{s}$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Circulation integrals respect boundary arithmetic.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{2}
        \pgfmathsetmacro{\H}{3}
        \pgfmathsetmacro{\r}{\R/8}

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:180:{\R} and {\r}) --
        (-\R, 0) arc(180:0:{\R} and {\r}) --
        cycle;

        \draw[beamgreen] (\R, 0) arc(0:180:{\R} and {\r});

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:-180:{\R} and {\r}) --
        (-\R, 0) arc(-180:0:{\R} and {\r}) --
        cycle;

        \draw[overlay, beamblue, ->]
        (\R, \H/2) -- ++(1, 0) node[right] {$\bv{N}$};

        \draw[beamgreen] (\R, 0) arc(0:-180:{\R} and {\r});
        \draw[->, beamgreen] plot[domain=-180:-45]
        ({\R*cos(\x)}, {\r*sin(\x)});

        \draw[->, red] plot[domain=-180:-45]
        ({\R*cos(\x)}, {\r*sin(\x)+\H});

        \draw[red] (0, \H) circle({\R} and {\r});

        \coordinate (C1) at (0, -\r);
        \draw[thick, beamgreen, overlay, <-, shorten <=2pt]
        (C1.south) |- ++(-1, -3mm) node[left] {$C_1$};

        \coordinate (C2) at (0, \H+\r);
        \draw[thick, red, overlay, <-, shorten <=2pt]
        (C2.north) |- ++(-1, 3mm) node[left] {$C_2$};

      \end{tikzpicture}
    \]
    Here, we have
    $\oint_{\partial S}\bv{F}\cdot d\bv{s}=\oint_{C_1}\bv{F}\cdot
    d\bv{s}-\oint_{C_2}\bv{F}\cdot d\bv{s}$.
  \end{block}

\end{frame}



\begin{sagesilent}
  var('x y z t')
  F = vector([x*y, 2*x*z, x*y])
  xtop = vector([cos(t), sin(t), 4])
  xbot = vector([cos(-t), sin(-t), 0])
  vtop = xtop.diff(t)
  vbot = xbot.diff(t)
  Ftop = F(x=xtop[0], y=xtop[1], z=xtop[2])
  Fbot = F(x=xbot[0], y=xbot[1], z=xbot[2])
  circtop = integral(Ftop*vtop, t, 0, 2*pi)
  circbot = integral(Fbot*vbot, t, 0, 2*pi)
  circ = circtop + circbot
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myCyl}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\H}{4}
        \pgfmathsetmacro{\r}{\R/8}

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:180:{\R} and {\r}) --
        (-\R, 0) arc(180:0:{\R} and {\r}) --
        cycle;

        \draw[overlay, beamblue, ->]
        (\R, \H/2) -- ++(-\R, 0) node[left] {$\bv{N}$};

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:-180:{\R} and {\r}) --
        (-\R, 0) arc(-180:0:{\R} and {\r}) --
        cycle;

        \draw[beamblue, ->] (\R, 0) arc(0:-90:{\R} and {\r});
        \draw[beamblue, ->] (-\R, \H) arc(180:270:{\R} and {\r});

        \draw[thick, red, <-, shorten <= 2pt]
        (-\R, \H/2) -| ++(-12mm, 1mm) node[above] {$x^2+y^2=1$};

        \draw[thick, red, <-, overlay, shorten <= 2pt]
        (-\R, 0) -- ++(-4mm, 0) node[left] {$z=0$};

        \draw[thick, red, <-, overlay, shorten <= 2pt]
        (-\R, \H) -- ++(-4mm, 0) node[left] {$z=4$};

        \draw[beamblue] (\R, 0) arc(0:-30:{\R} and {\r})
        node[name=bot] {};

        \onslide<3->{
          \draw[thick, red, <-, overlay]
          (bot.south) |- ++(-4mm, -3mm) node[left]
          {$\scriptstyle\bv{x}_{\operatorname{bot}}(t)=\sage{xbot}$};
        }

        \draw[beamblue] (\R, \H) arc(0:30:{\R} and {\r})
        node[name=top] {};

        \onslide<2->{
          \draw[thick, red, <-, overlay]
          (top.north) |- ++(-4mm, 3mm) node[left]
          {$\scriptstyle\bv{x}_{\operatorname{top}}(t)=\sage{xtop}$};
        }
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myCirc}{
    \begin{aligned}
      \onslide<4->{\oint_{\bv{x}_{\operatorname{top}}}\bv{F}\cdot d\bv{s}} &\onslide<4->{=} \onslide<5->{\int_{0}^{2\,\pi}\sage{F}\cdot\sage{vtop}\,dt} \\
      &\onslide<5->{=} \onslide<6->{\int_{0}^{2\,\pi}\sage{Ftop*vtop}\,dt} \\
      &\onslide<6->{=} \onslide<7->{\sage{circtop}\,\si{\newton\cdot\metre}} \\
      \onslide<8->{\oint_{\bv{x}_{\operatorname{bot}}}\bv{F}\cdot d\bv{s}} &\onslide<8->{=} \onslide<9->{\int_{0}^{2\,\pi}\sage{F}\cdot\sage{vbot}\,dt} \\
      &\onslide<9->{=} \onslide<10->{\int_{0}^{2\,\pi}\sage{Fbot*vbot}\,dt} \\
      &\onslide<10->{=} \onslide<11->{\sage{circbot}\,\si{\newton\cdot\metre}}
    \end{aligned}
  }
  \begin{example}
    Let $S$ be the depicted cylinder and consider
    $\bv{F}=\sage{F}\,\si{\newton}$.
    \begin{gather*}
      \begin{align*}
        \myCyl && \myCirc
      \end{align*}
    \end{gather*}
    \onslide<12->{The circulation of $\bv{F}$ around $S$ is}
    \[
      \onslide<12->{\oint_{\partial S}\bv{F}\cdot d\bv{s}
        =} \onslide<13->{\oint_{\bv{x}_{\operatorname{top}}}\bv{F}\cdot d\bv{s}
        + \oint_{\bv{x}_{\operatorname{bot}}}\bv{F}\cdot d\bv{s}
        =} \onslide<14->{\sage{circ}\,\si{\newton\cdot\metre}}
    \]
  \end{example}

\end{frame}


\section{Stokes' Theorem}
\subsection{Curl}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Suppose $\bv{F}$ is a vector field on a surface $S$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \pgfmathsetmacro{\myx}{1.5}
        \pgfmathsetmacro{\myy}{0.6*\myx}
        \begin{scope}[yscale=\myy, xscale=\myx, shift={(-2, -2)}]
          \coordinate (O) at (0, 0);
          \coordinate (p1) at (3, -1);
          \coordinate (p2) at (5, 2);
          \coordinate (p3) at ($ (p2)-(p1) $);

          \filldraw[bluefilldraw]
          (O)  to[bend left]
          (p1) to[bend left]
          (p2) to[bend right]
          (p3) to[bend right]
          (O) --cycle;
        \end{scope}


        \begin{scope}[scale=1/2]
          \pgfmathsetmacro{\myN}{10}
          \pgfmathsetmacro{\myDx}{11/\myN}
          \pgfmathsetmacro{\myDy}{8/\myN}
          \foreach \i in {0,...,9}
          \foreach \j in {0,...,9}{
            \pgfmathsetmacro{\myx}{-4+\i*\myDx}
            \pgfmathsetmacro{\myy}{-4+\j*\myDy}
            \pgfmathsetmacro{\myFx}{\myx+sin(\myy r)}
            \pgfmathsetmacro{\myFy}{\myy+sin(\myx r)}
            % \node at (\myx, \myy) {x};
            \draw[thick, beamgreen, arrows={-stealth}] (\myx, \myy) -- (\myFx, \myFy);
          }
        \end{scope}

      \end{tikzpicture}
    \]
    Can information ``on'' $S$ help calculate circulation
    $\oint_{\partial S}\bv{F}\cdot d\bv{s}$?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDet}{
    \left\lvert
      \begin{array}{ccc}
        \bv{e}_1   & \bv{e}_2   & \bv{e}_3   \\
        \partial_x & \partial_y & \partial_z \\
        F_1        & F_2        & F_3
      \end{array}
    \right\rvert
  }
  \newcommand{\myCross}{
    \left\langle
      \pdv{F_3}{y}-\pdv{F_2}{z}, \pdv{F_1}{z}-\pdv{F_3}{x}, \pdv{F_2}{x}-\pdv{F_1}{y}
    \right\rangle
  }
  \begin{definition}
    The \emph{curl} of a vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ is
    the symbolic cross product
    \begin{gather*}
      \curl(\bv{F})
      = \nabla\times\bv{F}
      = \myDet
      = \myCross
    \end{gather*}
    \onslide<2->{Note that $\curl(\bv{F})$ is a \emph{vector field}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mycurl}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, beamblue] (a) {$\curl(\bv{F})$};
      \onslide<2->{
        \node[overlay, below right= 0mm and 3mm of a] (text) {\stackanchor{direction producing}{max ccw rotation}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Interpretation}
    Curl measures the propensity of $\bv{F}$ to produce \emph{rotation}.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , rotate=-30
        ]

        \coordinate (O) at (0, 0);

        \draw[->, beamgreen] plot[smooth, domain=-pi/3:4*pi/3] ({cos(\x r)}, {sin(\x r)/4});
        \draw[beamblue, ->] (O) -- (0, 2) node[right, overlay] {\mycurl};

        \node[myDot, label=below:{$P$}] at (O) {};

      \end{tikzpicture}
    \]
    \onslide<3->{Here, we think of $\curl(\bv{F})=\nabla\times\bv{F}$ as
      \emph{infinitesimal torque}.}
  \end{block}

\end{frame}


\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Stokes' Theorem]
    $\displaystyle\iint_{S}\curl(\bv{F})\cdot d\bv{S}=\oint_{\partial S}\bv{F}\cdot d\bv{s}$
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myStokes}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<2->{red}\curl(\bv{F})}$};
      \onslide<2->{
        \node[overlay, below left= 4mm and -3mm of a] (text) {$\dfrac{\operatorname{force}}{\operatorname{distance}}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mydS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<3->{beamgreen}d\bv{S}}$};
      \onslide<3->{
        \node[overlay, below left= 14mm and 0mm of a] (text) {$\operatorname{distance}^2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myF}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\bv{F}}$};
      \onslide<4->{
        \node[overlay, below right= 6mm and 0mm of a] (text) {$\operatorname{force}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myds}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{beamgreen}d\bv{s}}$};
      \onslide<5->{
        \node[overlay, below right= 3mm and 0mm of a] (text) {$\operatorname{distance}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Observation}
    Tracking units helps make sense of Stokes' Theorem.
    \[
      \iint_S \myStokes\cdot\mydS
      =
      \oint_{\partial D}\myF\cdot\myds
      \onslide<6->{= \textnormal{circulation around }S}
    \]
  \end{block}

\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y z theta')
  F = vector([x*y, 2*x*z, x*y])
  X = vector([cos(theta), sin(theta), z])
  Ttheta = X.diff(theta)
  Tz = X.diff(z)
  N = Ttheta.cross_product(Tz)
  curlF = F.curl([x, y, z])
  curlFs = curlF(x=X[0], y=X[1], z=X[2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myCyl}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{0.75}
        \pgfmathsetmacro{\H}{4*\R}
        \pgfmathsetmacro{\r}{\R/8}

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:180:{\R} and {\r}) --
        (-\R, 0) arc(180:0:{\R} and {\r}) --
        cycle;

        \draw[overlay, beamblue, ->]
        (\R, \H/2) -- ++(-\R, 0) node[left] {$\bv{N}$};

        \filldraw[bluefilldraw]
        (\R, 0) --
        (\R, \H) arc(0:-180:{\R} and {\r}) --
        (-\R, 0) arc(-180:0:{\R} and {\r}) --
        cycle;

        \draw[beamblue, ->] (\R, 0) arc(0:-90:{\R} and {\r});
        \draw[beamblue, ->] (-\R, \H) arc(180:270:{\R} and {\r});

        \draw[thick, red, <-, shorten <= 2pt]
        (-\R, \H/2) -| ++(-12mm, 1mm) node[above] {$x^2+y^2=1$};

        \draw[thick, red, <-, overlay, shorten <= 2pt]
        (-\R, 0) -- ++(-4mm, 0) node[left] {$z=0$};

        \draw[thick, red, <-, overlay, shorten <= 2pt]
        (-\R, \H) -- ++(-4mm, 0) node[left] {$z=4$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myx}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}\cos(\theta)}$};
      \onslide<4->{
        \node[overlay, above left= 1mm and -1mm of a] (text) {$x$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myy}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {${\color<5->{beamblue}\sin(\theta)}$};
      \onslide<5->{
        \node[overlay, above left= 1mm and -1mm of a] (text) {$y$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myz}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {${\color<6->{beamgreen}z}$};
      \onslide<6->{
        \node[overlay, above right= 2mm and 1mm of a] (text) {$z$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<9->{red}\sage{N}}$};
      \onslide<9->{
        \node[overlay, above right= 0mm and 1mm of a] (text) {wrong direction!};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }
    }
  }
  \newcommand{\myNegN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<14->{red}(-\bv{N})}$};
      \onslide<14->{
        \node[overlay, below right= 1mm and 3mm of a] (text) {switch direction!};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myCurlFN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<20->{red}\curl(\bv{F})\cdot d\bv{S}}$};
      \onslide<20->{
        \node[overlay, below left= 4mm and -9mm of a] (text) {$\curl(\bv{F})\cdot(-\bv{N})\,dA=1\,dA$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myCalc}{
    \begin{aligned}
      \onslide<2->{\bv{X}(\theta, z)} &\onslide<2->{=} \onslide<3->{\langle\myx, \myy, \myz\rangle}      \\
      \onslide<7->{\bv{N}}            &\onslide<7->{=} \onslide<8->{\myN} \\
      \onslide<10->{\curl(\bv{F})}     &\onslide<10->{=} \onslide<11->{\sage{curlF}}  \\
      &\onslide<11->{=} \onslide<12->{\sage{curlFs}}                   \\
      \onslide<13->{\curl(\bv{F})\cdot\myNegN} &\onslide<13->{=} \onslide<15->{\cos^2(\theta)+\sin^2(\theta) =} \onslide<16->{\sage{(curlFs*(-N)).simplify_trig()}}
    \end{aligned}
  }
  \begin{example}
    Let $S$ be the depicted cylinder and consider
    $\bv{F}=\sage{F}\,\si{\newton}$.
    \begin{gather*}
      \begin{align*}
        \myCyl && \myCalc
      \end{align*}
    \end{gather*}
    \onslide<17->{According to Stokes' Theorem, the circulation is}
    \[
      \onslide<18->{\oint_{\partial S}\bv{F}\cdot d\bv{s}
        =} \onslide<19->{\iint_{S}\myCurlFN
        =} \onslide<21->{\int_{0}^{2\,\pi}\int_{0}^4 1\,dz\,d\theta
        =} \onslide<22->{8\,\pi\,\si{\newton\cdot\metre}}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([3*z, 5*x, -2*y])
  X = vector([x, y, y+3])
  Tx = X.diff(x)
  Ty = X.diff(y)
  N = Tx.cross_product(Ty)
  curlF = F.curl([x, y, z])
  curlFs = curlF(x=X[0], y=X[1], z=X[2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myC}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \pgfmathsetmacro{\R}{1}
        \pgfmathsetmacro{\Hl}{1}
        \pgfmathsetmacro{\Hr}{1.5}
        \pgfmathsetmacro{\pitch}{\R/6}
        \pgfmathsetmacro{\D}{abs(\Hr-\Hl)/2}

        \pgfmathsetmacro{\Rt}{sqrt(\R^2+\D^2)}
        \pgfmathsetmacro{\t}{atan(\D/\R)}

        \begin{scope}[rotate=\t]
          \coordinate (Spt) at (-\Rt/2, \pitch/2);
        \end{scope}

        \onslide<2->{
          \draw[<-, red, thick, overlay, shorten <= 2pt]
          (Spt) |- ++(-0.75, -4mm) node[left] {$z=y+3$};
        }

        \begin{scope}[rotate=\t, transform shape]
          \onslide<2->{
            \filldraw[bluefilldraw] (O) circle ({\Rt} and {\pitch});
          }
          \draw[beamblue, ->] (-\Rt, 0) arc(180:300:{\Rt} and {\pitch});
          \draw[beamblue] (O) circle ({\Rt} and {\pitch});

          \onslide<8->{
            \draw[beamblue, overlay, ->] (O) -- ++(0, 1/2)
            node[right] {$\bv{N}$};
          }

          \coordinate (C) at (\Rt, 0);
        \end{scope}

        \node[right, overlay, beamblue] at (C) {$C\onslide<3->{=\partial S}$};

        \draw[<-, red, overlay, thick]
        (-\R/2, -\Hl-\D) |- ++(-0.5, -3mm) node[left] {$x^2+y^2\leq 1$};
        \filldraw[greenfilldraw] (0, -\Hl-\D) circle ({\R} and {\pitch});


      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myCalc}{
    \begin{aligned}
      \onslide<4->{\bv{X}(x, y)} &\onslide<4->{=} \onslide<5->{\sage{X}} \\
      \onslide<6->{\bv{N}}       &\onslide<6->{=} \onslide<7->{\sage{N}} \\
      \onslide<9->{\curl(\bv{F})} &\onslide<9->{=} \onslide<10->{\sage{curlF}} \\
      \onslide<11->{\curl(\bv{F})\cdot\bv{N}} &\onslide<11->{=} \onslide<12->{\sage{curlFs*N}}
    \end{aligned}
  }
  \begin{example}
    Consider the depicted curve $C$ and $\bv{F}=\sage{F}\,\si{\newton}$.
    \begin{align*}
      \myC && \myCalc
    \end{align*}
    \onslide<13->{According to Stokes' Theorem, we have}
    \begin{gather*}
      \onslide<13->{\oint_{C}\bv{F}\cdot d\bv{s}
        =} \onslide<14->{\oint_{\partial S}\bv{F}\cdot d\bv{s}
        =} \onslide<15->{\iint_{S}\curl(\bv{F})\cdot d\bv{S}
        =} \onslide<16->{\iint_{D}2\,dA
        =} \onslide<17->{2\area(D)
        =} \onslide<18->{2\,\pi\,\si{\newton\cdot\metre}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([x*y+1, -x*z, -y*z])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\R}{2};
        \pgfmathsetmacro{\r}{\R/4};

        \draw[beamblue] (\R, 0) arc (0:180:{\R} and {\r});
        \filldraw[bluefilldraw] (O) circle (\R);

        \node[name=r] at (\R/2, 0) {$8$};
        \draw[dashed] (O) -- (r.west);
        \draw[dashed] (r.east) -- (\R, 0);
        \node[myDot] at (O) {};
        \node[left] at (O) {$(-3, 7, 4)$};
        \filldraw[bluefilldraw] (O) circle (\R);
        \draw[beamblue] (\R, 0) arc (0:-180:{\R} and {\r});

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myDiv}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<6->{red}\sage{F.div([x, y, z])}}$};
      \onslide<6->{
        \node[overlay, below right= 0mm and 3mm of a] (text) {$\vdiv(\bv{F})=0$ means $\bv{F}=\curl(\bv{G})$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myCalc}{
    \begin{aligned}
      \onslide<2->{\vdiv(\bv{F})} &\onslide<2->{=} \onslide<3->{\pdv*{\Set{\sage{F[0]}}}{x}+\pdv*{\Set{\sage{F[1]}}}{y}+\pdv*{\Set{\sage{F[0]}}}{z}} \\
      &\onslide<3->{=} \onslide<4->{\sage{F[0].diff(x)}+\sage{F[1].diff(y)}+(\sage{F[2].diff(z)})} \\
      &\onslide<4->{=} \onslide<5->{\myDiv}
    \end{aligned}
  }
  \begin{example}
    Calculate the flux of $\bv{F}=\sage{F}\,\si{\metre\per\second}$ over $S$.
    \begin{gather*}
      \begin{align*}
        \myS && \myCalc
      \end{align*}
    \end{gather*}
    \onslide<7->{Since $\partial S=\varnothing$, Stokes' Theorem implies}
    \[
      \onslide<8->{\iint_{S}\bv{F}\cdot d\bv{S}
        =} \onslide<9->{\iint_{S}\curl(\bv{G})\cdot d\bv{S}
        =} \onslide<10->{\oint_{\partial S}\bv{G}\cdot d\bv{s}
        =} \onslide<11->{0\,\si{\metre\cubed\per\second}}
    \]
  \end{example}

\end{frame}


\end{document}
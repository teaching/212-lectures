\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}

\title{grad, curl, and div}
\subtitle{Math 212}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{The Gradient Operator}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}<1->
    The \emph{gradient} of $f\in\mathscr{C}(\mathbb{R}^n)$ is a vector field
    $\nabla f\in\mathfrak{X}(\mathbb{R}^n)$.
  \end{definition}

  \begin{definition}<2->
    The \emph{gradient operator}
    \[
      \mathscr{C}(\mathbb{R}^n)\xrightarrow{\grad}\mathfrak{X}(\mathbb{R}^n)
    \]
    is defined by $\grad(f)=\nabla f$.
  \end{definition}


  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<4->{red}f}$};
      \onslide<4->{
        \node[overlay, below right= 0mm and 3mm of a] (text) {``potential'' or ``antigradient''};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{definition}<3->
    We say $\bv{F}$ is \emph{conservative} if $\bv{F}=\nabla \myf$.
  \end{definition}

\end{frame}


\subsection{Conservative Fields}

\begin{sagesilent}
  var('x y')
  F = vector([y, x])
  G = vector([y, -x])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myplotc}[2]{
    \draw[-stealth, very thick, beamblue] (####1, ####2) -- ++(####2/2, ####1/2);
    \draw[very thick, beamblue] (####1, ####2) -- ++(-####2/2, -####1/2);
  }
  \newcommand{\myConservative}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=1.5
        ]

        \coordinate (O) at (0, 0);

        \myplotc{0.25}{0.25}
        \myplotc{-0.25}{0.25}
        \myplotc{-0.25}{-0.25}
        \myplotc{0.25}{-0.25}

        \myplotc{0.75}{0}
        \myplotc{-0.75}{0}
        \myplotc{0}{0.75}
        \myplotc{0}{-0.75}

        \myplotc{0.5}{0.25}
        \myplotc{-0.5}{0.25}
        \myplotc{-0.5}{-0.25}
        \myplotc{0.5}{-0.25}

        \myplotc{0.25}{0.50}
        \myplotc{-0.25}{0.50}
        \myplotc{-0.25}{-0.50}
        \myplotc{0.25}{-0.50}

        \node[above, beamgreen] at (current bounding box.north) {Conservative};
        \node[below] at (current bounding box.south) {$\bv{F}=\nabla(xy)$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myplotn}[2]{
    \draw[-stealth, very thick, red] (####1, ####2) -- ++(####2/2, -####1/2);
    \draw[very thick, red] (####1, ####2) -- ++(-####2/2, ####1/2);
  }
  \newcommand{\myNonConservative}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=1.5
        ]

        \coordinate (O) at (0, 0);

        \myplotn{0.25}{0.25}
        \myplotn{-0.25}{0.25}
        \myplotn{-0.25}{-0.25}
        \myplotn{0.25}{-0.25}

        \myplotn{0.75}{0}
        \myplotn{-0.75}{0}
        \myplotn{0}{0.75}
        \myplotn{0}{-0.75}

        \myplotn{0.5}{0.25}
        \myplotn{-0.5}{0.25}
        \myplotn{-0.5}{-0.25}
        \myplotn{0.5}{-0.25}

        \myplotn{0.25}{0.50}
        \myplotn{-0.25}{0.50}
        \myplotn{-0.25}{-0.50}
        \myplotn{0.25}{-0.50}

        \node[above, beamgreen] at (current bounding box.north) {Non-Conservative};
        \node[below] at (current bounding box.south) {$\bv{G}=\sage{G}$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the following vector fields.
    \begin{align*}
      \onslide<2->{\myConservative} && \onslide<3->{\myNonConservative}
    \end{align*}
    \onslide<4->{Note that $\bv{G}\neq\nabla g$ since otherwise
      $\sage{G}=\langle g_x, g_y\rangle$ and}
    \begin{align*}
      \onslide<5->{g_{xy}} &\onslide<5->{=} \onslide<6->{\pdv{(g_x)}{y} =} \onslide<7->{\pdv{(\sage{G[0]})}{y} =} \onslide<8->{\sage{G[0].diff(y)}} & \onslide<9->{g_{yx}} &\onslide<9>{=} \onslide<10->{\pdv{(g_y)}{x} =} \onslide<11->{\pdv{(\sage{G[1]})}{x} =} \onslide<12->{\sage{G[1].diff(x)}}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Some vector fields are \emph{not conservative}.
  \end{block}

  \begin{block}{Question}<2->
    What makes conservative vector fields interesting?
  \end{block}

\end{frame}


\subsection{Fundamental Theorem of Line Integrals}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Fundamental Theorem of Line Integrals]
    Suppose a particle travels from $P$ to $Q$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);

        \pgfmathsetmacro{\xmax}{8}
        \pgfmathsetmacro{\ymax}{2}

        \coordinate (P) at (0, 0);
        \coordinate (Q) at (\xmax, \ymax);

        \useasboundingbox (0, -0.75) rectangle (\xmax, 2.75);

        \onslide<4-6>{
          \draw[beamblue] (P) .. controls +(\xmax/2, -3) and +(-\xmax/2, 3) .. (Q);
        }

        \onslide<7>{
          \draw[beamblue] (P) .. controls +(\xmax/2, -2) and +(-\xmax/2, 2) .. (Q);
        }

        \onslide<8>{
          \draw[beamblue] (P) .. controls +(\xmax/2, -1) and +(-\xmax/2, 1) .. (Q);
        }

        \onslide<9>{
          \draw[beamblue] (P) .. controls +(\xmax/2,  0) and +(-\xmax/2, 0) .. (Q);
        }

        \onslide<10>{
          \draw[beamblue]      (P) -- (Q);
        }

        \onslide<11>{
          \draw[beamblue] (P) .. controls +(\xmax/2, 2) and +(-\xmax/2, -2) .. (Q);
        }

        \onslide<12>{
          \draw[beamblue] (P) .. controls +(\xmax/2, 3) and +(-\xmax/2, -3) .. (Q);
        }

        \onslide<13>{
          \draw[beamblue] (P) .. controls +(\xmax/2, 4) and +(-\xmax/2, -4) .. (Q);
        }

        \onslide<14>{
          \draw[beamblue] (P) .. controls +(\xmax/2, 5) and +(-\xmax/2, -5) .. (Q);
        }

        \onslide<2->{
          \node[myDot, label=left:{$P$}] at (P) {};
        }

        \onslide<3->{
          \node[myDot, label=right:{$Q$}] at (Q) {};
        }

      \end{tikzpicture}
    \]
    \onslide<5->{The work done by $\nabla{f}$ on the particle is $f(Q)-f(P)$.}
  \end{theorem}

  \begin{block}{Observation}<6->
    For conservative vector fields, work is \emph{path independent}.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Fundamental Theorem of Line Integrals]
    $\displaystyle\int_{\bv{x}}\nabla f\cdot d\bv{s}=f(Q)-f(P)$
  \end{theorem}
  \begin{proof}<2->
    $
    \begin{aligned}
      \int_{\bv{x}}\nabla f\cdot d\bv{s}
      &= \onslide<3->{\int_a^b \nabla f(\bv{x}(t))\cdot\bv{x}^\prime(t)\,dt} \\
      &\onslide<3->{=} \onslide<4->{\int_a^b \odv*{\Set{f(\bv{x}(t))}}{t}\,dt} \\
      &\onslide<4->{=} \onslide<5->{f(\bv{x}(b))-f(\bv{x}(a))} \\
      &\onslide<5->{=} \onslide<6->{f(Q)-f(P)\qedhere}
    \end{aligned}
    $
  \end{proof}

\end{frame}


\begin{sagesilent}
  var('x y t')
  f = x**2*y**2+x**2-3*y**2
  F = f.gradient([x, y])
  r = vector([sec(pi*t)**3, exp(t**2)*cos(pi*t/2)])
  v = r.diff(t)
  a, b = 0, 1
  p = r(t=a)
  q = r(t=b)
  fq = f(x=q[0], y=q[1])
  fp = f(x=p[0], y=p[1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myf}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<1->{red}\sage{f}}$};
      \onslide<1->{
        \node[overlay, above right= 0mm and 6mm of a] (text) {$\bv{F}=\nabla f$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the data
    \begin{align*}
      \bv{F} &= \sage{F} & \bv{x}(t) &= \langle \sec^3(\pi\,t), e^{t^2}\cdot\cos(\sfrac{\pi t}{2})\rangle
    \end{align*}

    for $0\leq t\leq 1$. \onslide<2->{The work is}
    \begin{align*}
      \onslide<2->{\int_{\bv{x}}\bv{F}\cdot d\bv{s}} &\onslide<2->{=} \onslide<3->{\int_{\bv{x}}\sage{F}\cdot d\bv{s}}     \\
                                                     &\onslide<3->{=} \onslide<4->{\int_{\bv{x}}\nabla(\myf)\cdot d\bv{s}} \\
                                                     &\onslide<4->{=} \onslide<5->{f\sage{tuple(q)}-f\sage{tuple(p)}}      \\
                                                     &\onslide<5->{=} \onslide<6->{\sage{fq}-(\sage{fp})}                  \\
                                                     &\onslide<6->{=} \onslide<7->{\sage{fq-fp}}
    \end{align*}
  \end{example}

\end{frame}


\section{The Curl Operator}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{del operator} is the symbolic vector
    \[
      \nabla = \left\langle\pdv{}{x_1}, \pdv{}{x_2}, \dotsc, \pdv{}{x_n}\right\rangle
    \]
    \onslide<2->{For brevity, we also write
      $\nabla = \langle \partial_{x_1}, \partial_{x_2}, \dotsc,
      \partial_{x_n}\rangle$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDet}{
    \left\lvert
      \begin{array}{ccc}
        \bv{e}_1   & \bv{e}_2   & \bv{e}_3   \\
        \partial_x & \partial_y & \partial_z \\
        F_1        & F_2        & F_3
      \end{array}
    \right\rvert
  }
  \newcommand{\myCross}{
    \left\langle
      \pdv{F_3}{y}-\pdv{F_2}{z}, \pdv{F_1}{z}-\pdv{F_3}{x}, \pdv{F_2}{x}-\pdv{F_1}{y}
    \right\rangle
  }
  \begin{definition}
    The \emph{curl} of a vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ is
    the symbolic cross product
    \begin{gather*}
      \curl(\bv{F})
      = \nabla\times\bv{F}
      = \myDet
      = \myCross
    \end{gather*}
    \onslide<2->{Note that $\curl(\bv{F})$ is a \emph{vector field}.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([sin(x*y), x*z, y*log(x)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDet}{
    \left\lvert
      \begin{array}{ccc}
        \bv{e}_1    & \bv{e}_2    & \bv{e}_3   \\
        \partial_x  & \partial_y  & \partial_z \\
        \sage{F[0]} & \sage{F[1]} & \sage{F[2]}
      \end{array}
    \right\rvert
  }
  \begin{example}
    Consider the vector field
    \[
      \bv{F} = \sage{F}
    \]
    \onslide<2->{The curl of $\bv{F}$ is}
    \begin{gather*}
      \onslide<2->{\curl(\bv{F})
        =} \onslide<3->{\nabla\times\bv{F}
        =} \onslide<4->{\myDet
        =} \onslide<5->{\langle \log(x)-x, 0-\sfrac{y}{x}, z-x\cos(xy)\rangle}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Interpretation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mycurl}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, beamblue] (a) {$\curl(\bv{F})$};
      \onslide<2->{
        \node[overlay, below right= 0mm and 3mm of a] (text) {\stackanchor{direction producing}{max ccw rotation}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{block}{Interpretation}
    Curl measures the propensity of $\bv{F}$ to produce \emph{rotation}.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , rotate=-30
        ]

        \coordinate (O) at (0, 0);

        \draw[->, beamgreen] plot[smooth, domain=-pi/3:4*pi/3] ({cos(\x r)}, {sin(\x r)/4});
        \draw[beamblue, ->] (O) -- (0, 2) node[right, overlay] {\mycurl};

        \node[myDot, label=below:{$P$}] at (O) {};

      \end{tikzpicture}
    \]
    \onslide<3->{Here, we think of $\curl(\bv{F})=\nabla\times\bv{F}$ as
      \emph{infinitesimal torque}.}
  \end{block}

\end{frame}


\subsection{Scalar Curl}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDet}{
    \left\lvert
      \begin{array}{ccc}
        \bv{e}_1    & \bv{e}_2    & \bv{e}_3   \\
        \partial_x  & \partial_y  & \partial_z \\
        F_1         & F_2         & 0
      \end{array}
    \right\rvert
  }
  \begin{block}{Observation}
    We may identify
    $\bv{F}=\langle F_1, F_2\rangle\in\mathfrak{X}(\mathbb{R}^2)$ with
    $\bv{F}=\langle F_1, F_2, 0\rangle$, giving
    \begin{gather*}
      \curl(\bv{F})
      = \onslide<2->{\myDet
        =} \onslide<3->{\left\langle 0-\pdv{F_2}{z}, \pdv{F_1}{z}-0, \pdv{F_2}{x}-\pdv{F_1}{y}\right\rangle
        =} \onslide<4->{\left\langle 0, 0, \pdv{F_2}{x}-\pdv{F_1}{y}\right\rangle}
    \end{gather*}
    \onslide<5->{This allows us to study curl in two dimensions.}
  \end{block}

  \newcommand{\mygrn}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {${\color<7->{red}\grn}$};
      \onslide<7->{
        \node[overlay, below left= 0mm and 3mm of a] (text) {``Green's operator''};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{definition}<6->
    For $\bv{F}\in\mathfrak{X}(\mathbb{R}^2)$ we define
    \[
      \mygrn(\bv{F}) = \pdv{F_2}{x}-\pdv{F_1}{y}
    \]
    \onslide<8->{Note that $\grn(\bv{F})$ is a \emph{scalar}.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y')
  F = vector([x*sin(x*y), x**3-y])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^2)$ given by
    \[
      \bv{F} = \sage{F}
    \]
    \onslide<2->{The scalar curl of $\bv{F}$ is}
    \[
      \onslide<2->{\grn(\bv{F})
        =} \onslide<3->{\pdv*{\Set{\sage{F[1]}}}{x} - \pdv*{\Set{\sage{F[0]}}}{y}
        =} \onslide<4->{\sage{F[1].diff(x)} - \sage{F[0].diff(y)}}
    \]
  \end{example}

\end{frame}

\section{The Divergence Operator}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{divergence} of $\bv{F}\in\mathfrak{X}(\mathbb{R}^n)$ is the
    symbolic dot product
    \[
      \vdiv(\bv{F})
      = \nabla\cdot\bv{F}
      = \pdv{F_1}{x_1}+\pdv{F_2}{x_2}+\dotsb+\pdv{F_n}{x_n}
    \]
    \onslide<2->{Note that $\vdiv(\bv{F})$ is a \emph{scalar}.}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([sin(x*y)*z, cos(x*y), log(x**2+3*y**2)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ given by
    \[
      \bv{F} = \sage{F}
    \]
    \onslide<2->{The divergence of $\bv{F}$ is}
    \begin{align*}
      \onslide<3->{\vdiv(\bv{F})}
      &\onslide<3->{=} \onslide<4->{\pdv*{\Set{\sage{F[0]}}}{x} + \pdv*{\Set{\sage{F[1]}}}{y} + \pdv*{\Set{\sage{F[2]}}}{z}} \\
      &\onslide<4->{=} \onslide<5->{\sage{F[0].diff(x)}-\sage{-F[1].diff(y)}}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Interpretation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mysource}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \draw[beamgreen] (O) circle (2 and 3/2);

        \node at (O) {\stackanchor{source $\vdiv(\bv{F})>0$}{``net flow out''}};

        \pgfmathsetmacro{\myx}{2*cos(45)}
        \pgfmathsetmacro{\myy}{3*sin(45)/2}
        \draw[beamblue, ->] (2, 0) -- ++(1/2, 0);
        \draw[beamblue, ->] (\myx, \myy) -- ++(\myx/4, \myy*8/18);
        \draw[beamblue, ->] (0, 3/2) -- ++(0, 1/2);
        \draw[beamblue, ->] (-\myx, \myy) -- ++(-\myx/4, \myy*8/18);
        \draw[beamblue, ->] (-2, 0) -- ++(-1/2, 0);
        \draw[beamblue, ->] (-\myx, -\myy) -- ++(-\myx/4, -\myy*8/18);
        \draw[beamblue, ->] (0, -3/2) -- ++(0, -1/2);
        \draw[beamblue, ->] (\myx, -\myy) -- ++(\myx/4, -\myy*8/18);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\mysink}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        ]

        \coordinate (O) at (0, 0);
        \draw[red] (O) circle (2 and 3/2);

        \node at (O) {\stackanchor{sink $\vdiv(\bv{F})<0$}{``net flow in''}};

        \pgfmathsetmacro{\myx}{2*cos(45)}
        \pgfmathsetmacro{\myy}{3*sin(45)/2}
        \draw[beamblue, <-] (2, 0) -- ++(1/2, 0);
        \draw[beamblue, <-] (\myx, \myy) -- ++(\myx/4, \myy*8/18);
        \draw[beamblue, <-] (0, 3/2) -- ++(0, 1/2);
        \draw[beamblue, <-] (-\myx, \myy) -- ++(-\myx/4, \myy*8/18);
        \draw[beamblue, <-] (-2, 0) -- ++(-1/2, 0);
        \draw[beamblue, <-] (-\myx, -\myy) -- ++(-\myx/4, -\myy*8/18);
        \draw[beamblue, <-] (0, -3/2) -- ++(0, -1/2);
        \draw[beamblue, <-] (\myx, -\myy) -- ++(\myx/4, -\myy*8/18);

      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    Points can be classified as \emph{sources} or \emph{sinks} of $\bv{F}$.
    \begin{align*}
      \onslide<2->{\mysource} && \onslide<3->{\mysink}
    \end{align*}
  \end{definition}

\end{frame}



\begin{sagesilent}
  var('x y')
  F = vector([x, y])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myplotc}[2]{
    \draw[-stealth, very thick, beamblue] (####1, ####2) -- ++(####1/2, ####2/2);
    \draw[very thick, beamblue] (####1, ####2) -- ++(-####1/2, -####2/2);
  }
  \newcommand{\myF}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , myDot/.style={circle, fill, inner sep=2pt}
        , axis/.style={<->}
        , vector/.style={->}
        , scale=1.5
        ]

        \coordinate (O) at (0, 0);

        \myplotc{0.25}{0.25}
        \myplotc{-0.25}{0.25}
        \myplotc{-0.25}{-0.25}
        \myplotc{0.25}{-0.25}

        \myplotc{0.75}{0}
        \myplotc{-0.75}{0}
        \myplotc{0}{0.75}
        \myplotc{0}{-0.75}

        \myplotc{0.5}{0.25}
        \myplotc{-0.5}{0.25}
        \myplotc{-0.5}{-0.25}
        \myplotc{0.5}{-0.25}

        \myplotc{0.25}{0.50}
        \myplotc{-0.25}{0.50}
        \myplotc{-0.25}{-0.50}
        \myplotc{0.25}{-0.50}

        % \node[above, beamgreen] at (current bounding box.north) {Conservative};
        % \node[below] at (current bounding box.south) {$\bv{F}=\sage{F}$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Suppose fluid flow is described by $\bv{F}=\sage{F}$.
    \[
      \myF
    \]
    \onslide<2->{It appears that fluid is \emph{expanding} as it moves away from
      the origin.}
    \[
      \onslide<2->{\vdiv(\bv{F})
        =} \onslide<3->{\pdv*{\Set{x}}{x} + \pdv*{\Set{y}}{y}
        =} \onslide<4->{1+1
        =} \onslide<5->{2
        >} \onslide<6->{0}
    \]
    \onslide<7->{Every point is a \emph{source} of $\bv{F}$.}
  \end{example}

\end{frame}


\section{The Operator Exact Sequence}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Our operators may be organized into a diagram
    \[
      \mathscr{C}(\mathbb{R}^3)
      \xrightarrow{\grad}\mathfrak{X}(\mathbb{R}^3)
      \xrightarrow{\curl}\mathfrak{X}(\mathbb{R}^3)
      \xrightarrow{\vdiv}\mathscr{C}(\mathbb{R}^3)
    \]
    \onslide<2->{This diagram is called the \emph{operator exact sequence}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Each of the operators in
    \[
      \mathscr{C}(\mathbb{R}^3)
      \xrightarrow{\grad}\mathfrak{X}(\mathbb{R}^3)
      \xrightarrow{\curl}\mathfrak{X}(\mathbb{R}^3)
      \xrightarrow{\vdiv}\mathscr{C}(\mathbb{R}^3)
    \]
    is linear, meaning
    \begin{align*}
      \grad(c_1\cdot f_1+c_2\cdot f_2)              &= c_1\cdot\grad(f_1)+c_2\cdot\grad(f_2)      \\
      \curl(c_1\cdot\bv{F}_1+c_2\cdot\bv{F}_2) &= c_1\cdot\curl(\bv{F}_1)+c_2\cdot\curl(\bv{F}_2) \\
      \vdiv(c_1\cdot\bv{F}_1+c_2\cdot\bv{F}_2) &= c_1\cdot\vdiv(\bv{F}_1)+c_2\cdot\vdiv(\bv{F}_2)
    \end{align*}
    \onslide<2->{Furthermore, the operators compose to zero}
    \begin{align*}
      \onslide<2->{\curl(\grad(f))} &\onslide<2->{= \bv{O}} & \onslide<3->{\vdiv(\curl(\bv{F}))} &\onslide<3->{= 0}
    \end{align*}
    \onslide<4->{These equations can be used to test if $\bv{F}=\nabla f$ or
      $\bv{F}=\curl(\bv{G})$.}
  \end{theorem}

\end{frame}

\subsection{Constructing ``Antigradients''}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Poincar\'e's Lemma for Antigradients]
    Suppose $\curl(\bv{F})=\bv{O}$. Then $\bv{F}=\nabla f$ where
    \begin{gather*}
      f = \int_0^1
      x\cdot\bv{F}_1(t x, t y, t z)
      + y\cdot\bv{F}_2(t x, t y, t z)
      + z\cdot\bv{F}_3(t x, t y, t z)
      \,dt
    \end{gather*}
  \end{theorem}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([x*y, -sin(z), y])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDet}{
    \left\lvert
      \begin{array}{ccc}
        \bv{e}_1    & \bv{e}_2    & \bv{e}_3   \\
        \partial_x  & \partial_y  & \partial_z \\
        \sage{F[0]} & \sage{F[1]} & \sage{F[2]}
      \end{array}
    \right\rvert
  }
  \begin{example}
    Consider the vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ given by
    \[
      \bv{F}=\sage{F}
    \]
    \onslide<2->{The curl of $\bv{F}$ is}
    \[
      \onslide<2->{\curl(\bv{F})
        =} \onslide<3->{\myDet
        =} \onslide<4->{\sage{F.curl([x, y, z])}
        \neq} \onslide<5->{\bv{O}}
    \]
    \onslide<6->{This means that $\bv{F}$ is \emph{not conservative}, so
      $\bv{F}\neq\nabla f$.}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z t')
  F = vector([6*x*y, 3*x**2+z**2+1, 2*y*z])
  F1t, F2t, F3t = F(x=t*x, y=t*y, z=t*z)
  p = expand(x*F1t+y*F2t+z*F3t)
  f = expand(integral(p, t, 0, 1))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDet}{
    \left\lvert
      \begin{array}{ccc}
        \bv{e}_1    & \bv{e}_2    & \bv{e}_3   \\
        \partial_x  & \partial_y  & \partial_z \\
        \sage{F[0]} & \sage{F[1]} & \sage{F[2]}
      \end{array}
    \right\rvert
  }
  \begin{example}
    Consider the vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ given by
    \begin{gather*}
      \begin{align*}
        \bv{F} &= \sage{matrix.column(F)} & \onslide<2->{\curl(\bv{F})} &\onslide<2->{=} \onslide<3->{\myDet =} \onslide<4->{\sage{F.curl([x, y, z])}}
      \end{align*}
    \end{gather*}
    \onslide<5->{Since $\curl(\bv{F})=\bv{O}$ we have $\bv{F}=\nabla f$ where}
    \begin{align*}
      \onslide<5->{f}
      &\onslide<5->{=} \onslide<6->{\int_0^1 x\cdot 6(tx)(ty)+y\cdot(3\,t^2x^2+t^2z^2+1)+z\cdot 2(ty)(tz)\,dt} \\
      &\onslide<6->{=} \onslide<7->{\int_0^1\sage{p}\,dt} \\
      &\onslide<7->{=} \onslide<8->{\sage{f}}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Constructing ``Anticurl''}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Poincar\'e's Lemma for Anticurl]
    Suppose $\vdiv(\bv{F})=0$. Then $\bv{F}=\curl(\bv{G})$ where
    \begin{align*}
      G_1 &= \int_0^1 t\cdot zF_2(tx, ty, tz)-t\cdot yF_3(tx, ty, tz)\,dt \\
      G_2 &= \int_0^1 t\cdot xF_3(tx, ty, tz)-t\cdot zF_1(tx, ty, tz)\,dt \\
      G_3 &= \int_0^1 t\cdot yF_1(tx, ty, tz)-t\cdot xF_2(tx, ty, tz)\,dt
    \end{align*}
  \end{theorem}

\end{frame}


\begin{sagesilent}
  var('x y z')
  F = vector([x**3-y*z, x*y, x-z])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ given by
    \[
      \bv{F}=\sage{F}
    \]
    \onslide<2->{The divergence of $\bv{F}$ is}
    \[
      \onslide<2->{\vdiv(\bv{F})
        =} \onslide<3->{\pdv*{\Set{\sage{F[0]}}}{x} + \pdv*{\Set{\sage{F[1]}}}{y} + \pdv*{\Set{\sage{F[2]}}}{z}
        =} \onslide<4->{\sage{F.div([x, y, z])}
      \neq 0}
    \]
    \onslide<5->{This means that $\bv{F}\neq\curl(\bv{G})$ for any $\bv{G}$.}
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y z t')
  F = 3*vector([y, -y-z, z])
  F1t, F2t, F3t = F(x=t*x, y=t*y, z=t*z)
  G1 = expand(integral(t*z*F2t-t*y*F3t, t, 0, 1))
  G2 = expand(integral(t*x*F3t-t*z*F1t, t, 0, 1))
  G3 = expand(integral(t*y*F1t-t*x*F2t, t, 0, 1))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ given by
    \begin{gather*}
      \begin{align*}
        \bv{F} &= \sage{F} & \onslide<2->{\vdiv(\bv{F})} &\onslide<2->{=} \onslide<3->{(\sage{F[0]})_x+(\sage{F[1]})_y+(\sage{F[2]})_z =} \onslide<4->{\sage{F.div([x, y, z])}}
      \end{align*}
    \end{gather*}
    \onslide<5->{Since $\vdiv(\bv{F})=0$ we have $\bv{F}=\curl(\bv{G})$ where}
    \begin{align*}
      \onslide<5->{G_1} &\onslide<5->{=} \onslide<6->{\int_0^1 t\cdot z\Set{\sage{F2t}}-t\cdot y\Set{\sage{F3t}}\,dt = \sage{G1}} \\
      \onslide<7->{G_2} &\onslide<7->{=} \onslide<8->{\int_0^1 t\cdot x\Set{\sage{F3t}}-t\cdot z\Set{\sage{F1t}}\,dt = \sage{G2}} \\
      \onslide<9->{G_3} &\onslide<9->{=} \onslide<10->{\int_0^1 t\cdot y\Set{\sage{F1t}}-t\cdot x\Set{\sage{F2t}}\,dt = \sage{G3}}
    \end{align*}
  \end{example}
\end{frame}

\end{document}

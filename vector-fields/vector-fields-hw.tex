\documentclass[]{exam}

\usepackage{fitzmath}
\usepackage{fitzhw}

\input{../course-semester.tex}
\renewcommand{\hwtitle}{Vector Fields Exercises}

\begin{document}

\section*{Problems from Colley}

\begin{itemize}
\item \S1.3 \#'s 12-20, 24-26, 27(\emph{b})
\item \S2.1 \#'s 12\footnote{\label{range}``Describe the range'' means ``find a
    basis of the range.''}, 13\footnotemark[\ref{range}]
\item \S3.3 \#'s 1-6
\end{itemize}


\section*{Additional Problems}

\begin{questions}

  \begin{sagesilent}
    var('x y z')
    f = x**2-y*z
    set_random_seed(3180)
    P1, P2, P3, P4 = map(tuple, random_matrix(ZZ, 4, 3, x=-4, y=4))
    price = lambda t: f(x=t[0], y=t[1], z=t[2])
  \end{sagesilent}
\question You are considering purchasing the following piece of wire
  $\mathcal{W}$.
  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}]
      ]

      \coordinate (P1) at (-4, 0);
      \coordinate (P4) at ( 4, 1);

      \begin{scope}[
        decoration={
          , markings
          , mark=at position 0.33 with \coordinate (P2);
          , mark=at position 0.66 with \coordinate (P3);
        }
        ]
        \draw[postaction={decorate}] plot [smooth]
        coordinates {(P1) (-1, 2) (1, 0) (P4)};
      \end{scope}

      \node[myDot, label=below:{$P_1\sage{P1}$}] at (P1) {};
      \node[myDot, label=above:{$P_2\sage{P2}$}] at (P2) {};
      \node[myDot, label=below:{$P_3\sage{P3}$}] at (P3) {};
      \node[myDot, label=above:{$P_4\sage{P4}$}] at (P4) {};

    \end{tikzpicture}
  \]
  The ``price density'' throughout $\mathcal{W}$ is described by the scalar
  field $f\in\mathscr{C}(\mathbb{R}^3)$ given by
  \[
    f\sage{f.variables()}=\sage{f}\,\si{\$\per\metre}
  \]
  \begin{parts}
  \part At which of the points $\Set{P_1, P_2, P_3, P_4}$ is $\mathcal{W}$ most
    expensive?
  \part Explain the meaning of the equation
    \[
      \int_{\mathcal{W}}f\,ds=19
    \]
    Your explanation should include a discussion of units.
  \end{parts}


  \newpage
  \begin{sagesilent}
    set_random_seed(813)
    P1, P2, P3, P4 = map(tuple, random_matrix(ZZ, 4, 3, x=-4, y=4))
    var('x y z')
    f = 2*x**2+y**2+z**2
  \end{sagesilent}
\question A thick sheet of aluminum foil conforms to the shape of the following
  surface $X$.
  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , yscale=.60
      , myDot/.style={circle, fill, inner sep=2pt}]
      ]

      \filldraw[fill=blue!45, draw=black] (0, 0)
      to[bend left]  (3, -1)
      to[bend left]  (5,  2)
      to[bend right] (2,  3)
      to[bend right] (0,  0)
      --cycle;

      \coordinate (P1) at (1, 1);
      \coordinate (P2) at (5/2, 1/2);
      \coordinate (P3) at (2, 2);
      \coordinate (P4) at (7/2, 2);

      \node[myDot] at (P1) {};
      \node[overlay, red, below left= 0.5mm and 16mm of P1]
      (text) {$\scriptstyle P_1\sage{P1}$};
      \draw[overlay, red, <-, thick, shorten <=4pt]
      (P1.south) |- (text.east);

      \node[myDot] at (P2) {};
      \node[overlay, red, below right= 1mm and 16mm of P2]
      (text) {$\scriptstyle P_2\sage{P2}$};
      \draw[overlay, red, <-, thick, shorten <=4pt]
      (P2.south) |- (text.west);

      \node[myDot] at (P3) {};
      \node[overlay, red, above left= 1mm and 16mm of P3]
      (text) {$\scriptstyle P_3\sage{P3}$};
      \draw[overlay, red, <-, thick, shorten <=4pt]
      (P3.south) |- (text.east);

      \node[myDot] at (P4) {};
      \node[overlay, red, above right= 1mm and 16mm of P4]
      (text) {$\scriptstyle P_4\sage{P4}$};
      \draw[overlay, red, <-, thick, shorten <=4pt]
      (P4.north) |- (text.west);
    \end{tikzpicture}
  \]
  The ``weight density'' throughout $X$ is described by the scalar
  field $f\in\mathcal{C}(\mathbb{R}^3)$ given by
  \[
    f\sage{f.variables()}=\sage{f}\,\si{\operatorname{oz}\per\operatorname{ft}\squared}
  \]
  \begin{parts}
  \part At which of the points $\Set{P_1, P_2, P_3, P_4}$ is $\mathcal{W}$
    heaviest?
  \part Explain the meaning of the equation
    \[
      \iint_{X}f\,dS=34
    \]
    Your explanation should include a discussion of units.
  \end{parts}


\question An object conforms to the shape of a solid $B$ in $\mathbb{R}^3$.
  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}]
      ]
      \filldraw[fill=blue!45] (0,0) circle (2cm);
      \draw (-2,0) arc (180:360:2 and 0.6);
      \draw[dashed] (2,0) arc (0:180:2 and 0.6);

      \node[myDot, label=right:{$O(0, 0, 0)$}] at (0, 0) {};
    \end{tikzpicture}
  \]
  Each point inside of $B$ has an electromagnetic ``charge density'' given by
  $f(x, y, z)=xyz\,\si{\ampere\per\metre\cubed}$.
  \begin{parts}
  \part What is the charge density of the origin $O\in B$?
  \part Explain the meaning of the equation
    \[
      \iiint_B f\,dV=0
    \]
    Your explanation should include a discussion of units.
  \end{parts}


  \newpage
  \begin{sagesilent}
    P = (2, -1)
    v = vector([3, -2])
    R.<x, y> = QQ[]
    F = vector([x-2*y, x*y])
  \end{sagesilent}
\question A particle is traveling through the following path $\mathcal{P}$ in
  $\mathbb{R}^2$. When the particle is located at the point $P\sage{P}$, its
  velocity is described by the vector $\bv{v}=\sage{v}\,\si{\metre\per\second}$.
  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , myDot/.style={circle, fill, inner sep=2pt}]
      ]

      \coordinate (A) at (-4, 0);
      \coordinate (B) at (-1, 2);
      \coordinate (C) at ( 1, 0);
      \coordinate (D) at ( 4, 1);

      \path[name path=curve, smooth]
      plot coordinates {(A) (B) (C) (D)};

      \begin{scope}[
        decoration={
          , markings
          , mark=at position 0.50 with {\arrow{>}};
          , mark=at position 0.65 with \coordinate (P);
        }
        ]

        \draw[postaction={decorate}, smooth]
        plot coordinates {(A) (B) (C) (D)};
      \end{scope}

      \path [name path=aux] (P) circle [radius=1bp];
      \draw [name intersections={of=curve and aux}, ->, blue]
      (P) -- ($(intersection-1)!1.5cm!(intersection-2)$) node[right] {$\bv{v}=\sage{v}\,\si{\metre\per\second}$};

      \node[myDot] at (A) {};
      \node[myDot] at (D) {};
      \node[myDot, label=left:{$P\sage{P}$}] at (P) {};

    \end{tikzpicture}
  \]
  The force field $\bv{F}\in\mathfrak{X}(\mathbb{R}^2)$ given by
  $\bv{F}\sage{R.gens()}=\sage{F}\,\si{N}$ acts on the particle
  \begin{parts}
  \part Compute the work-density exerted by $\bv{F}$ on the particle at the
    depicted point $P$.
  \part Explain the meaning of the equation
    \[
      \int_{\mathcal{P}}\bv{F}\cdot d\bv{s}=37
    \]
    Your explanation should include a discussion of units.
  \end{parts}



  \begin{sagesilent}
    P = (3, -2, 1)
    N = vector([2, 1, -1])
    R.<x, y, z> = QQ[]
    F = vector([x+y, y*z, y+z])
  \end{sagesilent}
\question The figure below depicts a surface $X$. The point $P\sage{P}$ is on
  $X$ and the vector $\bv{N}=\sage{N}$ is orthogonal to the surface at $P$.
      \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , yscale=.60
        , myDot/.style={circle, fill, inner sep=2pt}]
        ]

        \filldraw[fill=blue!45, draw=black] (0, 0)
        to[bend left]  (3, -1)
        to[bend left]  (5,  2)
        to[bend right] (2,  3)
        to[bend right] (0,  0)
        --cycle;

        \coordinate (P) at (1, 1);

        \draw[->, blue] (P) -- (0, 3) node[above] {$\bv{N}=\sage{N}$};
        \node[myDot, label=right:{$P\sage{P}$}] at (P) {};

      \end{tikzpicture}
    \]
    The vector field $\bv{F}\in\mathfrak{X}(\mathbb{R}^3)$ given by
    $\bv{F}\sage{R.gens()}=\sage{F}\,\si{\metre\per\second}$ describes the flow
    of water through $X$.
    \begin{parts}
    \part Compute the flux density of $\bv{F}$ across $X$ at the point $P$.
    \part Explain the meaning of the equation
      \[
        \iint_X\bv{F}\cdot d\bv{S}=-12
      \]
      Your explanation should include a discussion of units.
    \end{parts}

\end{questions}

\end{document}